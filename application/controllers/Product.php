<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Sản phẩm',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                'scriptFooter' => array('js' => array(/*'ckfinder/ckfinder.js',*/ 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/product_list.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product')) {
            $this->loadModel(array('Mproductchilds', 'Mproducttypes', 'Mmanufacturers', 'Mfilters','Mcategories', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getList();
            //$data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList(3);
            $this->load->view('product/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function old(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Sản phẩm cũ',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/search_item.js', 'js/product_old.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product/old')) {
            $this->loadModel(array('Mproducttypes', 'Mmanufacturers', 'Mfilters','Mcategories', 'Mproductunits', 'Mproductformalstatus', 'Mproductusagestatus', 'Mproductaccessorystatus'));
            $data['listProductTypes'] = $this->Mproducttypes->getList();
            //$data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $whereStatus = array('StatusId' => STATUS_ACTIVED);
            $data['listManufacturers'] = $this->Mmanufacturers->getBy($whereStatus);
            $data['listProductUnits'] = $this->Mproductunits->getBy($whereStatus);
            $data['listProductFormalStatus'] = $this->Mproductformalstatus->getBy($whereStatus);
            $data['listProductUsageStatus'] = $this->Mproductusagestatus->getBy($whereStatus);
            $data['listProductAccessoryStatus'] = $this->Mproductaccessorystatus->getBy($whereStatus);
            $data['listFilters'] = $this->Mfilters->getList(33);
            $this->load->view('product/old', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Sản phẩm',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product')) {
            $this->loadModel(array('Mproducttypes', 'Mcategories', 'Mmanufacturers', 'Mtags', 'Mvariants', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            //$data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 3));
            $data['listVariants'] = $this->Mvariants->get();
            $this->load->view('product/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($productId = 0){
        if($productId > 0){
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Sửa Sản phẩm',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js')))
            );
            if($this->Mactions->checkAccess($data['listActions'], 'product')) {
                $this->loadModel(array('Mproducts', 'Mproducttypes', 'Mcategories', 'Mmanufacturers', 'Mtags', 'Mcategoryitems', 'Mitemmetadatas', 'Mfiles', 'Mvariants', 'Mproductunits', 'Mproductchilds', 'Mproductprices', 'Mproductaccessories', 'Mactionlogs'));
                $product = $this->Mproducts->get($productId);
                if($product && $product['ProductKindId'] != 3 && $product['ProductStatusId'] > 0){
                    $data['productId'] = $productId;
                    //$product['ProductDesc'] = str_replace('/hmd/', IMAGE_PATH, $product['ProductDesc']);
                    $data['product'] = $product;
                    $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
                    //$data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 3));
                    $data['cateIds'] = $this->Mcategoryitems->getCateIds($productId, 3);
                    $data['tagNames'] = $this->Mtags->getTagNames($productId, 3);
                    $data['listImages'] = $this->Mfiles->getFileUrls($productId, 3, 1);
                    $data['itemSEO'] = $this->Mitemmetadatas->getBy(array('ItemId' => $productId, 'ItemTypeId' => 3), true);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($productId, 3);
                    $data['listProductPrices'] = $this->Mproductprices->getByProductId($productId);
                    $data['listProductAccessories'] = $this->Mproductaccessories->getBy(array('ProductId' => $productId, 'StatusId' => STATUS_ACTIVED));
                    $data['listVariants'] = $this->Mvariants->get();
                    $data['variants'] = array();
                    $data['listProductChilds'] = array();
                    if($product['ProductKindId'] == 2){
                        $listProductChilds = $this->Mproductchilds->getByProductId($productId);
                        $variants = array();
                        foreach($listProductChilds as $pc){
                            if($pc['VariantId1'] > 0) $variants[$pc['VariantId1']][] = $pc['VariantValue1'];
                            if($pc['VariantId2'] > 0) $variants[$pc['VariantId2']][] = $pc['VariantValue2'];
                            if($pc['VariantId3'] > 0) $variants[$pc['VariantId3']][] = $pc['VariantValue3'];
                        }
                        $data['variants'] = $variants;
                        $data['listProductChilds'] = $listProductChilds;
                    }
                }
                else{
                    $data['productId'] = 0;
                    $data['txtError'] = "Không tìm thấy sản phẩm";
                }
                $this->load->view('product/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('product');
    }

    public function addCombo(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Combo Sản phẩm',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product')) {
            $this->loadModel(array('Mproducttypes', 'Mcategories', 'Mtags', 'Mvariants', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 3));
            $data['listVariants'] = $this->Mvariants->get();
            $this->load->view('product/add_combo', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function editCombo($productId = 0){
        if($productId > 0){
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Sửa Combo',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/choose_item.js', 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js')))
            );
            if($this->Mactions->checkAccess($data['listActions'], 'product')) {
                $this->loadModel(array('Mproducts', 'Mproducttypes', 'Mcategories', 'Mtags', 'Mcategoryitems', 'Mitemmetadatas', 'Mfiles', 'Mproductchilds', 'Mproductunits', 'Mproductprices', 'Mactionlogs'));
                $product = $this->Mproducts->get($productId);
                if($product && $product['ProductKindId'] == 3) {
                    $data['productId'] = $productId;
                    //$product['ProductDesc'] = str_replace('/hmd/', IMAGE_PATH, $product['ProductDesc']);
                    $data['product'] = $product;
                    $whereStatus = array('StatusId' => STATUS_ACTIVED);
                    $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 3));
                    $data['cateIds'] = $this->Mcategoryitems->getCateIds($productId, 3);
                    $data['tagNames'] = $this->Mtags->getTagNames($productId, 3);
                    $data['listImages'] = $this->Mfiles->getFileUrls($productId, 3, 1);
                    $data['itemSEO'] = $this->Mitemmetadatas->getBy(array('ItemId' => $productId, 'ItemTypeId' => 3), true);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($productId, 3);
                    $data['listProductPrices'] = $this->Mproductprices->getByProductId($productId);
                    $data['listProductChilds'] = $this->Mproductchilds->getByProductId($productId);
                }
                else{
                    $data['productId'] = 0;
                    $data['txtError'] = "Không tìm thấy combo sản phẩm";
                }
                $this->load->view('product/edit_combo', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('product');
    }

    public function addOld($productId = 0, $productChildId = 0){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thông tin sản phẩm cũ',
            array(
                'scriptFooter' => array('js' => array('vendor/plugins/jwerty/jwerty.js', 'js/choose_item.js', 'js/product_add_old.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product/old')) {
            $this->loadModel(array('Mproducts', 'Mproductchilds', 'Mcategories', 'Mproductunits', 'Mproductformalstatus', 'Mproductusagestatus', 'Mproductaccessorystatus'));
            $data['listCategories'] = $this->Mcategories->getListByItemType(1);
            $data['listProductFormalStatus'] = $this->Mproductformalstatus->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listProductUsageStatus'] = $this->Mproductusagestatus->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listProductAccessoryStatus'] = $this->Mproductaccessorystatus->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['productId'] = $productId;
            $data['productChildId'] = $productChildId;
            $listOldProducts = array();
            if($productChildId > 0) $listOldProducts = $this->Mproductchilds->getBy(array('ParentProductChildId' => $productChildId, 'StatusId' => STATUS_ACTIVED), false, '', 'ProductChildId, ProductId, IMEI, ProductFormalStatusId, ProductUsageStatusId, ProductAccessoryStatusId, Comment');
            elseif($productId > 0){
                $listOldProducts = $this->Mproducts->getBy(array('ParentProductId' => $productId, 'ProductStatusId >' => 0), false, '', 'ProductId, ProductShortDesc, IMEI, ProductFormalStatusId, ProductUsageStatusId, ProductAccessoryStatusId');
                for($i = 0; $i < count($listOldProducts); $i++) $listOldProducts[$i]['ProductChildId'] = 0;
            }
            $data['listOldProducts'] = $listOldProducts;
            $this->load->view('product/add_old', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function quantity(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tồn kho Sản phẩm',
            array('scriptFooter' => array('js' => array('js/search_item.js', 'js/product_quantity.js')))
        );
        $listActions = $data['listActions'];
        if($this->Mactions->checkAccess($listActions, 'product/quantity')) {
            $data['canViewPrice'] = $this->Mactions->checkAccess($listActions, 'product/price');
            $this->loadModel(array('Mproducttypes', 'Msuppliers', 'Mmanufacturers', 'Mfilters','Mcategories', 'Mstores', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getList();
            $data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList(15);
            $this->load->view('product/quantity', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function price(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thay đổi giá Sản phẩm',
            array('scriptFooter' => array('js' => array('js/search_item.js', 'js/product_price.js')))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'product/price')) {
            $this->loadModel(array('Mproducttypes', 'Msuppliers', 'Mmanufacturers', 'Mfilters','Mcategories', 'Mstores', 'Mproductunits'));
            $data['listProductTypes'] = $this->Mproducttypes->getList();
            $data['listSuppliers'] = $this->Msuppliers->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listManufacturers'] = $this->Mmanufacturers->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCategories'] = $this->Mcategories->getListByItemType(array(1, 2));
            $data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listProductUnits'] = $this->Mproductunits->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listFilters'] = $this->Mfilters->getList(24);
            $this->load->view('product/price', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function exportExcel(){
        $user = $this->checkUserLogin();
        if($this->Mactions->checkAccessFromDb('product', $user['UserId'])){
            $this->loadModel(array('Mproducts', 'Mproductchilds', 'Mproducttypes', 'Mcategories', 'Mcategoryitems'));
            $listProducts = $this->Mproducts->getBy(array('ProductStatusId >' => 0), false, '', 'ProductId, ProductName, ProductStatusId, Sku, BarCode, GuaranteeMonth, Price, Weight, ProductTypeId, ProductKindId');
            if(!empty($listProducts)){
                $listCategories = $this->Mcategories->getListByItemType(1);
                $listProductTypes = $this->Mproducttypes->getList();
                $fileUrl = FCPATH . 'assets/uploads/excels/export_product.xls';
                $this->load->library('excel');
                $objReader = PHPExcel_IOFactory::createReader('Excel5');
                $objPHPExcel = $objReader->load($fileUrl);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $i = 1;
                foreach($listProducts as $p) {
                    $flag = false;
                    $productTypeName = $this->Mconstants->getObjectValue($listProductTypes, 'ProductTypeId', $p['ProductTypeId'], 'ProductTypeName');
                    $productStatusName = $this->Mconstants->productStatus[$p['ProductStatusId']];
                    $cateNames = '';
                    $cateIds = $this->Mcategoryitems->getCateIds($p['ProductId'], 3);
                    foreach($cateIds as $cateId){
                        $cateName = $this->Mconstants->getObjectValue($listCategories, 'CategoryId', $cateId, 'CategoryName');
                        if(!empty($cateName)) $cateNames .= $cateName . ', ';
                    }
                    if(!empty($cateNames)) $cateNames = substr($cateNames, 0, strlen($cateNames) - 2);
                    if($p['ProductKindId'] == 2){
                        $listProductChilds =  $this->Mproductchilds->getBy(array('ProductId' => $p['ProductId'], 'ParentProductChildId' => 0), false, '', 'ProductName, Sku, BarCode, GuaranteeMonth, Price, Weight');
                        if(!empty($listProductChilds)){
                            foreach($listProductChilds as $pc){
                                $i++;
                                $sheet->setCellValue('A' . $i, $p['ProductName'].' ('.$pc['ProductName'].')');
                                $sheet->setCellValue('B' . $i, $pc['Sku']);
                                $sheet->setCellValue('C' . $i, $pc['BarCode']);
                                $sheet->setCellValue('D' . $i, $pc['GuaranteeMonth']);
                                $sheet->setCellValue('E' . $i, $pc['Price']);
                                $sheet->setCellValue('F' . $i, $pc['Weight']);
                                $sheet->setCellValue('G' . $i, $cateNames);
                                $sheet->setCellValue('H' . $i, $productTypeName);
                                $sheet->setCellValue('I' . $i, $productStatusName);
                            }
                        }
                        else $flag = true;
                    }
                    else $flag = true;
                    if($flag){
                        $i++;
                        $sheet->setCellValue('A' . $i, $p['ProductName']);
                        $sheet->setCellValue('B' . $i, $p['Sku']);
                        $sheet->setCellValue('C' . $i, $p['BarCode']);
                        $sheet->setCellValue('D' . $i, $p['GuaranteeMonth']);
                        $sheet->setCellValue('E' . $i, priceFormat($p['Price']));
                        $sheet->setCellValue('F' . $i, $p['Weight']);
                        $sheet->setCellValue('G' . $i, $cateNames);
                        $sheet->setCellValue('H' . $i, $productTypeName);
                        $sheet->setCellValue('I' . $i, $productStatusName);
                    }
                }
                $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                $filename = "exportProduct_".date('Y-m-d').".xls";
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                $objPHPExcel->disconnectWorksheets();
                unset($objPHPExcel);
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }
}