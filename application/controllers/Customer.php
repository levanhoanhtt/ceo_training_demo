<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller{

    public function index($customerKindId = 1, $customerGroupId = 0){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Khách hàng',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/customer_list.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
            $this->loadModel(array('Mfilters', 'Mcustomergroups'));
            $data['listFilters'] = $this->Mfilters->getList(5);
            $listCustomerGroups = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listCustomerGroups'] = $listCustomerGroups;
            $data['customerGroupId'] = $customerGroupId;
            $data['customerKindId'] = $customerKindId;
            if($customerGroupId > 0){
                $customerGroupName = $this->Mconstants->getObjectValue($listCustomerGroups, 'CustomerGroupId', $customerGroupId, 'CustomerGroupName');
                if(!empty($customerGroupName)) $data['title'] .= ' | '.$customerGroupName;
            }
            $this->load->view('customer/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function add($customerKindId = 0){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm mới Khách hàng',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/timepicker/bootstrap-timepicker.min.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/timepicker/bootstrap-timepicker.min.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/customer_update.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
            if(!is_numeric($customerKindId) || $customerKindId < 1 || $customerKindId > 3) $customerKindId = 1;
            $data['customerKindId'] = $customerKindId;
            $this->loadModel(array('Mcustomergroups', 'Mprovinces', 'Mdistricts', 'Mtags', 'Mcountries', 'Mwards', 'Mcustomers'));
            $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listProvinces'] = $this->Mprovinces->getList();
            $data['listCountries'] = $this->Mcountries->getList();
            $data['listUsers'] = $this->Musers->getListForSelect();
            $data['listCTVs'] = $this->Mcustomers->getBy(array('CustomerKindId' => 3, 'StatusId' => STATUS_ACTIVED));
            $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 5));
            $this->load->view('customer/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($customerId = 0, $tabId = 0){
        if ($customerId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Thông tin Khách hàng',
                array(
                    'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/tagsinput/jquery.tagsinput.min.css')),
                    'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'js/customer_update.js'))
                )
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'customer')) {
                $this->loadModel(array('Mcustomers', 'Mcustomergroups', 'Mprovinces', 'Mdistricts', 'Mcustomercomments', 'Mtags', 'Mactionlogs', 'Mcountries', 'Mwards', 'Morders'));
                $customer = $this->Mcustomers->get($customerId);
                if ($customer) {
                    if($tabId < 1 || $tabId > 4) $tabId = 1;
                    $data['tabId'] = $tabId;
                    $data['customerId'] = $customerId;
                    $data['customer'] = $customer;
                    $data['listCustomerGroups'] = $this->Mcustomergroups->getBy(array('StatusId' => STATUS_ACTIVED));
                    $data['listCountries'] = $this->Mcountries->getList();
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listDistricts'] = $this->Mdistricts->getList();
                    $data['listUsers'] = $this->Musers->getListForSelect();
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 5));
                    $data['tagNames'] = $this->Mtags->getTagNames($customerId, 5);
                    $data['listCustomerComments'] = $this->Mcustomercomments->getListByCustomerId($customerId);
                    $data['listCTVs'] = $this->Mcustomers->getBy(array('CustomerKindId' => 3, 'StatusId' => STATUS_ACTIVED));
                    $listOrders = $this->Morders->search(array('CustomerId' => $customerId), 0, 1, 'OrderStatusId, TotalCost');
                    $data['totalOrder'] = count($listOrders);
                    $successOrder = 0;
                    $successCost = 0;
                    foreach($listOrders as $o){
                        if($o['OrderStatusId'] == 6){
                            $successOrder++;
                            $successCost += $o['TotalCost'];
                        }
                    }
                    $data['successOrder'] = $successOrder;
                    $data['successCost'] = $successCost;
                    $data['listActionLogs'] = $this->Mactionlogs->getList($customerId, 5);
                }
                else {
                    $data['customerId'] = 0;
                    $data['txtError'] = "Không tìm thấy khách hàng";
                }
                $this->load->view('customer/edit', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('customer');
    }
}