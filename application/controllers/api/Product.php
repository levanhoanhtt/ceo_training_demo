<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller{

    public function changeStatusBatch(){
        $user = $this->checkUserLogin(true);
        $productIds = json_decode(trim($this->input->post('ItemIds')), true);
        $statusId = $this->input->post('StatusId');
        if(!empty($productIds) && $statusId >= 0){
            $this->load->model('Mproducts');
            $flag = $this->Mproducts->changeStatusBatch($productIds, $statusId, $user);
            if($flag) {
                $msg = 'Xóa sản phẩm thành công';
                $statusName = '';
                if ($statusId > 0) {
                    $msg = 'Thay đổi trạng thái thành công';
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->productStatus[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $msg, 'data' => $statusName));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductName', 'ProductSlug', 'ProductShortDesc', 'ProductDesc', 'ProductTypeId', 'ProductStatusId', 'ProductKindId', 'VATStatusId', 'ProductDisplayTypeId', 'ProductLevelId', 'ParentProductId', 'SupplierId', 'ManufacturerId', 'IsContactPrice', 'Price', 'OldPrice', 'ProductImage', 'BarCode', 'Sku', 'IMEI', 'Weight', 'ProductFormalStatusId', 'ProductUsageStatusId', 'ProductAccessoryStatusId', 'ProductUnitId', 'GuaranteeMonth', 'VideoUrls', 'ProductWebStatusId', 'ProductWebStatusComment'));
        if(!empty($postData['ProductName']) && $postData['ProductStatusId'] > 0 && $postData['ProductKindId'] > 0 && $postData['ProductDisplayTypeId'] > 0) {
            $productId = $this->input->post('ProductId');
            $this->load->model('Mproducts');
            $flag = $this->Mproducts->checkExist($postData['BarCode'], $postData['Sku'], '', $productId);
            if($flag) echo json_encode(array('code' => 0, 'message' => "BarCode hoặc Sku sản phẩm đã tồn tại"));
            else {
                $crDateTime = getCurentDateTime();
                $actionLogs = array(
                    'ItemTypeId' => 3,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                if ($productId > 0) {
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 2;
                    $actionLogs['Comment'] = $user['FullName'] . ': Cập nhật sản phẩm';
                }
                else {
                    $postData['CrUserId'] = $user['UserId'];
                    $postData['CrDateTime'] = $crDateTime;
                    $publishDateTime = trim($this->input->post('PublishDateTime'));
                    if (!empty($publishDateTime)) $postData['PublishDateTime'] = $publishDateTime;
                    else $postData['PublishDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 1;
                    $actionLogs['Comment'] = $user['FullName'] . ': Thêm mới sản phẩm';
                }
                $productSEO = $this->arrayFromPost(array('TitleSEO', 'MetaDesc', 'Canonical', 'IsRobotIndex', 'IsRobotFollow', 'IsOnSitemap'));
                $productSEO['ItemTypeId'] = 3;
                if (empty($postData['ProductSlug'])) {
                    $postData['ProductSlug'] = makeSlug($postData['ProductName']);
                    $productSEO['Canonical'] = $postData['ProductSlug'];
                }
                else {
                    $postData['ProductSlug'] = makeSlug($postData['ProductSlug']);
                    $productSEO['Canonical'] = $postData['ProductSlug'];
                }
                //$postData['ProductDesc'] = replaceFileUrl($postData['ProductDesc'], IMAGE_PATH, '/hmd/');
                if (empty($productSEO['TitleSEO'])) $productSEO['TitleSEO'] = $postData['ProductName'];
                $postData['ProductImage'] = replaceFileUrl($postData['ProductImage'], PRODUCT_PATH);
                $images = json_decode(replaceFileUrl(trim($this->input->post('Images')), PRODUCT_PATH), true);
                $cateIds1 = json_decode(trim($this->input->post('CateIds1')), true);
                $cateIds2 = json_decode(trim($this->input->post('CateIds2')), true);
                $tagNames = json_decode(trim($this->input->post('TagNames')), true);
                $productChilds = array();
                if ($postData['ProductKindId'] == 2) {
                    $variants = json_decode(trim($this->input->post('Variants')), true);
                    unset($variants[0]);
                    foreach ($variants as $i => $variant) {
                        if (!is_array($variant)) unset($variants[$i]);
                    }
                    $variantOptions = json_decode(trim($this->input->post('VariantOptions')), true);
                    $variantId1 = $variantId2 = $variantId3 = 0;
                    $variantValues1 = $variantValues2 = $variantValues3 = array();
                    $i = 0;
                    foreach ($variants as $variantId => $variantValues) {
                        $i++;
                        if ($i == 1) {
                            $variantId1 = $variantId;
                            $variantValues1 = $variantValues;
                        }
                        elseif ($i == 2) {
                            $variantId2 = $variantId;
                            $variantValues2 = $variantValues;
                        }
                        elseif ($i == 3) {
                            $variantId3 = $variantId;
                            $variantValues3 = $variantValues;
                        }
                    }
                    $variantCount = count($variants);
                    foreach ($variantOptions as $variantOption) {
                        $variantValues = explode('-', $variantOption['VariantValue']);
                        $variantOption['ProductName'] = $variantOption['VariantValue'];
                        $variantOption['ProductImage'] = replaceFileUrl($variantOption['ProductImage'], PRODUCT_PATH);
                        unset($variantOption['VariantValue']);
                        $variantOption['VATStatusId'] = $postData['VATStatusId'];
                        $variantOption['StatusId'] = STATUS_ACTIVED;
                        if (count($variantValues) == $variantCount) {
                            $i = 0;
                            foreach ($variantValues as $variantValue) {
                                $variantValue = trim($variantValue);
                                $i++;
                                if ($i == 1) {
                                    $variantOption['VariantValue1'] = $variantValue;
                                    if (in_array($variantValue, $variantValues1)) $variantOption['VariantId1'] = $variantId1;
                                    elseif (in_array($variantValue, $variantValues2)) $variantOption['VariantId1'] = $variantId2;
                                    elseif (in_array($variantValue, $variantValues3)) $variantOption['VariantId1'] = $variantId3;
                                }
                                elseif ($i == 2) {
                                    $variantOption['VariantValue2'] = $variantValue;
                                    if (in_array($variantValue, $variantValues1)) $variantOption['VariantId2'] = $variantId1;
                                    elseif (in_array($variantValue, $variantValues2)) $variantOption['VariantId2'] = $variantId2;
                                    elseif (in_array($variantValue, $variantValues3)) $variantOption['VariantId2'] = $variantId3;
                                }
                                elseif ($i == 3) {
                                    $variantOption['VariantValue3'] = $variantValue;
                                    if (in_array($variantValue, $variantValues1)) $variantOption['VariantId3'] = $variantId1;
                                    elseif (in_array($variantValue, $variantValues2)) $variantOption['VariantId3'] = $variantId2;
                                    elseif (in_array($variantValue, $variantValues3)) $variantOption['VariantId3'] = $variantId3;
                                }
                            }
                        }
                        if(isset($variantOption['ProductPrices']) && !empty($variantOption['ProductPrices'])){
                            $productPrices = array();
                            $pps = json_decode($variantOption['ProductPrices'], true);
                            foreach($pps as $pp){
                                if($pp['ProductPriceId'] == 0){
                                    unset($pp['ProductPriceId']);
                                    $pp['CrUserId'] = $user['UserId'];
                                    $pp['CrDateTime'] = $crDateTime;
                                    $productPrices[] = $pp;
                                }
                            }
                            $variantOption['ProductPrices'] = $productPrices;
                        }
                        else $variantOption['ProductPrices'] = array();
                        $productChilds[] = $variantOption;
                    }
                    if (empty($productChilds)) $postData['ProductKindId'] = 1;
                }
                elseif ($postData['ProductKindId'] == 3) $productChilds = json_decode(trim($this->input->post('VariantOptions')), true);
                $prices = json_decode(trim($this->input->post('Prices')), true);
                $accessories = json_decode(trim($this->input->post('Accessories')), true);
                $productId = $this->Mproducts->update($postData, $productId, $images, $productSEO, $cateIds1, $cateIds2, $tagNames, $productChilds, $prices, $accessories, $actionLogs);
                if ($productId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật sản phẩm thành công", 'data' => $productId));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updatePriceTable(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductId', 'ProductChildId', 'Quantity', 'Price'));
        if($postData['ProductId'] > 0 && $postData['Quantity'] > 0 && $postData['Price'] > 0) {
            $productPriceId = $this->input->post('ProductPriceId');
            if ($productPriceId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            $this->load->model('Mproductprices');
            $productPriceId = $this->Mproductprices->save($postData, $productPriceId);
            if ($productPriceId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật gíá sản phẩm thành công", 'data' => $productPriceId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function deletePriceTable(){
        $this->checkUserLogin(true);
        $productPriceId = $this->input->post('ProductPriceId');
        if($productPriceId > 0){
            $this->load->model('Mproductprices');
            $flag = $this->Mproductprices->delete($productPriceId);
            if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa gíá sản phẩm thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateAccessories(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductId', 'AccessoryName', 'StatusId'));
        if($postData['ProductId'] > 0 && !empty($postData['AccessoryName']) && $postData['StatusId'] > 0){
            $productAccessoryId = $this->input->post('ProductAccessoryId');
            if($productAccessoryId > 0){
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            $this->load->model('Mproductaccessories');
            $productAccessoryId = $this->Mproductaccessories->save($postData, $productAccessoryId);
            if($productAccessoryId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật phụ kiện thành công", 'data' => $productAccessoryId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function deleteAccessories(){
        $user = $this->checkUserLogin(true);
        $productAccessoryId = $this->input->post('ProductAccessoryId');
        if($productAccessoryId > 0){
            $this->load->model('Mproductaccessories');
            $flag = $this->Mproductaccessories->changeStatus(0, $productAccessoryId, '', $user['UserId']);
            if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa phụ kiện thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getListOldProduct(){
        $this->checkUserLogin(true);
        $productId = $this->input->post('ProductId');
        $productChildId = $this->input->post('ProductChildId');
        $this->loadModel(array('Mproducts','Mproductchilds','Mproductformalstatus', 'Mproductusagestatus', 'Mproductaccessorystatus'));
        if($productChildId > 0){
            $listOldProducts = $this->Mproductchilds->getBy(array('ParentProductChildId' => $productChildId, 'StatusId' => STATUS_ACTIVED), false, '', 'ProductChildId, ProductId, IMEI, ProductFormalStatusId, ProductUsageStatusId, ProductAccessoryStatusId, Comment');
            for ($i = 0; $i < count($listOldProducts); $i++) {
                $listOldProducts[$i]['FormalStatus'] = $listOldProducts[$i]['ProductFormalStatusId'] > 0 ? $this->Mproductformalstatus->get($listOldProducts[$i]['ProductFormalStatusId'])['ProductFormalStatusName'] : '';

                $listOldProducts[$i]['UsageStatus'] = $listOldProducts[$i]['ProductUsageStatusId'] > 0 ? $this->Mproductusagestatus->get($listOldProducts[$i]['ProductUsageStatusId'])['ProductUsageStatusName'] : '';

                $listOldProducts[$i]['AccessoryStatus'] = $listOldProducts[$i]['ProductAccessoryStatusId'] > 0 ? $this->Mproductaccessorystatus->get($listOldProducts[$i]['ProductAccessoryStatusId'])['ProductAccessoryStatusName'] : '';
            }
            echo json_encode(array('code' => 1, 'data' => $listOldProducts));
        }
        elseif($productId > 0){
            $listOldProducts = $this->Mproducts->getBy(array('ParentProductId' => $productId, 'ProductStatusId >' => 0), false, '', 'ProductId, ProductShortDesc, IMEI, ProductFormalStatusId, ProductUsageStatusId, ProductAccessoryStatusId, ProductShortDesc');
            for($i = 0; $i < count($listOldProducts); $i++){
                $listOldProducts[$i]['ProductChildId'] = 0;

                $listOldProducts[$i]['Comment'] = $listOldProducts[$i]['ProductShortDesc'];

                $listOldProducts[$i]['FormalStatus'] = $listOldProducts[$i]['ProductFormalStatusId'] > 0 ? $this->Mproductformalstatus->get($listOldProducts[$i]['ProductFormalStatusId'])['ProductFormalStatusName'] : '';

                $listOldProducts[$i]['UsageStatus'] = $listOldProducts[$i]['ProductUsageStatusId'] > 0 ? $this->Mproductusagestatus->get($listOldProducts[$i]['ProductUsageStatusId'])['ProductUsageStatusName'] : '';

                $listOldProducts[$i]['AccessoryStatus'] = $listOldProducts[$i]['ProductAccessoryStatusId'] > 0 ? $this->Mproductaccessorystatus->get($listOldProducts[$i]['ProductAccessoryStatusId'])['ProductAccessoryStatusName'] : '';
            } 
            echo json_encode(array('code' => 1, 'data' => $listOldProducts));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateOldProduct(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ParentProductId', 'IMEI', 'ProductFormalStatusId', 'ProductUsageStatusId', 'ProductAccessoryStatusId', 'ProductShortDesc'));
        if($postData['ParentProductId'] > 0 && !empty($postData['IMEI'])){
            $productId = $this->input->post('ProductId');
            $this->load->model('Mproducts');
            $flag = $this->Mproducts->checkExist('', '', $postData['IMEI'], $productId);
            if($flag) echo json_encode(array('code' => 0, 'message' => "IMEI sản phẩm đã tồn tại"));
            else{
                $crDateTime = getCurentDateTime();
                $actionLogs = array(
                    'ItemTypeId' => 3,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                if($productId > 0){
                    $postData['UpdateUserId'] = $user['UserId'];
                    $postData['UpdateDateTime'] = $crDateTime;
                    $actionLogs['ActionTypeId'] = 2;
                    $actionLogs['Comment'] = $user['FullName'] . ': Cập nhật sản phẩm';
                }
                else{
                    $postData = array_merge($postData, array(
                        'ProductName' => '',
                        'ProductSlug' => '',
                        'ProductDesc' => '',
                        'ProductTypeId' => 0,
                        'ProductStatusId' => 2,
                        'ProductKindId' => 1,
                        'VATStatusId' => 1,
                        'ProductDisplayTypeId'=> 3,
                        'ProductLevelId' => 1,
                        'SupplierId' => 0,
                        'ManufacturerId' => 0,
                        'IsContactPrice' => 2,
                        'Price' => 0,
                        'OldPrice' => 0,
                        'ProductImage' => '',
                        'BarCode' => '',
                        'Sku' => '',
                        'Weight' => 0,
                        'ProductUnitId' => 1,
                        'GuaranteeMonth' => 0,
                        'VideoUrls' => '',
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => $crDateTime
                    ));
                    $actionLogs['ActionTypeId'] = 1;
                    $actionLogs['Comment'] = $user['FullName'] . ': Thêm mới sản phẩm';
                }
                $flag = $this->Mproducts->updateOldProduct($postData, $productId, $actionLogs);
                if($flag > 0){
                    $data = array(
                        'ProductId' => $flag,
                        'IMEI' => $postData['IMEI'],
                        'ProductFormalStatusId' => $postData['ProductFormalStatusId'],
                        'ProductUsageStatusId' => $postData['ProductUsageStatusId'],
                        'ProductAccessoryStatusId' => $postData['ProductAccessoryStatusId'],
                        'ProductShortDesc' => $postData['ProductShortDesc'],
                        'IsAdd' => $productId > 0 ? 0 : 1
                    );
                    echo json_encode(array('code' => 1, 'message' => "Cập nhật sản phẩm thành công", 'data' => $data));
                }
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateOldProductChild(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductId', 'ParentProductChildId', 'IMEI', 'ProductFormalStatusId', 'ProductUsageStatusId', 'ProductAccessoryStatusId', 'Comment'));
        if($postData['ProductId'] > 0 && $postData['ParentProductChildId'] > 0 && !empty($postData['IMEI'])){
            $productChildId = $this->input->post('ProductChildId');
            $this->loadModel(array('Mproducts', 'Mproductchilds'));
            $flag = $this->Mproducts->checkExist('', '', $postData['IMEI'], 0, $productChildId);
            if($flag) echo json_encode(array('code' => 0, 'message' => "IMEI sản phẩm đã tồn tại"));
            else{
                if($productChildId == 0){
                    $postData = array_merge($postData, array(
                        'VariantId1' => 0,
                        'VariantValue1' => '',
                        'VariantId2' => '',
                        'VariantValue2' => '',
                        'VariantId3' => '',
                        'VariantValue3' => '',
                        'ProductName' => '',
                        'ProductImage' => '',
                        'BarCode' => '',
                        'Sku' => '',
                        'StatusId' => STATUS_ACTIVED,
                        'Quantity' => 0,
                        'Price' => 0,
                        'OldPrice' => 0,
                        'Weight' => 0,
                        'ProductPartId' => 0,
                        'ProductPartChildId' => 0,
                        'VATStatusId' => 0,
                        'GuaranteeMonth' => 0
                    ));
                }
                $flag = $this->Mproductchilds->save($postData, $productChildId);
                if($flag > 0){
                    $data = array(
                        'ProductChildId' => $flag,
                        'IMEI' => $postData['IMEI'],
                        'ProductFormalStatusId' => $postData['ProductFormalStatusId'],
                        'ProductUsageStatusId' => $postData['ProductUsageStatusId'],
                        'ProductAccessoryStatusId' => $postData['ProductAccessoryStatusId'],
                        'Comment' => $postData['Comment'],
                        'IsAdd' => $productChildId > 0 ? 0 : 1
                    );
                    echo json_encode(array('code' => 1, 'message' => "Cập nhật sản phẩm thành công", 'data' => $data));
                }
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function deleteProductChild(){
        $this->checkUserLogin(true);
        $productChildId = $this->input->post('ProductChildId');
        if($productChildId > 0){
            $this->load->model('Mproductchilds');
            $flag = $this->Mproductchilds->changeStatus(0, $productChildId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa sản phẩm cũ thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function get(){
        $this->checkUserLogin(true);
        $productId = $this->input->post('ProductId');
        if($productId > 0){
            $this->loadModel(array('Mproducts', 'Mproductchilds', 'Mproductprices', 'Mproductunits', 'Mpricechanges'));
            $product = $this->Mproducts->get($productId, true, '', 'ProductId, ProductName, ProductImage, ProductStatusId, ProductKindId, Price, BarCode, Weight, GuaranteeMonth, ProductUnitId, VATStatusId');
            if($product && $product['ProductStatusId'] == STATUS_ACTIVED){
                if(empty($product['ProductImage'])) $product['ProductImage'] = NO_IMAGE;
                $productUnitName = $product['ProductUnitId'] > 0 ? $this->Mproductunits->getFieldValue(array('ProductUnitId' => $product['ProductUnitId']), 'ProductUnitName') : '';
                $product['ProductUnitName'] = $productUnitName;
                $isGetPrice = $this->input->post('IsGetPrice');
                $productChildId = $this->input->post('ProductChildId');
                if($productChildId > 0){
                    $productChild = $this->Mproductchilds->get($productChildId, true, '', 'ProductChildId, ProductId, ProductName, ProductImage, StatusId, Price, BarCode, Weight, GuaranteeMonth, VATStatusId');
                    if($productChild && $productChild['StatusId'] == STATUS_ACTIVED){
                        if(empty($productChild['ProductImage'])) $productChild['ProductImage'] = NO_IMAGE;
                        $listProductPrices = $this->Mproductprices->getByProductId($productId);
                        $productRetVal = $productChild;
                        $productRetVal['ProductKindId'] = $product['ProductKindId'];
                        $productRetVal['ProductChildName'] = $productChild['ProductName'];
                        $productRetVal['ProductName'] = $product['ProductName'].' ('.$productChild['ProductName'].')';
                        $productRetVal['ProductUnitName'] = $productUnitName;
                        $productRetVal['PriceCapital'] = $this->Mpricechanges->getPrice($productId, $productChildId, $productChild['Price']);
                        echo json_encode(array('code' => 1, 'data' => array('Products' => array($productRetVal), 'ProductPrices' => $listProductPrices)));
                    }
                    else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
                }
                else{
                    $listProductPrices = $this->Mproductprices->getByProductId($productId);
                    $product['ProductChildId'] = 0;
                    $productRetVal = array();
                    if($product['ProductKindId'] == 1){
                        if($isGetPrice == 1) $product['PriceCapital'] = $this->Mpricechanges->getPrice($productId, 0, $product['Price']);
                        $productRetVal[] = $product;
                    }
                    elseif($product['ProductKindId'] == 2){
                        $listProductChilds = $this->Mproductchilds->getByProductId($productId);
                        foreach($listProductChilds as $pc){
                            $pc['ProductKindId'] = $product['ProductKindId'];
                            $pc['ProductChildName'] = $pc['ProductName'];
                            $pc['ProductName'] = $product['ProductName'].' ('.$pc['ProductName'].')';
                            if(empty($pc['ProductImage'])) $pc['ProductImage'] = NO_IMAGE;
                            $pc['ProductUnitName'] = $productUnitName;
                            if($isGetPrice == 1) $pc['PriceCapital'] = $this->Mpricechanges->getPrice($productId, $pc['ProductChildId'], $pc['Price']);
                            $productRetVal[] = $pc;
                        }
                    }
                    else{
                        $product['ProductName'] .= ' (Combo)';
                        if($isGetPrice == 1) $product['PriceCapital'] = $product['Price'];
                        $productRetVal[] = $product;
                    }
                    echo json_encode(array('code' => 1, 'data' => array('Products' => $productRetVal, 'ProductPrices' => $listProductPrices)));
                }
            }
            else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getProductChildCombo(){
        $this->checkUserLogin(true);
        $productId = $this->input->post('ProductId');
        if($productId > 0){
            $this->loadModel(array('Mproducts', 'Mproductchilds'));
            $listProductChilds = $this->Mproductchilds->getByProductId($productId);
            if(!empty($listProductChilds)){
                $data = array();
                $products = array();
                $productChilds = array();
                foreach($listProductChilds as $pc){
                    if($pc['ProductPartId'] > 0) {
                        if(!isset($products[$pc['ProductPartId']])) $products[$pc['ProductPartId']] = $this->Mproducts->get($pc['ProductPartId'], true, '', 'ProductName, ProductImage, Price, BarCode');
                        $productName = $products[$pc['ProductPartId']]['ProductName'];
                        $productImage = $products[$pc['ProductPartId']]['ProductImage'];
                        $price = $products[$pc['ProductPartId']]['Price'];
                        $barCode = $products[$pc['ProductPartId']]['BarCode'];
                        if($pc['ProductPartChildId'] > 0) {
                            if(!isset($productChilds[$pc['ProductPartChildId']])) $productChilds[$pc['ProductPartChildId']] = $this->Mproductchilds->get($pc['ProductPartChildId'], true, '', 'ProductName, ProductImage, Price, BarCode');
                            $productName .= ' (' . $productChilds[$pc['ProductPartChildId']]['ProductName'] .')';
                            $productImage = $productChilds[$pc['ProductPartChildId']]['ProductImage'];
                            $price = $productChilds[$pc['ProductPartChildId']]['Price'];
                            $barCode = $productChilds[$pc['ProductPartChildId']]['BarCode'];
                        }
                        $data[] = array(
                            'ProductName' => $productName,
                            'ProductImage' => empty($productImage) ? NO_IMAGE : $productImage,
                            'Price' => $price,
                            'BarCode' => $barCode,
                            'Quantity' => $pc['Quantity'],
                            'GuaranteeMonth' => $pc['GuaranteeMonth'],
                            'VATStatusId' => $pc['VATStatusId']
                        );
                    }
                }
                if(!empty($data)) echo json_encode(array('code' => 1, 'data' => $data));
                else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
            }
            else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getList(){
        $this->checkUserLogin(true);
        $pageId = trim($this->input->post('PageId'));
        $limit = trim($this->input->post('Limit'));
        if ($pageId > 0 && $limit > 0) {
            $postData = $this->arrayFromPost(array('SearchText', 'CategoryId'));
            $postData['ProductStatusId'] = STATUS_ACTIVED;
            $postData['ParentProductId'] = 0;
            $this->loadModel(array('Mproducts', 'Mproductchilds'));
            $data = array();
            $listProducts = $this->Mproducts->search($postData, $limit, $pageId);
            foreach ($listProducts as $p) {
                if ($p['ProductKindId'] == 2) $listProductChilds = $this->Mproductchilds->getByProductId($p['ProductId']);
                else $listProductChilds = array();
                $p['Childs'] = $listProductChilds;
                $data[] = $p;
            }
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getListByCustomerGuarantee(){
        $this->checkUserLogin(true);
        $customerId = trim($this->input->post('CustomerId'));
        if($customerId > 0) {
            $this->load->model('Mproducts');
            $listProducts = $this->Mproducts->getListByCustomerGuarantee($customerId);
            echo json_encode(array('code' => 1, 'data' => $listProducts));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getListAccessories(){
        $this->checkUserLogin(true);
        $productId = $this->input->post('ProductId');
        if($productId > 0){
            $this->load->model('Mproductaccessories');
            $data = $this->Mproductaccessories->getBy(array('ProductId' => $productId));
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getQuantityAllStore(){
        $productId = $this->input->post('ProductId');
        if($productId > 0){
            $productChildId = $this->input->post('ProductChildId');
            $this->loadModel(array('Mstores', 'Mproductquantity'));
            $listStores = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $listProductQuantity = $this->Mproductquantity->getAllStore($productId, $productChildId);
            $storeIds = array();
            $data = array();
            foreach($listProductQuantity as $pq){
                $pq['StoreName'] = $this->Mconstants->getObjectValue($listStores, 'StoreId', $pq['StoreId'], 'StoreName');
                $data[] = $pq;
                $storeIds[] = $pq['StoreId'];
            }
            if(count($data) < count($listStores)){
                foreach($listStores as $s){
                    if(!in_array($s['StoreId'], $storeIds)){
                        $data[] = array(
                            'StoreId' => $s['StoreId'],
                            'StoreName' => $s['StoreName'],
                            'Quantity' => 0
                        );
                        $storeIds[] = $s['StoreId'];
                    }
                }
            }
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getListForAffiliate(){
        $userName = $this->input->post('UserName');
        $userPass = $this->input->post('UserPass');
        if(!empty($userName) && !empty($userPass)) {
            $user = $this->Musers->login($userName, $userPass);
            if($user){
                $this->loadModel(array('Mproducts', 'Mproductchilds'));
                $listProducts = $this->Mproducts->getBy(array('ProductStatusId' => STATUS_ACTIVED));
                for($i = 0; $i < count($listProducts); $i++){
                    if($listProducts[$i]['ProductKindId'] > 1) $listProducts[$i]['Childs'] = $this->Mproductchilds->getByProductId($listProducts[$i]['ProductId']);
                }
                echo json_encode(array('code' => 1, 'data' => $listProducts));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    //mobile
    public function getInfo(){
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        if(!empty($barCode)){
            $this->loadModel(array('Mproducts', 'Mproductchilds'));
            $flag = false;
            $productChild = $this->Mproductchilds->getBy(array('BarCode' => $barCode, 'StatusId' => STATUS_ACTIVED, 'ParentProductChildId' => 0), true);
            if($productChild){
                $flag = true;
                $product = $this->Mproducts->get($productChild['ProductId']);
                $data = array(
                    'ProductName' => $product['ProductName'].' ('.$productChild['ProductName'].')',
                    'BarCode' => $barCode,
                    'Quantity' => $productChild['Quantity'],
                    'message' => 'Lấy thông tin sản phẩm thành công'
                );
                echo json_encode(array('code' => 1, 'message' => "Lấy thông tin sản phẩm thành công", 'data' => $data));
            }
            else{
                $product = $this->Mproducts->getBy(array('BarCode' => $barCode), true);
                if($product){
                    $flag = true;
                    $data = array(
                        'ProductName' => $product['ProductName'],
                        'BarCode' => $barCode,
                        'Quantity' => $product['Quantity'],
                        'message' => 'Lấy thông tin sản phẩm thành công'
                    );
                    echo json_encode(array('code' => 1, 'message' => "Lấy thông tin sản phẩm thành công", 'data' => $data));
                }
            }
            if(!$flag) echo json_encode(array('code' => 0, 'message' => "Sản phẩm không tồn tại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    public function warehouse(){ // Nhap kho - Kiem kho
        header('Content-Type: application/json');
        $userId = trim($this->input->post('UserId'));
        $scanName = trim($this->input->post('Name'));
        $scanTypeId = trim($this->input->post('ScanTypeId'));
        $storeId = trim($this->input->post('StoreId'));
        $createdDate = trim($this->input->post('CreatedDate'));
        $products = json_decode(trim($this->input->post('Products')), true);
        if($userId > 0 && $storeId > 0 && !empty($scanName) && $scanTypeId > 0 && !empty($products)){
            $crDateTime = getCurentDateTime();
            if(!empty($createdDate)) $createdDate = ddMMyyyyToDate($createdDate, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
            else $createdDate = $crDateTime;
            $postData = array(
                'ScanName' => $scanName,
                'ScanTypeId' => $scanTypeId,
                'ItemId' => 0,
                'StoreId' => $storeId,
                'ScanDateTime' => $createdDate,
                'CrUserId' => $userId,
                'CrDateTime' => getCurentDateTime()
            );
            $this->load->model('Mscanbarcodes');
            $scanBarCodeId = $this->Mscanbarcodes->update($postData, 0, $products);
            if($scanBarCodeId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật dữ liệu thành công"));
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function genBarCodePackage(){
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        $packageCount = trim($this->input->post('PackageCount'));
        if(!empty($barCode) && $packageCount > 0){
            $packageCodes = array();
            if($packageCount == 1) $packageCodes[] = $barCode;
            else{
                for($i = 1; $i <= $packageCount; $i++) $packageCodes[] = $barCode.'_'.$i;
            }
            echo json_encode(array('code' => 1, 'message' => "Sinh mã kiện thành công", 'data' => array('BarCode' => $barCode, 'PackageCodes' => implode(',', $packageCodes))));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function searchByFilter($searchTypeId = 0){
        $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $postData = $this->arrayFromGet(array('InventoryStatusId', 'StoreId'));
        $postData['SearchTypeId'] = $searchTypeId;
        $this->load->model('Mproducts');
        $data1 = $this->Mproducts->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }

    public function searchProductOldByFilter(){
        $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $postData = $this->arrayFromGet(array('InventoryStatusId', 'StoreId'));
        $this->load->model('Mproducts');
        $data1 = $this->Mproducts->searchProductOldByFilter($searchText, $itemFilters, $limit, $page, $postData);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}