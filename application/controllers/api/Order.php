<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller{

    public function getInfo(){
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        if (!empty($barCode)) {
            //$typeId = $this->input->post('TypeId'); //DH : đóng hàng, CG : check gộp, DHB : đóng hàng nhiều kiện, VC : Vận chuyển
            $this->loadModel(array('Morders', 'Mcustomers', 'Mproducts', 'Mproductchilds', 'Mtransports', 'Mtransporttypes'));
            $order = $this->Morders->getBy(array('OrderCode' => $barCode), true);
            if($order){
                $data = array(
                    'message' => 'Lấy thông tin đơn hàng thành công',
                    'Products' => $this->Mproducts->getInfoByOrder($order['OrderId']),
                    'Info' => array(
                        'Customer' => $this->Mcustomers->get($order['CustomerId'], true, '', 'FullName, PhoneNumber'),
                        'BarCode' => $barCode,
                        'OrderStatusId' => array('id' => $order['OrderStatusId'], 'name' => $this->Mconstants->orderStatus[$order['OrderStatusId']]),
                        'CrDateTime' => ddMMyyyy($order['CrDateTime']),
                        'TransportTypeId' => array('id' => 0, 'name' => '')
                    )
                );
                $transport = $this->Mtransports->getBy(array('OrderId' => $order['OrderId']), true);
                if($transport) $data['Info']['TransportTypeId'] = array('id' => $transport['TransportTypeId'], 'name' => $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $transport['TransportTypeId']), 'TransportTypeName'));
                echo json_encode(array('code' => 1, 'data' => $data));
            }
            else echo json_encode(array('code' => 0, 'message' => "Đơn hàng không tồn tại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    /*
     * type = 1: check tung don
     * type = 2: gop don
     */
    public function packing(){ //dong goi hang
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        $type = trim($this->input->post('Type'));
        $userId = trim($this->input->post('UserId'));//ng thuc hien
        if(!empty($barCode) && $type > 0 && $type < 3 && $userId > 0){
            $this->load->model('Mtransports');
            $transport = $this->Mtransports->getByOrderCode($barCode);
            if($transport){
                if($transport['TransportStatusId'] == 1){
                    /*$weight = trim($this->input->post('Weight'));
                    $desc = trim($this->input->post('Desc'));
                    $fromApp = trim($this->input->post('FromApp'));
                    $mistake = trim($this->input->post('Mistake'));*/ //0 - 1
                    $transportStatusId = 2;
                    $transportImages = array();
                    if(!empty($_FILES['Image'])){
                        $images = $this->reArrayFiles($_FILES['Image']);
                        foreach ($images as $img) {
                            $newName = date('YmdHis', time()) . mt_rand() . '.jpg';
                            move_uploaded_file($img['tmp_name'], 'assets/uploads/api/' . $newName);
                            $transportImages[] = array(
                                'TransportId' => $transport['TransportId'],
                                'Image' => $newName,
                                'TransportStatusId' => $transportStatusId
                            );
                        }
                    }
                    $crDateTime = getCurentDateTime();
                    $actionLog = array(
                        'ItemId' => $transport['TransportId'],
                        'ItemTypeId' => 9,
                        'ActionTypeId' => 2,
                        'Comment' => $this->Musers->getFieldValue(array('UserId' => $userId), 'FullName') . " cập nhật Trạng thái vận chuyển thành ".$this->Mconstants->transportStatus[$transportStatusId],
                        'CrUserId' => $userId,
                        'CrDateTime' => $crDateTime
                    );
                    $transportStatusLog = array(
                        'TransportId' => $transport['TransportId'],
                        'TransportStatusId' => $transportStatusId,
                        'CrUserId' => $userId,
                        'CrDateTime' => $crDateTime
                    );
                    $metaData = array('TransportImages' => $transportImages, 'TransportStatusLog' => $transportStatusLog);
                    $flag = $this->Mtransports->updateField(array('TransportStatusId' => $transportStatusId, 'UpdateUserId' => $userId, 'UpdateDateTime' => $crDateTime), $transport['TransportId'], $actionLog, $metaData);
                    if($flag) echo json_encode(array('code' => 1, 'message' => "Đóng gói hàng thành công", 'data' => array('message' => "Đóng gói hàng thành công")));
                    else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Vận chuyển đang ở trạng thái " . $this->Mconstants->transportStatus[$transport['TransportStatusId']]));
            }
            else echo json_encode(array('code' => -1, 'message' => "Đơn hàng chưa được chuyển sang vận chuyển"));

        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    private function reArrayFiles($file){
        $file_ary = array();
        $file_count = count($file['name']);
        $file_key = array_keys($file);
        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_key as $val) $file_ary[$i][$val] = $file[$val][$i];
        }
        return $file_ary;
    }

    /*
     * type = 1: check tung don
     * type = 2: gop don
     * type = 3: check nhieu kien
     */
    public function packingGoods(){ //đóng kiện
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        $type = trim($this->input->post('Type'));
        $userId = trim($this->input->post('UserId'));//ng thuc hien
        if (!empty($barCode) && $type > 0 && $type < 4 && $userId > 0) {
            $weight = trim($this->input->post('Weight'));
            $desc = trim($this->input->post('Desc'));
            $fromApp = trim($this->input->post('FromApp'));
            $mistake = trim($this->input->post('Mistake')); //0 - 1
            if (!empty($_FILES['Image'])) {
                $images = $this->reArrayFiles($_FILES['Image']);
                foreach ($images as $img) {
                    $newName = date('YmdHis', time()) . mt_rand() . '.jpg';
                    move_uploaded_file($img['tmp_name'], 'assets/uploads/api/' . $newName);
                }
            }
            $items = json_decode(trim($this->input->post('Items')), true);
            echo json_encode(array('code' => 1, 'message' => "Đóng gói hàng thành công", 'data' => array('message' => "Đóng gói hàng thành công")));
        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    public function mergeProduct(){ //check gop sp
        header('Content-Type: application/json');
        $barCodes = trim($this->input->post('BarCodes'));
        $type = trim($this->input->post('Type'));
        if (!empty($barCodes) && $type > 0 && $type < 3) {
            $barCodes = explode(';', $barCodes);
            if (!empty($barCodes)) echo json_encode(array('code' => 1, 'message' => "Gộp sản phẩm thành công", 'data' => array('message' => "Gộp sản phẩm thành công")));
            else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    public function changeStatus(){
        header('Content-Type: application/json');
        $barCode = trim($this->input->post('BarCode'));
        $orderStatusId = trim($this->input->post('OrderStatusId'));
        $userId = trim($this->input->post('UserId'));
        $transportTypeId = trim($this->input->post('TransportTypeId'));
        if (!empty($barCode) && $orderStatusId > 0 && $userId > 0 && $transportTypeId > 0) {
            $this->load->model('Morders');
            $order = $this->Morders->getBy(array('BarCode' => $barCode), true);
            if($order) {
                $flag = $this->Morders->changeStatusBatch(array($order['OrderId']), $orderStatusId, $this->Musers->get($userId));
                if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật trạng thái đơn hàng thành công", 'data' => array('message' => "Cập nhật trạng thái đơn hàng thành công")));
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => 0, 'message' => "Đơn hàng không tồn tại"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    public function changeStatusBatch(){
        header('Content-Type: application/json');
        $barCodes = explode(';', trim($this->input->post('BarCodes')));
        $orderStatusId = trim($this->input->post('OrderStatusId'));
        $userId = trim($this->input->post('UserId'));
        $transportTypeId = trim($this->input->post('TransportTypeId'));
        if (!empty($barCodes) && $orderStatusId > 0 && $userId > 0 && $transportTypeId > 0) {
            $this->load->model('Mtransporttypes');
            $transportTypeName = $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $transportTypeId), 'TransportTypeName');
            if (!empty($transportTypeName)) {
                $success = array();
                $error = array();
                $i = 0;
                foreach ($barCodes as $barCode) {
                    $i++;
                    if ($i % 2 == 0) {
                        $success[] = array(
                            'BarCode' => $barCode,
                            'TransportTypeName' => $transportTypeName
                        );
                    }
                    else {
                        $error[] = array(
                            'BarCode' => $barCode,
                            'TransportTypeName' => $transportTypeName
                        );
                    }
                }
                echo json_encode(array('code' => 1, 'message' => "Cập nhật trạng thái đơn hàng thành công", 'data' => array('message' => "Cập nhật trạng thái đơn hàng thành công", 'success' => $success, 'error' => $error)));
            }
            else echo json_encode(array('code' => -1, 'message' => "Không tồn tại loại Vận chuyển"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Mã vạch không được bỏ trống"));
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('CustomerId', 'CustomerAddressId', 'OrderChanelId', 'OrderStatusId', 'PendingStatusId', 'Discount', 'TransportCost', 'TransportTypeId', 'TransportReasonId', 'PaymentCost', 'PaymentStatusId', 'PaymentComment', 'TotalCost', 'VerifyStatusId', 'OrderTypeId', 'DeliveryTypeId', 'StoreId', 'OrderReasonId', 'CTVId'));
        if($postData['CustomerId'] > 0 && $postData['OrderStatusId'] > 0){
            $postData['Discount'] = replacePrice($postData['Discount']);
            $postData['TransportCost'] = replacePrice($postData['TransportCost']);
            $postData['PaymentCost'] = replacePrice($postData['PaymentCost']);
            $postData['TotalCost'] = replacePrice($postData['TotalCost']);
            $this->loadModel(array('Morders', 'Mproductchilds', 'Mproductquantity', 'Mcustomerconsults', 'Mconsultcomments'));
            $orderId = $this->input->post('OrderId');
            $isPOS = $postData['DeliveryTypeId'] == 1 && $postData['OrderStatusId'] == 6;
            $crDateTime = getCurentDateTime();
            $actionLog = array(
                'ItemTypeId' => 6,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
            $products = json_decode(trim($this->input->post('Products')), true);
            $services = json_decode(trim($this->input->post('OrderServices')), true);
            $POSData = array();
            if($isPOS){
                $actionLog['Comment'] = $user['FullName'] . ': Chốt đơn hàng';
                $debitCost = 0;
                $inventoryData = array();
                foreach($services as $s){
                    if($s['OtherServiceId'] == DEBIT_OTHER_TYPE_ID){
                        $debitCost = replacePrice($s['ServiceCost']);
                        break;
                    }
                }
                $orderCode = $this->Morders->genOrderCode($orderId);
                foreach($products as $op){
                    if($op['ProductKindId'] == 3){
                        $listProductChilds = $this->Mproductchilds->getByProductId($op['ProductId']);
                        foreach($listProductChilds as $pc){
                            if($pc['ProductPartId'] > 0){
                                $inventoryData[] = array(
                                    'ProductId' => $pc['ProductPartId'],
                                    'ProductChildId' => $pc['ProductPartChildId'],
                                    'OldQuantity' => $this->Mproductquantity->getQuantity($pc['ProductPartId'], $pc['ProductPartChildId'], $postData['StoreId']),
                                    'Quantity' => $op['Quantity'] * $pc['Quantity'],
                                    'InventoryTypeId' => 2,
                                    'StoreId' => $postData['StoreId'],
                                    'StatusId' => STATUS_ACTIVED,
                                    'IsManual' => 1,
                                    'Comment' => 'Trừ số lượng từ đơn hàng '.$orderCode,
                                    'CrUserId' => $user['UserId'],
                                    'CrDateTime' => $crDateTime,
                                    'UpdateUserId' => $user['UserId'],
                                    'UpdateDateTime' => $crDateTime
                                );
                            }
                        }
                    }
                    $inventoryData[] = array(
                        'ProductId' => $op['ProductId'],
                        'ProductChildId' => $op['ProductChildId'],
                        'OldQuantity' => $this->Mproductquantity->getQuantity($op['ProductId'], $op['ProductChildId'], $postData['StoreId']),
                        'Quantity' => $op['Quantity'],
                        'InventoryTypeId' => 2,
                        'StoreId' => $postData['StoreId'],
                        'StatusId' => STATUS_ACTIVED,
                        'IsManual' => 1,
                        'Comment' => 'Trừ số lượng từ đơn hàng '.$orderCode,
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => $crDateTime,
                        'UpdateUserId' => $user['UserId'],
                        'UpdateDateTime' => $crDateTime
                    );
                }
                $POSData = array('DebitCost' => $debitCost, 'InventoryData' => $inventoryData, 'FullName' => $user['FullName']);
                $POSData['MoneySourceId'] = $this->input->post('MoneySourceId');
                $POSData['BankId'] = $this->input->post('BankId');
                $POSData['RealPaymentCost'] = replacePrice($this->input->post('RealPaymentCost'));
            }
            if($orderId > 0){
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
                $actionLog['ActionTypeId'] = 2;
                $actionLog['Comment'] = $user['FullName'] . ': Cập nhật đơn hàng';
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
                $actionLog['ActionTypeId'] = 1;
                if($isPOS) $actionLog['Comment'] = $user['FullName'] . ': Thêm và chốt đơn hàng';
                else $actionLog['Comment'] = $user['FullName'] . ': Thêm mới đơn hàng';
            }
            $tagNames = json_decode(trim($this->input->post('TagNames')), true);
            $comments = json_decode(trim($this->input->post('Comments')), true);
            $promotion = array();
            if($postData['Discount'] > 0) {
                $promotion = array(
                    'PromotionId' => $this->input->post('PromotionId'),
                    'PromotionCode' => $this->input->post('PromotionCode'),
                    'DiscountPercent' => $this->input->post('DiscountPercent'),
                    'DiscountCost' => $postData['Discount'],
                    'StatusId' => STATUS_ACTIVED,
                    'Comment' => trim($this->input->post('PromotionComment')),
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
            }
            $remindData = $this->arrayFromPost(array('OwnCost', 'RemindDate', 'RemindComment'));
            $remindData['OwnCost'] = replacePrice($remindData['OwnCost']);
            if ($remindData['OwnCost'] > 0 && !empty($remindData['RemindDate'])){
                $remindData['RemindDate'] = ddMMyyyyToDate($remindData['RemindDate'], 'd/m/Y H:i', 'Y-m-d H:i');
                $remindData = array_merge($remindData, array(
                    'IsRepeat' => 1,
                    'RepeatDay' => 0,
                    'RepeatHour' => 0,
                    'RemindStatusId' => 1,
                    'RemindTypeId' => 2,
                    'UserId' => $user['UserId'],
                    'PartId' => 0,
                    'CustomerId' => $postData['CustomerId'],
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                ));
            }
            else $remindData = array();
            $customerConsult = array('CustomerConsultId' => 0, 'ConsultUserIds' => array());
            if($orderId == 0){
                $customerConsultId = $this->Mcustomerconsults->getFieldValue(array('CustomerId' => $postData['CustomerId'], 'OrderId' => 0), 'CustomerConsultId', 0);
                $customerConsult['CustomerConsultId'] = $customerConsultId;
                if($customerConsultId > 0) $customerConsult['ConsultUserIds'] = $this->Mconsultcomments->getUserIds($customerConsultId);
            }
            $orderId = $this->Morders->update($postData, $orderId, $products, $tagNames, $services, $promotion, $comments, $remindData, $POSData, $customerConsult, $actionLog);
            if ($orderId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật đơn hàng thành công", 'data' => $orderId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function insertFromWeb(){
        $postData = $this->arrayFromPost(array('CustomerId', 'CustomerAddressId', 'OrderChanelId', 'OrderStatusId', 'PendingStatusId', 'Discount', 'TransportCost', 'TransportTypeId', 'TransportReasonId', 'PaymentCost', 'PaymentStatusId', 'PaymentComment', 'TotalCost', 'VerifyStatusId', 'OrderTypeId', 'DeliveryTypeId', 'StoreId', 'OrderReasonId', 'CTVId'));
        if($postData['CustomerId'] > 0 && $postData['OrderStatusId'] > 0){
            $postData['Discount'] = replacePrice($postData['Discount']);
            $postData['TransportCost'] = replacePrice($postData['TransportCost']);
            $postData['PaymentCost'] = replacePrice($postData['PaymentCost']);
            $postData['TotalCost'] = replacePrice($postData['TotalCost']);
            $postData['CrUserId'] = 0;
            $crDateTime = getCurentDateTime();
            $postData['CrDateTime'] = $crDateTime;
            $actionLog = array(
                'ItemTypeId' => 6,
                'ActionTypeId' => 1,
                'Comment' => 'Khách hàng thêm đơn hàng',
                'CrUserId' => 0,
                'CrDateTime' => $crDateTime
            );
            $products = json_decode(trim($this->input->post('Products')), true);
            if($postData['CTVId'] > 0){
                $affiliateData = $this->arrayFromPost(array('LinkAffId', 'AffiliateName', 'OfferId', 'OfferName'));
                if($affiliateData['LinkAffId'] > 0 && $affiliateData['AffiliateName'] == 'Ricky' && $affiliateData['OfferId'] == 1 && $affiliateData['OfferName'] == 'Ricky'){
                    //check api linklogs
                    $this->load->model('Mproducts');
                    $productSlugs = array();
                    $productJson = array();
                    foreach($products as $p){
                        if(!isset($productSlugs[$p['ProductId']])) $productSlugs[$p['ProductId']] = $this->Mproducts->getFieldValue(array('ProductId' => $p['ProductId']), 'ProductSlug');
                        $productJson[] = array(
                            'ProductId' => $p['ProductId'],
                            'ProductChildId' => $p['ProductChildId'],
                            'ProductUrl' => 'https://ricky.vn/products/'.$productSlugs[$p['ProductId']]
                        );
                    }
                    $productJson = json_encode($productJson);
                    $json = curlCrawl('https://aff.ricky.vn/api/link/check', "UserName=hoanmuada&UserPass=123456789&CTVId={$postData['CTVId']}&LinkAffId={$affiliateData['LinkAffId']}&AffiliateName={$affiliateData['AffiliateName']}&OfferId={$affiliateData['OfferId']}&OfferName={$affiliateData['OfferName']}&Products={$productJson}");
                    $json = @json_decode($json, true);
                    if($json['code'] != 1) $postData['CTVId'] = 0;
                }
            }
            $this->loadModel(array('Mcustomerconsults', 'Mconsultcomments', 'Morders'));
            $customerConsult = array('ConsultUserIds' => array());
            $customerConsultId = $this->Mcustomerconsults->getFieldValue(array('CustomerId' => $postData['CustomerId'], 'OrderId' => 0), 'CustomerConsultId', 0);
            $customerConsult['CustomerConsultId'] = $customerConsultId;
            if($customerConsultId > 0) $customerConsult['ConsultUserIds'] = $this->Mconsultcomments->getUserIds($customerConsultId);
            $orderId = $this->Morders->update($postData, 0, $products, array(), array(), array(), array(), array(), array(), $customerConsult, $actionLog);
            if ($orderId > 0) echo json_encode(array('code' => 1, 'message' => "Thêm đơn hàng thành công", 'data' => $orderId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function insertComment(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('OrderId', 'Comment', 'CommentTypeId'));
        if($postData['OrderId'] > 0 && !empty($postData['Comment']) && $postData['CommentTypeId'] > 0){
            $postData['UserId'] = $user['UserId'];
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mordercomments');
            $flag = $this->Mordercomments->save($postData);
            if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeVerifyStatusBatch(){
        $user = $this->checkUserLogin(true);
        $orderIds = json_decode(trim($this->input->post('OrderIds')), true);
        $verifyStatusId = $this->input->post('VerifyStatusId');
        if (!empty($orderIds) && $verifyStatusId >= 0) {
            $this->load->model('Morders');
            $flag = $this->Morders->changeVerifyStatusBatch($orderIds, $verifyStatusId, $user);
            if($flag) echo json_encode(array('code' => 1, 'message' => 'Cập nhật trạng thái xác thực thành công', 'data' => $verifyStatusId));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    /*public function changeStatusBatchWeb(){
        $user = $this->checkUserLogin(true);
        $orderIds = json_decode(trim($this->input->post('ItemIds')), true);
        $statusId = $this->input->post('StatusId');
        if(!empty($orderIds) && $statusId >= 0){
            $metaData = array(
                'UserId' => $user['UserId'],
                'FullName' => $user['FullName'],
                'Comment' => trim($this->input->post('Comment'))
            );
            $this->load->model('Morders');
            $flag = $this->Morders->changeStatusBatch($orderIds, $statusId, $metaData);
            if($flag) echo json_encode(array('code' => 1, 'message' => 'Cập nhật trạng thái đơn hàng thành công'));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }*/

    public function updateField(){
        $user = $this->checkUserLogin(true);
        $orderId = $this->input->post('OrderId');
        $fieldName = trim($this->input->post('FieldName'));
        $fieldValue = trim($this->input->post('FieldValue'));
        if($orderId > 0 && !empty($fieldName) && !empty($fieldValue)){
            $label = '';
            $value = '';
            $comment = '';
            $message = '';
            $data = array();
            $crDateTime = getCurentDateTime();
            $postData = array($fieldName => $fieldValue, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime);
            $this->loadModel(array('Morders', 'Mactionlogs'));
            switch($fieldName){
                case 'OrderStatusId';
                    if($fieldValue > 0 && $fieldValue < 7) {
                        $orderStatusIdOld = $this->Morders->getFieldValue(array('OrderId' => $orderId), 'OrderStatusId', 0);
                        if ($orderStatusIdOld == 1 || $orderStatusIdOld == 5) {
                            $label = 'Trạng thái';
                            $value = $this->Mconstants->orderStatus[$fieldValue];
                            if($fieldValue == 3){
                                $postData['CancelReasonId'] = $this->input->post('CancelReasonId');
                                $postData['CancelComment'] = trim($this->input->post('CancelComment'));
                            }
                        }
                    }
                    break;
                case 'PendingStatusId':
                    if($fieldValue > 0){
                        $label = 'Trạng thái chờ xử lý';
                        $this->load->model('Mpendingstatus');
                        $value = $this->Mpendingstatus->getFieldValue(array('PendingStatusId' => $fieldValue), 'PendingStatusName');
                        $data['StatusName'] = $this->Mconstants->orderStatus[1].' ('.$value.')';
                    }
                    break;
                case 'StoreId':
                    if($fieldValue > 0){
                        $label = 'Cở sở';
                        $this->load->model('Mstores');
                        $value = $this->Mstores->getFieldValue(array('StoreId' => $fieldValue), 'StoreName');
                        $data['StoreName'] = $value;
                    }
                    break;
                case 'OrderReasonId':
                    if($fieldValue > 0){
                        $label = 'Lý do biết đến mua hàng';
                        $this->load->model('Morderreasons');
                        $value = $this->Morderreasons->getFieldValue(array('OrderReasonId' => $fieldValue), 'OrderReasonName');
                    }
                    break;
                case 'TransportCost':
                    if($fieldValue >= 0){
                        $label = 'Phí vận chuyển';
                        $value = priceFormat($fieldValue).' VNĐ';
                        $postData['TransportTypeId'] = $this->input->post('TransportTypeId');
                        $postData['TransportReasonId'] = $this->input->post('TransportReasonId');
                    }
                    break;
                default: break;
            }
            if(!empty($label)){
                if(empty($comment)) $comment = $user['FullName'] . " cập nhật {$label} thành {$value}";
                $data['Comment'] = $comment;
                $actionLog = array(
                    'ItemId' => $orderId,
                    'ItemTypeId' => 6,
                    'ActionTypeId' => 2,
                    'Comment' => $comment,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $flag = $this->Morders->updateField($postData, $orderId, $actionLog);
                if ($flag) echo json_encode(array('code' => 1, 'message' => empty($message) ? "Cập nhật {$label} thành công" : $message, 'data' => $data));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getDetail(){
        $this->checkUserLogin(true);
        $orderId = $this->input->post('OrderId');
        if($orderId > 0){
            $this->loadModel(array('Morders', 'Mordercomments', 'Mcustomers', 'Mcustomeraddress', 'Morderproducts',  'Mprovinces', 'Mdistricts', 'Mcountries', 'Mwards', 'Mproducts', 'Mproductchilds', 'Mtransports', 'Mtransportstatuslogs','Mtransporttypes'));
            $order = $this->Morders->get($orderId, true, '', 'CustomerId, CustomerAddressId');
            if($order){
                $customerAddress = $this->Mcustomeraddress->getInfo($order['CustomerAddressId'], $order['CustomerId']);
                if($customerAddress){
                    if($customerAddress['CountryId'] == 0 || $customerAddress['CountryId'] == 232) {
                        if ($customerAddress['ProvinceId'] > 0) $customerAddress['ProvinceName'] = $this->Mprovinces->getFieldValue(array('ProvinceId' => $customerAddress['ProvinceId']), 'ProvinceName');
                        else $customerAddress['ProvinceName'] = '';
                        if ($customerAddress['DistrictId'] > 0) $customerAddress['DistrictName'] = $this->Mdistricts->getFieldValue(array('DistrictId' => $customerAddress['DistrictId']), 'DistrictName');
                        else $customerAddress['DistrictName'] = '';
                        if ($customerAddress['WardId'] > 0) $customerAddress['WardName'] = $this->Mwards->getFieldValue(array('WardId' => $customerAddress['WardId']), 'WardName');
                        else $customerAddress['WardName'] = '';
                        $customerAddress['CountryName'] = 'Việt Nam';
                    }
                    else $customerAddress['CountryName'] = $this->Mcountries->getFieldValue(array('CountryId' => $customerAddress['CountryId']), 'CountryName');
                }
                $listProducts = array();
                $products = array();
                $productChilds = array();
                $listOrderProducts = $this->Morderproducts->getBy(array('OrderId' => $orderId));
                foreach($listOrderProducts as $op){
                    if($op['ProductKindId'] == 3){
                        $listProductChilds = $this->Mproductchilds->getByProductId($op['ProductId']);
                        foreach ($listProductChilds as $pc){
                            if(!isset($products[$pc['ProductPartId']])) $products[$pc['ProductPartId']] = $this->Mproducts->get($pc['ProductPartId'], true, '', 'ProductName, ProductImage, ProductUnitId, Price');
                            $productName = $products[$pc['ProductPartId']]['ProductName'];
                            $productImage = $products[$pc['ProductPartId']]['ProductImage'];
                            $productChildName = '';
                            if($pc['ProductPartChildId'] > 0){
                                if(!isset($productChilds[$pc['ProductPartChildId']])) $productChilds[$pc['ProductPartChildId']] = $this->Mproductchilds->get($pc['ProductPartChildId'], true, '', 'ProductName, ProductImage, Price');
                                $productChildName = $productChilds[$pc['ProductPartChildId']]['ProductName'];
                                $productImage = $productChilds[$pc['ProductPartChildId']]['ProductImage'];
                            }
                            if(empty($productImage)) $productImage = NO_IMAGE;
                            $listProducts[] = array(
                                'ProductId' => $op['ProductId'],
                                'ProductName' => $productName,
                                'ProductChildId' => $op['ProductChildId'],
                                'ProductChildName' => $productChildName,
                                'ProductImage' => $productImage,
                                'Quantity' => $op['Quantity'] * $pc['Quantity'],
                                'Price' => $op['Price'],
                                'ProductKindId' => $op['ProductKindId'],
                            );
                        }
                    }
                    else{
                        if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, BarCode');
                        $productName = $products[$op['ProductId']]['ProductName'];
                        $productImage = $products[$op['ProductId']]['ProductImage'];
                        $productChildName = '';
                        if($op['ProductChildId'] > 0){
                            if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, BarCode');
                            $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                            $productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                        }
                        if(empty($productImage)) $productImage = NO_IMAGE;
                        $listProducts[] = array(
                            'ProductId' => $op['ProductId'],
                            'ProductName' => $productName,
                            'ProductChildId' => $op['ProductChildId'],
                            'ProductChildName' => $productChildName,
                            'ProductImage' => $productImage,
                            'Quantity' => $op['Quantity'],
                            'Price' => $op['Price'],
                            'ProductKindId' => $op['ProductKindId'],
                        );
                    }
                }
                $transport = $this->Mtransports->getBy(array('OrderId' => $orderId, 'TransportStatusId >' => 0), true, 'TransportId', 'TransportId, TransportCode, TransportStatusId, TransportTypeId, Tracking, CODStatusId');
                $transportStatusIds = array();
                $transportTypeName = '';
                if($transport){
                    $transportStatusIds = $this->Mtransportstatuslogs->getListStatusIds($transport['TransportId'], $transport['TransportStatusId']);
                    if($transport['TransportTypeId'] > 0) $transportTypeName =  $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $transport['TransportTypeId']), 'TransportTypeName');
                }
                $data = array(
                    'customerAddress' => $customerAddress,
                    'Products' => $listProducts,
                    'listOrderComments' => $this->Mordercomments->getListByOrderId($orderId),
                    'transport' => $transport,
                    'transportStatusIds' => $transportStatusIds,
                    'transportTypeName' => $transportTypeName,
                    'labelCss' => $this->Mtransports->labelCss,
                    'transportStatus' => $this->Mconstants->transportStatus,
                    'CODStatus' => $this->Mconstants->CODStatus
                );
                echo json_encode(array('code' => 1, 'data' => $data));
            }
            else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy đơn hàng"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function checkQuantity(){
        $this->checkUserLogin(true);
        $products = $this->input->post('Products');
        $countProduct = count($products);
        if($countProduct > 0){
            $productIds = array();
            foreach($products as $p){
                if(!in_array($p['ProductId'], $productIds)) $productIds[] = $p['ProductId'];
            }
            $storeId = $this->input->post('StoreId');
            $this->loadModel(array('Mproductquantity', 'Mstores'));
            $this->load->model('Mproductquantity');
            $listProductQuantity = $this->Mproductquantity->getListQuantity($productIds, $storeId);
            $listStores = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data = array();
            foreach($listStores as $s){
                $productStocks = array();
                $i = 0;
                foreach($products as $p){
                    $productTmp = array(
                        'ProductName' => $p['ProductName'],
                        'ProductId' => $p['ProductId'],
                        'ProductChildId' => $p['ProductChildId'],
                        'Quantity' => intval($p['Quantity']),
                        'StockQuantity' => 0
                    );
                    foreach($listProductQuantity as $pq){
                        if($p['ProductId'] == $pq['ProductId'] && $p['ProductChildId'] == $pq['ProductChildId'] && $pq['StoreId'] == $s['StoreId']){
                            $productTmp['StockQuantity'] = $pq['Quantity'];
                            break;
                        }
                    }
                    $productStocks[] = $productTmp;
                    if($productTmp['StockQuantity'] > $productTmp['Quantity']) $i++;
                }
                $data[] = array(
                    'StoreId' => $s['StoreId'],
                    'StoreName' => $s['StoreName'],
                    'Products' => $productStocks,
                    'IsInStock' => $i == $countProduct
                );
            }
            echo json_encode(array('code' => 1, 'data' => $data));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function revenue(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BeginDate', 'EndDate', 'StoreId', 'ProductTypeId', 'DeliveryTypeId', 'CrUserId'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        $this->load->model('Morders');
        $revenue = $this->Morders->revenue($postData);
        echo json_encode(array('code' => 1, 'data' => $revenue));
    }

    public function searchByOrderStatus(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BeginDate', 'EndDate', 'OrderStatusIds'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        $this->load->model(array('Morders','Mtransports'));
        $listOrders = $this->Morders->searchByOrderStatus($postData);
        echo json_encode(array('code' => 1, 'data' => $listOrders));
    }

    public function productSelling(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        $this->loadModel(array('Morders', 'Mproducts', 'Mproductchilds', 'Mproductquantity'));
        $products = $this->Morders->productSelling($postData);
        echo json_encode(array('code' => 1, 'data' => $products));
    }

    public function orderReason(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        $this->load->model('Morders');
        $listOrders = $this->Morders->orderReason($postData);
        $data = array();
        $countDate = array();
        foreach($listOrders as $o){
            if(isset($data[$o['CrDate']])){
                $data[$o['CrDate']]['Data'][] = array(
                    'OrderReasonId' => $o['OrderReasonId'],
                    'CountOrder' => $o['CountOrder']
                );
                $countDate[$o['CrDate']] += $o['CountOrder'];
            }
            else{
                $data[$o['CrDate']] = array(
                    'CrDate' => $o['CrDate'],
                    'CrDateTime' => ddMMyyyy($o['CrDate']),
                    'Data' => array(array(
                        'OrderReasonId' => $o['OrderReasonId'],
                        'CountOrder' => $o['CountOrder']
                    ))
                );
                $countDate[$o['CrDate']] = $o['CountOrder'];
            }
        }
        foreach($data as $crDate => $arr){
            $data[$crDate]['TotalOrder'] = isset($countDate[$crDate]) ? $countDate[$crDate] : 0;
        }
        usort($data, function ($a, $b) {
            return strtotime($a['CrDate']) <= strtotime($b['CrDate']);
        });
        echo json_encode(array('code' => 1, 'data' => $data));
    }

    public function searchByFilter($customerKindId = 0, $isSearchExactlyPhone = 0){
        $user = $this->checkUserLogin(true);
        $data = array();
        $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
        $itemFilters = $this->input->post('itemFilters');
        if(!is_array($itemFilters)) $itemFilters = array();
        if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
        $postData = array('CustomerId' => $this->input->post('CustomerId'), 'CustomerKindId' => $customerKindId, 'IsSearchExactlyPhone' => $isSearchExactlyPhone);
        if($customerKindId == 3){
            $customerKindId = 1;
            $postData['CustomerKindId'] = $customerKindId;
            $postData['HasCTV'] = 1;
        }
        $flag = $this->Mactions->checkAccessFromDb('order/viewAll', $user['UserId']);
        if(!$flag) $postData['CrUserId'] = $user['UserId'];
        $this->loadModel(array('Morders', 'Mtransports'));
        $data1 = $this->Morders->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}