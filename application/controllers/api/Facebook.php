<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook extends MY_Controller{

    public function listen(){
        header('Access-Control-Allow-Origin: *');
        $json = trim($this->input->post('Json'));
        if(!empty($json)){
            $json = @json_decode($json, true);
            if($json && $json['object'] == 'page'){
                $this->loadModel(array('Mfbpages', 'Mfbusers', 'Mfb_posts', 'Mfb_comments', 'Mfb_threads'));
                $data = array();
                foreach($json['entry'] as $entry){
                    foreach($entry['changes'] as $changes){
                        $value = $changes['value'];
                        if($changes['field'] == 'feed'){ //comment + post
                            if($value['item'] == 'status'){ //post
                                $postData = array(
                                    'PostId' => $value['post_id'],
                                    'FbPageId' => 0,
                                    'PostContent' => $value['message'],
                                    'FbUserId' => 0,
                                    'PostLink' => 'https://www.facebook.com/permalink.php?story_fbid='.$value['post_id'],
                                    'SenderId' => $value['sender_id'],
                                    'SenderName' => $value['sender_name'],
                                    'CreatedDateTime' => $value['created_time'],
                                    'CrDateTime' => getCurentDateTime()
                                );
                                //insert + log
                                $this->Mfb_posts->setPrefix($entry['id']);
                                $flag = $this->Mfb_posts->getFieldValue(array('PostId' => $value['post_id']), 'FbPostId', 0);
                                if($flag == 0){
                                    $postData['FbPageId'] = $this->Mfbpages->getFieldValue(array('FbPageCode' => $entry['id']), 'FbPageId', 0);
                                    $postData['FbUserId'] = $this->Mfbusers->update($value['sender_id'], $value['sender_name']);
                                    $fbPostId = $this->Mfb_posts->save($postData);
                                    $postData['FbPostId'] = $fbPostId;
                                    $data[] = array('TypeId' => 3, 'Post' => $postData);
                                }
                            }
                            elseif($value['item'] == 'comment'){ //comment
                                $commentData = array(
                                    'FbPageId' => 0,
                                    'FbUserId' => 0,
                                    'CommentId' => $value['comment_id'],
                                    'SenderId' => $value['sender_id'],
                                    'SenderName' => $value['sender_name'],
                                    'FbPostCode' => $value['post_id'],
                                    'FbPostId' => 0,
                                    'ParentId' => $value['parent_id'],
                                    'ParentCommentId' => 0,
                                    'Message' => isset($value['message']) ? $value['message'] : '',
                                    'Image' => isset($value['photo']) ? $value['photo'] : '',
                                    'ViewStatusId' => 1,
                                    'AnswerStatusId' => 1,
                                    'CommentStatusId' => 2,
                                    'CreatedDateTime' => $value['created_time'],
                                    'CrDateTime' => getCurentDateTime()
                                );
                                $this->Mfb_comments->setPrefix($entry['id']);
                                $flag = $this->Mfb_comments->getFieldValue(array('CommentId' => $value['comment_id']), 'FbCommentId', 0);
                                if($flag == 0){
                                    $commentData['FbPageId'] = $this->Mfbpages->getFieldValue(array('FbPageCode' => $entry['id']), 'FbPageId', 0);
                                    $commentData['FbUserId'] = $this->Mfbusers->update($value['sender_id'], $value['sender_name']);
                                    $this->Mfb_posts->setPrefix($entry['id']);
                                    $commentData['FbPostId'] = $this->Mfb_posts->getFieldValue(array('PostId' => $value['post_id']), 'FbPostId', 0);
                                    if($value['post_id'] != $value['parent_id']) $commentData['ParentCommentId'] = $this->Mfb_comments->getFieldValue(array('CommentId' => $value['parent_id']), 'FbCommentId', 0);
                                    $fbCommentId = $this->Mfb_comments->save($commentData);
                                    $commentData['FbCommentId'] = $fbCommentId;
                                    $data[] = array('TypeId' => 2, 'Comment' => $commentData);
                                }
                            }
                        }
                        elseif($changes['field'] == 'conversations'){
                            $this->Mfb_threads->setPrefix($entry['id']);
                            $theadData = array('FbPageCode' => $value['page_id'], 'TimeStr' => $entry['time'], 'ThreadId' => $value['thread_id']);
                            $fbThreadId = $this->Mfb_threads->getFieldValue($theadData, 'FbThreadId', 0);
                            if($fbThreadId == 0){
                                $theadData['FbPageId'] = $this->Mfbpages->getFieldValue(array('FbPageCode' => $value['page_id']), 'FbPageId', 0);
                                $fbThreadId = $this->Mfb_threads->save($theadData);
                                $theadData['FbThreadId'] = $fbThreadId;
                                $data[] = array('TypeId' => 1, 'Thread' => $theadData);
                            }
                        }
                    }
                }
                echo json_encode(array('code' => 1, 'data' => $data));
            }
            else echo json_encode(array('code' => -1));
        }
        else echo json_encode(array('code' => -1));
    }
}