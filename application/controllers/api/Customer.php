<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('FirstName', 'LastName', 'FullName', 'PhoneNumber', 'Email', 'GenderId', 'StatusId', 'BirthDay', 'CustomerTypeId', 'CustomerKindId', 'CountryId', 'ProvinceId', 'DistrictId', 'WardId', 'Address', 'ZipCode', 'CustomerGroupId', 'FaceBook', 'Comment', 'CareStaffId', 'DiscountTypeId', 'PaymentTimeId', 'CTVId', 'PositionName', 'CompanyName', 'TaxCode', 'IsReceiveAd', 'DebtCost', 'Password'));
		if($postData['GenderId'] > 0 && $postData['StatusId'] > 0 && $postData['CustomerTypeId'] > 0 && $postData['CustomerKindId'] > 0) {
			$customerId = $this->input->post('CustomerId');
			$this->load->model('Mcustomers');
			if($this->Mcustomers->checkExist($customerId, $postData) > 0) {
				echo json_encode(array('code' => -1, 'message' => "Email hoặc Số điện thoại đã tồn tại trong hệ thống"));
			}
			else{
				if(empty($postData['FullName'])) $postData['FullName'] = $postData['FirstName'] . ' ' . $postData['LastName'];
				$parts = explode('-', $postData['PhoneNumber']);
				if (count($parts) == 2) {
					$postData['PhoneNumber'] = trim($parts[0]);
					$postData['PhoneNumber2'] = trim($parts[1]);
				}
				else $postData['PhoneNumber2'] = '';
				if (!empty($postData['BirthDay'])) $postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
				$postData['DebtCost'] = replacePrice($postData['DebtCost']);
				$crDateTime = getCurentDateTime();
				$actionLogs = array(
					'ItemTypeId' => 5,
					'CrUserId' => $user['UserId'],
					'CrDateTime' => $crDateTime
				);
				if($customerId > 0){
					$postData['UpdateUserId'] = $user['UserId'];
					$postData['UpdateDateTime'] = $crDateTime;
					if(!empty($postData['Password'])) $postData['Password'] = md5($postData['Password']);
					else unset($postData['Password']);
					$actionLogs['ActionTypeId'] = 2;
					$actionLogs['Comment'] = $user['FullName'] . ': Cập nhật khách hàng';
				}
				else {
					$postData['Balance'] = 0;
					$postData['CrUserId'] = $user['UserId'];
					$postData['CrDateTime'] = $crDateTime;
					if(!empty($postData['Password'])) $postData['Password'] = md5($postData['Password']);
					else $postData['Password'] = md5('123456789');
					$actionLogs['ActionTypeId'] = 1;
					$actionLogs['Comment'] = $user['FullName'] . ': Thêm mới khách hàng';
				}
				$tagNames = json_decode(trim($this->input->post('TagNames')), true);
				$customerId = $this->Mcustomers->update($postData, $customerId, $tagNames, $actionLogs);
				if($customerId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Khách hàng thành công", 'data' => $customerId));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

	/*public function deleteBatch(){
		$user = $this->checkUserLogin(true);
		$customerIds = json_decode(trim($this->input->post('ItemIds')), true);
		if(!empty($customerIds)){
			$this->load->model('Mcustomers');
			$flag = $this->Mcustomers->deleteBatch($customerIds, $user);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Khách hàng thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}*/

	public function updateGroup(){
		$user = $this->checkUserLogin(true);
		$customerGroupId = $this->input->post('CustomerGroupId');
		$postData = $this->arrayFromPost(array('CustomerGroupName', 'CustomerKindId')); // , 'Conditions'
		if($postData['CustomerKindId'] > 0 && ($customerGroupId > 0 || !empty($postData['CustomerGroupName']))){
			$postData['StatusId'] = STATUS_ACTIVED;
			$this->loadModel(array('Mcustomergroups', 'Mcustomers', 'Mfilters'));
			$filterId = $this->input->post('FilterId');
			if($filterId > 0) $postData['FilterId'] = $filterId;
			else{
				$filterData = $this->input->post('FilterData');
				$tagFilter = $this->input->post('TagFilter');
				if(!empty($filterData)) {
					$postFilter['StatusId'] = 1;
					$postFilter['CrUserId'] = $user['UserId'];
					$postFilter['CrDateTime'] = getCurentDateTime();
					$postFilter['ItemTypeId'] = 5;
					$postFilter['FilterData'] = $filterData;
					$postFilter['TagFilter'] = $tagFilter;
					$filterId = $this->Mfilters->save($postFilter, $filterId);
					$postData['FilterId'] = $filterId;
				}
			}
			if($customerGroupId > 0){
				$postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
			else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
			$customerGroupId = $this->Mcustomergroups->update($postData, $customerGroupId);
			$metaData = array(
				'UserId' => $user['UserId'],
				'FullName' => $user['FullName'],
				'CustomerGroupName' => $postData['CustomerGroupName']
			);
			if($customerGroupId > 0){
				$customerIds = json_decode(trim($this->input->post('CustomerIds')), true);
				if($filterId == 0 && !empty($customerIds)) $flag = $this->Mcustomers->updateGroup($customerIds, $customerGroupId, $metaData);
				else $flag = true;
				if ($flag) echo json_encode(array('code' => 1, 'message' => 'Cập nhật nhóm khách hàng thành công'));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function insertAddress(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('CustomerId', 'CustomerName', 'Email', 'PhoneNumber', 'Address', 'ProvinceId', 'DistrictId', 'WardId', 'CountryId', 'ZipCode'));
		if($postData['CustomerId'] > 0 && !empty($postData['CustomerName'] && !empty($postData['PhoneNumber'])) && !empty($postData['Address']) && $postData['ProvinceId'] > 0) {
			$postData['CrUserId'] = $user['UserId'];
			$postData['CrDateTime'] = getCurentDateTime();
			$this->load->model('Mcustomeraddress');
			$id = $this->Mcustomeraddress->update($postData);
			if($id > 0){
				$itemId = $this->input->post('ItemId');
				$itemTypeId = $this->input->post('ItemTypeId');
				if($itemId > 0 && $itemTypeId > 0) {
					if($itemTypeId == 6) $this->db->update('orders', array('CustomerAddressId' => $id), array('OrderId' => $itemId));
					elseif($itemTypeId == 9){
						$this->db->update('transports', array('CustomerAddressId' => $id), array('TransportId' => $itemId));
						$this->db->update('orders', array('CustomerAddressId' => $id), array('OrderId' => $this->input->post('OrderId')));
					}
				}
				echo json_encode(array('code' => 1, 'data' => $id));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function insertComment(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('CustomerId', 'Comment'));
		if($postData['CustomerId'] > 0 && !empty($postData['Comment'])){
			$postData['UserId'] = $user['UserId'];
			$postData['CrDateTime'] = getCurentDateTime();
			$this->load->model('Mcustomercomments');
			$flag = $this->Mcustomercomments->save($postData);
			if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ghi chú thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getList(){
		$this->checkUserLogin(true);
		$pageId = trim($this->input->post('PageId'));
		$limit = trim($this->input->post('Limit'));
		if($pageId > 0 && $limit > 0){
			$postData = $this->arrayFromPost(array('SearchText', 'CustomerKindId'));
			$postData['StatusId'] = STATUS_ACTIVED;
			$this->load->model('Mcustomers');
			$listCustomers = $this->Mcustomers->search($postData, $limit, $pageId);
			echo json_encode(array('code' => 1, 'data' => $listCustomers));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function get(){
		$this->checkUserLogin(true);
		$customerId = $this->input->post('CustomerId');
		if($customerId > 0){
			$this->loadModel(array('Mcustomers', 'Morders', 'Mcustomergroups', 'Mprovinces', 'Mdistricts', 'Mwards', 'Mcountries'));
			$customer = $this->Mcustomers->get($customerId);
			if($customer){
				//$customer['CustomerLink'] = base_url('customer/edit/'.$customerId);
				$customer['CustomerGroupName'] = $this->Mcustomergroups->getFieldValue(array('CustomerGroupId' => $customer['CustomerGroupId']), 'CustomerGroupName');
				if($customer['CountryId'] == 232 || $customer['CountryId'] == 0) $customer['CountryName'] = 'Việt Nam';
				else $customer['CountryName'] = $this->Mcountries->getFieldValue(array('CountryId' => $customer['CountryId']), 'CountryName');
				$customer['ProvinceName'] = $customer['ProvinceId'] > 0 ? $this->Mprovinces->getFieldValue(array('ProvinceId' => $customer['ProvinceId']), 'ProvinceName') : '';
				$customer['DistrictName'] = $customer['DistrictId'] > 0 ? $this->Mdistricts->getFieldValue(array('DistrictId' => $customer['DistrictId']), 'DistrictName') : '';
				$customer['WardName'] = $customer['WardId'] > 0 ? $this->Mwards->getWardName($customer['WardId']) : '';
				$customer['TotalOrders'] = $this->Morders->countRows(array('CustomerId' => $customerId, 'OrderStatusId >' => 0));
				echo json_encode(array('code' => 1, 'data' => $customer));
			}
			else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy Khách hàng"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function checkPhone(){
		$phoneNumber = $this->input->post('PhoneNumber');
		if(!empty($phoneNumber)){
			$this->load->model('Mcustomers');
			$result = $this->Mcustomers->getFieldValue(array('PhoneNumber' => $phoneNumber, 'StatusId' => STATUS_ACTIVED), 'CustomerId', 0);
			echo json_encode(array('code' => 1,'id'=> $result));
		}
	}

	public function searchByFilter($customerKindId = 0, $customerGroupId = 0){
        $user = $this->checkUserLogin(true);
        $data = array();
		$filterId = 0;
		$postData = array('CustomerKindId' => $customerKindId);
		$this->loadModel(array('Mcustomergroups', 'Mcustomers'));
		if($customerGroupId > 0){
			$filterId = $this->Mcustomergroups->getFieldValue(array('CustomerGroupId' => $customerGroupId), 'FilterId');
			if($filterId == 0) $postData['CustomerGroupId'] = $customerGroupId;
		}
		if($filterId == 0) $filterId = $this->input->post('filterId');
        $searchText = $this->input->post('searchText');
		$itemFilters = $this->input->post('itemFilters');
		if(!is_array($itemFilters)) $itemFilters = array();
		if ($filterId > 0 && empty($itemFilters)){
            $this->load->model('Mfilters');
            $data = $this->Mfilters->getInfo($filterId);
            $itemFilters = $data['itemFilters'];
        }
        $page = $this->input->post('page');
        if (!is_numeric($page) || $page < 1) $page = 1;
        $limit = $this->input->post('limit');
        if (!is_numeric($limit) || $limit < 1) $limit = DEFAULT_LIMIT;
		if(!empty($searchText) || $this->Mactions->checkAccessFromDb('customer/viewAll', $user['UserId'])){
			$data1 = $this->Mcustomers->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
		}
		elseif($this->Mactions->checkAccessFromDb('customer/'.$customerKindId, $user['UserId'])){
			$postData['CrUserId'] = $user['UserId'];
			$data1 = $this->Mcustomers->searchByFilter($searchText, $itemFilters, $limit, $page, $postData);
		}
		else $data1 = array();

        $data = array_merge($data, $data1);
        echo json_encode($data);
    }
}