<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actionlog extends MY_Controller{

	private $tableLink = array(
		1 => 'categories',//'Nhóm Sản phẩm', //chuyen muc sp
        2 => 'categories',//'Loại hàng hóa', //loai sp
        3 => 'products',//'Sản phẩm',
        4 => 'categories',//'Bài viết',
        5 => 'customers',//'Khách hàng',
        6 => 'orders',//'Đơn hàng',
        7 => 'storecirculations',//'Lưu chuyển kho',
        8 => 'imports',//'Nhập kho',
        9 => 'transports',//'Vận chuyển',
        10 => '',//'Tài chính',
        11 => 'customergroups',//'Nhóm Khách hàng',
        12 => 'positions',//'Khuyến mại',
        13 => 'productchilds',//'Sản phẩm con',
        14 => 'returngoods',//'Hoàn hàng về',
        15 => 'inventories', //'Tồn kho',
        16 => 'products', //"Combo Sản phẩm",
        17 => 'transactions', //'Phiếu thu',
        18 => 'transactions', //'Phiếu chi',
        19 => '', //'Lịch sử thay đổi tồn kho',
        20 => 'transactioninternals', //'Phiếu thu nội bộ',
        21 => 'transactioninternals', //'Phiếu chi nội bộ',
        22 => 'productprints', //'Phiếu in BarCode',
        23 => 'reminds', //'Lịch nhắc nhở',
        24 => 'products', //'Giá vốn',
        25 => 'storecirculationtransports', //'Vận chuyển LCK',
        26 => 'transactions', //'Sổ quỹ',
        27 => 'guarantees', //'Bảo hành',
        28 => 'familytransactions', //'Phiếu thu gia đình',
        29 => 'familytransactions', //'Phiếu chi gia đình',
        30 => 'customerconsults', //'Tư vấn lại',
        31 => 'transactionbusiness', //'Phiếu thu kinh doanh tam',
        32 => 'transactionbusiness', //'Phiếu chi kinh doanh tam',
        33 => 'products', //'Sản phẩm cũ',
        34 => '', //'Lịch sử xóa'
	);

    public $itemTypes = array(
        1 => 'Nhóm Sản phẩm', //chuyen muc sp
        2 => 'Loại hàng hóa', //loai sp
        3 => 'Sản phẩm',
        4 => 'Bài viết',
        5 => 'Khách hàng',
        6 => 'Đơn hàng',
        7 => 'Lưu chuyển kho',
        8 => 'Nhập kho',
        9 => 'Vận chuyển',
        10 => 'Tài chính',
        11 => 'Nhóm Khách hàng',
        12 => 'Khuyến mại',
        13 => 'Sản phẩm con',
        14 => 'Hoàn hàng về',
        15 => 'Tồn kho',
        16 => "Combo Sản phẩm",
        17 => 'Phiếu thu',
        18 => 'Phiếu chi',
        19 => 'Lịch sử thay đổi tồn kho',
        20 => 'Phiếu thu nội bộ',
        21 => 'Phiếu chi nội bộ',
        22 => 'Phiếu in BarCode',
        23 => 'Lịch nhắc nhở',
        24 => 'Giá vốn',
        25 => 'Vận chuyển LCK',
        26 => 'Sổ quỹ',
        27 => 'Bảo hành',
        28 => 'Phiếu thu gia đình',
        29 => 'Phiếu chi gia đình',
        30 => 'Tư vấn lại',
        31 => 'Phiếu thu kinh doanh tam',
        32 => 'Phiếu chi kinh doanh tam',
        33 => 'Sản phẩm cũ',
        34 => 'Lịch sử xóa',
        35 => 'Send SMS'
    );

    public function index(){
    	$user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Lịch sử thao tác',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/daterangepicker/daterangepicker.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/daterangepicker/moment.min.js', 'vendor/plugins/daterangepicker/vi.js', 'vendor/plugins/daterangepicker/knockout-3.4.2.js', 'vendor/plugins/daterangepicker/daterangepicker_vi.js', 'js/action_log.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'actionlog')) {
            $this->loadModel(array('Mactionlogs'));
            $postData = $this->arrayFromPost(array('SearchText', 'ItemTypeId'));
            $this->loadModel(array('Mactionlogs', 'Musers'));
            $postData = $this->arrayFromPost(array('SearchText', 'ItemTypeId','UserId'));
            $dateRangePicker = trim($this->input->post('DateRangePicker'));
            if(!empty($dateRangePicker)){
                $parts = explode('-', $dateRangePicker);
                if(count($parts) == 2){
                    $postData['BeginDate'] = ddMMyyyyToDate($parts[0]);
                    $postData['EndDate'] = ddMMyyyyToDate($parts[1], 'd/m/Y', 'Y-m-d 23:59:59');
                }
            }
            $data['listUsers'] = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED));
            $rowCount = $this->Mactionlogs->getCount($postData);
            $data['listActionLogs'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $listActions = $this->Mactionlogs->search($postData, $perPage, $page);
                $now = new DateTime(date('Y-m-d'));
                for ($i = 0; $i < count($listActions); $i++) {
		            $listActions[$i]['itemTypes'] = $listActions[$i]['ItemTypeId'] > 0 ? $this->itemTypes[$listActions[$i]['ItemTypeId']] : 'Không có';
		            $listActions[$i]['nameTable'] = $listActions[$i]['ItemTypeId'] > 0 ? substr($this->tableLink[$listActions[$i]['ItemTypeId']], 0, -1) : '';
		            $listActions[$i]['idTable'] = $listActions[$i]['ItemId'] > 0 ? $listActions[$i]['ItemId']: '';
		            $dayDiff = getDayDiff($listActions[$i]['CrDateTime'], $now);
		            $listActions[$i]['CrDateTime'] = ddMMyyyy($listActions[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
		            $listActions[$i]['DayDiff'] = $dayDiff;
		        }
                $data['listActionLogs'] = $listActions;
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('actionlog/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }
}