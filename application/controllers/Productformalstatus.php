<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productformalstatus extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Hình thức sản phẩm',
            array('scriptFooter' => array('js' => 'js/product_formal_status.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'productformalstatus')) {
            $this->load->model('Mproductformalstatus');
            $data['listProductFormalStatus'] = $this->Mproductformalstatus->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/product_formal_status', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductFormalStatusName'));
        if(!empty($postData['ProductFormalStatusName'])){
            $postData['StatusId'] = STATUS_ACTIVED;
            $productFormalStatusId = $this->input->post('ProductFormalStatusId');
            $this->load->model('Mproductformalstatus');
            $flag = $this->Mproductformalstatus->save($postData, $productFormalStatusId);
            if ($flag > 0) {
                $postData['ProductFormalStatusId'] = $flag;
                $postData['IsAdd'] = ($productFormalStatusId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật Hình thức sản phẩm thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $productFormalStatusId = $this->input->post('ProductFormalStatusId');
        if($productFormalStatusId > 0){
            $this->load->model('Mproductformalstatus');
            $flag = $this->Mproductformalstatus->changeStatus(0, $productFormalStatusId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Hình thức sản phẩm thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
