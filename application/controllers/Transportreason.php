<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transportreason extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Lý do miễn phí ship',
            array('scriptFooter' => array('js' => 'js/transport_reason.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'transportreason')) {
            $this->load->model('Mtransportreasons');
            $data['listTransportReasons'] = $this->Mtransportreasons->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/transport_reason', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('TransportReasonName'));
        if(!empty($postData['TransportReasonName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $transportReasonId = $this->input->post('TransportReasonId');
            $this->load->model('Mtransportreasons');
            $flag = $this->Mtransportreasons->save($postData, $transportReasonId);
            if ($flag > 0) {
                $postData['TransportReasonId'] = $flag;
                $postData['IsAdd'] = ($transportReasonId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật lý do miễn phí ship thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $transportReasonId = $this->input->post('TransportReasonId');
        if($transportReasonId > 0){
            $this->load->model('Mtransportreasons');
            $flag = $this->Mtransportreasons->changeStatus(0, $transportReasonId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa lý do miễn phí ship thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}
