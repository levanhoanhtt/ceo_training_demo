<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách vận chuyển',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.css','vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js','vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/search_item.js', 'js/transport_list.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'transport')) {
            $this->loadModel(array('Mfilters', 'Mtransporters', 'Mstores', 'Mtransporttypes','Mcancelreasons'));
            $data['listTransporters'] = $this->Mtransporters->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listTransportTypes'] = $this->Mtransporttypes->getBy(['StatusId' => STATUS_ACTIVED]);
            //$data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], $this->Mactions->checkAccess($data['listActions'], 'store/viewAll'));
            $data['listCancelReasons'] = $this->Mcancelreasons->getBy(['StatusId' => STATUS_ACTIVED]);
            $data['listFilters'] = $this->Mfilters->getList(9);
            $this->load->view('transport/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($transportId = 0){
        if($transportId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Danh sách vận chuyển',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/tagsinput/jquery.tagsinput.min.css',  'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/tagsinput/jquery.tagsinput.min.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'vendor/plugins/bxslider/jquery.bxslider.min.js', 'js/transport_update.js'))
                )
            );
            $this->loadModel(array('Mtransports', 'Morders', 'Mtransporttypes', 'Mtransporters', 'Mpendingstatus', 'Mstores', 'Mcustomeraddress', 'Mcustomers', 'Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards', 'Mcustomergroups' , 'Mactionlogs', 'Mtags', 'Morderproducts', 'Mproducts', 'Mproductchilds','Mtransportcomments', 'Mordercomments', 'Mparts', 'Mcancelreasons', 'Mtransportstatuslogs'));
            $transport = $this->Mtransports->get($transportId);
            if ($transport) {
                if ($this->Mactions->checkAccess($data['listActions'], 'transport')) {
                    $data['title'] .= ' / ' . $transport['TransportCode'];
                    $data['canEdit'] = true;// $transport['TransportStatusId'] == 1 || $transport['TransportStatusId'] == 9;
                    $data['transportId'] = $transportId;
                    $data['transport'] = $transport;
                    $data['order'] = $this->Morders->get($transport['OrderId'], true, '', 'OrderCode, TransportCost, TotalCost, CrDateTime');
                    $data['customerAddress'] = $this->Mcustomeraddress->getInfo($transport['CustomerAddressId'], $transport['CustomerId']);
                    $data['listCountries'] = $this->Mcountries->getList();
                    $data['listProvinces'] = $this->Mprovinces->getList();
                    $data['listDistricts'] = $this->Mdistricts->getList();
                    $data['tagNames'] = $this->Mtags->getTagNames($transportId, 9);
                    $data['listTags'] = $this->Mtags->getBy(array('ItemTypeId' => 9));
                    $data['listOrderProducts'] = $this->Morderproducts->getBy(array('OrderId' => $transport['OrderId']));
                    $data['listTransporters'] = $this->Mtransporters->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $data['listStores'] = $this->Mstores->getBy(array('ItemStatusId' => STATUS_ACTIVED));
                    $data['listPendingStatus'] = $this->Mpendingstatus->getList(9);
                    $data['listActionLogs'] = $this->Mactionlogs->getList($transportId, 9);
                    $data['listTransportComments'] = $this->Mtransportcomments->getList($transportId);
                    $data['listOrderComments'] = $this->Mordercomments->getListByOrderId($transport['OrderId'], 2);
                    $whereStatus = array('StatusId' => STATUS_ACTIVED);
                    $data['listTransportTypes'] = $this->Mtransporttypes->getBy($whereStatus);
                    $data['listCancelReasons'] = $this->Mcancelreasons->getBy($whereStatus);
                    $data['transportStatusLogIds'] = $this->Mtransportstatuslogs->getListStatusIds($transportId, $transport['TransportStatusId']);
                    $data['listUsers'] = $this->Musers->getListForSelect($user['UserId'], 'Chỉ mình tôi');
                    $data['listParts'] = $this->Mparts->getList(true);
                    $this->load->view('transport/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['canEdit'] = false;
                $data['transportId'] = 0;
                $data['txtError'] = "Không tìm thấý Vận chuyển";
                $this->load->view('transport/edit', $data);
            }
        }
        else redirect('transport');
    }

    public function printPdf($transportId = 0){
        if($transportId > 0) {
            $this->checkUserLogin();
            $this->loadModel(array('Mtransports', 'Mcustomers', 'Mcustomeraddress', 'Morderproducts', 'Mproducts', 'Mproductchilds','Mstores', 'Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards'));
            $transport = $this->Mtransports->get($transportId);
            if ($transport) {
                $customerAddress = $this->Mcustomeraddress->getInfo($transport['CustomerAddressId'], $transport['CustomerId']);
                if($customerAddress){
                    if($customerAddress['CountryId'] == 0 || $customerAddress['CountryId'] == 232){
                        $address = $customerAddress['Address'];
                        if($customerAddress['WardId'] > 0) $address .= ', xã '.$this->Mwards->getFieldValue(array('WardId' => $customerAddress['WardId']), 'WardName');
                        if($customerAddress['DistrictId'] > 0) $address .= ', huyện '.$this->Mdistricts->getFieldValue(array('DistrictId' => $customerAddress['DistrictId']), 'DistrictName');
                        if($customerAddress['ProvinceId'] > 0) $address .= ', tỉnh '.$this->Mprovinces->getFieldValue(array('ProvinceId' => $customerAddress['ProvinceId']), 'ProvinceName');
                        $customerAddress['Address'] = $address;
                    }
                    else $customerAddress['Address'] .= $customerAddress['ZipCode'].' '.$this->Mcountries->getFieldValue(array('CountryId' => $customerAddress['CountryId']), 'CountryName');
                }
                $this->load->library('ciqrcode');
                $params = array();
                $transportCode = $transport['TransportCode'];
                $params['data'] = $transportCode;
                $params['savename'] = "./assets/uploads/qr/{$transportCode}.png";
                $this->ciqrcode->generate($params);
                $data = array(
                    'configs' => $this->session->userdata('configs'),
                    'transport' => $transport,
                    'storeName' => $transport['StoreId'] > 0 ? $this->Mstores->getFieldValue(array('StoreId' => $transport['StoreId']), 'StoreName') : '',
                    'customerAddress' => $customerAddress,
                    'listOrderProducts' => $this->Morderproducts->getBy(array('OrderId' => $transport['OrderId'])),
                    'barcodeSrc' => $params['savename']
                );
                $this->load->view('transport/print_pdf', $data);
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
        
    }

    public function printPdfMultiple(){
        $this->checkUserLogin();
        $transportIds = json_decode(trim($this->input->post('TransportIds')), true);
        if(!empty($transportIds)) {
            $this->loadModel(array('Mtransports', 'Mcustomers', 'Mcustomeraddress', 'Morderproducts', 'Mproducts', 'Mproductchilds','Mstores', 'Mcountries', 'Mprovinces', 'Mdistricts', 'Mwards'));
            $listTransports = $this->Mtransports->getByIds($transportIds);
            if(!empty($listTransports)) {
                $this->load->library('ciqrcode');
                $printData = array();
                $storeNames = array(0 => '');
                $wardNames = array();
                $districtNames = array();
                $provinceNames = array();
                $customerAddress = array();
                foreach($listTransports as $transport) {
                    $params = array();
                    $transportCode = $transport['TransportCode'];
                    $params['data'] = $transportCode;
                    $params['savename'] = "./assets/uploads/qr/{$transportCode}.png";
                    $this->ciqrcode->generate($params);
                    if(!isset($storeNames[$transport['StoreId']])) $storeNames[$transport['StoreId']] = $this->Mstores->getFieldValue(array('StoreId' => $transport['StoreId']), 'StoreName');
                    if(!isset($customerAddress[$transport['CustomerId']])){
                        $customerAddress[$transport['CustomerId']] = $this->Mcustomeraddress->getInfo($transport['CustomerAddressId'], $transport['CustomerId']);
                        if($customerAddress[$transport['CustomerId']]){
                            if($customerAddress[$transport['CustomerId']]['CountryId'] == 0 || $customerAddress[$transport['CustomerId']]['CountryId'] == 232){
                                $address = $customerAddress[$transport['CustomerId']]['Address'];
                                if($customerAddress[$transport['CustomerId']]['WardId'] > 0){
                                    if(!isset($wardNames[$customerAddress[$transport['CustomerId']]['WardId']])) $wardNames[$customerAddress[$transport['CustomerId']]['WardId']] = $this->Mwards->getFieldValue(array('WardId' => $customerAddress[$transport['CustomerId']]['WardId']), 'WardName');
                                    $address .= ', xã '.$wardNames[$customerAddress[$transport['CustomerId']]['WardId']];
                                }
                                if($customerAddress[$transport['CustomerId']]['DistrictId'] > 0){
                                    if(!isset($districtNames[$customerAddress[$transport['CustomerId']]['DistrictId']])) $districtNames[$customerAddress[$transport['CustomerId']]['DistrictId']] = $this->Mdistricts->getFieldValue(array('DistrictId' => $customerAddress[$transport['CustomerId']]['DistrictId']), 'DistrictName');
                                    $address .= ', huyện '.$districtNames[$customerAddress[$transport['CustomerId']]['DistrictId']];
                                }
                                if($customerAddress[$transport['CustomerId']]['ProvinceId'] > 0){
                                    if(!isset($provinceNames[$customerAddress[$transport['CustomerId']]['ProvinceId']])) $provinceNames[$customerAddress[$transport['CustomerId']]['ProvinceId']] = $this->Mprovinces->getFieldValue(array('ProvinceId' => $customerAddress[$transport['CustomerId']]['ProvinceId']), 'ProvinceName');
                                    $address .= ', tỉnh '.$provinceNames[$customerAddress[$transport['CustomerId']]['ProvinceId']];
                                }
                                $customerAddress[$transport['CustomerId']]['Address'] = $address;
                            }
                            else{
                                if(!isset($countryNames[$customerAddress[$transport['CustomerId']]['CountryId']])) $countryNames[$customerAddress[$transport['CustomerId']]['CountryId']] = $this->Mcountries->getFieldValue(array('CountryId' => $customerAddress[$transport['CustomerId']]['CountryId']), 'CountryName');
                                $customerAddress[$transport['CustomerId']]['Address'] .= $customerAddress[$transport['CustomerId']]['ZipCode'].' '.$countryNames[$customerAddress[$transport['CustomerId']]['CountryId']];
                            }
                        }
                    }
                    $printData[] = array(
                        'transport' => $transport,
                        'storeName' => $storeNames[$transport['StoreId']],
                        'customerAddress' => $customerAddress[$transport['CustomerId']],
                        'listOrderProducts' => $this->Morderproducts->getBy(array('OrderId' => $transport['OrderId'])),
                        'barcodeSrc' => $params['savename']
                    );
                }
                $this->load->view('transport/print_pdf_multi', array('configs' => $this->session->userdata('configs'), 'printData' => $printData));
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }

    public function exportTransport(){
        $this->checkUserLogin();
        $transportIds = json_decode(trim($this->input->post('TransportIds')), true);
        if(!empty($transportIds)) {
            $this->load->model(array('Mtransports', 'Mcustomers', 'Mcustomeraddress', 'Mcountries', 'Mdistricts', 'Mprovinces', 'Mwards'));
            $listTransports = $this->Mtransports->getByIds($transportIds);
            if(!empty($listTransports)) {
                $this->load->library('excel');
                //$this->excel->setActiveSheetIndex(0);
                $fileUrl = FCPATH . 'assets/uploads/excels/export_transport.xls';
                $this->load->library('excel');
                $objReader = PHPExcel_IOFactory::createReader('Excel5');
                $objPHPExcel = $objReader->load($fileUrl);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $i = 2;
                $customerAddress = array();
                $wardNames = array();
                $districtNames = array();
                $provinceNames = array();
                foreach($listTransports as $transport) {
                    if(!isset($customerAddress[$transport['CustomerId']])){
                        $customerAddress[$transport['CustomerId']] = $this->Mcustomeraddress->getInfo($transport['CustomerAddressId'], $transport['CustomerId']);
                        if($customerAddress[$transport['CustomerId']]){
                            if($customerAddress[$transport['CustomerId']]['CountryId'] == 0 || $customerAddress[$transport['CustomerId']]['CountryId'] == 232){
                                $address = $customerAddress[$transport['CustomerId']]['Address'];
                                if($customerAddress[$transport['CustomerId']]['WardId'] > 0){
                                    if(!isset($wardNames[$customerAddress[$transport['CustomerId']]['WardId']])) $wardNames[$customerAddress[$transport['CustomerId']]['WardId']] = $this->Mwards->getFieldValue(array('WardId' => $customerAddress[$transport['CustomerId']]['WardId']), 'WardName');
                                    $address .= ', xã '.$wardNames[$customerAddress[$transport['CustomerId']]['WardId']];
                                }
                                if($customerAddress[$transport['CustomerId']]['DistrictId'] > 0){
                                    if(!isset($districtNames[$customerAddress[$transport['CustomerId']]['DistrictId']])) $districtNames[$customerAddress[$transport['CustomerId']]['DistrictId']] = $this->Mdistricts->getFieldValue(array('DistrictId' => $customerAddress[$transport['CustomerId']]['DistrictId']), 'DistrictName');
                                    $address .= ', huyện '.$districtNames[$customerAddress[$transport['CustomerId']]['DistrictId']];
                                }
                                if($customerAddress[$transport['CustomerId']]['ProvinceId'] > 0){
                                    if(!isset($provinceNames[$customerAddress[$transport['CustomerId']]['ProvinceId']])) $provinceNames[$customerAddress[$transport['CustomerId']]['ProvinceId']] = $this->Mprovinces->getFieldValue(array('ProvinceId' => $customerAddress[$transport['CustomerId']]['ProvinceId']), 'ProvinceName');
                                    $address .= ', tỉnh '.$provinceNames[$customerAddress[$transport['CustomerId']]['ProvinceId']];
                                }
                                $customerAddress[$transport['CustomerId']]['Address'] = $address;
                            }
                            else{
                                if(!isset($countryNames[$customerAddress[$transport['CustomerId']]['CountryId']])) $countryNames[$customerAddress[$transport['CustomerId']]['CountryId']] = $this->Mcountries->getFieldValue(array('CountryId' => $customerAddress[$transport['CustomerId']]['CountryId']), 'CountryName');
                                $customerAddress[$transport['CustomerId']]['Address'] .= $customerAddress[$transport['CustomerId']]['ZipCode'].' '.$countryNames[$customerAddress[$transport['CustomerId']]['CountryId']];
                            }
                        }
                    }
                    $sheet->setCellValue('A' . $i, $i-1);
                    if(isset($customerAddress[$transport['CustomerId']])) {
                        $sheet->setCellValue('B' . $i, $customerAddress[$transport['CustomerId']]['CustomerName']);
                        $sheet->setCellValueExplicit('C' . $i, $customerAddress[$transport['CustomerId']]['PhoneNumber'], PHPExcel_Cell_DataType::TYPE_STRING);
                        $sheet->setCellValue('D' . $i, $customerAddress[$transport['CustomerId']]['Address']);
                    }
                    else{
                        $sheet->setCellValue('B' . $i, '');
                        $sheet->setCellValue('C' . $i, '');
                        $sheet->setCellValue('D' . $i, '');
                    }
                    $sheet->setCellValue('E' . $i, $transport['CODCost']);
                    $i++;
                }
                $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                $filename = "exportTransport_".date('Y-m-d').".xls";
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                $objPHPExcel->disconnectWorksheets();
                unset($objPHPExcel);
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }
}