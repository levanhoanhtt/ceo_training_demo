<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transportcaretype extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Chế độ chăm sóc đặc biệt',
			array('scriptFooter' => array('js' => 'js/transport_care_type.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'transportcaretype')) {
			$this->load->model(array('Mtransportcaretypes', 'Mtransporttypes'));
			$data['listTransportTypes'] = $this->Mtransporttypes->getBy(array('StatusId' => STATUS_ACTIVED));
			$data['listTransportCareTypes'] = $this->Mtransportcaretypes->getBy(array('StatusId' => STATUS_ACTIVED), false, 'TransportTypeId');
			$this->load->view('setting/transport_care_type', $data);
		}
		else $this->load->view('user/permission', $data);
	}


	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('TransportCareTypeName','TransportTypeId'));
		if(!empty($postData['TransportCareTypeName']) && $postData['TransportTypeId'] > 0) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$transportCareTypeId = $this->input->post('TransportCareTypeId');
			$this->load->model(array('Mtransportcaretypes', 'Mtransporttypes'));
			$flag = $this->Mtransportcaretypes->save($postData, $transportCareTypeId);
			if ($flag > 0) {
				$postData['TransportCareTypeId'] = $flag;
				$postData['TransportTypeName'] = $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $postData['TransportTypeId']), 'TransportTypeName');
				$postData['IsAdd'] = ($transportCareTypeId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Chế độ chăm sóc đặc biệt thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$transportCareTypeId = $this->input->post('TransportCareTypeId');
		if($transportCareTypeId > 0){
			$this->load->model('Mtransportcaretypes');
			$flag = $this->Mtransportcaretypes->changeStatus(0, $transportCareTypeId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Chế độ chăm sóc đặc biệt thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}