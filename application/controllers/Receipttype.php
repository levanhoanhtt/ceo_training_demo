<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receipttype extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Phương thức nhận hàng',
            array('scriptFooter' => array('js' => 'js/receipt_type.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'receipttype')) {
            $this->load->model('Mreceipttypes');
            $data['listReceiptTypes'] = $this->Mreceipttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/receipt_type', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ReceiptTypeName'));
        if(!empty($postData['ReceiptTypeName'])) {
            $postData['StatusId'] = STATUS_ACTIVED;
            $receiptTypeId = $this->input->post('ReceiptTypeId');
            $this->load->model('Mreceipttypes');
            $flag = $this->Mreceipttypes->save($postData, $receiptTypeId);
            if ($flag > 0) {
                $postData['ReceiptTypeId'] = $flag;
                $postData['IsAdd'] = ($receiptTypeId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật phương thức nhận hàng thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $receiptTypeId = $this->input->post('ReceiptTypeId');
        if($receiptTypeId > 0){
            $this->load->model('Mreceipttypes');
            $flag = $this->Mreceipttypes->changeStatus(0, $receiptTypeId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa phương thức nhận hàng thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}