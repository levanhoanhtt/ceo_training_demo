<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productaccessorystatus extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Danh sách Tình trạng phụ kiện',
            array('scriptFooter' => array('js' => 'js/product_accessory_status.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'productaccessorystatus')) {
            $this->load->model('Mproductaccessorystatus');
            $data['listProductAccessoryStatus'] = $this->Mproductaccessorystatus->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/product_accessory_status', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ProductAccessoryStatusName'));
        if(!empty($postData['ProductAccessoryStatusName'])){
            $postData['StatusId'] = STATUS_ACTIVED;
            $productAccessoryStatusId = $this->input->post('ProductAccessoryStatusId');
            $this->load->model('Mproductaccessorystatus');
            $flag = $this->Mproductaccessorystatus->save($postData, $productAccessoryStatusId);
            if ($flag > 0) {
                $postData['ProductAccessoryStatusId'] = $flag;
                $postData['IsAdd'] = ($productAccessoryStatusId > 0) ? 0 : 1;
                echo json_encode(array('code' => 1, 'message' => "Cập nhật Tình trạng phụ kiện thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function delete(){
        $this->checkUserLogin(true);
        $productAccessoryStatusId = $this->input->post('ProductAccessoryStatusId');
        if($productAccessoryStatusId > 0){
            $this->load->model('Mproductaccessorystatus');
            $flag = $this->Mproductaccessorystatus->changeStatus(0, $productAccessoryStatusId);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Tình trạng phụ kiện thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}