<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fund extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách Sổ quỹ tiền mặt',
			array('scriptFooter' => array('js' => 'js/fund.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'fund')) {
			$this->loadModel(array('Mstores', 'Mfunds', 'Mstorefunds'));
			$data['listUsers'] = $this->Musers->getListForSelect();
			$data['listStores'] = $this->Mstores->getByUserId($user['UserId'], true);
			$data['listFunds'] = $this->Mfunds->getBy(array('ItemStatusId >' => 0));
			$this->load->view('fund/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function add(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thêm Sổ quỹ tiền mặt',
            array('scriptFooter' => array('js' => 'js/fund_update.js'))
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'fund')) {
            $this->load->model('Mstores');
            $data['listUsers'] = $this->Musers->getListForSelect();
            $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], true);
            $this->load->view('fund/add', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function edit($fundId = 0){
        if($fundId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Cập nhật Sổ quỹ tiền mặt',
                array('scriptFooter' => array('js' => 'js/fund_update.js'))
            );
            $this->loadModel(array('Mstores', 'Mfunds', 'Mstorefunds'));
            $fund = $this->Mfunds->get($fundId);
            if ($fund) {
                if ($this->Mactions->checkAccess($data['listActions'], 'fund')) {
                    $data['fundId'] = $fundId;
                    $data['fund'] = $fund;
                    $data['listUsers'] = $this->Musers->getListForSelect();
                    $data['listStores'] = $this->Mstores->getByUserId($user['UserId'], true);
                    $data['listStoreIds'] = $this->Mstorefunds->getListFieldValue(array('FundId' => $fundId), 'StoreId');
                    $this->load->view('fund/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['fundId'] = 0;
                $data['txtError'] = "Không tìm thấy Sổ quỹ tiền mặt";
                $this->load->view('fund/edit', $data);
            }
        }
        else redirect('fund');
    }

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('FundCode', 'FundName', 'ItemStatusId', 'TreasureId'));
		if(!empty($postData['FundCode']) && !empty($postData['FundName']) && $postData['ItemStatusId'] > 0 && $postData['TreasureId'] > 0) {
            $crDateTime = getCurentDateTime();
			$fundId = $this->input->post('FundId');
			if($fundId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
                $postData['Balance'] = 0;
            }
            $storeIds = $this->input->post('StoreIds');
			$this->load->model('Mfunds');
			if(!is_array($storeIds))  $storeIds = array();
			$flag = $this->Mfunds->update($postData, $fundId, $storeIds);
			if ($flag > 0) {
				$postData['FundId'] = $flag;
				$postData['IsAdd'] = ($fundId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Sổ quỹ tiền mặt thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$fundId = $this->input->post('FundId');
		$itemStatusId = $this->input->post('ItemStatusId');
		if($fundId > 0 && $itemStatusId >= 0){
			$this->load->model('Mfunds');
			$flag = $this->Mfunds->changeStatus($itemStatusId, $fundId, 'ItemStatusId', $user['UserId']);
			if($flag){
				$statusName = '';
				if($itemStatusId >  0) $statusName = '<span class="'.$this->Mconstants->labelCss[$itemStatusId].'">'.$this->Mconstants->itemStatus[$itemStatusId].'</span>';
				echo json_encode(array('code' => 1, 'message' => $itemStatusId > 0 ? "Cập nhật trạng thái thành công" : "Xóa Sổ quỹ tiền mặt thành công", 'data' => array('StatusName' => $statusName)));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
