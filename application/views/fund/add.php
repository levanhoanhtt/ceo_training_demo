<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Cập nhật</button></li>
                    <li><a href="<?php echo base_url('fund'); ?>" id="fundListUrl" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('fund/update', array('id' => 'fundForm')); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Mã sổ quỹ <span class="required">*</span></label>
                            <input type="text" name="FundCode" class="form-control hmdrequired" value="" data-field="Mã sổ quỹ">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tên sổ quỹ <span class="required">*</span></label>
                            <input type="text" name="FundName" class="form-control hmdrequired" value="" data-field="Tên sổ quỹ">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tình trạng</label>
                            <?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Thủ quỹ</label>
                            <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'TreasureId', 0, true, '--Chọn--', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="control-label">Cơ sở áp dụng</label>
                            <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreIds[]', 0, false, '', ' select2', ' multiple'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <input type="text" name="FundId" id="fundId" hidden="hidden" value="0">
                    <input class="btn btn-primary submit" type="submit" name="submit" value="Cập nhật">
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>