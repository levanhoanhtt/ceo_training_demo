<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Lưu</button></li>
                    <li><a href="<?php echo base_url('customerconsult'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('customerconsult/update', array('id' => 'customerConsultForm')); ?>
                <div class="row">
                    <div class="col-sm-8 no-padding">
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Tư vấn lại</h3>
                                <div class="box-tools pull-right" style="line-height: 35px;">
                                    <?php echo $objName; ?>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề nhắc nhở <span class="required">*</span></label>
                                    <input class="form-control hmdrequired" type="text" value="" id="consultTitle" data-field="Tiêu đề nhắc nhở">
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label class="light-blue" style="line-height: 30px;margin-bottom: 15px;">
                                                Cập nhật tình hình hiện tại
                                                <div class="radio-group" style="display: inline;margin-left: 10px;">
                                                    <span class="item"><input type="radio" name="CommentTypeId" class="iCheck iCheckCommentType" value="1" checked> Gọi điện</span>
                                                    <span class="item"><input type="radio" name="CommentTypeId" class="iCheck iCheckCommentType" value="2"> Facebook</span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" id="divCustomerFacebook" style="display: none;">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="consultFacebook" value="" placeholder="Mã FB Hara">
                                            <span class="input-group-addon" id="spanEditFb" style="cursor: pointer;"><i class="fa fa-pencil"></i></span>
                                            <span class="input-group-addon" id="spanCopyFb" style="cursor: pointer;"><i class="fa fa-copy"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="box-transprt clearfix mb10">
                                        <button type="button" class="btn-updaten save" id="btnInsertComment">
                                            Lưu
                                        </button>
                                        <input type="text" class="add-text " id="comment" data-field="Cập nhật tình hình hiện tại">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Thời điểm cần xử lý <span class="required">*</span></label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control hmdrequired" id="consultDate" value="" data-field="Thời điểm cần xử lý">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label normal">Tình trạng xử lý</label>
                                            <?php $this->Mconstants->selectConstants('remindStatus', 'RemindStatusId'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="divCommentStatus" style="display: none;">
                                    <label class="control-label normal lbCommentStatus">Ghi chú</label>
                                    <input type="text" class="form-control" disabled value="">
                                </div>
                            </div>
                        </div>
                        <div class="box box-default padding15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Sản phẩm được tư vấn</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th>Sản phẩm</th>
                                            <th class="text-center" style="width: 150px;">Giá</th>
                                            <th style="width: 85px;">Số lượng</th>
                                            <th>Ghi chú</th>
                                            <th style="width: 5px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyProduct"></tbody>
                                    </table>
                                </div>
                                <div class="border-top-title-main">
                                    <div class="clearfix">
                                        <div class="box-search-advance product">
                                            <div>
                                                <input type="text" class="form-control textbox-advancesearch" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm">
                                            </div>
                                            <div class="panel panel-default" id="panelProduct">
                                                <div class="panel-body" style="width:100%;">
                                                    <div class="list-search-data">
                                                        <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                        <div>
                                                            <div class="form-group pull-right" style="width: 300px;">
                                                                <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="table-responsive no-padding divTable">
                                                            <table class="table table-hover table-bordered">
                                                                <thead class="theadNormal">
                                                                <tr>
                                                                    <th style="width: 100px;">Ảnh</th>
                                                                    <th>Sản phẩm</th>
                                                                    <th style="width: 100px;">Mã sản phẩm</th>
                                                                    <th style="width: 100px;">Giá bán</th>
                                                                    <th style="width: 100px;">Bảo hành</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tbodyProductSearch"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
                                                        <input type="text" hidden="hidden" id="pageIdProduct" value="1">
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-default classify" style="padding: 10px;">
                            <label class="light-blue">Lịch sử xử lý</label>
                            <div class="listComment" id="listComment"></div>
                        </div>
                        <div class="box box-default boxChooseCustomer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnAddCustomer"><i class="fa fa-plus"></i> Thêm</button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="box-search-advance customer">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng">
                                    </div>
                                    <div class="panel panel-default" id="panelCustomer">
                                        <div class="panel-body">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <ul id="ulListCustomers"></ul>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name"></h4>
                                        <div class="phones i-phone"></div>
                                        <div class="phones n-phone" style="display: none;"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span>₫</span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-facebook" aria-hidden="true"></i><span id="spanFacebook"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body">
                                <!-- <div>
                                    <h4 class="mgbt-20 light-blue">Thông tin khách hàng</h4>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- <div class="item">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            <span class="i-name"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span class="i-phone"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span class="i-email"></span>
                                        </div> -->
                                        <div class="item i-address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span class="i-ward"></span>
                                            <span class="br-line i-district"></span>
                                            <span class="br-line i-province"></span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-id-card" aria-hidden="true"></i>
                                            <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">Việt Nam</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Lưu"></li>
                    <li><a href="<?php echo base_url('customerconsult'); ?>" class="btn btn-default">Đóng</a></li>
                    <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                    <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                    <input type="text" hidden="hidden" id="ediCustomerConsultUrl" value="<?php echo base_url('customerconsult/edit'); ?>">
                    <input type="text" hidden="hidden" id="updateCustomerFacebookUrl" value="<?php echo base_url('customerconsult/updateCustomerFacebook'); ?>">
                    <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                    <input type="text" hidden="hidden" id="userId" value="<?php echo $isUser == 1 ? $user['UserId'] : 0; ?>">
                    <input type="text" hidden="hidden" id="partId" value="<?php echo $isUser == 0 ? PART_SALE_ID : 0; ?>">
                    <input type="text" hidden="hidden" id="customerConsultId" value="0">
                    <input type="text" hidden="hidden" id="customerId" value="0">
                    <input type="text" hidden="hidden" id="ctvId" value="0">
                    <input type="text" hidden="hidden" id="orderId" value="0">
                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                </ul>
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <div class="modal fade" id="modalConfirmStatus" tabindex="-1" role="dialog" aria-labelledby="modalConfirmStatus">
                    <div class="modal-dialog">
                        <div class="modal-content"  style="width: 500px;margin: 0 auto">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Xác nhận trạng thái</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center text-info divOrderCode" style="display: none;">Tư vấn lại thành công chỉ khi khách được tạo đơn hàng</p>
                                <div class="form-group">
                                    <label class="control-label normal lbCommentStatus">Ghi chú</label>
                                    <input type="text" id="commentStatus" class="form-control" value="">
                                </div>
                                <div class="form-group divOrderCode" style="display: none;">
                                    <label class="control-label normal">Đơn hàng</label>
                                    <input type="text" id="orderCode" class="form-control" value="">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input class="btn btn-primary submit" type="button" value="Hoàn thành">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>