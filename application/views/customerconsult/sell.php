<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('customerconsult/sell'); ?>
                        <div class="row">
                            <div class="col-sm-4">
		                        <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserId', set_value('UserId'), true, '--Nhân viên--', ' select2'); ?>
		                    </div>
		                    <div class="col-sm-4">
		                        <div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" name="DateRange" value="<?php echo set_value('DateRange') ? set_value('DateRange') : (date('01/m/Y').' - '.date('d/m/Y')); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
		                    </div>
		                    <div class="col-sm-4">
		                        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
		                        <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
		                    </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                <div class="">
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Đơn hàng</th>
                                <th>Ngày chốt</th>
                                <th>Khách hàng</th>
                                <th class="text-right">Tổng tiền</th>
                                <th>Người tạo</th>
                                <th>Danh sách đăng ký chốt</th>
                                <th>Mã TVL</th>
                                <th>Thực chốt</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCustomerConsultSell">
                            	<?php foreach($listConsultSells as $cs) {
                                    $listConsultUsers = $this->Mcustomerconsultusers->getBy(array('ConsultSellId' => $cs['ConsultSellId']));
                                    $crUserId = 0;
                                    foreach($listConsultUsers as $cu){
                                        if($cu['IsCrOrder'] == 2){
                                            $crUserId = $cu['UserId'];
                                            break;
                                        }
                                    } ?>
                            		<tr id="trSell_<?php echo $cs['ConsultSellId']; ?>">
                            			<td class="tdOrderCode"><a href="<?php echo base_url('order/edit/'.$cs['OrderId']); ?>"><?php echo $cs['OrderCode']; ?></a></td>
                            			<td class="tdCrDateTime"><?php echo ddMMyyyy($cs['CrDateTime']); ?></td>
                            			<td class="tdCustomerName"><a href="<?php echo base_url('customer/edit/'.$cs['CustomerId']); ?>"><?php echo $cs['FullName']; ?></a></td>
                            			<td class="text-right tdSumCost"><?php echo priceFormat($cs['SumCost']); ?> đ</td>
                            			<td class="tdCrUserName"><?php if($crUserId > 0) echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $crUserId, 'FullName'); ?></td>
                            			<td>
                                            <?php $fullNames = '';
                                            foreach($listConsultUsers as $cu){
                                                //if($cu['IsCrOrder'] == 1)
                                                    $fullNames .= $this->Mconstants->getObjectValue($listUsers, 'UserId', $cu['UserId'], 'FullName').' | ';
                                            }
                                            if(!empty($fullNames)) echo substr($fullNames, 0, strlen($fullNames) - 3); ?>
                                        </td>
                            			<td><?php if($cs['CustomerConsultId'] > 0) echo '<a href="'.base_url('customerconsult/edit/'.$cs['CustomerConsultId']).'">TVL-'.($cs['CustomerConsultId'] + 10000).'</a>'; ?></td>
                            			<td class="tdPercent">
                                            <?php $fullNames = '';
                                            foreach($listConsultUsers as $cu){
                                                if($cu['Percent'] > 0) $fullNames .= $this->Mconstants->getObjectValue($listUsers, 'UserId', $cu['UserId'], 'FullName').': '.$cu['Percent'].'% | ';
                                            }
                                            if(!empty($fullNames)) echo substr($fullNames, 0, strlen($fullNames) - 3); ?>
                                        </td>
                                        <td class="actions text-center">
                                            <?php if($cs['StatusId'] == 1){ ?>
                                                <a href="javascript:void(0)" class="link_edit"><i class="fa fa-pencil"></i></a>
                                                <span style="display: none;" class="spanJson"><?php echo json_encode($listConsultUsers); ?></span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                            	<?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="modalSell" tabindex="-1" role="dialog" aria-labelledby="modalSell">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><i class="fa fa-money"></i> Thỏa thuận doanh số</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="no-padding">
                                        <label class="text-info"><i class="fa fa-check-circle"></i> Thông tin đơn hàng</label>
                                        <div class="table-responsive divTable">
                                            <table class="table table-hover">
                                                <thead class="theadNormal">
                                                <tr>
                                                    <th>Đơn hàng</th>
                                                    <th>Ngày chốt</th>
                                                    <th>Khách hàng</th>
                                                    <th class="text-right">Tổng tiền</th>
                                                    <th>Người tạo</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyOrderInfo"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="no-padding">
                                        <label class="text-info"><i class="fa fa-check-circle"></i> Đăng ký chốt</label>
                                        <div class="table-responsive divTable">
                                            <table class="table table-hover">
                                                <thead class="theadNormal">
                                                <tr>
                                                    <th>Nhân viên</th>
                                                    <th>Phần trăm</th>
                                                    <th>Ghi chú</th>
                                                    <th class="text-center" style="width: 50px;"></th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyUserPercent"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    <input type="text" hidden="hidden" id="updateCustomerConsultUserUrl" value="<?php echo base_url('customerconsult/updateCustomerConsultUser'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
