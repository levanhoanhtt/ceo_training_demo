<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><a href="<?php echo base_url('customerconsult/add'); ?>" class="btn btn-primary">Thêm mới</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả TVL</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="remind_status">Trạng thái</option>
                                        <option value="consult_date">Thời gian cần xử lý</option>
                                        <option value="consult_date_period">Thời điểm cần xử lý</option>
                                        <option value="consult_time">Số lần tư vấn lại</option>
                                        <option value="order_channel">Kênh</option>
                                        <option value="customer_consult_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 consult_time none-display">
                                    <select class="value_operator form-control">
                                        <option value="=">bằng</option>
                                        <option value="!=">khác</option>
                                        <option value="<">nhỏ hơn</option>
                                        <option value=">">lớn hơn</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 remind_status order_channel">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control remind_status block-display">
                                        <?php foreach($this->Mconstants->remindStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control order_channel none-display">
                                        <?php foreach($this->Mconstants->orderChannels as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control consult_date_period none-display">
                                        <option value="today">trong hôm nay</option>
                                        <option value="yesterday">trong hôm qua</option>
                                        <option value="current_week">trong tuần hiện tại</option>
                                        <option value="current_month">trong tháng hiện tại</option>
                                    </select>
                                    <select class="form-control consult_date none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control consult_time input-number none-display" type="text">
                                    <input class="form-control customer_consult_tag none-display" type="text">
                                    <input class="form-control datepicker consult_date none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker consult_date none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <button id="btn-filter" data-href="<?php echo base_url('customerconsult/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                        </select>
                    </div>
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Tiêu đề tư vấn</th>
                                <th class="text-center">Số lần TVL</th>
                                <th>Thời điểm cần xử lý</th>
                                <th>Ngày tạo</th>
                                <th>Tên khách hàng</th>
                                <th>SĐT</th>
                                <th>Người tạo</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="text-center">Kênh</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCustomerConsult"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="30">
                    <input type="hidden" value="<?php echo base_url('customerconsult/edit'); ?>" id="urlEditCustomerConsult">
                    <input type="hidden" value="<?php echo base_url('customer/edit'); ?>" id="urlEditCustomer">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>