<div class="modal fade" id="modalRemind" tabindex="-1" role="dialog" aria-labelledby="modalRemind">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-clock-o"></i> Thêm nhắc nhở</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label normal">Tiêu đề nhắc nhở <span class="required">*</span></label>
                    <input class="form-control hmdrequired" type="text" value="" id="remindTitle" data-field="Tiêu đề nhắc nhở">
                </div>
                <div class="form-group">
                    <label class="control-label normal">Nội dung nhắc nhở </label>
                    <input class="form-control" type="text" value="" id="remindComment" placeholder="Thêm nội dung ghi chú">
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label normal">Thời điểm cần xử lý <span class="required">*</span></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datetimepicker hmdrequired" id="remindDate1" value="" data-field="Thời điểm cần xử lý">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label normal">Nhắc nhở cho nhân viên</label>
                            <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserId', $user['UserId'], true, '--Chọn--', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label normal">Hoặc phòng ban</label>
                            <?php echo $this->Mparts->selectHtml(0, 'PartId', $listParts); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" id="btnAddRemind">Hoàn thành</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <input type="text" hidden="hidden" id="insertRemindUrl" value="<?php echo base_url('remind/update'); ?>">
            </div>
        </div>
    </div>
</div>