<?php $this->load->view('site/includes/header'); ?>
    <div id="mango-product">
        <div class="breadcrumb-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <ol class="breadcrumb breadcrumb-arrow hidden-sm hidden-xs">
                            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                            <li><a href="<?php echo base_url('san-pham.html'); ?>">Danh mục</a></li>
                            <li class="active"><span> <?php echo $product['ProductName']; ?></span></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="box_collection">
            <div class="container">
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-5 col-md-4 col-lg-5 information-entry">
                            <div class="product-preview-box">
                                <?php $productImage = PRODUCT_PATH.(empty($product['ProductImage']) ? NO_IMAGE : $product['ProductImage']); ?>
                                <div class="swiper-container product-preview-swiper" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div>
                                                <a class="fancybox" rel="gallery1" href="<?php echo $productImage; ?>" title="<?php echo $product['ProductName']; ?>">
                                                    <img src="<?php echo $productImage; ?>" alt="<?php echo $product['ProductName']; ?>"/>
                                                </a>
                                            </div>
                                        </div>
                                        <?php foreach ($listImages as $img) { ?>
                                            <div class="swiper-slide">
                                                <div>
                                                    <a class="fancybox" rel="gallery1" href="<?php echo PRODUCT_PATH.$img; ?>" title="<?php echo $product['ProductName']; ?>">
                                                        <img src="<?php echo PRODUCT_PATH . $img; ?>" alt="<?php echo $product['ProductName']; ?>"/>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                                        <input type="text" hidden="hidden" id="productChildChosen" value="0">
                                    </div>
                                    <div class="pagination"></div>
                                </div>
                                <div class="swiper-hidden-edges">
                                    <div class="swiper-container product-thumbnails-swiper" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="3" data-int-slides="3" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide selected">
                                                <div class="paddings-container">
                                                    <img alt="<?php echo $product['ProductName']; ?>" data-image="<?php echo $productImage; ?>" src="<?php echo $productImage; ?>">
                                                </div>
                                            </div>
                                            <?php foreach ($listImages as $img) { ?>
                                                <div class="swiper-slide">
                                                    <div class="paddings-container">
                                                        <img alt="<?php echo $product['ProductName']; ?>" data-image="<?php echo base_url(PRODUCT_PATH.$img); ?>" src="<?php echo PRODUCT_PATH . $img; ?>">
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="pagination"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="loading-img text-center">
                                <img src="<?php echo base_url('assets/front/v1/images/fancybox_loading.gif') ?>">
                            </div>
                        </div>
                        <script type="text/javascript">
                            var minPrice = <?= $min_price ?>;
                            var variantProduct = <?php echo json_encode($listProductChilds); ?> ;
                        </script>

                        <div class="col-sm-7 col-md-4 col-lg-4 information-entry">
                            <div class="product-detail-box">
                                <!--<h3 class="sku-product">SKU: <b><?php //echo $product['Sku']; ?></b></h3>-->
                                <h1 class="product-title"><?php echo $product['ProductName']; ?></h1>
                                <hr>
                                <span>Tình trạng: <?php echo $product['ProductWebStatusId'] == 2 ? 'Còn hàng' : '<span style="color: #D61C1F;">Đã dừng kinh doanh</span>'; ?></span>
                                <?php if($product['ProductWebStatusId'] != 2 && !empty($product['ProductWebStatusComment'])) echo '<p style="margin-top: 5px;color: #aaa;">Lý do: '.$product['ProductWebStatusComment'].'</p>'; ?>
                                <hr>
                               <?php $i = 0;
                                if (!empty($variants)) {  
                                    foreach ($variants as $variantId => $variantValues) {
                                        $i++;
                                        $variantValueDiffs = array();
                                        foreach ($variantValues as $variantValue) {
                                            if (!in_array($variantValue, $variantValueDiffs)) $variantValueDiffs[] = $variantValue;
                                        } ?>
                                        <div class="colors">
                                            <span>
                                                <?php foreach ($listVariants as $v) {  ?>
                                                    <?php if ($v['VariantId'] == $variantId) {
                                                        echo $v['VariantName'];
                                                        break;
                                                    } ?>
                                                <?php } ?>
                                            </span>
                                        </div>
                                        <div class="list-size attribute_chosen">
                                            <?php foreach ($variantValueDiffs as $v):  ?>
                                                <span class="check_active"><?= $v; ?></span>
                                            <?php endforeach; ?>
                                        </div>
                                        <hr>
                                    <?php }
                                } ?>
                                <div class="price detail-info-entry">
                                    <?php if ($product['IsContactPrice'] == 2) { ?>
                                        <div class="current">Giá liên hệ</div>
                                    <?php } else { ?>
                                        <div class="current">
                                            <?php if(isset($min_price)): ?>
                                                <?php echo $min_price > 0 ? priceFormat($min_price).'₫' : 'Giá liên hệ'; ?>
                                            <?php else: ?>
                                                <?php echo $product['Price'] > 0 ? priceFormat( $product['Price']).'₫' : 'Giá liên hệ'; ?>
                                            <?php endif; ?>
                                            
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="product-description detail-info-entry frame_des">
                                    <p class="pkch-title"><strong>PHỤ KIỆN CHÍNH HÃNG</strong></p>
                                    <p class="pkch-title"><strong>RICKY MIỀN BẮC :</strong></p>
                                    <p>Add: 203-Bờ Sông Mới-Hoàng Mai-Hà Nội</p>
                                    <p class="pkch-title"><strong>RICKY MIỀN NAM :</strong></p>
                                    <p>Add: 704-Sông Mây-Trảng Bom-Đồng Nai</p>
                                    <p>Hotline: 04.22121.999 - 097.147.7007</p>
                                </div>
                                <div class="product-description detail-info-entry frame_des">
                                    <p class="pkch-title"><strong>CAM KẾT :</strong></p>
                                    <p>NÓI <span class="text-danger"><b>KHÔNG</b></span> VỚI <b>HÀNG FAKE</b>, XÁCH TAY,
                                        <b>BẢO HÀNH "MIỆNG"</b>, BỞI <b>CÁ NHÂN</b>, <b>LÁI BUÔN</b>.
                                    </p>
                                </div>
                                <div class="quantity-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Số lượng</div>
                                    <div class="entry number-minus">&nbsp;</div>
                                    <div class="entry number">1</div>
                                    <div class="entry number-plus">&nbsp;</div>
                                </div>
                                <div class="detail-info-entry">
                                    <a href="javascript:void(0)" class="button detail-btn fix_btn_cart btn-addCart Addcart detail-p" data-id="<?php echo $product['ProductId']; ?>">
                                        <span class="muangay">MUA NGAY</span> <br/>(Nhận hàng tại nhà)</a>
                                    <a href="javascript:void(0)" class="button btn-consult detail-btn" data-id="<?php echo $product['ProductId']; ?>">
                                            <span class="muangay">ĐỂ LẠI SĐT</span> <br/>(Ricky sẽ gọi tư vấn)</a>
                                    <div class="clear"></div>
                                </div>
                                <div class="product_socaial">
                                    <div class="box_social">
                                        <div class="fb">
                                            <div class="fb-like" data-href="<?php echo $productUrl; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                        </div>
                                        <div class="gg">
                                            <div class="g-plus" data-action="share" data-annotation="none" data-href="<?php echo $productUrl; ?>"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear visible-xs visible-sm"></div>
                        <div class="col-md-4 col-lg-3 hidden-sm hidden-xs information-entry product-sidebar">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pd_policies_wrapper">
                                        <div class="pd_policies style_2">
                                            <div class="pd_policies_title">
                                                <h5>SẼ CÓ TẠI NHÀ BẠN</h5>
                                                <span>từ 1-3 ngày làm việc</span>
                                            </div>
                                            <ul class="unstyled">
                                                <li class="clearfix">
                                                    <a href="javascript:void(0)">
                                                        <img src="assets/front/v1/images/pd_policies_17efa.png">
                                                        <div class="policies_tit"> MIỄN PHÍ VẬN CHUYỂN</div>
                                                        <div class="policies_descrip italic-style"> Với đơn hàng thanh toán trước</div>
                                                    </a>
                                                </li>
                                                <li class="clearfix">
                                                    <a href="javascript:void(0)">
                                                        <img src="assets/front/v1/images/pd_policies_27efa.png?1">
                                                        <div class="policies_tit"> ĐỔI TRẢ MIỄN PHÍ</div>
                                                        <div class="policies_descrip italic-style"> Đổi trả miễn phí 3 ngày</div>
                                                    </a>
                                                </li>
                                                <li class="clearfix">
                                                    <a href="javascript:void(0)">
                                                        <img src="assets/front/v1/images/pd_policies_37efa.png">
                                                        <div class="policies_tit"> THANH TOÁN</div>
                                                        <br/>
                                                        <div class="policies_descrip italic-style"> Thanh toán khi nhận hàng</div>
                                                    </a>
                                                </li>
                                                <li class="clearfix">
                                                    <a href="javascript:void(0)">
                                                        <img src="assets/front/v1/images/pd_policies_47efa.png">
                                                        <div class="policies_tit"> HOTLINE</div>
                                                        <br/>
                                                        <div class="policies_descrip"><strong style="color: #288f06; font-size: 18px;">097.147.7007</strong>
                                                            <br> <span class="italic-style">24/24-Tất cả các ngày</span>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row nopadding">
                    <div class="col-md-10 col-xs-12 nopadding">
                        <?php $videoUrls = empty($product['VideoUrls']) ? array() : json_decode($product['VideoUrls'], true);
                        if (!empty($videoUrls)) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title_channel">
                                            <p class="titleBoxServicesHome"><span> <i class="fa fa-youtube-play"></i> Video sản phẩm</span></p>
                                    </div>
                                    <div class="content_channel">
                                        <div class="owl-list-video owl_pages">
                                            <?php foreach ($videoUrls as $videoUrl) {
                                                $videoId = getVideoYoutubeId($videoUrl);
                                                if (!empty($videoId)) { ?>
                                                    <div class="item">
                                                        <iframe src="//www.youtube.com/embed/<?php echo $videoId; ?>" style="height: 158px !important" allowfullscreen="allowfullscreen"></iframe>
                                                    </div>
                                                <?php }
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="tabs-container style-1">
                            <div class="swiper-tabs tabs-switch">
                                <div class="title">Product info</div>
                                <div class="list">
                                    <a class="tab-switcher active">Chi tiết sản phẩm</a>
                                    <a class="tab-switcher">Bình luận</a>
                                    <a class="tab-switcher">Hướng dẫn mua hàng</a>
                                    <a class="tab-switcher">Chính sách đổi trả</a>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div>
                                <div class="tabs-entry">
                                    <div class="article-container style-1">
                                        <?php echo $product['ProductDesc']; ?>
                                    </div>
                                </div>
                                <div class="tabs-entry">
                                    <div class="article-container style-1">
                                        <div class="container-fluid">
                                            <div id="fb-root"></div>
                                            <div class="fb-comments" data-href="<?php echo $productUrl; ?>" data-numposts="5" width="100%" data-colorscheme="light"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--Hướng dẫn mua hàng-->
                                <div class="tabs-entry">
                                    <div class="article-container style-1">
                                        <p class="pkch-title"><strong>KHÁCH HÀNG NỘI THÀNH:</strong></p>
                                        <p>Qúy khách có thể mua hàng theo 2 cách : </p>
                                        <ul>
                                            <li>Đến trực tiếp cửa hàng của RICKY tại địa chỉ : 203 Đường Bờ Sông - Gần
                                                cầu Khỉ - Hoàng Mai - Hà Nội.
                                            </li>
                                            <li>Nhận hàng tại nhà ! Đặt hàng nhanh qua WEB RICKY.VN !</li>
                                            <li>Hotline đặt hàng : 097.147.7007 - 04.22121.999</li>
                                        </ul>
                                        <p class="pkch-title"><strong>KHÁCH HÀNG NGOẠI THÀNH: </strong>
                                        </p>
                                        <ul>
                                            <li>Khách hàng ở xa hoàn toàn có thể nhận hàng tại nhà, thanh toán tiền hàng
                                                trực tiếp cho nhân viên giao hàng.
                                            </li>
                                            <li>Thời gian giao hàng thông thường từ 1-3 ngày làm việc.</li>
                                            <li>RICKY MIỀN NAM địa chỉ tại&nbsp;: 704-Sông Mây-Trảng Bom-Đồng Nai( cho
                                                những khách hàng ở khu vực này )
                                            </li>
                                            <li>Hotline đặt hàng : 097.147.7007 - 04.22121.999</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tabs-entry">
                                    <div class="article-container style-1">
                                        <ul>
                                            <li>Mọi sản phẩm được đổi trả miễn phí trong vòng 7 ngày đầu với bất cứ lỗi
                                                nào của nhà sản xuất.
                                            </li>
                                            <li>Đổi mới sang sản phẩm mới MIỄN PHÍ ( sản phẩm mới có giá trị lớn hơn
                                                hoặc tương tương sản phẩm đã mua ) trong thời gian 1-3 ngày đầu kể từ
                                                ngày mua hàng.
                                            </li>
                                            <li>Mọi chi tiết liên hệ :&nbsp;<span>097.147.7007 - 04.22121.999</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                
                                <div class="title_channel">
                                        <p class="titleBoxServicesHome"><span> <i class="fa fa-youtube-play"></i> RICKY'S Channel</span></p>
                                </div>

                                <div class="content_channel">
                                    <a href="https://www.youtube.com/channel/UCK1L90-84fSNuFPbWDsjRIA/feed">
                                        <img src="assets/front/v1/images/banner_y_channelc466.png" class="img-responsive"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php if (!empty($listProducts)) { ?>
                            <div class="information-blocks related_product">
                                <div class="tabs-container style-1">
                                    <!-- <div class="swiper-tabs tabs-switch">
                                        <div class="list">
                                            <a class="tab-switcher active">Sản phẩm liên quan</a>
                                            <div class="clear"></div>
                                        </div>
                                    </div> -->
                                    <div class="title_channel">
                                            <p class="titleBoxServicesHome"><span> <i class="fa fa-youtube-play"></i> Sản phẩm liên quan</span></p>
                                    </div>
                                    <div>
                                        <div class="tabs-entry">
                                            <div class="article-container style-1">
                                                <div class="content-related-product owl_pages">
                                                    <?php foreach ($listProducts as $product) {
                                                        $url = $this->Mconstants->getUrl($product['ProductSlug'], $product['ProductId'], 3); ?>
                                                        <div class="product-slide-entry shift-image fix_box">
                                                            <div class="paddings-container">
                                                                <div class="product-slide-entry shift-image">
                                                                    <div class="product-image">
                                                                        <a href="<?php echo $url; ?>" title="<?php echo $product['ProductName']; ?>">
                                                                            <?php $image = PRODUCT_PATH.(empty($product['ProductImage']) ? NO_IMAGE : $product['ProductImage']); ?>
                                                                            <img src="<?php echo $image; ?>" alt="<?php echo $product['ProductName']; ?>" class="product-image">
                                                                            <img src="<?php echo $image; ?>" alt="<?php echo $product['ProductName']; ?>" class="product-image-hover">
                                                                        </a>
                                                                        <a href="javascript:void(0)" title="Xem nhanh" class="top-line-a left btn-quickview-1" data-handle="<?php echo $url; ?>">
                                                                            <i class="fa fa-retweet"></i>
                                                                        </a>
                                                                        <a href="<?php echo $url; ?>" title="Xem chi tiết" class="top-line-a right">
                                                                            <i class="fa fa-eye"></i>
                                                                        </a>
                                                                        <div class="bottom-line">
                                                                            <a href="javascript:void(0)" data-id="<?php echo $product['ProductId'] ?>" class="bottom-line-a Addcart">
                                                                                <i class="fa fa-shopping-cart"></i> Thêm vào giỏ
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="product-info">
                                                                        <a class="title_product" href="<?php echo $url; ?>" title="<?php echo $product['ProductName']; ?>"><?php echo $product['ProductName']; ?></a>
                                                                        <div class="price">
                                                                            <?php if ($product['IsContactPrice'] == 2) { ?>
                                                                                <div class="current">Giá liên hệ</div>
                                                                            <?php } else { ?>
                                                                                <?php if ($product['OldPrice'] > 0) { ?>
                                                                                    <div class="prev"><?php echo priceFormat($product['OldPrice']); ?> ₫</div>
                                                                                <?php } ?>
                                                                                <div class="current"><?php echo $product['Price'] > 0 ? priceFormat($product['Price']).'₫' : 'Giá liên hệ'; ?> </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="information-blocks related_product">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="title_channel">
                                            <p class="titleBoxServicesHome"><span> <i class="fa fa-comments"></i> Feedback Khách Hàng</span></p>
                                    </div>
                                    <div class="content_channel">
                                        <div class="col-md-12 nopadding">
                                            <img src="assets/front/v1/images/Nhanxetkhc466.png" class="img-responsive"/>
                                        </div>
                                        <div class="row customer_cmt">

                                            <div class="blog-entry text-center col-md-13 col-xs-12 col-sm-12">
                                                <a class="" href="../blogs/nhan-xet-khach-hang/khach-hang-5.html"><img
                                                        src="assets/front/v1/images/nguyen_quang_tiep_2_medium.jpg"
                                                        alt=""/>
                                                </a>

                                                <div class="">
                                                    <a class="title"
                                                       href="../blogs/nhan-xet-khach-hang/khach-hang-5.html">Quang
                                                        Tiệp</a>

                                                    <div class="description">
                                                        <p>"...<span>xóa tan mọi nghi ngờ khi đặt hàng online... Tôi rất hài lòng với cách làm việc của RICKY, mong RICKY ngày càng phát triển hơn để nhiều người biết đến và sử dụng sản phẩm của cửa hàng nhiều hơn !</span>"
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="blog-entry text-center col-md-13 col-xs-12 col-sm-12">
                                                <a class="" href="../blogs/nhan-xet-khach-hang/khach-hang-4.html"><img
                                                        src="assets/front/v1/images/quan_phung_medium.jpg" alt=""/>
                                                </a>

                                                <div class="">
                                                    <a class="title"
                                                       href="../blogs/nhan-xet-khach-hang/khach-hang-4.html">Quan
                                                        Phung</a>

                                                    <div class="description">
                                                        <p><span>"...thái độ rất thân thiện niềm nở với khách hàng, tiếp đến là tư vấn cũng như hướng dẫn sử dụng rất chu đáo và"rất thật" mình nhấn mạnh hai chữ"rất thật" trong thời buổi loạn thông tin này. Và cuối cùng là giá cả..."</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="blog-entry text-center col-md-13 col-xs-12 col-sm-12">
                                                <a class="" href="../blogs/nhan-xet-khach-hang/khach-hang-3.html"><img
                                                        src="assets/front/v1/images/giang_den_medium.jpg" alt=""/>
                                                </a>

                                                <div class="">
                                                    <a class="title"
                                                       href="../blogs/nhan-xet-khach-hang/khach-hang-3.html">Giang
                                                        Đen</a>

                                                    <div class="description">
                                                        <p><span style="font-size: 10pt;"
                                                                 data-mce-style="font-size: 10pt;">"...<span>mình ko biết nói gì hơn...trân thành cảm ơn ricky đã mang đến cho mình bộ sản phẩm hoàn hảo và chuc bên ricky sẽ có được các mặt hàng chất lượng và chính hãng để phục vụ cho các bạn...</span>"</span>
                                                            <br>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="blog-entry text-center col-md-13 col-xs-12 col-sm-12">
                                                <a class="" href="../blogs/nhan-xet-khach-hang/khach-hang-2.html"><img
                                                        src="assets/front/v1/images/trang_ha_medium.jpg" alt=""/>
                                                </a>

                                                <div class="">
                                                    <a class="title"
                                                       href="../blogs/nhan-xet-khach-hang/khach-hang-2.html">Trang
                                                        Ha</a>

                                                    <div class="description">
                                                        <p><span style="font-size: 11pt;"
                                                                 data-mce-style="font-size: 11pt;">"Ông chủ nhiệt tình, vui vẻ. Dịch vụ tốt. Cho đổi sản phẩm khi chưa hài lòng. Thích sang đây mua hàng lắm mỗi tội bán mỗi đồ thu âm mà mua đủ bộ hết rồi <span
                                                                    class="_47e3 _5mfr"
                                                                    title="Biểu tượng cảm xúc smile"><span
                                                                        aria-hidden="1"
                                                                        class="_7oe">:) ..."</span></span>
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="blog-entry text-center col-md-13 col-xs-12 col-sm-12">
                                                <a class="" href="../blogs/nhan-xet-khach-hang/khach-hang-1.html"><img
                                                        src="assets/front/v1/images/trung_hieu_medium.jpg" alt=""/>
                                                </a>

                                                <div class="">
                                                    <a class="title"
                                                       href="../blogs/nhan-xet-khach-hang/khach-hang-1.html">Trung
                                                        Hiếu</a>

                                                    <div class="description">
                                                        <p><span style="font-size: 10pt;"
                                                                 data-mce-style="font-size: 10pt;">"... Đáp ứng và giải đáp câu hỏi của khách hàng rất nhanh chóng và tận tình. Thiết bị bán ra là thiết bị chĩnh hãng. Tuy tôi chưa mua thiết bị từ shop. Nhưng shop đã giải đáp những thắc mắc của tôi 1 cách cụ thể và dễ hiểu..."</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ConsultModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="title-consult">Đừng ngần ngại, Hãy để chúng mình gọi lại chia sẻ với bạn trước khi quyết
                        định, tránh mất tiền mà vẫn không hiệu quả!</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-consult">
                        <div class="col-md-5">
                            <input type="text" id="nameConsult" placeholder="Họ tên" class="form-control">
                        </div>
                        <div class="col-md-5">
                            <input type="text" id="phoneConsult" placeholder="Số điện thoại" class="form-control">
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-consult-submit">GỬI</button>
                        </div>
                    </div>
                    <br>
                    <p class="text-danger consultInform"></p>
                </div>
            </div>
        </div>
    </div>
    <input type="text" hidden="hidden" id="linkAffId" value="<?php echo $linkAffId; ?>">
    <input type="text" hidden="hidden" id="ctvId" value="<?php echo $ctvId; ?>">
<?php $this->load->view('site/includes/footer'); ?>