<div class="footer-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-sm-12 col-xs-12">
                <h4>VỀ CHÚNG TÔI</h4>
                <ul>
                    <li><a href="<?php echo base_url('pages/gioi-thieu'); ?>">Giới thiệu</a></li>
                    <li><a href="<?php echo base_url('pages/nguyen-tac-quy-dinh'); ?>">Nguyên tắc - Quy định</a></li>
                    <li><a href="<?php echo base_url('pages/thanh-toan-va-bao-mat-thong-tin'); ?>">Thanh toán và bảo mật thông tin</a></li>
                    <li><a href="<?php echo base_url('pages/dieu-khoan-cam-ket'); ?>">Điều khoản và cam kết</a></li>
                </ul>
                <a target="_blank" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=43878"><img width="120px" style="margin-top: 15px;" src="assets/front/v1/images/dathongbao.png?1" alt="da thong bao bo cong thuong"></a>
            </div>
            <div class="col-sm-3 col-sm-12 col-xs-12">
                <h4>CHÍNH SÁCH</h4>
                <ul style="margin-bottom: 25px;">
                    <li><a href="<?php echo base_url('pages/quyen-va-nghia-vu-cua-ban-quan-ly'); ?>">Quản lý RICKY</a></li>
                    <li><a href="<?php echo base_url('pages/khieu-nai'); ?>">Góp ý - Khiếu nại</a></li>
                    <li><a href="<?php echo base_url('pages/quy-trinh-bao-hanh'); ?>">Quy trình và bảo hành</a></li>
                </ul>
                <p style="margin-bottom: 5px;"><a target="_blank" href="https://www.facebook.com/rickystudiovn/"><i class="fa fa-facebook"></i> Ricky Studio VN</a></p>
                <p><a target="_blank" href="https://www.youtube.com/channel/UCK1L90-84fSNuFPbWDsjRIA/feed"><i class="fa fa-youtube-play"></i> Youtube</a></p>
            </div>
            <div class="col-sm-7 col-sm-12 col-xs-12">
                <img src="<?php echo empty($configSites['ADDRESS_IMAGE']) ? 'assets/front/v1/images/banner_footerc466.jpg' : IMAGE_PATH.$configSites['ADDRESS_IMAGE']; ?>" class="img-responsive"/>
            </div>
        </div>
        <div class="text-center" style="margin-top: 20px">
            <p style="font-size: 10px;">CÔNG TY TNHH RICKY VIỆT NAM - GPĐKKD số 0107788880 do Sở KHĐT Hà Nội cấp ngày 04/04/2017 - MST:  0107788880 - Địa chỉ: Số  2/Q27 ngõ 136 Nguyễn An Ninh, Tương Mai - Hoàng Mai - Hà Nội</p>
        </div>
    </div>
</div>
</div>
<div class="search-box popup">
    <form action="<?php echo base_url('tim-kiem.html'); ?>">
        <div class="search-button">
            <i class="fa fa-search"></i>
            <input type="submit"/>
        </div>
        <div class="search-field">
            <input type="hidden" name="type" value="product"/>
            <input type="text" value="" name="q" placeholder="Tìm kiếm..."/>
        </div>
    </form>
</div>
<div id="cartModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <div class="cart-box">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <div class="row">
                                            <div class="col-xs-5 col-sm-6">
                                                <h5 class="mgt-5"><span class="glyphicon glyphicon-shopping-cart"></span> Giỏ hàng</h5>
                                            </div>
                                            <div class="col-xs-7 col-sm-6">
                                                <a href="<?php echo base_url('san-pham.html'); ?>" class="btn btn-ricky btn btn-block">
                                                    <span class="glyphicon glyphicon-share-alt"></span> Tiếp tục mua hàng
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">

                                </div>
                                <hr>
                                <div class="row pd-15">
                                    <div>
                                        <div class="col-sm-6 col-sm-6">
                                            <input type="text" class="coupon-code text-center" placeholder="Mã giảm giá">
                                        </div>
                                        <div class="col-sm-3 col-sm-6 col-sm-offset-3 text-right">
                                            <button type="button" class="btn btn-default btn btn-block update-cart">
                                                Cập nhật lại giỏ hàng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer border-full">
                                <div class="well mg-15 open-form">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label>Họ tên</label>
                                            <input class="form-control focus-i" id="name" type="text">
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label>Email</label>
                                            <input class="form-control focus-i" id="email" type="email">
                                        </div>
                                        <div class="col-sm-6" form-group>
                                            <label>Số điện thoại</label>
                                            <input class="form-control focus-i" id="phone" type="text">
                                        </div>
                                        <div class="col-sm-6 form-group">
                                            <label>Địa chỉ</label>
                                            <input class="form-control focus-i" id="address" type="text">
                                        </div>
                                        <div class="col-sm-6 wrap-contries-select form-group">
                                            <label>Quốc gia</label>
                                        </div>
                                        <div class="col-sm-6 wrap-provinces-select form-group VNon">
                                            <label>Tỉnh/ Thành phố</label>
                                        </div>
                                        <div class="col-sm-6 form-group wrap-districts-select form-group VNon">
                                            <label>Quận huyện</label>
                                        </div>
                                        <div class="col-sm-6 form-group wrap-wards-select form-group VNon">
                                            <label>Phường / Xã</label>
                                        </div>
                                        <div class="col-sm-6 form-group VNoff" style="display: none">
                                            <label class="control-label">ZipCode</label>
                                            <input type="text" id="zipCode" class="form-control" value="">
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <label>Chú thích</label>
                                            <input class="form-control focus-i" id="note" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 text-center">
                                        <p class="checkout-info text-danger"></p>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-xs-7 col-sm-9">
                                        <h4 class="text-right mgt-11">Tổng tiền:
                                            <strong class="totalMoney">0</strong></h4>
                                    </div>
                                    <div class="col-xs-5 col-sm-3">
                                        <button type="button" class="btn btn-ricky btn-block checkout">Mua hàng</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="text" hidden="hidden" id="getListWardUrl" value="<?php echo base_url('api/config/getListWard'); ?>">
<script src="assets/front/v1/js/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="assets/front/v1/js/plugins/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="assets/front/v1/js/js/option_selection.min.js"></script>
<script src="assets/front/v1/js/js/api.jquery.min.js"></script>
<script src="assets/front/v1/js/modernizr.min.js" type="text/javascript"></script>
<script src="assets/front/v1/js/theme-default/html5shiv.js"></script>
<script src="assets/front/v1/js/theme-default/jquery-migrate-1.2.0.min.js"></script>
<script src="assets/front/v1/js/theme-default/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="assets/front/v1/js/js/haravan.plugin.1.0.min.js"></script>
<script src="assets/front/v1/js/idangerous.swiper.min.js" type="text/javascript"></script>
<script src="assets/front/v1/js/owl.carousel.js" type="text/javascript"></script>
<script src="assets/front/v1/js/global.js" type="text/javascript"></script>
<script src="assets/front/v1/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="assets/front/v1/js/jquery.jscrollpane.min.js" type="text/javascript"></script>
<script src="assets/front/v1/js/ajax-cart.js?20180511" type="text/javascript"></script>
<script src="assets/front/v1/js/scripts.js?20171031" type="text/javascript"></script>
<script src="assets/front/v1/js/jquery.fancybox.js" type="text/javascript"></script>
<?php if (isset($scriptFooter)) outputScript($scriptFooter); ?>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-89018846-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-89018846-1');
</script>
</body>
</html>