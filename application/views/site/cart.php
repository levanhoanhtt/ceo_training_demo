<?php $this->load->view('site/includes/header'); ?>
<div class="cart-page">
    <div class="breadcrumb-box">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <ol class="breadcrumb breadcrumb-arrow hidden-sm hidden-xs">
                        <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                        <li class="active"><span> Giỏ hàng</span></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="cart-box cart-page">
        <div class="container pdbt-30">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <div class="row">
                                    <div class="col-xs-5 col-md-6">
                                        <h5 class="mgt-5"><span class="glyphicon glyphicon-shopping-cart"></span> Giỏ hàng</h5>
                                    </div>
                                    <div class="col-xs-7 col-md-6">
                                        <a href="<?php echo base_url('san-pham.html'); ?>" class="btn btn-ricky btn btn-block">
                                            <span class="glyphicon glyphicon-share-alt"></span> Tiếp tục mua hàng
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php $totalMoney = 0;
                            foreach ($carts as $item): ?>
                                <?php $totalMoney += $item['price'] * $item['qty']; ?>
                                <div class="row product-item" data-child="<?php echo $item['product_child'] ?>" data-id="<?php echo $item['id'] ?>">
                                    <div class="col-xs-3 col-md-1">
                                        <img width="100px" class="img-responsive" src="<?php echo PRODUCT_PATH . $item['image_link']; ?>">
                                    </div>
                                    <div class="col-xs-9 col-md-3">
                                        <h4 class="product-name"><strong><?php echo $item['name'] ?></strong></h4>
                                        <h4 class="product-name"><strong><?php echo $item['child_name'] ?></strong></h4>
                                        <h4><small>Sku: <b><?php echo $item['sku'] ?></b></small></h4>
                                    </div>
                                    <div class="col-xs-5 col-md-2">
                                        <strong>BH: <?php echo $item['guaranteemonth'] ?> tháng</strong>
                                    </div>
                                    <div class="col-xs-12 col-sm-9 col-md-6 col-price">
                                        <div class="col-xs-4 text-right col-md-4 xs-price">
                                            <?php if ($item['old_price'] > 0): ?>
                                                <h6 class=" line-thr">
                                                    <strong><?php echo priceFormat($item['old_price']); ?>đ</strong>
                                                </h6>
                                                <h6 class="mgt-5">
                                                    <strong class="spanPrice"><?php echo priceFormat($item['price']) ?>đ </strong>
                                                </h6>
                                            <?php else: ?>
                                                <h6 class="mgt-9"><strong class="spanPrice"><?php echo priceFormat($item['price']); ?>đ </strong></h6>
                                            <?php endif ?>
                                        </div>
                                        <div class="col-xs-3 col-md-3">
                                            x <input type="number" class="input-qty" min="1" value="<?php echo $item['qty'] ?>">
                                        </div>
                                        <div class="col-xs-4 mgt-7 col-md-3 xs-price">
                                            = <strong><?php echo priceFormat($item['price'] * $item['qty']); ?>đ</strong>
                                        </div>
                                        <div class="col-xs-1 xs-pd-0 col-md-2">
                                            <button type="button" class="btn btn-link btn-xs">
                                                <span class="glyphicon glyphicon-trash delcart" data-id="<?php echo $item['id']; ?>"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            <?php endforeach ?>
                            <?php if (count($carts) == 0) echo 'Giỏ hàng rỗng !' ?>
                        </div>
                        <hr>
                        <div class="row pd-15">
                            <div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="coupon-code text-center" placeholder="Mã giảm giá">
                                    <div class="col-xs-12"><p class="checkout-coupon-code text-danger"></p></div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-md-offset-3 text-right">
                                    <button type="button" class="btn btn-default btn btn-block update-cart">
                                        Cập nhật lại giỏ hàng
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer border-full">
                        <div class="well mg-15 open-form">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Họ tên</label>
                                    <input class="form-control focus-i" id="name" type="text">
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Email</label>
                                    <input class="form-control focus-i" id="email" type="email">
                                </div>
                                <div class="col-sm-6" form-group>
                                    <label>Số điện thoại</label>
                                    <input class="form-control focus-i" id="phone" type="text">
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Địa chỉ</label>
                                    <input class="form-control focus-i" id="address" type="text">
                                </div>
                                <div class="col-sm-6 wrap-contries-select form-group">
                                    <label>Quốc gia</label>
                                </div>
                                <div class="col-sm-6 wrap-provinces-select form-group VNon">
                                    <label>Tỉnh/ Thành phố</label>
                                </div>
                                <div class="col-sm-6 form-group wrap-districts-select form-group VNon">
                                    <label>Quận huyện</label>
                                </div>
                                <div class="col-sm-6 form-group wrap-wards-select form-group VNon">
                                    <label>Phường / Xã</label>
                                </div>
                                <div class="col-sm-6 form-group VNoff" style="display: none">
                                    <label class="control-label">ZipCode</label>
                                    <input type="text" id="zipCode" class="form-control" value="">
                                </div>
                                <div class="col-sm-12 form-group">
                                    <label>Chú thích</label>
                                    <input class="form-control focus-i" id="note" value="">
                                </div>
                            </div>
                            <div class="col-xs-12 text-center"><p class="checkout-info text-danger"></p></div>
                        </div>
                        <div class="row text-center">
                            <div class="col-xs-7 col-md-9">
                                <h4 class="text-right mgt-11">Tổng tiền:
                                    <strong class="totalMoney"><?php echo priceFormat($totalMoney); ?> đ</strong></h4>
                            </div>
                            <div class="col-xs-5 col-md-3">
                                <button type="button" class="btn-ricky btn btn-block checkout">
                                    Mua hàng
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('site/includes/footer'); ?>
