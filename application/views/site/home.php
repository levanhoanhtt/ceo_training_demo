<?php $this->load->view('site/includes/header'); ?>
    <div class="parallax-slide">
        <div class="swiper-container" data-autoplay="7000" data-loop="1" data-speed="1000" data-center="0" data-slides-per-view="1">
            <div class="swiper-wrapper">
                <!--<div class="swiper-slide no-shadow active" data-val="2" style="background-image: url(assets/front/v1/images/presentation-3c466.jpg);"></div>-->
                <div class="swiper-slide no-shadow active" data-val="4" style="background-image: url(assets/front/v1/images/presentation-5c466.jpg);"></div>
            </div>
            <div class="pagination"></div>
        </div>
    </div>
    <div class="container-fuild hidden-lg hidden-md nopadding">
        <div class="row">
            <div class="col-md-12">
                <div id="owl-example" class="owl-carousel">
                    <img src="assets/front/v1/images/banner_1c466.jpg" alt="Banner 1">
                    <img src="assets/front/v1/images/banner_2c466.jpg" alt="Banner 2">
                    <img src="assets/front/v1/images/banner_3c466.jpg" alt="Banner 3">
                </div>
            </div>
        </div>
    </div>
    <!--<div class="container-fluid nopadding hidden-xs hidden-sm">
        <div class="row nopadding">
            <div class="col-md-4 nopadding">
                <div class="box-collection-banner">
                    <div class="wrapper-image-banner">
                        <a href="<?php //echo base_url('san-pham.html'); ?>">
                            <picture>
                                <img src="assets/front/v1/images/banner_1c466.jpg" alt="Banner 1">
                            </picture>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 nopadding">
                <div class="box-collection-banner">
                    <div class="wrapper-image-banner">
                        <a href="<?php //echo base_url('san-pham.html'); ?>">
                            <picture>
                                <img src="assets/front/v1/images/banner_2c466.jpg" alt="Banner 1">
                            </picture>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 nopadding">
                <div class="box-collection-banner">
                    <div class="wrapper-image-banner">
                        <a href="<?php //echo base_url('san-pham.html'); ?>">
                            <picture>
                                <img src="assets/front/v1/images/banner_3c466.jpg" alt="Banner 1">
                            </picture>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <div class="side-square-wrapper" style="margin-top: 10px;">
        <div class="row nopadding">
            <div class="col-md-6 col-md-push-6 nopadding side-square-entry">
                <div class="parallax-clip">
                    <div class="fixed-parallax" style="background-image: url(assets/front/v1/images/square-parallax-1c466.jpg);"></div>
                    <div class="parallax-info">
                        <div class="list-parallax">
                            <div class="parallax-article">
                                <h2 class="subtitle">Sự kiện sắp tới</h2>
                                <h1 class="title">CUỘC THI LET'S SING</h1>
                                <div class="description">Sắp tới RICKY.VN tiếp tục đem đến cho các bạn cuộc thi ca hát
                                    được
                                    giới trẻ yêu thích !
                                    LET'S SING, với vô vàn phần quà hấp dẫn.
                                </div>
                                <div class="info">
                                    <a href="<?php echo base_url('san-pham.html'); ?>" class="button style-8">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 nopadding side-square-entry">
                <div class="align-content">
                    <div style="padding-top: 0;" class="block-header">
                        <h3 class="title">SẢN PHẨM BÁN CHẠY NHẤT</h3>
                        <div class="description">RICKY VIỆT NAM Là 1 Thương hiệu đầu tiên ở Việt Nam, đi đầu về cung cấp
                            các
                            thiết bị Studio ỦY QUYỀN CHÍNH HÃNG  !Thời gian bảo hành lên đến : 2 năm 1 đổi 1 và chế độ
                            HẬU
                            MÃI chuyên nghiệp thay đổi hoàn toàn sự trải nghiệm của khách hàng trong thờ...
                        </div>
                    </div>
                    <div class="products-swiper">
                        <div class="swiper-containers">
                            <div class="swiper-wrapper owl_pages home_slider_product">
                                <?php foreach($listSells as $product) $this->load->view('site/includes/product', array('product' => $product)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="side-square-wrapper">
        <div class="row nopadding">
            <div class="col-md-6 nopadding side-square-entry">
                <div class="parallax-clip">
                    <div class="fixed-parallax"
                         style="background-image: url(assets/front/v1/images/square-parallax-2c466.jpg);"></div>
                    <div class="parallax-info">
                        <div class="list-parallax">
                            <div class="parallax-article">
                                <h2 class="subtitle">THỎA SỨC ĐAM MÊ</h2>
                                <h1 class="title"></h1>
                                <div class="description">Với sự trợ giúp hoàn hảo của các bộ xử lý âm thanh (Soundcard)
                                    sẽ hỗ trợ
                                    giọng hát của bạn được tốt nhất !
                                </div>
                                <div class="info">
                                    <a href="<?php echo base_url('sound-card-c3.html'); ?>" class="button style-8">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 nopadding side-square-entry">
                <div class="align-content">
                    <div style="padding-top: 0;" class="block-header">
                        <h3 class="title">SOUNDCARD ÂM THANH</h3>
                        <div class="description"></div>
                    </div>
                    <div class="products-swiper">
                        <div class="swiper-containers">
                            <div class="swiper-wrapper owl_pages home_slider_product">
                                <?php foreach($listSoundCards as $product) $this->load->view('site/includes/product', array('product' => $product)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('site/includes/footer'); ?>