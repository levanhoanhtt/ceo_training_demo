<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('fbpage/update', array('id' => 'fbPageForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Fb Page Name</th>
                                <th>Fb Page Code</th>
                                <th>Prefix</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyFbPage">
                            <?php
                            foreach($listFbpages as $p){ ?>
                                <tr id="fbpage_<?php echo $p['FbPageId']; ?>">
                                    <td id="fbPageName_<?php echo $p['FbPageId']; ?>"><?php echo $p['FbPageName']; ?></td>
                                    <td id="fbPageCode_<?php echo $p['FbPageId']; ?>"><?php echo $p['FbPageCode']; ?></td>
                                    <td id="prefix_<?php echo $p['FbPageId']; ?>"><?php echo $p['Prefix']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['FbPageId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['FbPageId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="fbPageName" name="FbPageName" value="" data-field="Fb Page Name"></td>
                                <td><input type="text" class="form-control hmdrequired" id="fbPageCode" name="FbPageCode" value="" data-field="Fb Page Code"></td>
                                <td><input type="text" class="form-control hmdrequired" id="prefix" name="Prefix" value="" data-field="Prefix"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="FbPageId" id="fbPageId" value="0" hidden="hidden">
                                    <input type="text" id="deleteFbPageUrl" value="<?php echo base_url('fbpage/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>