<?php $this->load->view('chatstaff/header'); ?>
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/vendor/dist/img/logo.png" class="user-image" alt="User Image">
                    <span class="hidden-xs" id="spanCustomerName"><?php echo $user['FullName']; ?></span>
                </a>
            </li>
        </ul>
    </div>
    </div>
    </nav>
    </header>
    <div class="content-wrapper" id="chatAdminPage">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="box box-primary">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs" id="ulStaffRole">
                                    <li class="active text-center"><a href="#tabCare" data-toggle="tab" data-id="1">Tư vấn - CSKH</a></li>
                                    <li class="text-center"><a href="#tabTech" data-toggle="tab" data-id="2">Kỹ thuật</a></li>
                                </ul>
                                <div class="tab-content">
                                    <?php $customerAvatar = USER_PATH.'customer.png'; ?>
                                    <div class="tab-pane active" id="tabCare">
                                        <select class="form-control selectStaff" data-id="1">
                                            <option value="0">--Chọn--</option>
                                            <?php foreach($listStaffs as $u){
                                                if(in_array($u['UserId'], $careIds)){ ?>
                                                    <option value="<?php echo $u['UserId']; ?>"><?php echo $u['FullName']; ?></option>
                                                <?php }
                                            } ?>
                                        </select>
                                        <div class="box-body box-comments" id="listCustomerChat1"></div>
                                    </div>
                                    <div class="tab-pane" id="tabTech">
                                        <select class="form-control selectStaff" data-id="2">
                                            <option value="0">--Chọn--</option>
                                            <?php foreach($listStaffs as $u){
                                                if(in_array($u['UserId'], $techIds)){ ?>
                                                    <option value="<?php echo $u['UserId']; ?>"><?php echo $u['FullName']; ?></option>
                                                <?php }
                                            } ?>
                                        </select>
                                        <div class="box-body box-comments" id="listCustomerChat2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="box box-primary direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title" id="h3StaffChat">Tin nhắn chat với khách hàng</h3>
                            </div>
                            <div class="box-body" style="display: block;">
                                <div class="direct-chat-messages" id="listChat"></div>
                                <div class="direct-chat-contacts">
                                    <input type="text" hidden="hidden" id="imagePath" value="<?php echo PRODUCT_PATH; ?>">
                                    <input type="text" hidden="hidden" id="getChatUrl" value="<?php echo base_url('chatstaff/getList'); ?>">
                                    <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('chatstaff/getListCustomer'); ?>">
                                    <input type="text" hidden="hidden" id="updateCountChatUnReadUrl" value="<?php echo base_url('chatstaff/updateCountChatUnRead'); ?>">
                                    <input type="text" hidden="hidden" id="startChatPagging" value="0">
                                    <input type="text" hidden="hidden" id="totalChatMsg" value="0">
                                    <input type="text" hidden="hidden" id="staffId" value="0">
                                    <input type="text" hidden="hidden" id="staffRoleId" value="1">
                                    <input type="text" hidden="hidden" id="customerId" value="0">
                                    <input type="text" hidden="hidden" id="customerName" value="">
                                    <input type="text" hidden="hidden" id="customerPhone" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>