<?php $this->load->view('chatstaff/header'); ?>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo USER_PATH; ?>customer.png" class="user-image" alt="User Image">
                                <span class="hidden-xs" id="spanCustomerName"><?php echo $customerChat ? $customerChat['FullName'].' ('.$customerChat['PhoneNumber'].')' : ''; ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <div class="content-wrapper" id="chatToStaffPage">
        <div class="container-fluid">
            <section class="content">
                <style>
                    #ulStaffRole li{width: 47%;}
                    .box-comments .box-comment{cursor: pointer;}
                    .box-comments .box-comment.active {background-color: #c6c9ce;}
                    .box-comment span.online {
                        background: rgb(66, 183, 42);
                        border-radius: 50%;
                        display: inline-block;
                        height: 10px;
                        width: 10px;
                        float: right;
                        margin-right: 5px;
                        margin-top: 5px;
                    }
                    #chatForm .input-group{width: 100%;margin-bottom: 10px;}
                    #chatMsg{resize: none;overflow: hidden;display: block;}
                    #chatForm .lbChatEnter{float: left;height: 33px;line-height: 33px;}
                    #chatForm .input-group-btn{float: right;margin-right: 72px;}
                    #listChat .direct-chat-msg .direct-chat-text a{color: #3c8dbc;}
                    #listChat .direct-chat-msg.right .direct-chat-text a{color: #fff;}
                    /*#listChat .direct-chat-msg.unread .direct-chat-text{background-color: #E98561;border: 1px solid #E98561;color: #fff;}
                    #listChat .direct-chat-msg.unread .direct-chat-text a{color: #0000ff;}
                    #listChat .direct-chat-msg.unread .direct-chat-text:after, #listChat .direct-chat-msg.unread .direct-chat-text:before{border-right-color: #E98561;}
                    #listChat .direct-chat-msg.unread.right>.direct-chat-text:after, #listChat .direct-chat-msg.unread.right>.direct-chat-text:before{border-left-color: #E98561;}*/
                    #listChat .direct-chat-text img{max-width: 100%;}
                </style>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="box box-primary">
                            <!--<div class="box-body box-comments" id="listChatStaff"></div>-->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs" id="ulStaffRole">
                                    <li class="active text-center"><a href="#tabCare" data-toggle="tab" data-id="1">Tư vấn - CSKH</a></li>
                                    <li class="text-center"><a href="#tabTech" data-toggle="tab" data-id="2">Kỹ thuật</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tabCare">
                                        <div class="box-body box-comments"></div>
                                    </div>
                                    <div class="tab-pane" id="tabTech">
                                        <div class="box-body box-comments"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="box box-primary direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h3 class="box-title" id="h3StaffChat">Chat với kĩ thuật</h3>
                                    </div>
                                    <div class="col-sm-6" style="text-align: right;">
                                        <label class="box-title control-label">Đánh giá: </label>
                                        <div class='starrr' id='star1'></div>
                                        <label class="box-title control-label" id="rate">  </label>
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="box-body" style="display: block;">
                                <div class="direct-chat-messages" id="listChat"></div>
                                <div class="direct-chat-contacts">
                                    <input type="text" hidden="hidden" id="imagePath" value="<?php echo PRODUCT_PATH; ?>">
                                    <input type="text" hidden="hidden" id="getChatUrl" value="<?php echo base_url('chatstaff/getList'); ?>">
                                    <input type="text" hidden="hidden" id="updateCountChatUnReadUrl" value="<?php echo base_url('chatstaff/updateCountChatUnRead'); ?>">
                                    <input type="text" hidden="hidden" id="getListStaffUrl" value="<?php echo base_url('chatstaff/getListStaff'); ?>">
                                    <input type="text" hidden="hidden" id="startChatPagging" value="0">
                                    <input type="text" hidden="hidden" id="totalChatMsg" value="0">
                                    <input type="text" hidden="hidden" id="staffId" value="0">
                                    <input type="text" hidden="hidden" id="staffRoleId" value="1">
                                    <input type="text" hidden="hidden" id="staffName" value="">
                                    <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerChat ? $customerChat['CustomerId'] : ''; ?>">
                                    <input type="text" hidden="hidden" id="customerName" value="<?php echo $customerChat ? $customerChat['FullName'] : ''; ?>">
                                    <input type="text" hidden="hidden" id="customerPhone" value="<?php echo $customerChat ? $customerChat['PhoneNumber'] : ''; ?>">
                                </div>
                            </div>
                            <div class="box-footer" style="display: block;">
                                <form action="javascript:void(0)" method="post" id="chatForm">
                                    <div class="input-group">
                                        <input placeholder="Tin nhắn..." class="form-control" id="chatMsg">
                                    </div>
                                    <!--<label class="lbChatEnter"><input type="checkbox" id="cbChatEnter" checked> Enter để gửi</label>
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary btn-flat">Gửi</button>
                                        <button id="btnChatSendFile" type="button" class="btn btn-default btn-flat"><i class="fa fa-paperclip"></i></button>
                                    </span>-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="modal fade" id="modalCustomerName" role="dialog" aria-labelledby="modalCustomerName" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Chat trực tiếp với nhân viên</h4>
                </div>
                <div class="modal-body">
                    <p class="text-center" style="color: #798c9c;">Hãy để lại SĐT bạn đã mua hàng để được hỗ trợ nhanh và chính xác nhất !</p>
                    <div class="row">
                        <div class="col-xs-12  col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Tên</label>
                                <input type="text" class="form-control" id="customerNameInput">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Số điện thoại <span class="required">*</span></label>
                                <input type="text" class="form-control hmdrequired" id="customerPhoneInput" data-field="Số điện thoại">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnSetCustomerName">Bắt đầu chat</button>
                    <input type="text" hidden="hidden" id="setCustomerChatUrl" value="<?php echo base_url('chatstaff/setCustomerChat'); ?>">
                </div>
            </div>
        </div>
    </div>
    <?php $siteName = 'Ricky';
    $email = 'ricky@gmail.com';
    $configs = $this->session->userdata('configs');
    if($configs){
        if(isset($configs['SITE_NAME'])) $siteName = $configs['SITE_NAME'];
        if(isset($configs['EMAIL_COMPANY'])) $email = $configs['EMAIL_COMPANY'];
    } ?>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Bản quyền của <a href="http://ricky.vn">RICKY VIỆT NAM</a>.</strong> Phát triển bởi RICKY Developer
        - <strong>Email: <a id="aSysEmail" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></strong>.
    </footer>
</div>
<input type="text" hidden="hidden" id="rootPath" value="<?php echo ROOT_PATH; ?>">
<input type="text" hidden="hidden" id="siteName" value="<?php echo $siteName; ?>">
<input type="text" hidden="hidden" id="userImagePath" value="<?php echo USER_PATH; ?>">
<?php if(!$user) $user = $this->session->userdata('user');
if($user){ ?>
    <input type="text" hidden="hidden" id="userLoginId" value="<?php echo $user['UserId']; ?>">
    <input type="text" hidden="hidden" id="fullNameLoginId" value="<?php echo $user['FullName'] ?>">
    <input type="text" hidden="hidden" id="avatarLoginId" value="<?php echo empty($user['Avatar']) ? NO_IMAGE : $user['Avatar']; ?>">
<?php } else { ?>
    <input type="text" hidden="hidden" id="userLoginId" value="0">
    <input type="text" hidden="hidden" id="fullNameLoginId" value="">
    <input type="text" hidden="hidden" id="avatarLoginId" value="<?php echo NO_IMAGE; ?>">
<?php } ?>
<input type="text" hidden="hidden" id="getListWardUrl" value="<?php echo base_url('api/config/getListWard'); ?>">
<input type="text" hidden="hidden" id="getRemindUrl" value="<?php echo base_url('remind/getReminds'); ?>">
<input type="text" hidden="hidden" id="detailRemindUrl" value="<?php echo base_url('remind/edit'); ?>">
<input type="text" hidden="hidden" id="chatServerUrl" value="<?php echo CHAT_SERVER; ?>">
<div id="divInputRemindIds" style="display: none"></div>
<noscript><meta http-equiv="refresh" content="0; url=<?php echo base_url('user/permission'); ?>" /></noscript>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/pace/pace.min.js"></script>
<script src="assets/vendor/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/plugins/fastclick/fastclick.js"></script>
<script src="assets/vendor/dist/js/app.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.min.js"></script>
<script src="assets/vendor/plugins/select2/select2.full.min.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script src="assets/vendor/plugins/star_rating/starrr.js"></script>
<script type="text/javascript" src="assets/js/common.js"></script>
<script type="text/javascript" src="assets/vendor/plugins/socket.io/socket.io.js"></script>
<script type="text/javascript" src="assets/vendor/plugins/jquery.playSound.js"></script>
<script type="text/javascript" src="assets/js/chat_customer.js"></script>
<?php if(isset($scriptFooter)) outputScript($scriptFooter); ?>
</body>
</html>
