<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả</a></li>
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="userId">Người xóa</option>
                                        <option value="create_date">Ngày xóa</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 userId tableLog_tag">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control create_date none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control userId block-display">
                                        <?php foreach($listUsers as $t) :?>
                                            <option value="<?php echo $t['UserId']; ?>"><?php echo $t['FullName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="form-group block-display widthauto">
                                    <input class="form-control datepicker create_date none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker create_date none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                    <button id="btn-filter" data-href="<?php echo base_url('tablelog/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Ngày xóa</th>
                                <th>Mã</th>
                                <th>Người xóa</th>
                                <th>Mô tả</th>
                                <th class="text-center">Hồi phục</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTableLog"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="34">
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('tablelog/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>