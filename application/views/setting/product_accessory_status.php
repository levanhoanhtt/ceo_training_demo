<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('productaccessorystatus/update', array('id' => 'productAccessoryStatusForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Tình trạng phụ kiện</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProductAccessoryStatus">
                            <?php foreach($listProductAccessoryStatus as $p){ ?>
                                <tr id="productAccessoryStatus_<?php echo $p['ProductAccessoryStatusId']; ?>">
                                    <td id="productAccessoryStatusName_<?php echo $p['ProductAccessoryStatusId']; ?>"><?php echo $p['ProductAccessoryStatusName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['ProductAccessoryStatusId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['ProductAccessoryStatusId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="productAccessoryStatusName" name="ProductAccessoryStatusName" value="" data-field="Tình trạng phụ kiện"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ProductAccessoryStatusId" id="productAccessoryStatusId" value="0" hidden="hidden">
                                    <input type="text" id="deleteProductAccessoryStatusUrl" value="<?php echo base_url('productaccessorystatus/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>