<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('part/update', array('id' => 'partForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Mã phòng ban</th>
                                <th>Tên phòng ban</th>
                                <th>Phòng ban cha</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPart">
                            <?php
                            foreach($listParts as $p){ ?>
                                <tr id="part_<?php echo $p['PartId']; ?>">
                                    <td id="partCode_<?php echo $p['PartId']; ?>"><?php echo $p['PartCode']; ?></td>
                                    <td id="partName_<?php echo $p['PartId']; ?>"><?php echo $p['PartName']; ?></td>
                                    <td id="parentPartName_<?php echo $p['PartId']; ?>"><?php echo $p['ParentPartId'] > 0 ? $this->Mconstants->getObjectValue($listParts, 'PartId', $p['ParentPartId'], 'PartName') : ''; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['PartId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['PartId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="parentPartId_<?php echo $p['PartId']; ?>" value="<?php echo $p['ParentPartId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="partCode" name="PartCode" value="" data-field="Mã phòng ban"></td>
                                <td><input type="text" class="form-control hmdrequired" id="partName" name="PartName" value="" data-field="Tên phòng ban"></td>
                                <td>
                                    <select class="form-control" name="ParentPartId" id="parentPartId">
                                        <option value="0">--</option>
                                        <?php foreach($listParts as $p1){
                                            if($p1['ParentPartId'] == 0) echo '<option value="'.$p1['PartId'].'">'.$p1['PartName'].'</option>';
                                        } ?>
                                    </select>
                                </td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="PartId" id="partId" value="0" hidden="hidden">
                                    <input type="text" id="deletePartUrl" value="<?php echo base_url('part/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>