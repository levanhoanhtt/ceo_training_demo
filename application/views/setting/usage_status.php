<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('usagestatus/update', array('id' => 'usageStatusForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Hình thức/ tình trạng sử dụng</th>
                                <th>Hình thức cha</th>
                                <th>Loại</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyUsageStatus">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $usageStatusTypes = $this->Mconstants->usageStatusTypes;
                            foreach($listUsageStatus as $bt){ ?>
                                <tr id="usageStatus_<?php echo $bt['UsageStatusId']; ?>">
                                    <td id="usageStatusName_<?php echo $bt['UsageStatusId']; ?>"><?php echo $bt['UsageStatusName']; ?></td>
                                    <td id="parentUsageStatusName_<?php echo $bt['UsageStatusId']; ?>"><?php echo $bt['ParentUsageStatusId'] > 0 ? $this->Mconstants->getObjectValue($listUsageStatus, 'UsageStatusId', $bt['ParentUsageStatusId'], 'UsageStatusName') : ''; ?></td>
                                    <td id="usageStatusTypeName_<?php echo $bt['UsageStatusId']; ?>"><span class="<?php echo $labelCss[$bt['UsageStatusTypeId']]; ?>"><?php echo $usageStatusTypes[$bt['UsageStatusTypeId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $bt['UsageStatusId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $bt['UsageStatusId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="parentUsageStatusId_<?php echo $bt['UsageStatusId']; ?>" value="<?php echo $bt['ParentUsageStatusId']; ?>">
                                        <input type="text" hidden="hidden" id="usageStatusTypeId_<?php echo $bt['UsageStatusId']; ?>" value="<?php echo $bt['UsageStatusTypeId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="usageStatusName" name="UsageStatusName" value="" data-field="Hình thức/ tình trạng sử dụng"></td>
                                <td>
                                    <select class="form-control" name="ParentUsageStatusId" id="parentUsageStatusId">
                                        <option value="0">--</option>
                                        <?php foreach($listUsageStatus as $bt1){
                                            if($bt1['ParentUsageStatusId'] == 0) echo '<option value="'.$bt1['UsageStatusId'].'">'.$bt1['UsageStatusName'].'</option>';
                                        } ?>
                                    </select>
                                </td>
                                <td><?php $this->Mconstants->selectConstants('usageStatusTypes', 'UsageStatusTypeId'); ?></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="UsageStatusId" id="usageStatusId" value="0" hidden="hidden">
                                    <input type="text" id="deleteUsageStatusUrl" value="<?php echo base_url('usagestatus/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>