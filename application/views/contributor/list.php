<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('contributor/add'); ?>" class="btn btn-primary">Thêm cổ đông</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên cổ đông</th>
                                <th>SĐT</th>
                                <th>Ngày sinh</th>
                                <th>Địa chỉ</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyContributor">
                            <?php $i = 0;
                            $status = $this->Mconstants->itemStatus;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listContributors as $c){
                                $i++; ?>
                                <tr id="contributor_<?php echo $c['ContributorId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('contributor/edit/'.$c['ContributorId']); ?>"><?php echo $c['ContributorName']; ?></a></td>
                                    <td><?php echo $c['ContributorPhone']; ?></td>
                                    <td><?php echo ddMMyyyy($c['BirthDay']); ?></td>
                                    <td><?php echo $c['Address']; ?></td>
                                    <td id="statusName_<?php echo $c['ContributorId']; ?>"><span class="<?php echo $labelCss[$c['ItemStatusId']]; ?>"><?php echo $status[$c['ItemStatusId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $c['ContributorId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <div class="btn-group" id="btnGroup_<?php echo $c['ContributorId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $c['ContributorId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $c['ContributorId']; ?>" value="<?php echo $c['ItemStatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('contributor/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>