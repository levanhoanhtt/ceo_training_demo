<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><button class="btn btn-default">Xuất dữ liệu</button></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả vận chuyển</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group margin ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả vận chuyển theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="transport_store">Cơ sở giao hàng</option>
                                        <option value="transport_status">Trạng thái giao hàng</option>
                                        <option value="transport_status_cod">Trạng thái COD</option>
                                        <option value="transport_create">Thời gian tạo phiếu</option>
                                        <option value="transport_day_process">Thời gian xử lý</option>
                                        <option value="transport_transporter">Nhà vận chuyển</option>
                                        <option value="transport_transport_type">Phương thức vận chuyển</option>
                                        <option value="transport_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 transport_store none-display">
                                    <div class="text_opertor">ở</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group mb10 transport_status transport_status_cod transport_transporter transport_transport_type">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group mb10 transport_day_process none-display">
                                    <select class="value_operator form-control">
                                        <option value="=">bằng</option>
                                        <option value="!=">khác</option>
                                        <option value="<">nhỏ hơn</option>
                                        <option value=">">lớn hơn</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control transport_store">
                                        <?php foreach($listStores as $s) :?>
                                            <option value="<?php echo $s['StoreId']; ?>"><?php echo $s['StoreName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control transport_status none-display">
                                        <?php foreach($this->Mconstants->transportStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control transport_status_cod none-display">
                                        <?php foreach($this->Mconstants->CODStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control transport_create none-display" id="select_operator_date">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                    <select class="form-control transport_transporter none-display">
                                        <?php foreach($listTransporters as $t) :?>
                                            <option value="<?php echo $t['TransporterId']; ?>"><?php echo $t['TransporterName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control transport_transport_type none-display">
                                        <?php foreach($listTransportTypes as $tt) :?>
                                            <option value="<?php echo $tt['TransportTypeId']; ?>"><?php echo $tt['TransportTypeName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <select class="form-control transport_tag none-display">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <input class="form-control transport_day_process input-number none-display" type="text">
                                    <input class="form-control transport_tag none-display" type="text">
                                    <input class="form-control datepicker transport_create none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker transport_create none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <button id="btn-filter" data-href="<?php echo base_url('api/transport/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                            <option value="add_tags">Thêm nhãn</option>
                            <option value="delete_tags">Bỏ nhãn</option>
                            <option value="print_order">In phiếu bán hàng</option>
                            <option value="print_transport">In phiếu giao hàng</option>
                            <option value="export_transport">Xuất gửi bưu điện</option>
                            <option value="change_status">Cập nhật trạng thái</option>
                            <option value="delete_item">Xóa vận chuyển đã chọn</option>
                        </select>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th style="width: 60px"><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã vận chuyển</th>
                                <th>Mã đơn hàng</th>
                                <th>Ngày tạo VC</th>
                                <th>Khách hàng</th>
                                <th>Nhà vận chuyển</th>
                                <th class="text-center">TT giao hàng</th>
                                <th class="text-center">Tình trạng thu hộ (COD)</th>
                                <th class="text-right">Tiền thu (COD)</th>
                                <th>Cớ sở xử lý</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransport"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="printOrderMultiple" value="<?php echo base_url('order/printPdfMultipleByTransport'); ?>">
                    <input type="text" hidden="hidden" id="printTransportMultiple" value="<?php echo base_url('transport/printPdfMultiple'); ?>">
                    <input type="text" hidden="hidden" id="exportTransport" value="<?php echo base_url('transport/exportTransport'); ?>">
                    <input type="text" hidden="hidden" id="changeStatusBatchUrl" value="<?php echo base_url('api/transport/changeStatusBatch'); ?>">
                    <input type="text" hidden="hidden" id="productImagePath" value="<?php echo PRODUCT_PATH; ?>">
                    <input type="hidden" value="<?php echo base_url('api/transport/getDetail')?>" id="getDetailUrl">
                    <input type="text" hidden="hidden" id="itemTypeId" value="9">
                    <input type="hidden" value="<?php echo base_url('order/edit')?>" id="urlEditOrder">
                    <input type="hidden" value="<?php echo base_url('customer/edit')?>" id="urlEditCustomer">
                    <input type="hidden" value="<?php echo base_url('transport/edit')?>" id="urlEditTransport">
                    <input type="text" hidden="hidden" id="insertTransportCommentUrl" value="<?php echo base_url('api/transport/insertComment'); ?>">
                    <input type="text" hidden="hidden" id="updateFieldUrl" value="<?php echo base_url('api/transport/updateField'); ?>">
                    <input type="text" hidden="hidden" id="transportId" value="0"> 
                    <input type="text" hidden="hidden" id="transportStatusId" value="0">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                    <div id="divTranspostJson" style="display: none;"></div>
                    <div class="modal fade" id="modalTransportStatus" tabindex="-1" role="dialog" aria-labelledby="modalTransportStatus">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-truck" aria-hidden="true"></i> Cập nhật trạng thái vận chuyển</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="light-blue">Trạng thái vận chuyển</p>
                                    <div class="form-group">
                                        <?php $this->Mconstants->selectConstants('transportStatus', 'TransportStatusId'); ?>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <span hidden="hidden" id="jsonTransport"></span>
                                    <button class="btn btn-primary" type="button" id="btnUpdateTransportStatus">Hoàn thành</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="<?php echo base_url('transport/printPdfMultiple'); ?>" method="POST" target="_blank" id="printForm" style="display: none;">
                        <input type="hidden" name="TransportIds" value="" id="transportIds"/>
                        <input type="submit" value="Submit">
                    </form>

                    <div class="modal fade" id="modalItemComment" tabindex="-1" role="dialog" aria-labelledby="modalItemComment">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-comments-o"></i> Ghi chú</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="listCommentAll"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalTracking" tabindex="-1" role="dialog" aria-labelledby="modalTracking">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật mã vận đơn</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="light-blue">Mã vận đơn</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tracking" placeholder="Mã vận đơn">
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <button class="btn btn-primary" type="button" id="btnUpdateTracking">Hoàn thành</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalShipCost" tabindex="-1" role="dialog" aria-labelledby="modalShipCost">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-wrench" aria-hidden="true"></i> Cập nhật phí giao hàng</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="light-blue">Phí ship thực tế</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control cost" id="shipCost" value="0">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" id="btnUpdateShipCost">Hoàn thành</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalTransportStatus" tabindex="-1" role="dialog" aria-labelledby="modalTransportStatus">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="box-title"><i class="fa fa-truck" aria-hidden="true"></i> Cập nhật trạng thái vận chuyển</h4>
                                </div>
                                <div class="modal-body">
                                    <p class="light-blue">Trạng thái vận chuyển</p>
                                    <div class="form-group">
                                        <?php $this->Mconstants->selectConstants('transportStatus', 'TransportStatusId', $transport['TransportStatusId']); ?>
                                    </div>
                                    <div id="divCancelReason" style="display: none;">
                                        <div class="form-group">
                                            <label class="control-label">Lý do hủy:</label>
                                            <?php $this->Mconstants->selectObject($listCancelReasons, 'CancelReasonId', 'CancelReasonName', 'CancelReasonId', 0, false, '--Chọn lý do--'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Ghi chú <span class="required">*</span></label>
                                            <input class="form-control" type="text" id="cancelComment" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" type="button" id="btnUpdateTransportStatus">Hoàn thành</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>