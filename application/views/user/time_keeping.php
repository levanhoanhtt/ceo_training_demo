<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <style>
                    .alert h4{margin: auto;text-align: center;}
                    .divKeeping, .divKeepingType span{cursor: pointer;}
                    .divKeeping{margin-bottom: 0;}
                    .divKeepingType{height: 51px;margin-bottom: 20px;}
                    .divKeepingType .first{border-right: 1px solid #159b95;}
                    .divKeepingType .active{border: 1px solid #159b95;}
                    .divKeepingType i{color: #159b95;}
                    #tbodyUserOnline .fa-circle{color: rgb(66, 183, 42);font-size: 8px;}
                </style>
                <?php $userTimeKeepingId = $userTimeKeeping ? $userTimeKeeping['UserTimeKeepingId'] : 0;
                $keepingTypeId = $userTimeKeeping ? $userTimeKeeping['KeepingTypeId'] : 1; ?>
                <div class="row">
                    <div class="col-sm-7 no-padding">
                        <div class="box box-default padding20">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?php $avatar = (empty($user['Avatar']) ? NO_IMAGE : $user['Avatar']); ?>
                                        <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 100%;">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <h3 class="profile-username text-center"><?php echo $user['FullName']; ?></h3>
                                        </li>
                                        <li class="list-group-item">
                                            <?php foreach ($listUserParts as $up){ ?>
                                                <p>
                                                    <b><?php echo $this->Mconstants->getObjectValue($listParts, 'PartId', $up['PartId'], 'PartName'); ?></b>
                                                    <a class="pull-right" href="javascript:void(0)"><?php echo $this->Mconstants->getObjectValue($listRoles, 'RoleId', $up['RoleId'], 'RoleName'); ?></a>
                                                </p>
                                            <?php } ?>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Mã nhân viên</b>
                                            <a class="pull-right" href="javascript:void(0)"><?php echo $user['UserName']; ?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Ngày sinh</b>
                                            <a class="pull-right" href="javascript:void(0)"><?php echo ddMMyyyy($user['BirthDay']); ?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Số điện thoại</b>
                                            <a class="pull-right" href="javascript:void(0)"><?php echo $user['PhoneNumber']; ?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Email</b>
                                            <a class="pull-right" href="javascript:void(0)"><?php echo $user['Email']; ?></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default padding20" style="padding-top: 10px;">
                            <div class="box-header with-border text-center">
                                <h3 class="box-title">Lịch sử</h3>
                            </div>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" style="vertical-align: middle;">Ngày</th>
                                        <th rowspan="2" class="text-center" style="vertical-align: middle;">Lần</th>
                                        <th colspan="3" class="text-center">Công OFF</th>
                                        <th colspan="3" class="text-center">Công ON</th>
                                        <th rowspan="2" style="vertical-align: middle;">Tổng (h)</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">Vào</th>
                                        <th class="text-center">Ra</th>
                                        <th class="text-center">Thời lượng (h)</th>
                                        <th class="text-center">Vào</th>
                                        <th class="text-center">Ra</th>
                                        <th class="text-center">Thời lượng (h)</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyTimeLog">
                                    <?php $i = 0;
                                    $keepingDateOld = '';
                                    $timeDates = array();
                                    foreach($listUserTimeKeepings as $utk){
                                        $keepingDate = ddMMyyyy($utk['KeepingDate']);
                                        $duration = round($utk['Duration']/60, 2);
                                        if(!isset($timeDates[$keepingDate])) $timeDates[$keepingDate] = $duration;
                                        else $timeDates[$keepingDate] += $duration;
                                        if($keepingDate != $keepingDateOld){
                                            $i = 1;
                                            $keepingDateOld = $keepingDate;
                                        }
                                        else $i++; ?>
                                        <tr data-id="<?php echo $keepingDate; ?>">
                                            <td class="tdMerge" rowspan="1"><?php echo $keepingDate; ?></td>
                                            <td class="text-center"><?php echo $i; ?></td>
                                            <?php if($utk['KeepingTypeId'] == 1){ ?>
                                                <td class="text-center"><?php echo ddMMyyyy($utk['DateTimeIn'], 'H:i'); ?></td>
                                                <td class="text-center"><?php echo ddMMyyyy($utk['DateTimeOut'], 'H:i'); ?></td>
                                                <td class="text-center text-primary"><?php if($utk['StatusId'] == 2) echo $duration; ?></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            <?php } else{ ?>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-center"><?php echo ddMMyyyy($utk['DateTimeIn'], 'H:i'); ?></td>
                                                <td class="text-center"><?php echo ddMMyyyy($utk['DateTimeOut'], 'H:i'); ?></td>
                                                <td class="text-center text-primary"><?php if($utk['StatusId'] == 2) echo $duration; ?></td>
                                            <?php } ?>
                                            <td class="text-center text-primary tdTotalTime tdMerge" rowspan="1"><?php if($utk['StatusId'] == 2) echo $duration; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php foreach($timeDates as $date => $duration){ ?>
                                <input type="text" hidden="hidden" id="duration_<?php echo str_replace('/', '_', $date); ?>" value="<?php echo $duration; ?>">
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="alert alert-warning">
                            <h4>HIỆN TẠI: <span id="spanTimeCurrent"></span> <?php echo date('d/m/Y'); ?></h4>
                        </div>
                        <div class="alert alert-success divKeeping" id="divKeepingIn"<?php if($userTimeKeepingId > 0) echo ' style="display: none;"'; ?>>
                            <h4><i class="fa fa-sign-in"></i> CHẤM CÔNG VÀO</h4>
                        </div>
                        <div class="input-group divKeepingType" id="divKeepingTypeIn"<?php if($userTimeKeepingId > 0) echo ' style="display: none;"'; ?>>
                            <span class="input-group-addon first<?php if($keepingTypeId == 1) echo ' active'; ?>" data-id="1"><?php if($keepingTypeId == 1) echo '<i class="fa fa-check"></i> '; ?>OFFICE</span>
                            <span class="input-group-addon<?php if($keepingTypeId == 2) echo ' active'; ?>" data-id="2"><?php if($keepingTypeId == 2) echo '<i class="fa fa-check"></i> '; ?>HOME</span>
                        </div>
                        <div class="alert alert-danger divKeeping" id="divKeepingOut"<?php if($userTimeKeepingId == 0) echo ' style="display: none;"'; ?>>
                            <h4><i class="fa fa-sign-out"></i> CHẤM CÔNG RA | <span id="spanRealDuration"></span></h4>
                        </div>
                        <div class="input-group divKeepingType" id="divKeepingTypeOut"<?php if($userTimeKeepingId == 0) echo ' style="display: none;"'; ?>>
                            <span class="input-group-addon" id="spanKeepingOut"><i class="fa fa-check"></i> <?php echo $keepingTypeId == 1 ? 'OFFLINE' : 'ONLINE'; ?></span>
                        </div>
                        <div class="box box-default padding20" style="padding-top: 10px;">
                            <div class="box-header with-border text-center">
                                <h3 class="box-title">Thống kê</h3>
                            </div>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">ON</th>
                                        <th class="text-center">OFF</th>
                                        <th class="text-center">Tổng</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Tuần công hiện tại</td>
                                        <td class="text-center"><?php echo $onThisWeek; ?></td>
                                        <td class="text-center"><?php echo $offThisWeek; ?></td>
                                        <td class="text-center"><?php echo $onThisWeek + $offThisWeek; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tháng công hiện tại</td>
                                        <td class="text-center"><?php echo $onThisMonth; ?></td>
                                        <td class="text-center"><?php echo $offThisMonth; ?></td>
                                        <td class="text-center"><?php echo $onThisMonth + $offThisMonth; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Chỉ tiêu đăng ký</td>
                                        <td colspan="3">
                                            <?php $percent = 0;
                                            if($registerThisWeek > 0) $percent = Math.ceil(($onThisWeek + $offThisWeek) /  $registerThisWeek * 100); ?>
                                            <div class="clearfix">
                                                <small class="pull-right"><?php echo $percent; ?>%</small>
                                            </div>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-green" style="width: <?php echo $percent; ?>%;"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Số lần chấm muộn</td>
                                        <td colspan="3" class="text-center"><?php echo $lateThisWeek; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Số lần quên chấm ra</td>
                                        <td colspan="3" class="text-center"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="box box-default padding20" style="padding-top: 10px;">
                            <div class="box-header with-border text-center">
                                <h3 class="box-title">Lịch trực tuần này của bạn</h3>
                            </div>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Ngày</th>
                                            <th class="text-center">Giờ vào</th>
                                            <th class="text-center">Giờ ra</th>
                                            <th class="text-center">Thời lượng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($listUserTimeKeepingRegisters as $utkr) { ?>
                                            <tr>
                                                <td class="text-center"><?php echo ddMMyyyy($utkr["KeepingDate"]); ?></td>
                                                <td class="text-center"><?php echo ddMMyyyy($utkr["DateTimeIn"],'H:i'); ?></td>
                                                <td class="text-center"><?php echo ddMMyyyy($utkr["DateTimeOut"],'H:i'); ?></td>
                                                <td  class="text-center text-primary"><?php echo round($utkr["Duration"]/60,2); ?></td> 
                                            </tr>
                                        <?php } ?>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="box box-default padding20" style="padding-top: 10px;">
                            <div class="box-header with-border text-center">
                                <h3 class="box-title">Nhân viên đang online</h3>
                            </div>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Avatar</th>
                                        <th>Nhân viên</th>
                                        <th>Chức vụ</th>
                                        <th class="text-center">Giờ vào</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyUserOnline">
                                    <?php foreach($listUserWorking as $u){
                                        $listUserParts = $this->Muserparts->getListCurrent($u['UserId']); ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo base_url('user/edit/'.$u['UserId']); ?>"><img class="img-table" src="<?php echo USER_PATH.$u['Avatar']; ?>" alt="<?php echo $u['FullName']; ?>"></a>
                                            </td>
                                            <td><a href="<?php echo base_url('user/edit/'.$u['UserId']); ?>"><?php echo $u['FullName']; ?> <i class="fa fa-circle"></i></a></td>
                                            <td>
                                                <?php foreach($listUserParts as $up) echo '<p>'.$this->Mconstants->getObjectValue($listRoles, 'RoleId', $up['RoleId'], 'RoleName').'</p>'; ?>
                                            </td>
                                            <td class="text-center"><?php echo ddMMyyyy($u['DateTimeIn'], 'H:i'); ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="keepingTypeId" value="<?php echo $keepingTypeId; ?>">
                <input type="text" hidden="hidden" id="userTimeKeepingId" value="<?php echo $userTimeKeepingId; ?>">
                <input type="text" hidden="hidden" id="dateTimeIn" value="<?php echo $userTimeKeeping ? $userTimeKeeping['DateTimeIn'] : ''; ?>">
                <input type="text" hidden="hidden" id="updateTimeKeepingUrl" value="<?php echo base_url('api/user/updateTimeKeeping'); ?>">
                <div class="modal fade" id="modalKeepingInOut" tabindex="-1" role="dialog" aria-labelledby="modalKeepingInOut">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><i class="fa fa-clock-o"></i> Xác nhận chấm công vào</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="control-label normal">Ghi chú</label>
                                    <input class="form-control" type="text" id="timeInOutComment">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" id="btnKeepingInOut">Hoàn thành</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>