<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li<?php if($isRegister == 0) echo ' class="active"'; ?>><a href="<?php echo base_url('user/timeKeepingList'); ?>">Chấm công thực tế</a></li>
                        <li<?php if($isRegister == 1) echo ' class="active"'; ?>><a href="<?php echo base_url('user/timeKeepingList/1'); ?>">Lịch đăng ký</a></li>
                    </ul>
                </div>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('user/timeKeepingList/'.$isRegister); ?>
                        <div class="row">
                        	<div class="col-sm-4">
                        		<div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" name="DateRangePicker" value="<?php echo set_value('DateRangePicker') ? set_value('DateRangePicker') : date('d/m/Y').' - '.date('d/m/Y'); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserId', set_value('UserId'), true, 'Nhân viên', ' select2'); ?>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                <button type="button" class="btn btn-info" id="showModalImport">Import lịch tuần</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <div class="box box-default padding20" style="padding-top: 10px;">
                        <div class="box-body table-responsive no-padding divTable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle;">Nhân viên</th>
                                    <th rowspan="2" style="vertical-align: middle;">Ngày</th>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle;">Lần</th>
                                    <th colspan="3" class="text-center">Công OFF</th>
                                    <th colspan="3" class="text-center">Công ON</th>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle;">Tổng (h)</th>
                                </tr>
                                <tr>
                                    <th class="text-center">Vào</th>
                                    <th class="text-center">Ra</th>
                                    <th class="text-center">Thời lượng (h)</th>
                                    <th class="text-center">Vào</th>
                                    <th class="text-center">Ra</th>
                                    <th class="text-center">Thời lượng (h)</th>
                                </tr>
                                </thead>
                                <tbody id="tbodyTimeLog">
                                <?php $i = 0;
                                $keepingDateOld = '';
                                $timeDates = array();
                                foreach($listUserTimeKeepings as $utk){
                                    $keepingDate = ddMMyyyy($utk['KeepingDate']);
                                    $duration = round($utk['Duration']/60, 2);
                                    if(!isset($timeDates[$keepingDate])) $timeDates[$keepingDate] = $duration;
                                    else $timeDates[$keepingDate] += $duration;
                                    if($keepingDate != $keepingDateOld){
                                        $i = 1;
                                        $keepingDateOld = $keepingDate;
                                    }
                                    else $i++; ?>
                                    <tr data-id="<?php echo $keepingDate; ?>">
                                        <td class="col-x" data-rowspan="1"><span hidden="hidden" class="spanMergeName"><?php echo $keepingDate.'-'.$utk['FullName']; ?></span><?php echo $utk['FullName']; ?></td>
                                        <td class="tdMerge" rowspan="1"><?php echo $keepingDate; ?></td>
                                        <td class="text-center"><?php echo $i; ?></td>
                                        <?php if($utk['KeepingTypeId'] == 1){ ?>
                                            <td class="text-center"><?php echo ddMMyyyy($utk['DateTimeIn'], 'H:i'); ?></td>
                                            <td class="text-center"><?php echo ddMMyyyy($utk['DateTimeOut'], 'H:i'); ?></td>
                                            <td class="text-center text-primary"><?php if($utk['StatusId'] == 2) echo $duration; ?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        <?php } else{ ?>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="text-center"><?php echo ddMMyyyy($utk['DateTimeIn'], 'H:i'); ?></td>
                                            <td class="text-center"><?php echo ddMMyyyy($utk['DateTimeOut'], 'H:i'); ?></td>
                                            <td class="text-center text-primary"><?php if($utk['StatusId'] == 2) echo $duration; ?></td>
                                        <?php } ?>
                                        <td class="text-center text-primary tdTotalTime tdMerge" rowspan="1"><?php if($utk['StatusId'] == 2) echo $duration; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <?php foreach($timeDates as $date => $duration){ ?>
                            <input type="text" hidden="hidden" id="duration_<?php echo str_replace('/', '_', $date); ?>" value="<?php echo $duration; ?>">
                        <?php } ?>
                    </div>
                </div>
            </section>
            <div class="modal fade" id="modalImportExcel" tabindex="-1" role="dialog" aria-labelledby="modalImportExcel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Import Lịch tuần from Excel</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" value="" class="form-control" id="fileExcelUrl" placeholder="File Excel" disabled>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat" id="btnUploadExcel">Upload</button>
                                    </span>
                                </div>
                            </div>
                            <img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter" style="display: none;">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" id="btnImportExcel">Import</button>
                            <input type="text" hidden="hidden" id="importTimeKeepingUrl" value="<?php echo base_url('api/user/importTimeKeepingExcel'); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>