<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo" style="margin-bottom: 0 !important;"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a id="aGuaranteeList" href="<?php echo base_url('guarantee'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php echo form_open('api/guarantee/update', array('id' => 'guaranteeForm')); ?>
                <div class="row">
                    <div class="col-sm-9 no-padding">
                        <style>
                            .nav-tabs-custom>.nav-tabs>li>a.active{color: #3c8dbc !important;}
                            .tbodyProduct .trPadding{padding: 0 2px;}
                        </style>
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="ulGuaranteeStep">
                                <?php foreach($this->Mconstants->guaranteeSteps as $i => $v){ ?>
                                    <li<?php if($i == 2) echo ' class="active"'; ?>>
                                        <a href="javascript:void(0)" href="#tab_<?php echo $i; ?>" data-id="<?php echo $i; ?>"<?php if($i == 2) echo ' class="active"'; ?>><?php echo $v; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <p>Ngày nhận: <?php echo date('d/m/Y H:i'); ?></p>
                                <p>Bộ phận: Kho vận</p>
                                <p>Người xử lý: <?php echo $user['FullName']; ?></p>
                                <hr class="hr-ths">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>Danh sách sản phẩm đã nhận</p>
                                        <p><button type="button" class="btn btn-default" id="btnAddProduct">Thêm sản phẩm BH</button></p>
                                        <p>Danh sách sản phẩm đã nhận</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p><a href="javascript:void(0)" id="aGuaranteeImage"><i class="fa fa-file-image-o"></i> Ảnh chụp toàn bộ kiện hàng</a></p>
                                        <p><a href="" id="aGuaranteeImage1" target="_blank"><img src="" id="imgGuaranteeImage" style="display: none;height: 70px;"></a></p>
                                    </div>
                                </div>
                                <div class="table-responsive no-padding divTable divTableGuarantee" id="divTableGuarantee_1">
                                    <table class="table table-hover table-bordered-bottom">
                                        <thead class="theadNormal">
                                        <tr>
                                            <th style="width: 80px;">ĐH</th>
                                            <th>Sản phẩm</th>
                                            <th style="width: 60px;">Đơn vị</th>
                                            <th style="width: 50px;">SL</th>
                                            <th style="width: 90px;">Ngày mua</th>
                                            <th style="width: 110px;">Hình thức</th>
                                            <th style="width: 200px;">Phụ kiện</th>
                                            <th class="text-center" style="width: 60px;">Ảnh</th>
                                            <th style="width: 30px;"></th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbodyProduct" id="tbodyProduct_1"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="box box-default" id="boxChooseCustomer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Khách hàng</h3>
                            </div>
                            <div class="box-body">
                                <div class="box-search-advance customer">
                                    <div>
                                        <input type="text" class="form-control textbox-advancesearch" id="txtSearchCustomer" placeholder="Tìm khách hàng (F4)">
                                    </div>
                                    <div class="panel panel-default" id="panelCustomer">
                                        <div class="panel-body">
                                            <div class="list-search-data">
                                                <div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
                                                <ul id="ulListCustomers"></ul>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default" id="btnPrevCustomer"><i class="fa fa-chevron-left"></i></button>
                                                <button type="button" class="btn btn-default" id="btnNextCustomer"><i class="fa fa-chevron-right"></i></button>
                                                <input type="text" hidden="hidden" id="pageIdCustomer" value="1">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default mh-wrap-customer" id="divCustomer" style="display: none;">
                            <div class="with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" id="btnCloseBoxCustomer"><i class="fa fa-times" style="font-size: 18px;color:#777"></i></button>
                                </div>
                                <div class="mh-info-customer">
                                    <img class="avatar-user" src="assets/vendor/dist/img/users.png">
                                    <div class="name-info">
                                        <h4 class="i-name"></h4>
                                        <div class="phones i-phone"></div>
                                    </div>
                                    <div class="i-cusType">
                                        <span class="label label-success"></span>
                                    </div>
                                    <div class="total-orders">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>Số đơn hàng &nbsp;:&nbsp;&nbsp;<span class="i-total-orders"></span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-money" aria-hidden="true"></i><span>Số dư tài khoản :&nbsp;&nbsp;<span id="customerBalance">0</span> ₫</span>
                                    </div>
                                    <div class="bank">
                                        <i class="fa fa-question-circle" aria-hidden="true"></i><span>BH gần nhất &nbsp;:&nbsp;&nbsp;<span id="spanLastGuarantee"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-default classify padding20">
                            <div class="form-group">
                                <label class="control-label light-blue">CS xử lý</label>
                                <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', 0, true, '--Chọn cơ sở--', ' hmdrequiredNumber', ' data-field="Cơ sở"'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label light-blue">Phương thức nhận hàng</label>
                                <?php $this->Mconstants->selectObject($listReceiptTypes, 'ReceiptTypeId', 'ReceiptTypeName', 'ReceiptTypeId', 0, true, '--Loại phương thức--'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label light-blue">Phương thức thanh toán</label>
                                <?php $this->Mconstants->selectObject($listPaymentTypes, 'PaymentTypeId', 'PaymentTypeName', 'PaymentTypeId', 0, true, '--Chọn phương thức--'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label light-blue">Phí ship thực tế (nếu có)</label>
                                <input type="text" class="form-control cost text-right" id="shipCost" value="0">
                            </div>
                        </div>
                        <div class="box box-default classify padding20">
                            <label class="light-blue">Ghi chú nội bộ</label>
                            <div class="box-transprt clearfix mb10">
                                <button type="button" class="btn-updaten save btnInsertComment" data-id="1">
                                    Lưu
                                </button>
                                <input type="text" class="add-text" id="comment_1" value="">
                            </div>
                            <div class="listComment" id="listComment_1"></div>
                        </div>
                        <div class="box box-default more-task">
                            <!--<a href="javascript:void(0)" class="task1 aRemind"><img src="assets/vendor/dist/img/icon07.png">Thêm task chăm sóc khách hàng</a>-->
                            <a href="javascript:void(0)" class="aRemind"><img src="assets/vendor/dist/img/icon08.png">Tạo chương trình nhắc nhở</a>
                        </div>
                        <div class="box box-default padding20">
                            <ul class="list-group text-center">
                                <li style="margin-bottom: 10px;"><button type="button" class="btn btn-primary" id="btnSaveGuarantee">Lưu thay đổi</button></li>
                                <!--<li><button type="button" class="btn btn-primary">Chuyển sang CSKH</button></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getListByCustomerGuarantee'); ?>">
                <input type="text" hidden="hidden" id="getListAccessoryUrl" value="<?php echo base_url('api/product/getListAccessories'); ?>">
                <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                <input type="text" hidden="hidden" id="editOrderUrl" value="<?php echo base_url('order/edit'); ?>">
                <input type="text" hidden="hidden" id="editProductUrl" value="<?php echo base_url('product/edit'); ?>">
                <input type="text" hidden="hidden" id="guaranteeId" value="0">
                <input type="text" hidden="hidden" id="customerId" value="0">
                <input type="text" hidden="hidden" id="guaranteeStep" value="2">
                <input type="text" hidden="hidden" id="guaranteeStatusId" value="1">
                <input type="text" hidden="hidden" id="currentGuaranteeStep" value="1">
                <input type="text" hidden="hidden" id="productIndex" value="0">
                <input type="text" hidden="hidden" id="canEditAll" value="<?php echo $canEditAll ? 1 : 0 ?>">
                <input type="text" hidden="hidden" id="curentProductIndex" value="0">
                <?php echo $this->Mconstants->selectObject($listUsageStatus, 'UsageStatusId', 'UsageStatusName', 'UsageStatusIdOriginal', 0, false, '', '', ' style="display:none;"'); ?>
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/remind'); ?>
                <?php $this->load->view('guarantee/modal'); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>