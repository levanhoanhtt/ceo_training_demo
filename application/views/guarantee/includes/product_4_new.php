<div class="divTableGuarantee" id="divTableGuarantee_4"<?php if($guarantee['GuaranteeStep'] != 4) echo ' style="display: none;"'; ?>>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs" id="ulGuaranteeStep4">
            <li class="active"><a href="#tab_4_1" data-toggle="tab" data-id="1">Phương án xử lý</a></li>
            <li><a href="#tab_4_2" data-toggle="tab" data-id="2">Tài chính và chốt</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_4_1">
                <div class="table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered-bottom">
                        <thead class="theadNormal">
                        <tr>
                            <th style="width: 80px;">ĐH</th>
                            <th>Sản phẩm</th>
                            <th style="width: 110px;">Ngày mua</th>
                            <th style="width: 140px;">TT sản phẩm</th>
                            <th style="width: 140px;">Kết luận từ KT</th>
                            <th style="width: 140px;">Yêu cầu KH</th>
                            <th style="width: 180px;">Kết luận chốt</th>
                        </tr>
                        </thead>
                        <tbody class="tbodyProduct" id="tbodyProduct_4">
                        <?php $i = 0;
                        foreach($listGuaranteeProducts as $op){
                            $i++;
                            $productAccessoryIds = array();
                            if(!empty($op['ProductAccessoryIds'])) $productAccessoryIds = json_decode($op['ProductAccessoryIds'], true);
                            $productName = $products[$op['ProductId']]['ProductName'];
                            //$productImage = $products[$op['ProductId']]['ProductImage'];
                            $productUnitId = $products[$op['ProductId']]['ProductUnitId'];
                            $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
                            $productChildName = '';
                            if($op['ProductChildId'] > 0){
                                $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                                //$productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                                $guaranteeMonth = $productChilds[$op['ProductChildId']]['GuaranteeMonth'];
                            }
                            //if(empty($productImage)) $productImage = NO_IMAGE; ?>
                            <tr data-index="<?php echo $i; ?>" class="trProductMain trProduct_4_<?php echo $i; ?>" data-id="<?php echo $op['GuaranteeProductId']; ?>" data-product="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>" data-order="<?php echo $op['OrderId']; ?>">
                                <td><a href="<?php echo base_url('order/edit/'.$op['OrderId']); ?>" target="_blank"><?php echo $this->Morders->genOrderCode($op['OrderId']); ?></a></td>
                                <td>
                                    <a href="<?php echo base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark" target="_blank">
                                        <?php echo $productName;
                                        if(!empty($productChildName)) echo ' ('.$productChildName.')'; ?>
                                    </a>
                                </td>
                                <td>
                                    Mua: <?php echo ddMMyyyy($orders[$op['OrderId']]['CrDateTime']); ?><br>
                                    BH: <?php echo $guaranteeMonth-$this->Mguarantees->monthDiff($orders[$op['OrderId']]['CrDateTime'], $guarantee['CrDateTime']); ?>/<?php echo $guaranteeMonth; ?> tháng
                                </td>
                                <td>
                                    - Hình thức: <?php echo $this->Mconstants->getObjectValue($listUsageStatus, 'UsageStatusId', $op['UsageStatusId1'], 'UsageStatusName'); ?><br/>
                                    - Tình trạng SD: <?php echo $this->Mconstants->getObjectValue($listUsageStatus, 'UsageStatusId', $op['UsageStatusId2'], 'UsageStatusName'); ?><br/>
                                    - Tình trạng PK: <?php echo count($productAccessoryIds).'/ '.count($productAccessories[$op['ProductId']]); ?>
                                </td>
                                <td class="trPadding">
                                    Hướng xử lý<br>
                                    <select class="form-control guaranteeSolutionId1" disabled>
                                        <option value="0">--Chọn--</option>
                                        <?php foreach($this->Mconstants->guaranteeSolutions as $j => $v){ ?>
                                            <option value="<?php echo $j; ?>"<?php if($j == $op['GuaranteeSolutionId1']) echo ' selected="selected"'; ?>><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    Lỗi do ai<br>
                                    <select class="form-control usageStatusId3" disabled>
                                        <option value="0">--Chọn--</option>
                                        <?php foreach($listUsageStatus as $us){
                                            if($us['UsageStatusTypeId'] == 2 && $us['ParentUsageStatusId'] > 0){ ?>
                                                <option value="<?php echo $us['UsageStatusId']; ?>" data-id="<?php echo $us['ParentUsageStatusId']; ?>"<?php if($us['UsageStatusId'] == $op['UsageStatusId3']) echo ' selected="selected"'; if($us['ParentUsageStatusId'] != $op['UsageStatusId2']) echo ' style="display: none;"'; ?>><?php echo $us['UsageStatusName']; ?></option>
                                            <?php } } ?>
                                    </select>
                                </td>
                                <td class="trPadding">
                                    Ban đầu muốn<br>
                                    <select class="form-control guaranteeSolutionId2">
                                        <option value="0">--Chọn--</option>
                                        <?php foreach($this->Mconstants->guaranteeSolutions as $j => $v){ ?>
                                            <option value="<?php echo $j; ?>"<?php if($j == $op['GuaranteeSolutionId2']) echo ' selected="selected"'; ?>><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    Hiện tại muốn<br>
                                    <select class="form-control guaranteeSolutionId3">
                                        <option value="0">--Chọn--</option>
                                        <?php foreach($this->Mconstants->guaranteeSolutions as $j => $v){ ?>
                                            <option value="<?php echo $j; ?>"<?php if($j == $op['GuaranteeSolutionId3']) echo ' selected="selected"'; ?>><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td class="trPadding">
                                    Điều kiện KH<br>
                                    <select class="form-control customerConditionId">
                                        <option value="0">--Chọn--</option>
                                        <option value="1"<?php if(1 == $op['CustomerConditionId']) echo ' selected="selected"'; ?>>Không đủ ĐK</option>
                                        <option value="2"<?php if(2 == $op['CustomerConditionId']) echo ' selected="selected"'; ?>>Đủ ĐK</option>
                                        <option value="3"<?php if(3 == $op['CustomerConditionId']) echo ' selected="selected"'; ?>>Không nhưng Hỗ trợ</option>
                                    </select>
                                    Phương án xử lý<br>
                                    <select class="form-control guaranteeSolutionId4">
                                        <option value="0">--Chọn--</option>
                                        <?php foreach($this->Mconstants->guaranteeSolutions as $j => $v){ ?>
                                            <option value="<?php echo $j; ?>"<?php if($j == $op['GuaranteeSolutionId4']) echo ' selected="selected"'; ?>><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control guaranteeSolutionId5">
                                        <option value="0">--Chọn--</option>
                                        <option value="1"<?php if(1 == $op['GuaranteeSolutionId5']) echo ' selected="selected"'; ?>>Sữa chữa</option>
                                        <option value="2"<?php if(2 == $op['GuaranteeSolutionId5']) echo ' selected="selected"'; ?>>Bàn giao lại hàng</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="trPadding trProduct_4_<?php echo $i; ?>">
                                <td colspan="7">
                                    <div class="input-group" style="width: 100%;">
                                        <span class="input-group-addon">BPVC</span>
                                        <input type="text" class="form-control comment1_<?php echo $op['GuaranteeProductId']; ?>" id="productComment_4_<?php echo $i; ?>" value="<?php echo $op['Comment1']; ?>" placeholder="Ghi chú mô tả">
                                        <span class="input-group-addon spanComment" data-id="<?php echo $op['GuaranteeProductId']; ?>" data-no="1"><i class="fa fa-paper-plane"></i></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="trPadding trProduct_4_<?php echo $i; ?>">
                                <td colspan="7">
                                    <div class="input-group" style="width: 100%;">
                                        <span class="input-group-addon">BPKT</span>
                                        <input type="text" class="form-control comment2_<?php echo $op['GuaranteeProductId']; ?>" id="productComment_4_<?php echo $i; ?>" value="<?php echo $op['Comment2']; ?>" placeholder="Ghi chú mô tả">
                                        <span class="input-group-addon spanComment" data-id="<?php echo $op['GuaranteeProductId']; ?>" data-no="2"><i class="fa fa-paper-plane"></i></span>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="tab_4_2">
                <div class="row">
                    <div class="col-lg-4 col-xs-6">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <p>Tổng phát sinh thu KH</p>
                                <h3><span id="spanCost1">0</span> đ</h3>
                            </div>
                            <div class="icon">
                                <i class="fa fa-bar-chart"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-6">
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <p>Tổng phát sinh hoàn KH</p>
                                <h3><span id="spanCost2">0</span> đ</h3>
                            </div>
                            <div class="icon">
                                <i class="fa fa-bar-chart"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-6">
                        <div class="small-box bg-green">
                            <div class="inner">
                                <p>Tổng cần thu thêm khách</p>
                                <h3><span id="spanCost3">0</span> đ</h3>
                            </div>
                            <div class="icon">
                                <i class="fa fa-bar-chart"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive no-padding divTable">
                    <table class="table table-bordered ">
                        <thead class="theadNormal">
                        <!-- <tr>
                            <th style="width: 80px;">ĐH</th>
                            <th>Sản phẩm</th>
                            <th style="width: 110px;">Ngày mua</th>
                            <th class="text-center" style="width: 50px;">TTSP</th>
                            <th style="width: 140px;">Giá đã mua</th>
                            <th style="width: 160px;">Phí phát sinh</th>
                            <th style="width: 120px;">Tiền còn lại KH</th>
                            <th style="width: 180px;">Phương án xử lý</th>
                        </tr> -->
                        <tr>
						  	<th rowspan="2" class="text-center" style="vertical-align : middle;">ĐH</th>
						    <th colspan="3" class="text-center" style="vertical-align : middle;">TTSP</th>
						    <th rowspan="2" class="text-center" style="vertical-align : middle;">Giá mua</th>
						    <th colspan="2" class="text-center" style="vertical-align : middle;">Phí phát sinh</th>
						    <th rowspan="2" class="text-center" style="vertical-align : middle;">Tiền còn lại</th>
						    <th colspan="2" class="text-center" style="vertical-align : middle;" >Phương án xử lý</th>
						  </tr>
						  <tr>
						  	<th class="text-center" style="vertical-align : middle;">Tên SP</th>
						    <th class="text-center" style="vertical-align : middle;">Ngày mua</th>
						    <th class="text-center" style="vertical-align : middle;">Bảo hành</th>
						    <th class="text-center" style="width: 80px;vertical-align : middle">(%) phát sinh</th>
						    <th class="text-center" style="width: 100px;vertical-align : middle;">Tiền phát sinh</th>
						    <th class="text-center" style="width: 100px;vertical-align : middle">P/A chính</th>
						    <th class="text-center" style="width: 100px;vertical-align : middle">P/A phụ</th>
						  </tr>
                        </thead>
                        <tbody id="tbodyProductMoney">
                        <?php $orderProducts = array();
                        foreach($listGuaranteeProducts as $op){
                            if(!isset($orderProducts[$op['OrderId']])) $orderProducts[$op['OrderId']] = $this->Morderproducts->getBy(array('OrderId' => $op['OrderId']));
                            $productAccessoryIds = array();
                            if(!empty($op['ProductAccessoryIds'])) $productAccessoryIds = json_decode($op['ProductAccessoryIds'], true);
                            $productName = $products[$op['ProductId']]['ProductName'];
                            $productPrice = $products[$op['ProductId']]['Price'];
                            //$productImage = $products[$op['ProductId']]['ProductImage'];
                            $productUnitId = $products[$op['ProductId']]['ProductUnitId'];
                            $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
                            $productChildName = '';
                            if($op['ProductChildId'] > 0){
                                $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                                $productPrice = $productChilds[$op['ProductChildId']]['Price'];
                                //$productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                                $guaranteeMonth = $productChilds[$op['ProductChildId']]['GuaranteeMonth'];
                            }
                            $diffCost = -$op['ReduceNumber'];
                            //if(empty($productImage)) $productImage = NO_IMAGE; ?>
                            <tr data-index="<?php echo $i; ?>" class="trProductMain trProduct_4_<?php echo $i; ?>" data-id="<?php echo $op['GuaranteeProductId']; ?>" data-product="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>" data-order="<?php echo $op['OrderId']; ?>">
                                <td style="vertical-align : middle;"><a href="<?php echo base_url('order/edit/'.$op['OrderId']); ?>" target="_blank"><?php echo $this->Morders->genOrderCode($op['OrderId']); ?></a></td>
                                <td style="vertical-align : middle;">
                                    <a href="<?php echo base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark" target="_blank">
                                        <?php echo $productName;
                                        if(!empty($productChildName)) echo ' ('.$productChildName.')'; ?>
                                    </a>
                                    <a href="#" title="Thông tin sản phẩm"><i class="fa fa-info"></i></a>
                                </td>
                                <td style="vertical-align : middle;">
                                    <?php echo ddMMyyyy($orders[$op['OrderId']]['CrDateTime']); ?><br>
                                </td>
                                <td style="vertical-align : middle;"><?php echo $guaranteeMonth-$this->Mguarantees->monthDiff($orders[$op['OrderId']]['CrDateTime'], $guarantee['CrDateTime']); ?>/<?php echo $guaranteeMonth; ?> tháng</td>
                                <!-- <td class="text-center"><i class="fa fa-info"></i></td> -->
                                <td style="vertical-align : middle;">
                                    <?php $totalPrice = 0;
                                    $price = 0;
                                    foreach($orderProducts[$op['OrderId']] as $p){
                                        $totalPrice += $p['Quantity'] * $p['Price'];
                                        if($p['ProductId'] == $op['ProductId'] && $p['ProductChildId'] == $op['ProductChildId']) $price = $p['Price'];
                                    }
                                    if($price > 0){
                                        $diffCost += $price;
                                        echo '<span class="spanPrice">'.priceFormat($price).'</span>';
                                        if($price != $productPrice) echo '<br>HT: '.priceFormat($productPrice);
                                        if($orders[$op['OrderId']]['Discount'] > 0){
                                            $discount = ceil($orders[$op['OrderId']]['Discount'] / $totalPrice * $price);
                                            $diffCost -= $diffCost;
                                            echo '<br>KM: <span class="spanDiscount">'.priceFormat($discount).'</span>';
                                        }
                                    }
                                    ?>
                                </td>
                                <td class="trPadding" style="vertical-align : middle;">
                                    <!--<div class="input-group">
                                        <span class="input-group-addon spanControl has-border<?php //if($op['ReduceTypeId'] == 2) echo ' active'; ?>" data-id="2">Tăng</span>
                                        <span class="input-group-addon spanControl<?php //if($op['ReduceTypeId'] == 1) echo ' active'; ?>" data-id="1">Giảm</span>
                                    </div>-->
                                    <div class="input-group">
                                        <input type="text" class="form-control percent" value="<?php echo $op['ReducePercent']; ?>" style="padding-left: 3px; padding-right: 3px"
                                        >
                                        <span class="input-group-addon" style="width: 15px;">%</span>
                                    </div>
                                    
                                </td>
                                <td class="trPadding" style="vertical-align : middle;">
                                	<div class="input-group">
                                        <input type="text" class="form-control number" value="<?php echo priceFormat($op['ReduceNumber']); ?>">
                                        <span class="input-group-addon" style="width: 37px;">đ</span>
                                    </div>
                                </td>
                                <td class="trPadding" style="vertical-align : middle;"><input type="text" class="form-control diffCost" value="<?php echo priceFormat($diffCost); ?>" disabled></td>
                                <td class="trPadding" style="vertical-align : middle;">
                                    <select class="form-control guaranteeSolutionId4" disabled>
                                        <option value="0">-Chọn-</option>
                                        <?php foreach($this->Mconstants->guaranteeSolutions as $j => $v){ ?>
                                            <option value="<?php echo $j; ?>"<?php if($j == $op['GuaranteeSolutionId4']) echo ' selected="selected"'; ?>><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    
                                </td>
                                <td class="trPadding" style="vertical-align : middle;">
                                	<select class="form-control guaranteeSolutionId5" disabled>
                                        <option value="0">-Chọn-</option>
                                        <option value="1"<?php if(1 == $op['GuaranteeSolutionId5']) echo ' selected="selected"'; ?>>Sữa chữa</option>
                                        <option value="2"<?php if(2 == $op['GuaranteeSolutionId5']) echo ' selected="selected"'; ?>>Bàn giao lại hàng</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>