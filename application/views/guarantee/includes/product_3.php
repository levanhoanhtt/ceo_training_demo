<div class="table-responsive no-padding divTable divTableGuarantee" id="divTableGuarantee_3"<?php if($guarantee['GuaranteeStep'] != 3) echo ' style="display: none;"'; ?>>
    <table class="table table-hover table-bordered-bottom">
        <thead class="theadNormal">
        <tr>
            <th style="width: 80px;">ĐH</th>
            <th>Sản phẩm</th>
            <th style="width: 50px;">SL</th>
            <th style="width: 110px;">Ngày mua</th>
            <th style="width: 110px;">Hình thức</th>
            <th style="width: 140px;">TT sử dụng</th>
            <th class="text-center" style="width: 60px;">Video</th>
            <th class="text-center" style="width: 60px;">Ảnh</th>
            <th style="width: 180px;">Gợi ý xử lý KT</th>
        </tr>
        </thead>
        <tbody class="tbodyProduct" id="tbodyProduct_3">
        <?php $i = 0;
        foreach($listGuaranteeProducts as $op){
            $i++;
            $productAccessoryIds = array();
            if(!empty($op['ProductAccessoryIds'])) $productAccessoryIds = json_decode($op['ProductAccessoryIds'], true);
            $productName = $products[$op['ProductId']]['ProductName'];
            //$productImage = $products[$op['ProductId']]['ProductImage'];
            $productUnitId = $products[$op['ProductId']]['ProductUnitId'];
            $guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
            $productChildName = '';
            if($op['ProductChildId'] > 0){
                $productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
                //$productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
                $guaranteeMonth = $productChilds[$op['ProductChildId']]['GuaranteeMonth'];
            }
            //if(empty($productImage)) $productImage = NO_IMAGE; ?>
            <tr data-index="<?php echo $i; ?>" class="trProductMain trProduct_3_<?php echo $i; ?>" data-id="<?php echo $op['GuaranteeProductId']; ?>" data-product="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>" data-order="<?php echo $op['OrderId']; ?>">
                <td><a href="<?php echo base_url('order/edit/'.$op['OrderId']); ?>" target="_blank"><?php echo $this->Morders->genOrderCode($op['OrderId']); ?></a></td>
                <td>
                    <a href="<?php echo base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark" target="_blank">
                        <?php echo $productName;
                        if(!empty($productChildName)) echo ' ('.$productChildName.')'; ?>
                    </a>
                </td>
                <td class="quantity">1 <?php echo $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'); ?></td>
                <td>
                    Mua: <?php echo ddMMyyyy($orders[$op['OrderId']]['CrDateTime']); ?><br>
                    BH: <?php echo $guaranteeMonth-$this->Mguarantees->monthDiff($orders[$op['OrderId']]['CrDateTime'], $guarantee['CrDateTime']); ?>/<?php echo $guaranteeMonth; ?> tháng
                </td>
                <td class="trPadding">
                    <select class="form-control usageStatusId1">
                        <option value="0">--Chọn--</option>
                        <?php foreach($listUsageStatus as $us){
                            if($us['UsageStatusTypeId'] == 1){ ?>
                            <option value="<?php echo $us['UsageStatusId']; ?>"<?php if($us['UsageStatusId'] == $op['UsageStatusId1']) echo ' selected="selected"'; ?>><?php echo $us['UsageStatusName']; ?></option>
                        <?php } } ?>
                    </select>
                </td>
                <td class="trPadding">
                    <select class="form-control usageStatusId2">
                        <option value="0">--Chọn--</option>
                        <?php foreach($listUsageStatus as $us){
                            if($us['UsageStatusTypeId'] == 2 && $us['ParentUsageStatusId'] == 0){ ?>
                                <option value="<?php echo $us['UsageStatusId']; ?>"<?php if($us['UsageStatusId'] == $op['UsageStatusId2']) echo ' selected="selected"'; ?>><?php echo $us['UsageStatusName']; ?></option>
                            <?php } } ?>
                    </select>
                    <select class="form-control usageStatusId3">
                        <option value="0">--Chọn--</option>
                        <?php foreach($listUsageStatus as $us){
                            if($us['UsageStatusTypeId'] == 2 && $us['ParentUsageStatusId'] > 0){ ?>
                                <option value="<?php echo $us['UsageStatusId']; ?>" data-id="<?php echo $us['ParentUsageStatusId']; ?>"<?php if($us['UsageStatusId'] == $op['UsageStatusId3']) echo ' selected="selected"'; if($us['ParentUsageStatusId'] != $op['UsageStatusId2']) echo ' style="display: none;"'; ?>><?php echo $us['UsageStatusName']; ?></option>
                            <?php } } ?>
                    </select>
                </td>
                <td class="text-center">
                    <a href="javascript:void(0)" class="aProductVideo" data-video="<?php echo $op['VideoUrl']; ?>">
                        <i class="fa fa-youtube-play"></i>
                    </a>
                </td>
                <td class="text-center">
                    <a href="javascript:void(0)" class="aProductImage">
                        <?php if(!empty($op['ProductImage2'])) echo '<img src="'.GUARANTEE_PATH.$op['ProductImage2'].'" style="height: 50px;">';
                        else echo '<i class="fa fa-file-image-o"></i>'; ?>
                    </a>
                </td>
                <td class="trPadding">
                    <select class="form-control guaranteeSolutionId1">
                        <option value="0">--Chọn--</option>
                        <?php foreach($this->Mconstants->guaranteeSolutions as $j => $v){ ?>
                            <option value="<?php echo $j; ?>"<?php if($j == $op['GuaranteeSolutionId1']) echo ' selected="selected"'; ?>><?php echo $v; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr class="trPadding trProduct_3_<?php echo $i; ?>">
                <td colspan="9">
                    <div class="input-group" style="width: 100%;">
                        <input type="text" class="form-control comment2_<?php echo $op['GuaranteeProductId']; ?>" id="productComment_3_<?php echo $i; ?>" value="<?php echo $op['Comment2']; ?>" placeholder="Ghi chú mô tả">
                        <span class="input-group-addon spanComment" data-id="<?php echo $op['GuaranteeProductId']; ?>" data-no="2"><i class="fa fa-paper-plane"></i></span>
                    </div>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>