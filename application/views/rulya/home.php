<?php $this->load->view('rulya/includes/header'); ?>
<div class="main">
    <div class="main-slider">
        <div class="bxslider">
            <?php foreach($listSliders as $slider){ ?>
                <div><a href="<?php echo empty($slider['SliderLink']) ? 'javascript:void(0)' : $slider['SliderLink']; ?>"><img src="<?php echo IMAGE_PATH.$slider['SliderImage']; ?>" alt="<?php echo $slider['SliderName']; ?>"></a></div>
            <?php } ?>
        </div>
        <div class="wrapper">
            <section class="section-product-top">
                <div class="container">
                    <h2 class="ttl-cmn">SẢN PHẨM NỔI BẬT</h2>
                    <div class="row">
                        <?php foreach($listProducts as $product){ ?>
                            <div class="col-sm-6 col-md-3 col-xs-12">
                                <?php $this->load->view('rulya/includes/product', array('product' => $product)); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <div class="banner-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-12 wow fadeInDown"><a href="#" class="trans"><img
                                    src="assets/front/rulya/img/banner01.jpg" alt="img"/></a></div>
                        <div class="col-sm-6 col-md-6 col-xs-12 wow fadeInDown"><a href="#" class="trans"><img
                                    src="assets/front/rulya/img/banner02.jpg" alt="img"/></a></div>
                        <div class="col-sm-6 col-md-6 col-xs-12 wow fadeInDown"><a href="#" class="trans"><img
                                    src="assets/front/rulya/img/banner03.jpg" alt="img"/></a></div>
                        <div class="col-sm-6 col-md-6 col-xs-12 wow fadeInDown"><a href="#" class="trans"><img
                                    src="assets/front/rulya/img/banner04.jpg" alt="img"/></a></div>
                    </div>
                </div>
            </div>
            <section class="section-join-with-us">
                <div class="container">
                    <h2 class="ttl-cmn">THAM GIA CÙNG CHÚNG TÔI</h2>
                    <div class="row pd15">
                        <div class="logo-com col-sm-6 col-md-3 col-xs-6 wow fadeInDown" data-wow-delay="0.2s">
                            <div class="inner"><a class="trans" href="#"><img src="assets/front/rulya/img/image.png" alt=" "/></a></div>
                        </div>
                        <div class="logo-com col-sm-6 col-md-3 col-xs-6 wow fadeInDown" data-wow-delay="0.3s">
                            <div class="inner"><a class="trans" href="#"><img src="assets/front/rulya/img/image.png" alt=" "/></a></div>
                        </div>
                        <div class="logo-com col-sm-6 col-md-3 col-xs-6 wow fadeInDown" data-wow-delay="0.4s">
                            <div class="inner"><a class="trans" href="#"><img src="assets/front/rulya/img/image.png" alt=" "/></a></div>
                        </div>
                        <div class="logo-com col-sm-6 col-md-3 col-xs-6 wow fadeInDown" data-wow-delay="0.5s">
                            <div class="inner"><a class="trans" href="#"><img src="assets/front/rulya/img/image.png" alt=" "/></a></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?php $this->load->view('rulya/includes/footer'); ?>