<?php $this->load->view('rulya/includes/header_agency'); ?>
<div class="main">
      <div class="key-visual">
        <div class="text-inner text-center">
          <p class="banner-1-1">Tìm kiếm đại lý Dược mỹ phẩm tế bào gốc Hàn Quốc</p>
          <div class="gap-element" style="display:block; height:auto; padding-top:10px"></div>
          <div class="dang-ky-dai-ly pum-trigger" style="max-width: 260px; cursor: pointer;"><a data-fancybox href="#modal-agency">RULYA VIỆT NAM</a></div>
          <div class="gap-element" style="display:block; height:auto; padding-top:10px"></div>
          <p class="banner-1-2">CHIẾT KHẤU CAO 50% HỖ TRỢ HỆ THỐNG MARKETING BÁN HÀNG TỰ ĐỘNG</p>
        </div>
      </div>
      <section class="benefit">
        <span class="anchor" id="link-inner01"></span>
        <div class="container">
          <h2 class="ttl-agency">QUYỀN LỢI ĐẶC BIỆT KHI TRỞ THÀNH ĐẠI LÝ RULYA</h2>
          <div class="content-benefit">
            <ul class="list-agency-list">
              <li>Được huấn luyện bán hàng từ A – Z: Kéo khách – Tư vấn – Xử lý từ chối – Chốt sale – CSKH</li>
              <li>Được RULYA cung cấp phần mềm ERP quản lý kinh doanh toàn diện, <strong>trị giá 530 triệu đồng</strong>.</li>
              <li>Được sở hữu hệ thống Automation CSKH tự động gia tăng gấp 6 lần doanh số, <strong>trị giá 220 triệu đồng</strong>.</li>
              <li>Được tham gia Trại Thủ Lĩnh Kim Cương “Leader Diamond”, có cơ hội thưởng oto Mazda, SH và du lịch khắp thế giới.</li>
              <li>Được đào tạo Công thức <strong>“San Bằng Tất Cả”</strong> – Công thức gia tăng hệ thống đại lý theo cấp số nhân bền vững.</li>
              <li>Được đào tạo chiến lược marketing bậc cao KB STAR- <strong> Công thức gia tăng khách lẻ theo cấp số nhân – Độc quyền RULYA</strong></li>
              <li>Được đào tạo tại học viện KB ACADEMY bài bản và chuyên sâu về Sản phẩm, sale, marekting, CSKH, chiến lược phát triển hệ thống phân phối giúp bạn trở thành Siêu thủ lĩnh bán hàng</li>
              <li>Được hệ thống nhân viên chăm sóc khách hàng của RULYA trực tiếp hỗ trợ <strong>chăm sóc 1:1</strong> với khách hàng 24/7, giúp gia tăng gấp 6 lần doanh số.</li>
              <li>Được tham gia khóa huấn luyện “SIÊU THỦ LĨNH 1%” <strong>trị giá 18 triệu đồng</strong></li>
              <li>Được tham gia khóa huấn luyện “RULYA – Đột Phá Tự Do Tài Chính” <strong>trị giá 25 triệu đồng</strong></li>
              <li>Được tham gia khoá huấn luyện “TỰ ĐỘNG HOÁ DOANH NGHIỆP” <strong>trị giá 30 triệu đồng</strong> giúp bạn tự do thời gian, doanh nghiệp tự động vận hành!</li>
              <li>Được tư vấn, giải đáp các thắc mắc khi có nhu cầu trực tiếp.</li>
              <li>Được cập nhất những sản phẩm mới nhất, những tính năng, công nghệ mới nhất của sản phẩm.</li>
              <li>Được thưởng các <strong>mức thưởng cao hàng tháng</strong> từ doanh thu bán hàng.</li>
              <li>Được tham dự các chương trình khuyến mại thúc đẩy bán hàng của công ty.</li>
              <li>Được tham gia các buổi đào tạo từ <strong>chuyên gia marketing, chuyên gia đào tạo bán hàng và các chuyên gia mỹ phẩm</strong>.</li>
              <li>Được cung cấp catalog, túi đựng bán lẻ cao cấp, các ấn phẩm quảng bá sản phẩm, thương hiệu RULYA</li>
              <li>Được trả lại hàng cho công ty nếu không có nhu cầu hợp tác nữa hoặc bất kỳ lý do gì. Hoàn toàn không có bất kỳ rủi ro nào!</li>
            </ul>
            <div class="content-tb01">
              <div class="over-auto">
                <table class="tb-benefit"  >
                  <tbody>
                    <tr>
                      <td style="text-align: center;" colspan="11"><span style="font-size: 110%;"><strong>ĐÀO TẠO KINH DOANH ONLINE – HÌNH THỨC HỌC: ONLINE QUA VIDEO ĐÀO TẠO</strong></span></td>
                    </tr>
                    <tr>
                      <td width="50%"><strong>Khóa đào tạo</strong></td>
                      <td style="text-align: center;" colspan="2" width="10%"><strong>Đại lý cấp 3</strong></td>
                      <td style="text-align: center;" colspan="2" width="10%"><strong>Đại lý cấp 2</strong></td>
                      <td style="text-align: center;" colspan="2" width="10%"><strong>Đại lý cấp 1</strong></td>
                      <td style="text-align: center;" colspan="2" width="10%"><strong>Tổng đại lý</strong></td>
                      <td style="text-align: center;" width="10%"><strong>Nhà phân phối</strong></td>
                    </tr>
                    <tr>
                      <td width="50%">Bán hàng trên Facebook Profile</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" width="10%">✓</td>
                    </tr>
                    <tr>
                      <td width="50%">Bán hàng trên Facebook Fanpage</td>
                      <td colspan="2" width="10%"></td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" width="10%">✓</td>
                    </tr>
                    <tr>
                      <td width="50%">Bán hàng trên Facebook Group</td>
                      <td colspan="2" width="10%"></td>
                      <td colspan="2" width="10%"></td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" width="10%">✓</td>
                    </tr>
                    <tr>
                      <td width="50%">Bán hàng trên Zalo</td>
                      <td colspan="2" width="10%"></td>
                      <td colspan="2" width="10%"></td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" width="10%">✓</td>
                    </tr>
                    <tr>
                      <td width="50%">Quản lý đội nhóm (Trello, Nhanh.vn, RULYA ERP)</td>
                      <td colspan="2" width="10%"></td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" width="10%">✓</td>
                    </tr>
                    <tr>
                      <td style="text-align: center;" colspan="11"><span style="font-size: 110%;"><strong>ĐÀO TẠO KINH DOANH XÂY DỰNG HỆ THỐNG – HÌNH THỨC HỌC: OFFLINE / ONLINE TRỰC TUYẾN</strong></span></td>
                    </tr>
                    <tr>
                      <td width="50%">Đào tạo Quy trình 7 bước Raving Fan</td>
                      <td colspan="2" width="10%"></td>
                      <td colspan="2" width="10%"></td>
                      <td colspan="2" width="10%"></td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                    </tr>
                    <tr>
                      <td width="50%">Đào tạo Quy trình 7 bước Nhân bản hệ thống</td>
                      <td colspan="2" width="10%"></td>
                      <td colspan="2" width="10%"></td>
                      <td colspan="2" width="10%"></td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                    </tr>
                    <tr>
                      <td width="50%">Đào tạo Offline toàn hệ thống</td>
                      <td colspan="2" width="10%"></td>
                      <td colspan="2" width="10%"></td>
                      <td colspan="2" width="10%"></td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                      <td style="text-align: center;" colspan="2" width="10%">✓</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <ul class="list-agency-list mb40">
              <li>Bên cạnh được tham dự các khóa học, các video đào tạo kinh doanh, công ty sẽ <strong>thường xuyên chia sẻ các tài liệu về kinh doanh bán hàng, kiến thức sản phẩm cho đại lý</strong>.</li>
              <li>Được công ty pass đơn lẻ, pass đại lý tại khu vực bản quản lý.</li>
            </ul>
            <div class="row pd15">
              <div class="col-md-4">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/agency.jpg" alt=" " >
                  Đại lý đang kiểm hàng
                </div>
              </div>
              <div class="col-md-4">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/agency.jpg" alt=" " >
                  Đại lý đang kiểm hàng
                </div>
              </div>
              <div class="col-md-4">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/agency.jpg" alt=" " >
                  Đại lý đang kiểm hàng
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="apply text-center">
        <span class="anchor" id="link-inner02"></span>
        <div class="container">
          <h2 class="ttl-apply">ĐĂNG KÝ LÀM ĐẠI LÝ NGAY HÔM NAY!</h2>
          <div class="dang-ky-dai-ly pum-trigger" style=" cursor: pointer;"><a data-fancybox href="#modal-agency">ĐĂNG KÝ LÀM ĐẠI LÝ RULYA</a></div>
        </div>
      </section>
      <section class="paper">
        <div class="container">
          <h2 class="ttl-agency">CÁC GIẤY TỜ CHỨNG MINH NGUỒN GỐC TỪ HÀN QUỐC</h2>
          <div class="row pd15">
            <div class="col-md-6 mb20">
              <div class="inner">
                <img src="assets/front/rulya/img/giay-1.jpg" alt="">
              </div>
            </div>
            <div class="col-md-6">
              <div class="inner">
                <img src="assets/front/rulya/img/giay-2.jpg" alt="">
              </div>
            </div>
          </div>
          <div class="content-benefit">
            <span class="anchor" id="link-inner01"></span>
            <p style="font-style: italic; text-align: center;">(Để tìm hiểu thông tin về chính sách hỗ trợ, bạn vui lòng điền thông tin liên hệ vào mục đăng ký)</p>
            <h3 class="ttl-gia">Bảng giá sản phẩm</h3>
            <div class="over-auto">
              <table class="table-banggia">
                <thead>
                  <tr>
                    <th>STT</th>
                    <th>Tên tiếng anh</th>
                    <th>Tên tiếng việt</th>
                    <th>Giá bán lẻ</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>PURE CLEANSING GEL RULYA</td>
                    <td>SỮA RỬA MẶT PURE CLEANSING GEL</td>
                    <td>430.000đ</td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>WHITENING TONER RULYA</td>
                    <td>NƯỚC HOA HỒNG WHITENING TONER RULYA</td>
                    <td>510.000đ</td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>WHITENING SERUM RULYA</td>
                    <td>SERUM DƯỠNG TRẮNG WHITENING RULYA</td>
                    <td>650.000đ</td>
                  </tr>
                  <tr>
                    <th scope="row">4</th>
                    <td>WHITENING CREAM KBKSIN</td>
                    <td>KEM DƯỠNG TRẮNG WHITENING CREAM RULYA</td>
                    <td>610.000đ</td>
                  </tr>
                  <tr>
                    <th scope="row">5</th>
                    <td>WHITE SUN BLOCK RULYA</td>
                    <td>KEM CHỐNG NẮNG WHITE SUN BLOCK RULYA</td>
                    <td>530.000đ</td>
                  </tr>
                  <tr>
                    <th scope="row">6</th>
                    <td>ANTI AGEING SERUM RULYA</td>
                    <td>SERUM TRẺ HOÁ DA ANTI AGEING RULYA</td>
                    <td>730.000đ</td>
                  </tr>
                  <tr>
                    <th scope="row">7</th>
                    <td>ACNE SERUM RULYA</td>
                    <td>SERUM TRỊ MỤN ACNE RULYA</td>
                    <td>710.000đ</td>
                  </tr>
                  <tr>
                    <th scope="row">8</th>
                    <td>VITAMIN C RULYA</td>
                    <td>SERUM TRỊ THÂM NÁM VITAMIN C</td>
                    <td>750.000đ</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="img-ithumb">
              <img src="assets/front/rulya/img/3.jpg" alt=" " >
            </div>
          </div>
        </div>
      </section>
      <section class="how-to">
        <span class="anchor" id="link-inner03"></span>
        <div class="container">
          <h2 class="ttl-agency">CÁCH THỨC TRỞ THÀNH ĐẠI LÝ</h2>
          <div class="content-benefit">
            <h4>Đăng ký làm đại lý theo các bước sau:</h4>
            <p class="mb10"><span class="numb">1</span> Đăng ký thông tin vào mục ĐĂNG KÝ tại <a  href="#dang-ky-tu-van" class="pum-trigger link" > ĐÂY </a>.</p>
            <p class="mb10"><span class="numb">2</span> Công ty gọi điện thoại tư vấn chính sách hợp tác và các quyền lợi phát triển</p>
            <img class="mb20" src="assets/front/rulya/img/4.jpg" alt=" " >
          </div>
        </div>
      </section>
      <section class="apply text-center bg-white">
        <div class="container">
          
          <div class="dang-ky-dai-ly pum-trigger" style=" cursor: pointer;"><a data-fancybox href="#modal-agency">ĐĂNG KÝ LÀM ĐẠI LÝ NGAY HÔM NAY!</a></div>
        </div>
      </section>
      <section class="infor bg-gray">
        <span class="anchor" id="link-inner04"></span>
        <div class="container">
          <h2 class="ttl-agency">THÔNG TIN VỀ SẢN PHẨM RULYA</h2>
          <div class="content-benefit">
            <h4>Hình ảnh về sản phẩm</h4>
            <div class="row mb40">
              <div class="col-md-3 col-sm-6">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/sp1.jpg" alt=" " >
                  VITAMIN C RULYA
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/sp1.jpg" alt=" " >
                  ANTI AGEING SERUM RULYA
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/sp1.jpg" alt=" " >
                  ACNE SERUM RULYA
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/sp1.jpg" alt=" " >
                  WHITENING SERUM RULYA
                </div>
              </div>
            </div>
            <div class="row ">
              <div class="col-md-3 col-sm-6">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/sp1.jpg" alt=" " >
                  VITAMIN C RULYA
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/sp1.jpg" alt=" " >
                  ANTI AGEING SERUM RULYA
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/sp1.jpg" alt=" " >
                  ACNE SERUM RULYA
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="img-ithumb">
                  <img src="assets/front/rulya/img/sp1.jpg" alt=" " >
                  WHITENING SERUM RULYA
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="customer bg-white">
        <div class="container">
          <h2 class="ttl-agency">CÁC ĐẠI LÝ NÓI GÌ SAU KHI KINH DOANH CÙNG RULYA</h2>
          <div class="list-customer">
            <div class="row pd15 ">
              <div class="col-md-4">
                <div class="inner">
                  <div class="thumb">
                    <img src="assets/front/rulya/img/a1.jpg" alt="">
                  </div>
                  <h3 class="name">Chị Trần Thị Thanh Vân – Bà mẹ bỉm sữa</h3>
                  <p>Trở thành đại lý của RULYA là một bước ngoặt quan trọng trong cuộc đời của tôi. Vốn là một single mom, nhờ RULYA mà lần đầu tiên tôi được đứng trên đôi chân của chính mình, được cầm những đồng tiền do chính mình làm ra. Giờ đây tôi có thể tự tin vào bản thân, có thể đủ sức lo cho cuộc sống của mình và gia đình. Tôi rất cảm ơn RULYA.</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="inner">
                  <div class="thumb">
                    <img src="assets/front/rulya/img/a1.jpg" alt="">
                  </div>
                  <h3 class="name">Chị Trần Thị Thanh Vân – Bà mẹ bỉm sữa</h3>
                  <p>Trở thành đại lý của RULYA là một bước ngoặt quan trọng trong cuộc đời của tôi. Vốn là một single mom, nhờ RULYA mà lần đầu tiên tôi được đứng trên đôi chân của chính mình, được cầm những đồng tiền do chính mình làm ra. Giờ đây tôi có thể tự tin vào bản thân, có thể đủ sức lo cho cuộc sống của mình và gia đình. Tôi rất cảm ơn RULYA.</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="inner">
                  <div class="thumb">
                    <img src="assets/front/rulya/img/a1.jpg" alt="">
                  </div>
                  <h3 class="name">Chị Trần Thị Thanh Vân – Bà mẹ bỉm sữa</h3>
                  <p>Trở thành đại lý của RULYA là một bước ngoặt quan trọng trong cuộc đời của tôi. Vốn là một single mom, nhờ RULYA mà lần đầu tiên tôi được đứng trên đôi chân của chính mình, được cầm những đồng tiền do chính mình làm ra. Giờ đây tôi có thể tự tin vào bản thân, có thể đủ sức lo cho cuộc sống của mình và gia đình. Tôi rất cảm ơn RULYA.</p>
                </div>
              </div>
            </div>
          </div>
          <h2 class="ttl-agency">QUÀ TẶNG ĐẶC BIỆT KHI ĐĂNG KÝ LÀM ĐẠI LÝ DUY NHẤT NGÀY HÔM NAY</h2>
          <div class="content-benefit">
            <ul class="list-agency-list">
              <li>Được tặng website bán hàng chuyên nghiệp – <strong>Trị giá 15 triệu đồng</strong>, giúp xây dựng uy tín trong mắt khách hàng và áp dụng nhiều chiến lược marketing.</li>
              <li>Tặng 2 chỉ vàng 9999 khi đăng ký làm NPP duy nhất ngày hôm nay!</li>
            </ul>
          </div>
        </div>
      </section>
      <section class="apply-now">
        <div class="container">
          <h2 class="ttl-apply text-center">ĐĂNG KÝ LÀM ĐẠI LÝ NGAY HÔM NAY!</h2>
          <div class="dang-ky-dai-ly pum-trigger" style=" cursor: pointer;"><a data-fancybox href="#modal-agency">ĐĂNG KÝ LÀM ĐẠI LÝ RULYA</a></div>
          <span class="anchor" id="link-inner05"></span>
          <h2 class="ttl-agency">THÔNG TIN LIÊN HỆ DÀNH CHO ĐẠI LÝ</h2>
          
          <div class="content-benefit bg-gray">
            <div class="row pd15">
              <div class="col-md-6">
                <p><strong>Công ty Cổ phần RULYA Việt Nam</strong><br>Địa chỉ: P.3102, D.Tower, Vinaconex 2, KĐT Kim Văn Kim Lũ, Hoàng Mai, Hà Nội<br>Điện thoại dành riêng cho đại lý:<a class="value" href="tel:02473022686">024 730 22686</a><br>
              Email: Customerservice.rulya@gmail.com</p>
            </div>
            <div class="col-md-6">
              <p>Chào đón bạn trở thành Tân siêu thủ lĩnh trong Dải ngân hà RULYA !</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    
  </div>
  <div id="modal-agency" class="hidde" >
    <h2 class="ttl-pop-argency">ĐĂNG KÝ ĐẠI LÝ RULYA</h2>
    <p class="mb20">Vui lòng điền thông tin đăng ký của bạn vào bên dưới, chúng tôi sẽ liên hệ tư vấn cho bạn.</p>

    <div class="row ">
      <div class="col-md-6 mb20">
        <input type="text" class="input-text" placeholder="Họ và tên *">
      </div>
      <div class="col-md-6  mb20">
        <input type="text" class="input-text" placeholder="Số điện thoại *">
      </div>
    </div>
    <div class="row ">
      <div class="col-md-6  mb20">
        <input type="text" class="input-text" placeholder="Email *">
      </div>
      <div class="col-md-6  mb20">
        <div class="sort-category full">
          <select class="stl-select" name="inf_field_Title">
            <option value="">Địa chỉ (Tỉnh/thành)</option>
            <option value="Anh">Hồ Chí Minh</option>
            <option value="Chị">Hà Nội</option>
          </select>
        </div>
      </div>
    </div>
    <div class="row ">
      <div class="col-md-6  mb20">
        <div class="sort-category full">
          <select class="stl-select" name="inf_field_Title">
            <option value="">Nghề Nghiệp</option>
            <option value="Anh">Giáo Viên</option>
            <option value="Chị">Nội trợ</option>
          </select>
        </div>
      </div>
      <div class="col-md-6  mb20">
        <div class="sort-category full">
          <select class="stl-select" name="inf_field_Title">
            <option value="">Cấp đại lý mong muốn</option>
            <option value="Anh">Cấp 1</option>
            <option value="Chị">Cấp 2</option>
            <option value="Chị">Cấp 3</option>
          </select>
        </div>
      </div>
    </div>
    <div class="row ">
      <div class="col-md-12 mb20">
        <textarea class="text-area"></textarea>
      </div>
    </div>

    <div class="text-center"><button class="btn-cmn">Gửi liên hệ</button></div>
 
  </div>
<?php $this->load->view('rulya/includes/footer_agency'); ?>