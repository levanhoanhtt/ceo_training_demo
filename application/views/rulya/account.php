<?php $this->load->view('rulya/includes/header'); ?>
    <div class="main">
        <div class="wrapper">
            <div class="container">
                <div class="content-account">
                    <h2 class="ttl-account">Tài khoản của tôi</h2>
                    <div class="row pd15">
                        <div class="col-md-6 col-sm-6 mb20">
                            <h3 class="ttl-col-ac mb10">Đăng nhập</h3>
                          
                            <div class="mb20">
                                <p class="mb10">Tên tài khoản hoặc địa chỉ email *</p>
                                <input type="text" class="mb20 input-text hmdrequired" name="UserName" id="userName" value="">
                            </div>
                            <div class="mb20">
                                <p class="mb10">Mật khẩu *</p>
                                <input type="text" class="mb20 input-text hmdrequired" name="UserPass" id="userPass" value="">
                            </div>
                            <div class="text-left mb10">
                                <button type="button" class="btn-cmn mr20 mb10 log-in">Đăng nhập</button>
                                <input type="text" hidden="hidden" value="<?php echo base_url('api/user/checkLogin')?>" id='urlCheckLogin' name="">
                                <input type="text" hidden="hidden" name="IsGetConfigs" value="1">
                                <label><input type="checkbox" name="IsRemember" class="iCheck" checked="checked"><span class="ml20">Ghi nhớ mật khẩu</span></label>
                            </div>
                            <div class="text-left">
                                <a href="<?php echo base_url('user/forgotpass'); ?>" class="link trans">Quên mật khẩu</a>
                            </div>

                        </div>
                        <div class="col-md-6 col-sm-6 border-left">
                            <h3 class="ttl-col-ac mb10">Đăng ký</h3>
                            <div class="mb20 div-input">
                                <p class="mb10">Họ tên*</p>
                                <input type="text" class="mb20 input-text" id="customerName">
                            </div>
                            <div class="mb20 div-input">
                                <p class="mb10">Số điện thoại*</p>
                                <input type="text" class="mb20 input-text" id="customerPhone">
                            </div>
                            <div class="mb20 div-input">
                                <p class="mb10">Địa chỉ*</p>
                                <input type="text" class="mb20 input-text" id="cusotmerAddress">
                            </div>
                            <div class="mb20 div-input">
                                <p class="mb10">Địa chỉ email *</p>
                                <input type="text" class="mb20 input-text" id="customerEmail">
                            </div>
                            <div class="mb20 div-input">
                                <p class="mb10">Mật khẩu *</p>
                                <input type="password" class="mb20 input-text" id="password">
                            </div>
                            <div class="mb20 div-input">
                                <p class="mb10">Xác nhận mật khẩu *</p>
                                <input type="password" class="mb20 input-text" id="autPassword">
                            </div>
                            <div class="text-left">
                                <button class="btn-cmn mr20 registration" type="button">Đăng ký</button>
                                <input type="text" hidden="hidden" id="urlRegistrationCustomer" value="<?php echo base_url('api/customer/update'); ?>">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="alert alert-danger" id="commentAlert" style="margin-top: 10px;display: none;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <noscript><meta http-equiv="refresh" content="0; url=<?php echo base_url('user/permission'); ?>" /></noscript>
<?php $this->load->view('rulya/includes/footer'); ?>