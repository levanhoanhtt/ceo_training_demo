<?php $this->load->view('rulya/includes/header'); ?>
<div class="main">
    <div class="wrapper">
        <div class="container">
            <div class="content-blog">
                <div class="bread-crumb-blog">
                    <ul>
                        <li><a href="<?php echo base_url('rulya'); ?>">Trang chủ</a></li>
                        <li><span>»</span></li>
                        <li><a href="<?php echo base_url('blogs'); ?>">Blog</a></li>
                    </ul>
                    <h2 class="ttl-blog">Blog</h2>
                </div>
                <div class="row list-blog">
                    <div class="col-md-9 col-sm-9 border-right">
                        <div class="row pd15">
                            <?php foreach ($listBlogs as $blog) { ?>
                                <div class="col-sm-6 col-md-4 col-xs-12 box-blog">
                                    <a href="<?php echo $this->Mconstants->getUrl($blog['ArticleSlug'], $blog['ArticleId'], 4, 2); ?>">
                                      <span class="image">
                                          <img src="<?php echo IMAGE_PATH.$blog['ArticleImage']; ?>" alt="<?php echo $blog['ArticleTitle']; ?>"/>
                                          <span class="date-visi"><?php echo ddMMyyyy($blog['CrDateTime'], 'd'); ?><span class="mounth">Th<?php echo ddMMyyyy($blog['CrDateTime'], 'm'); ?></span></span>
                                      </span>
                                      <h3 class="ttl-dl-blog"><?php echo $blog['ArticleTitle']; ?></h3>
                                        <?php echo $blog['ArticleLead']; ?>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if($pageCount > 1){ ?>
                            <div class="pager-blog">
                                <ul>
                                    <?php if($pageCurrent > 1) echo '<li><a href="'.str_replace('{$1}', $pageCurrent - 1, $categoryUrlPage).'"><i class="icon-angle-left"></i></a></li>';
                                    for($i = 1; $i <= $pageCount; $i++){
                                        if($i == $pageCurrent) echo '<li><a href="javascript:;" class = "active-custom">'.$i.'</a></li>';
                                        else echo '<li><a href="'.str_replace('{$1}', $i, $categoryUrlPage).'">'.$i.'</a></li>';
                                    }
                                    if($pageCurrent < $pageCount) echo '<li><a href="'.str_replace('{$1}', $pageCurrent + 1, $categoryUrlPage).'"><i class="icon-angle-right"></i></a></li>'; ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?php $this->load->view('rulya/includes/news'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('rulya/includes/footer'); ?>