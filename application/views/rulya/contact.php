<?php $this->load->view('rulya/includes/header'); ?>
<div class="main">
    <div class="wrapper">
        <div class="container">
            <div class="content-blog">
                <div class="row">
                    <div class="col-md-9 col-sm-9 border-right ">
                        <div class="content-vision">
                            <?php echo $articleContent; ?>
                            <!--<p><strong>CÔNG TY CỔ PHẦN RULYA VIỆT NAM</strong></p>
                            <ul>
                                <li>Địa chỉ: Số 26 ngõ 312 Kim Giang, Hoàng Mai, Hà Nội</li>
                                <li>Hotline: 024 730 22686</li>
                                <li>Email: Customerservice.rulya@gmail.com</li>
                            </ul>
                            <p class="italic">Vui lòng để lại thông tin liên hệ và chúng tôi sẽ phản hồi sớm nhất
                                đến quý khách hàng.</p>-->
                            <div class="sort-category ct">
                                <select class="stl-select" id="genderId">
                                    <option value="">Chọn xưng hô</option>
                                    <option value="1">Anh</option>
                                    <option value="2">Chị</option>
                                </select>
                            </div>
                            <input type="text" class="mb20 input-text" placeholder="Họ tên khách hàng *" id="customerName">
                            <input type="text" class="mb20 input-text" placeholder="Địa chỉ email *" id="customerEmail">
                            <input type="text" class="mb20 input-text" placeholder="Số điện thoại *" id="customerPhone">
                            <input type="text" class="mb20 input-text" placeholder="Tiêu đề *" id="contactTitle">
                            <textarea class="text-area" placeholder="Nội dung liên hệ *" id="contactContent"></textarea>
                            <div class="text-center">
                                <button class="btn-cmn">Gửi liên hệ</button>
                                <div class="alert alert-danger" id="commentAlert" style="margin-top: 10px;display: none;"></div>
                                <input type="hidden" id="insertContactUrl" value="<?php echo base_url('contact/update'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?php $this->load->view('rulya/includes/news'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('rulya/includes/footer'); ?>