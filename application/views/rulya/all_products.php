<?php $this->load->view('rulya/includes/header'); ?>
<div class="main">
    <div class="bread-crumb">
        <div class="container">
            <div class="row">
                <div class="col-md-6 left">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                        <li><span>/</span></li>
                        <li><?php echo $pageTitle; ?></li>
                    </ul>
                </div>
                <div class="col-md-6 sort-category">
                    <p>Hiển thị một kết quả duy nhất</p>
                    <select name="orderby" class="orderby">
                        <option value="menu_order" selected="selected">Thứ tự mặc định</option>
                        <option value="popularity">Thứ tự theo mức độ phổ biến</option>
                        <option value="rating">Thứ tự theo điểm đánh giá</option>
                        <option value="date">Thứ tự theo sản phẩm mới</option>
                        <option value="price">Thứ tự theo giá: thấp đến cao</option>
                        <option value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-md-push-3">
                    <div class="row">
                        <?php foreach ($listProducts as $product) { ?>
                            <div class="col-sm-6 col-md-4 col-xs-12">
                                <?php $this->load->view('rulya/includes/product', array('product' => $product)); ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if($pageCount > 1){ ?>
                        <div class="pager-blog">
                            <ul>
                                <?php if($pageCurrent > 1) echo '<li><a href="'.str_replace('{$1}', $pageCurrent - 1, $categoryUrlPage).'"><i class="icon-angle-left"></i></a></li>';
                                for($i = 1; $i <= $pageCount; $i++){
                                    if($i == $pageCurrent) echo '<li><a href="javascript:;" class = "active-custom">'.$i.'</a></li>';
                                    else echo '<li><a href="'.str_replace('{$1}', $i, $categoryUrlPage).'">'.$i.'</a></li>';
                                }
                                if($pageCurrent < $pageCount) echo '<li><a href="'.str_replace('{$1}', $pageCurrent + 1, $categoryUrlPage).'"><i class="icon-angle-right"></i></a></li>'; ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-3  col-md-pull-9">
                    <div class="box-sidebar">
                        <h3 class="ttl-sidebar">Danh mục sản phẩm</h3>
                        <ul>
                            <?php foreach($listProductCategories as $c){ ?>
                                <li><a href="<?php echo $this->Mconstants->getUrl($c['CategorySlug'], $c['CategoryId'], 1, 2); ?>"<?php if($categoryId == $c['CategoryId']) echo ' class="active"'; ?>><?php echo $c['CategoryName']; ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!--<div class="box-sidebar">
                        <h3 class="ttl-sidebar">Sản phẩm xem gần đây</h3>
                        <div class="box-view">
                            <a href="#" class="clearfix trans">
                                <img src="assets/front/rulya/img/product01.jpg" alt="product"/>
                                <p>ACNE SERUM RULYA - SERUM TRỊ MỤN RULYA</p>
                                <p class="prc">710.000<span>₫</span></p>
                            </a>
                        </div>
                        <div class="box-view">
                            <a href="#" class="clearfix trans">
                                <img src="assets/front/rulya/img/product01.jpg" alt="product"/>
                                <p>ACNE SERUM RULYA - SERUM TRỊ MỤN RULYA</p>
                                <p class="prc">710.000<span>₫</span></p>
                            </a>
                        </div>
                        <div class="box-view">
                            <a href="#" class="clearfix trans">
                                <img src="assets/front/rulya/img/product01.jpg" alt="product"/>
                                <p>ACNE SERUM RULYA - SERUM TRỊ MỤN RULYA</p>
                                <p class="prc">710.000<span>₫</span></p>
                            </a>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('rulya/includes/footer'); ?>