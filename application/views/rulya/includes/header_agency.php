<!DOCTYPE html>
<html lang="vi" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" id="baseUrl"/>
    <?php $this->load->view("includes/favicon"); ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="assets/front/rulya/css/bootstrap.css">
    <link rel="stylesheet" href="assets/front/rulya/css/animate.css">
    <link rel="stylesheet" href="assets/front/rulya/css/jquery-ui.css">
    <link rel="stylesheet" href="assets/front/rulya/css/jquery.fancybox.css">
    <link rel="stylesheet" href="assets/front/rulya/css/style.css">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?php echo $configSites['COMPANY_NAME']; ?>"/>
    <meta property="og:description" content="<?php echo $configSites['META_DESC']; ?>"/>
    <meta property="og:url" content="<?php echo $configSites['pageUrl']; ?>"/>
    <meta property="og:site_name" content="<?php echo $configSites['COMPANY_NAME']; ?>"/>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cleartype" content="on">
    <![endif]-->
</head>
<body>
<header>
  <div class="overlay"></div>
  <div class="main-header pg-agency">
    <div class="container header-argency-md">
      
      
      <ul class="nav left">
        <li><a class="" href="#">Trang chủ</a></li>
        <li><a class="scroll" href="#link-inner01">Chính sách đại lý</a></li>
        <li><a class="scroll" href="#link-inner02">Đăng ký tư vấn</a></li>
      </ul>
      <h1 class="logo">
      <a href="<?php echo base_url(); ?>"><img src="assets/front/rulya/img/logo.png" alt="Rulya"></a>
      </h1>
      <ul class="nav right">
        <li><a class="scroll" href="#link-inner03">Hướng dẫn</a></li>
        <li><a class="scroll" href="#link-inner04">Hình ảnh sản phẩn</a></li>
        <li><a class="scroll" href="#link-inner05">Liên hệ</a></li>
      </ul>
    </div>
    <div class="container header-argency-sm">
      <div class="bt-menu sm" id="bt-menu" ><i class="icon-menu"></i></div>
      <div class="btn-close" id="btn-close" >×</div>
      <h1 class="logo">
     <a href="<?php echo base_url(); ?>"><img src="assets/front/rulya/img/logo.png" alt="Rulya"></a>
      </h1>
      <ul class="nav" id="navigation">
        <li><a class="" href="#">Trang chủ</a></li>
        <li><a class="scroll-argency" href="#link-inner01">Chính sách đại lý</a></li>
        <li><a class="scroll-argency" href="#link-inner02">Đăng ký tư vấn</a></li>
        <li><a class="scroll-argency" href="#link-inner03">Hướng dẫn</a></li>
        <li><a class="scroll-argency" href="#link-inner04">Hình ảnh sản phẩn</a></li>
        <li><a class="scroll-argency" href="#link-inner05">Liên hệ</a></li>
      </ul>
    </div>
  </div>
</header>