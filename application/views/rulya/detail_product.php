<?php $this->load->view('rulya/includes/header'); ?>
<div class="main">
    <div class="wrapper">
        <div class="container">
            <div class="row pd15 top-product mb15 ">
                <div class="col-md-6 left-pro mb15">
                    <div id="zoom-eff" class="zoom img-pro">
                        <img src="<?php echo PRODUCT_PATH.$product['ProductImage']; ?>" alt="<?php echo $product['ProductName']; ?>" />
                        <a data-fancybox data-srcset="<?php echo PRODUCT_PATH.$product['ProductImage']; ?>" href="<?php echo PRODUCT_PATH.$product['ProductImage']; ?>" class="popup">
                            <i class="icon-expand"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 right-product mb15">
                    <div class="pro-breadcrumbs">
                        <ul>
                            <li><a href="<?php echo base_url(); ?>">TRANG CHỦ</a></li>
                            <li><span>/</span></li>
                            <li><?php echo $product['ProductName']; ?></li>
                        </ul>
                    </div>
                    <h2 class="ttl-detail" style="text-transform: uppercase;"><?php echo $product['ProductName']; ?></h2>
                    <div class="clearfix inner-left">
                        <div class="star-rating"><span style="width: 90%;"></span></div>
                    </div>
                    <div class="price-detail">
                        <?php if($product['IsContactPrice'] == 2) { ?>
                            <div class="current">Giá liên hệ</div>
                        <?php } else { ?>
                            <?php if($product['OldPrice'] > 0){ ?><div class="prev"><?php echo priceFormat($product['OldPrice']); ?><span class="dv">₫</span></div><?php } ?>
                            <div class="current"><?php echo $product['Price'] > 0 ? priceFormat($product['Price']).'<span>₫</span>' : 'Giá liên hệ'; ?></div>
                        <?php } ?>
                    </div>
                    <?php echo $product['ProductShortDesc']; ?>
                    <div class="clearfix qual-bt">
                        <div class="qualty">
                            <a href="#" class="des click-down">-</a>
                            <input type="text" class="numeric" value="1" onkeydown="return ( event.ctrlKey || event.altKey || (47<event.keyCode &amp;&amp; event.keyCode<58 &amp;&amp; event.shiftKey==false) || (95<event.keyCode &amp;&amp; event.keyCode<106)|| (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 &amp;&amp; event.keyCode<40) || (event.keyCode==46) )">
                            <a href="#" class="inc click-up">+</a>
                        </div>
                        <button class="btn-cmn btn-add-cart" data-id="<?php echo $product['ProductId'];?>">Thêm vào giỏ</button>
                    </div>
                    <div class="sku">
                        Mã: <?php echo $product['Sku']; ?>
                    </div>
                    <div class="posted_in">
                        <?php $cateHtml = '';
                        foreach($listCategories as $c) $cateHtml .= '<a href="'.$this->Mconstants->getUrl($c['CategorySlug'], $c['CategoryId'], 1, 2).'">'.$c['CategoryName'].'</a>, ';
                        if(!empty($cateHtml)) $cateHtml = substr($cateHtml, 0, strlen($cateHtml) - 2);
                        echo 'Danh mục: '.$cateHtml; ?>
                    </div>
                    <div class="social-icons" >
                        <a href="#" class="trans face"><i class="icon-facebook"></i></a>
                        <a href="#" class="trans twitter"><i class="icon-twitter"></i></a>
                        <a href="#" class="trans envelop"><i class="icon-envelop"></i></a>
                        <a href="#" class="trans pinterest"><i class="icon-pinterest"></i></a>
                        <a href="#" class="trans google"><i class="icon-google-plus"></i></a>
                        <a href="#" class="trans linkedin"><i class="icon-linkedin"></i></a>

                    </div>
                </div>
            </div>
            <div class="bottom-detail">
                <ul class="tabs-menu head-tab clearfix">
                    <li class="current"><a href="#tab-1">mô tả</a></li>
                    <li><a href="#tab-2">đánh giá (<?php echo count($listComments) ?>)</a></li>
                </ul>
                <div class="tab content-detail">
                    <div id="tab-1" class="tab-content content-text">
                        <!--<h2>Chi tiết sản phẩm</h2>-->
                        <?php echo $product['ProductDesc']; ?>
                    </div>
                    <div id="tab-2" class="tab-content">
                        <h3 class="ttl-review"><?php echo count($listComments) ?> đánh giá cho <?php echo $product['ProductName']; ?></h3>
                        <div class="row pd15">
                            <div class="col-md-7" id="listComments">
                                <?php foreach ($listComments as $comment) {  ?>
                                    <div class="box-comment">
                                        <div class="avatar"><img src="assets/front/rulya/img/avatar.png" alt=" " /></div>
                                        <div class="clearfix inner-left">
                                            <div class="star-rating"><span style="width: 90%;"></span></div>
                                        </div>
                                        <div class="meta"><strong ><?php echo $comment['CustomerName']; ?></strong> <span class="date"> – <?php echo ddMMyyyy($comment['CrDateTime']); ?></span></div>
                                        <p><?php echo $comment['Comment']; ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-5">
                                <div class="box-rating">
                                    <h3 class="ttl-rating">Thêm đánh giá</h3>
                                    <p>Đánh giá của bạn</p>
                                    <p class="stars selected">
                                        <span>
                                            <a class="star star-1" href="#">1</a>
                                            <a class="star star-2" href="#">2</a>
                                            <a class="star star-3" href="#">3</a>
                                            <a class="star star-4" href="#">4</a>
                                            <a class="star star-5" href="#">5</a>
                                        </span>
                                    </p>
                                    <p>Đánh giá của bạn</p>
                                    <textarea class="text-area" id="comment"></textarea>
                                    <div class="row pd15">
                                        <div class="col-md-6">
                                            <p>Tên *</p>
                                            <input type="text" id="customerName" class="input-text" value="">
                                        </div>
                                        <div class="col-md-6">
                                            <p>Email *</p>
                                            <input type="email" id="customerEmail" class="input-text" value="">
                                        </div>
                                    </div>
                                    <br />
                                    <button type="button" class="btn-cmn" id="btnSendComment">gửi đi</button>
                                    <div class="alert alert-danger" id="commentAlert" style="margin-top: 10px;display: none;"></div>
                                    <input type="hidden" id="productId" value="<?php echo $product['ProductId'];?>">
                                    <input type="hidden" id="commentStarId" value="1">
                                    <input type="hidden" id="insertCommentUrl" value="<?php echo base_url('comment/update'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fb-comments" data-width="100%" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5"></div>
                <div class="ttl-same-pro">Sản phẩm liên quan</div>
                <div class="row same-pro">
                    <?php foreach($listProducts as $p){ ?>
                        <div class="col-sm-6 col-md-3 col-xs-12">
                            <?php $this->load->view('rulya/includes/product', array('product' => $p)); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('rulya/includes/footer'); ?>