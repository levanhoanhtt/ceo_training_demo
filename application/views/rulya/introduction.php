<?php $this->load->view('rulya/includes/header'); ?>

<div class="main">
    <div class="wrapper">
        <div class="container">
            <div class="conten-introduction">
                <h2 class="ttl-introduction">Giới thiệu chung</h2>
                <div id="bars">
                    <div class="row postsRow">
                        <div class="elPost" id="post-163">
                            <p class="postDate">Năm 2015</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               class="thumbnail " alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Chúng tôi ấp ủ ý định gây dựng sự nghiệp với mong muốn
                                        mang đến vẻ đẹp tự nhiên cho phụ nữ Việt Nam.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-161">
                            <p class="postDate">Tháng 06 năm 2016</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh1a.jpg"
                                                                               class="thumbnail " alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Trải qua thời gian nghiên cứu, tìm tòi về dược mỹ phẩm và
                                        thị trường dược mỹ phẩm. Chúng tôi tìm thấy điểm sáng mới đầy tiềm năng ở
                                        Công ty ENBIOSCIENCE CO., LTD</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-163">
                            <p class="postDate">Tháng 09 năm 2016</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh1a.jpg"
                                                                               class="thumbnail " alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Chúng tôi thực hiện hành trình sang Hàn Quốc tìm hiểu về
                                        dây chuyền sản xuất và những dòng sản phẩm hiện có của công ty này.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-161">
                            <p class="postDate">Tháng 12 năm 2016</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1A.jpg"
                                                                               data-src="assets/front/rulya/img/anh1A.jpg"
                                                                               class="thumbnail " alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Bị thuyết phục bởi sự “tự nhiên” từ sản phẩm của công ty,
                                        chúng tôi quyết định ký hợp đồng hợp tác, độc quyền thương hiệu, chính thức
                                        đưa sản phẩm về Việt Nam.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-163">
                            <p class="postDate">Tháng 03 năm 2017</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh1a.jpg"
                                                                               class="thumbnail " alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Công ty dược mỹ phẩm RULYA ra đời đánh dấu bước trưởng
                                        thành của chúng tôi.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-161">
                            <p class="postDate">Tháng 05 năm 2017</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh1a.jpg"
                                                                               class="thumbnail " alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Chúng tôi cho ra mắt đứa con đầu tiên – Bộ sản phẩm dưỡng
                                        trắng da chuyên sâu.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-163">
                            <p class="postDate">Tháng 06 năm 2017</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh1A-1.jpg"
                                                                               class="thumbnail" alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Tiếp nối thành công của bộ dưỡng trắng da chuyên sâu,
                                        chúng tôi cho ra mắt bộ sản phẩm trị nám.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-163">
                            <p class="postDate">Tháng 06 năm 2017</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh1A-1.jpg"
                                                                               class="thumbnail" alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Cùng thời điểm đó, chúng tôi tiếp tục cho ra mắt bộ sản
                                        phẩm trị mụn.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-163">
                            <p class="postDate">Tháng 06 năm 2017</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh1A-1.jpg"
                                                                               class="thumbnail" alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Không dừng lại ở đó, bộ sản phẩm trẻ hoá da ra đời như sự
                                        hoàn thiện ý nguyện giải quyết mọi vấn đề về da cho phụ nữ Việt Nam từ dưỡng
                                        trắng, trị mụn, trị nám cho đến trẻ hoá da.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-161">
                            <p class="postDate">Tháng 07 năm 2017</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh10a.jpg"
                                                                               class="thumbnail" alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Để phát triển công ty lớn mạnh, chúng tôi đi đến việc ký
                                        kết hợp đồng hợp tác đào tạo với công ty TAKI ACADEMY.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="elPost" id="post-163">
                            <p class="postDate">Tháng 07 năm 2017</p>
                            <div class="postBlock">
                                <div class="postImg"><a href="/" title=""><img src="assets/front/rulya/img/anh1a.jpg"
                                                                               data-src="assets/front/rulya/img/anh11a.jpg"
                                                                               class="thumbnail" alt=""></a></div>
                                <div class="postBody">
                                    <h3 class="postTitle">Ngay sau khi ký kết hơp đồng với công ty TAKI ACADEMY,
                                        chúng tôi thiết lập chính sách RULYA PARTNER nhằm đem lại nhiều quyền lợi
                                        hơn cho các đại lý, khuyến khích kinh doanh.</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('rulya/includes/footer'); ?>