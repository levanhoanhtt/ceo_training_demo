<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <div class="box-body row-margin">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class='input-group' >
                                        <input type='text' class="form-control daterangepicker" id='dateRangePicker' value="<?php echo date('01/m/Y').' - '.date('d/m/Y'); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box-body divTable">
                    <div class=" table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Ngày tháng</th>
                                <th class="text-center">Số ĐH đã chốt</th>
                                <?php foreach($listOrderReasons as $r){ ?>
                                    <th class="text-center"><?php echo $r['OrderReasonName']; ?></th>
                                <?php } ?>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrderReason">
                            <tr class="success">
                                <td>Tổng</td>
                                <td id="tdTotalOrder" class="tdCount text-center">-</td>
                                <?php foreach($listOrderReasons as $r){ ?>
                                    <td id="tdCountOrder_<?php echo $r['OrderReasonId']; ?>" class="tdCount text-center">-</td>
                                <?php } ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <input type="hidden" id="getReportUrl" value="<?php echo base_url("api/order/orderReason"); ?>">
                <?php foreach($listOrderReasons as $r){ ?>
                    <input type="hidden" class="orderReasonId" value="<?php echo $r['OrderReasonId']; ?>">
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>