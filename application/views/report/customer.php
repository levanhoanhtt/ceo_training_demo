<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
            </section>
            <section class="content upn ft-seogeo">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('report/customer'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                        		<div class="form-group">
                                    <div class='input-group'>
                                        <input type='text' class="form-control daterangepicker" name="DateRangePicker" value="<?php echo set_value('DateRangePicker'); ?>" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserId', set_value('UserId'), true, '--Người tạo--', ' select2'); ?>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content upn ft-seogeo">
                 <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th class="text-center">Ngày tháng</th>
                                <th class="text-center">Số khách mới</th>
                                <th class="text-center">Số khách mua hàng</th>
                                <th class="text-center">Tỉ lệ mua hàng</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCustomers">
                            <?php
                            foreach($listCustomers as $key => $a){?>
                                <tr>
                                	<td class="text-center"><?php echo ddMMyyyy($a["CrDateTime"], 'd/m/Y'); ?></td>
                                    <td class="text-center"><?php echo $a['CustomerNew']; ?></td>
                                	<td class="text-center"><?php echo $a['CustomerOrders']; ?></td>
                                	<td class="text-center"><?php echo $a['CustomerOrders'] > 0 ? ceil($a['CustomerOrders'] / $a['CustomerNew'] * 100) : 0; ?>%</td>
                                </tr>
                            <?php } ?>
                            <?php if(isset($paggingHtml)){ ?>
                            <tr>
                                <td colspan="4"><?php echo $paggingHtml; ?></td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
