<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><a href="<?php echo base_url('remind/add'); ?>" class="btn btn-primary">Tạo nhắc nhở</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <?php $remindStatus = array(
                    1 => 'Chờ xử lý',
                    7 => 'Quá hạn',
                    4 => 'Đã hủy bỏ',
                    6 => 'Thất bại',
                    5 => 'Hoàn thành'
                ); ?>
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li<?php if($remindStatusId == 0) echo ' class="active"'; ?>><a href="<?php echo base_url('remind/2'); ?>">Hôm nay</a></li>
                        <?php foreach($remindStatus as $i => $v){ ?>
                            <li<?php if($remindStatusId == $i) echo ' class="active"'; ?>><a href="<?php echo base_url('remind/2/'.$i); ?>"><?php echo $v; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div style="display: none">
                    <?php echo form_open('remind/2/'.$i); ?>
                    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                    <?php echo form_close(); ?>
                </div>
                <div class="">
                    <?php sectionTitleHtml('', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th>Mã</th>
                                <th>Thời điểm cần xử lý</th>
                                <th>Số lần</th>
                                <th>Ngày tạo</th>
                                <th>Người tạo</th>
                                <th>Khách hàng</th>
                                <th>Đơn hàng</th>
                                <th class="text-right">Số tiền</th>
                                <th>Người xem</th>
                                <th class="text-center">Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyRemind">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $remindStatus = $this->Mconstants->remindStatus;
                            $fullNames = array();
                            $customerNames = array();
                            foreach($listReminds as $r){
                                if(!isset($fullNames[$r['CrUserId']])) $fullNames[$r['CrUserId']] = $this->Musers->getFieldValue(array('UserId' => $r['CrUserId']), 'FullName');
                                if($r['UserId'] > 0 && !isset($fullNames[$r['UserId']])) $fullNames[$r['UserId']] = $this->Musers->getFieldValue(array('UserId' => $r['UserId']), 'FullName');
                                if($r['CustomerId'] > 0 && !isset($customerNames[$r['CustomerId']])) $customerNames[$r['CustomerId']] = $this->Mcustomers->getFieldValue(array('CustomerId' => $r['CustomerId']), 'FullName');
                                $dayDiffRemind = getDayDiff($r['RemindDate'], $now, true);
                                $flag = $dayDiffRemind > 0 && $r['RemindStatusId'] != 5; ?>
                                <tr class="trItem<?php if($flag) echo ' trItemCancel'; ?>">
                                    <td><a href="<?php echo base_url('remind/edit/'.$r['RemindId']); ?>"><?php echo $r['RemindCode']; ?></a></td>
                                    <td>
                                        <?php if($flag) echo 'Quá hạn ' . $dayDiffRemind . ' ngày';
                                        else echo getDayDiffText($dayDiffRemind) . ddMMyyyy($r['RemindDate'], $dayDiffRemind < -2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                    </td>
                                    <td><?php echo $r['TimeProcessed']; ?></td>
                                    <td>
                                        <?php $dayDiff = getDayDiff($r['CrDateTime'], $now);
                                        echo getDayDiffText($dayDiff) . ddMMyyyy($r['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
                                    </td>
                                    <td><?php echo $fullNames[$r['CrUserId']]; ?></td>
                                    <td><a href="<?php echo base_url('customer/edit/'.$r['CustomerId']); ?>"><?php echo $customerNames[$r['CustomerId']]; ?></a></td>
                                    <td><a href="<?php echo base_url('order/edit/'.$r['OrderId']); ?>"><?php echo 'DH-'.($r['OrderId'] + 10000); ?></a></td>
                                    <td class="text-right"><?php echo priceFormat($r['OwnCost']); ?></td>
                                    <td>
                                        <?php if($r['UserId'] > 0){
                                            if($r['UserId'] == $user['UserId']) echo 'Chỉ mình tôi';
                                            else echo $fullNames[$r['UserId']];
                                        } ?>
                                    </td>
                                    <td class="text-center"><span class="<?php echo $labelCss[$r['RemindStatusId']]; ?>"><?php echo $remindStatus[$r['RemindStatusId']]; ?></span></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>