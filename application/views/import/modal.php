<div class="modal fade" id="modalConfigExpand" tabindex="-1" role="dialog" aria-labelledby="modalConfigExpand">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thêm thuộc tính mở rộng</h4>
            </div>
            <div class="modal-body">
                <div class="listAdvancedItems">
                    <?php foreach($listOtherServices as $os){ ?>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p class="mg-0 text-bold" id="pOtherServiceName_<?php echo $os['OtherServiceId']; ?>"><?php echo $os['OtherServiceName']; ?></p>
                                    <p class="small"><?php echo $os['OtherServiceDesc']; ?></p>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control cost" value="<?php echo priceFormat($os['ServiceCost']); ?>" id="otherCost_<?php echo $os['OtherServiceId']; ?>">
                                </div>
                                <div class="col-sm-2">
                                    <div class="btn btn-primary btnApplyConfigExpand" data-id="<?php echo $os['OtherServiceId']; ?>">Thêm</div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<!--<div class="modal fade" id="modalSupplier" tabindex="-1" role="dialog" aria-labelledby="modalSupplier">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open('supplier/update', array('id' => 'supplierForm')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thêm nhà cung cấp</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Mã Nhà cung cấp <span class="required">*</span></label>
                                    <input type="text" id="supplierCode" class="form-control hmdrequired" value="" data-field="Mã Nhà cung cấp">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Tên Nhà cung cấp <span class="required">*</span></label>
                                    <input type="text" id="supplierName" class="form-control hmdrequired" value="" data-field="Tên Nhà cung cấp">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Loại</label><br/>
                                    <div class="radio-group">
                                        <span class="item"><input type="radio" name="SupplierType" class="stype" value="1" checked> Công ty</span>
                                        <span class="item"><input type="radio" name="SupplierType" class="stype" value="2"> Cá nhân</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Trạng thái</label>
                                    <?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId'); ?>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label class="control-label">Ghi chú</label>
                                    <input type="text" id="comment" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div id="forCompany">
                                    <div class="form-group">
                                        <label class="control-label">Mã số thuế </label>
                                        <input type="text" id="taxCode" class="form-control" value="" data-field="Mã số thuế">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Cấp hoá đơn</label>
                                        <div class="radio-group">
                                            <span class="item"><input type="radio" name="hasBill" class="hasBill" value="2" checked> Có</span>
                                            <span class="item"><input type="radio" name="hasBill" class="hasBill" value="1"> Không</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label class="control-label">Liên hệ</label>
                                    <div class="box box-default">
                                        <div class="box-body table-responsive no-padding divTable">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Chức vụ</th>
                                                    <th>Tên</th>
                                                    <th>Số điện thoại</th>
                                                    <th><a href="javascript:void(0)" id="add_contact" title="Thêm"><i class="fa fa-plus"></i></a></th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbodyContact">
                                                <tr>
                                                    <td><input type="text" class="form-control positionName" value=""></td>
                                                    <td><input type="text" class="form-control contactName" value=""></td>
                                                    <td><input type="text" class="form-control contactPhone" value=""></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary save-supplier">Lưu</button>
            </div>
            <?php echo form_close(); ?>
            <input type="text" id="supplierId" hidden="hidden" value="0">
            <input type="text" id="sType" hidden="hidden" value="1">
            <input type="text" id="hasBill" hidden="hidden" value="2">
        </div>
    </div>
</div>-->