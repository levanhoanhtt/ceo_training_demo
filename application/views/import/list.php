<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('import/add'); ?>" class="btn btn-primary">Tạo phiếu nhập kho</a></li>
                    <li><button class="btn btn-default">Xuất dữ liệu</button></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả phiếu nhập</a></li>
                        <?php foreach($listFilters as $f){ ?>
                            <li id="liFilter_<?php echo $f['FilterId'] ?>"><a href="#tab_<?php echo $f['FilterId'] ?>" data-id="<?php echo $f['FilterId'] ?>" data-toggle="tab" aria-expanded="false"><?php echo $f['FilterName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="input-group ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả nhập kho theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="import_status">Tình trạng</option>
                                        <option value="import_store">Cơ sở</option>
                                        <option value="import_suppliers">Nhà cung cấp</option>
                                        <option value="import_create">Thời điểm nhập kho</option>
                                        <option value="import_tag">Tag</option>
                                    </select>
                                </div>
                                <div class="form-group mb10 import_status import_store import_suppliers display-display">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group mb10 import_create none-display">
                                    <select class="value_operator form-control">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control import_status block-display">
                                        <?php foreach($this->Mconstants->importStatus as $i => $v){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control import_store none-display">
                                        <?php foreach($listStores as $st){ ?>
                                            <option value="<?php echo $st['StoreId']; ?>"><?php echo $st['StoreName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control import_suppliers none-display">
                                        <?php foreach($listSuppliers as $st){ ?>
                                            <option value="<?php echo $st['SupplierId']; ?>"><?php echo $st['SupplierName']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <select class="form-control import_tag none-display">
                                        <option value="in">chứa</option>
                                        <option value="not in">không chứa</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <!-- group_money group_order field đây là các filter được sử dụng input này-->
                                    <input class="form-control import_tag none-display" type="text">
                                    <input class="form-control datepicker import_create none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker import_create none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <!-- data-href : Đây là link gọi để filter mỗi trang sẽ có 1 link khác nhau -->
                                    <button id="btn-filter" data-href="<?php echo base_url('import/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>

                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <div class="box-header with-border">
                        <select class="form-control input-sm select-action" id="selectAction" style="display: none;">
                            <option value="">Chọn hành động</option>
                            <option value="add_tags">Thêm nhãn</option>
                            <option value="delete_tags">Bỏ nhãn</option>
                            <option value="delete_item">Xóa nhập kho đã chọn</option>
                        </select>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Mã</th>
                                <th>Ngày nhập</th>
                                <th>Cơ sở</th>
                                <th>Nhà cung cấp</th>
                                <th>Người giao hàng</th>
                                <th class="text-center">Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyImport"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="8">
                    <input type="hidden" value="<?php echo base_url('import/edit')?>" id="urlEditImport">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>