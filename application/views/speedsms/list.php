<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl">
                    <li><a href="<?php echo base_url('speedsms/add'); ?>" class="btn btn-primary">Thêm mới</a></li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="nav-tabs-custom updaten">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li class="active" id="liFilter_0"><a href="#tab_0" data-id="0" data-toggle="tab" aria-expanded="true">Tất cả</a></li>
                    </ul>
                </div>
                <div class="input-group margin ctrl-filter updaten">
                    <div class="input-group-btn dropdown" id="searchGroup">
                        <button type="button" class="btn dropdown-toggle transform" data-toggle="dropdown" aria-expanded="false">
                            Điều kiện lọc <span class="fa fa-caret-down"></span>
                        </button>
                        <div class="dropdown-menu mt10 pos-arrow-dropdown animate-scale-dropdown" role="menu">
                            <label class="next-label"><span>Hiển thị tất cả lịch sử gửi SMS theo</span>:</label>
                            <form class="form-inline">
                                <div class="form-group block-display mb10" role="presentation">
                                    <select class="form-control" id="field_select">
                                        <option value="sms_campaign">Tên chiến dịch</option>
                                        <option value="sms_create">Thời gian tạo</option>
                                        <!-- <option value="sms_tag">Tag</option> -->
                                    </select>
                                </div>
                                
                                <div class="form-group mb10 sms_campaign">
                                    <div class="text_opertor">là</div>
                                    <input class="value_operator" value="=" type="hidden"/>
                                </div>
                                <div class="form-group block-display mb10">
                                    <select class="form-control sms_campaign">
                                        <?php foreach($listSmsCampaigns as $s) :?>
                                            <option value="<?php echo $s['SMSCampaignId']; ?>"><?php echo $s['SMSCampaignName']; ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    
                                    <select class="form-control sms_create none-display" id="sms_create">
                                        <option value="between">trong khoảng</option>
                                        <option value="<">trước</option>
                                        <option value="=">bằng</option>
                                        <option value=">">sau</option>
                                    </select>
                                </div>
                                <div class="form-group block-display mb10">
                                    <!-- <input class="form-control sms_tag none-display" type="text"> -->
                                    <input class="form-control datepicker sms_create none-display" placeholder="Nhập thời gian bắt đầu" type="text" id="timeStart">
                                    <input class="form-control datepicker sms_create none-display" placeholder="Nhập thời gian kết thúc" type="text" id="timeEnd">
                                </div>
                                <div class="form-group block-display widthauto">
                                    <button id="btn-filter" data-href="<?php echo base_url('speedsms/searchByFilter'); ?>" type="submit" data-toggle="dropdown" class="btn btn-default">Thêm điều kiện lọc</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="itemSearchName" placeholder="Nhập thông tin tìm kiếm" />
                    <span class="input-group-btn">
                        <button id="btn-popup-filter" disabled type="button" data-toggle="modal" data-target="#save-filter" class="btn btn-disable">Lưu bộ lọc</button>
                    </span>
                    <span class="input-group-btn">
                        <button id="remove-filter" data-href="<?php echo base_url('filter/delete'); ?>" type="button" disabled class="btn btn-disable"><i class="fa fa-times"></i></button>
                    </span>
                </div>
                <div class="mb10 mgt-10">
                    <ul id="container-filters"></ul>
                </div>
                <div class="">
                    <!-- <div class="box-header">
                    </div> -->
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered" id="table-data">
                            <thead>
                            <tr>
                                <!-- <th style="width: 60px"><input type="checkbox" class="iCheckTable" id="checkAll"></th> -->
                                <th>Tên chiên dịch</th>
                                <th>Nội dung tin nhắn</th>
                                <th>Số người nhắn</th>
                                <th>Trạng thái</th>
                                <th>Số tin thành công</th>
                                <th class="text-right">Tổng tiền</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodySpeedSms"></tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="itemTypeId" value="35">
                    <?php $this->load->view('includes/modal/tag'); ?>
                    <?php $this->load->view('includes/modal/filter'); ?>
                    <input type="text" hidden="hidden" id="urlStatusSMS" value="<?php echo base_url('speedsms/showStatusSMS') ?>">
                    <input type="text" hidden="hidden" id="urlDeliveryStatusReport" value="<?php echo base_url('speedsms/delivered') ?>">
                    <input type="text" hidden="hidden" id="urlIncomingSms" value="<?php echo base_url('speedsms/incoming') ?>">
                </div>
            </section>
            <div class="modal fade" id="modalSmsStatus" tabindex="-1" role="dialog" aria-labelledby="modalSmsStatus">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="box-title"><i class="fa fa-comments-o"></i>Số điện thoại gửi thuộc chiến dịch <span id="sMSCampaignName">ssss</span> </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table new-style table-hover table-bordered" id="table-status-sms">
                                        <thead>
                                        <tr>
                                            <th>Số điện thoại</th>
                                            <th>Trạng thái gửi</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyStatusSms"></tbody>
                                    </table>
                                    <h4 class="box-title" id="sms-error" style="display: none"></h4>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>