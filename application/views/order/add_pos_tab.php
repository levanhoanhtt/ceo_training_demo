<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                
            </section>
            <section class="content new-box-stl ft-seogeo" style="padding-left: 0;">
                <div class="nav-tabs-custom no-padding">
                    <ul class="nav nav-tabs tagsOrder">
                        <li class="active">
                            <a href="#tabo_1" data-toggle="tab" data-id="1" flags="0" class="tagPos" id="check_tag_1">Tạo đơn POS 1</a></li>
                        <li><a href="#tabo_2" data-toggle="tab" data-id="2" flags="0" class="tagPos" id="check_tag_2">Tạo đơn POS 2</a></li>
                        <li><a href="#tabo_3" data-toggle="tab" data-id="3" flags="0" class="tagPos" id="check_tag_3">Tạo đơn POS 3</a></li>
                        <li><a href="javascript:void(0);" class="create-tag" data-id="0"><i class="fa fa-plus-square"></i></a></li>
                    </ul>
                    <div class="tab-content">
                        <?php echo form_open('api/order/update', array('id' => 'orderForm')); ?>
                            <div class="tab_comtent row">
                                
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>

        
                <script type="text/javascript">
                    var listCategories = <?php echo json_encode($listCategories); ?>;
                    var listOrderTypes = <?php echo json_encode($listOrderTypes); ?>;
                    var listOrderReasons = <?php echo json_encode($listOrderReasons); ?>;
                    var linkcheckPromotion = "<?php echo base_url('promotion/checkPromotion') ?>";
                    var DEBIT_OTHER_TYPE_ID = "<?php echo DEBIT_OTHER_TYPE_ID; ?>";
                    var listTags = <?php echo json_encode($listTags); ?>;
                    var storeName = "<?php if(count($listStores) == 1) echo $listStores[0]['StoreName'] ?>";
                    var orderStoreId = "<?php echo count($listStores) == 1 ? $listStores[0]['StoreId'] : 0 ?>";
                    var listPromotiontypes = <?php echo json_encode($listPromotionTypes)?>;
                </script>


               
                
                <ul class="list-inline pull-right margin-right-10" style="display: none;">
                    <!--<li><button class="btn btn-primary submit" type="button" data-id="1">Lưu</button></li>
                    <li><a href="<?php //echo base_url('order'); ?>" class="btn btn-default">Đóng</a></li>-->
                    <input type="text" hidden="hidden" id="orderEditUrl" value="<?php echo base_url('order/edit'); ?>">
                    <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
                    <input type="text" hidden="hidden" id="getProductChildComboUrl" value="<?php echo base_url('api/product/getProductChildCombo'); ?>">
                    <input type="text" hidden="hidden" id="getCustomerDetailUrl" value="<?php echo base_url('api/customer/get'); ?>">
                    <input type="text" hidden="hidden" id="getListCustomerUrl" value="<?php echo base_url('api/customer/getList'); ?>">
                    <input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
                    <input type="text" hidden="hidden" id="checkOrderUrl" value="<?php echo base_url('api/order/checkQuantity'); ?>">
                    <input type="text" hidden="hidden" id="editCustomerUrl" value="<?php echo base_url('customer/edit'); ?>">
                    <input type="text" hidden="hidden" id="orderId" value="0">
                    <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerId; ?>">
                    <input type="text" hidden="hidden" id="customerKindId1" value="<?php echo $customerKindId; ?>">
                    <input type="text" hidden="hidden" id="orderStatusId" value="1">
                    <input type="text" hidden="hidden" id="pendingStatusId" value="0">
                    <input type="text" hidden="hidden" id="paymentStatusId" value="0">
                    <input type="text" hidden="hidden" id="verifyStatusId" value="2">
                    <!-- <input type="text" hidden="hidden" id="orderStoreId" value="<?php //echo count($listStores) == 1 ? $listStores[0]['StoreId'] : 0; ?>"> -->
                    <input type="text" hidden="hidden" id="deliveryTypeId" value="1">
                    <input type="text" hidden="hidden" id="offsetOrderTypeId" value="<?php echo OFFSET_ORDER_TYPE_ID; ?>">
                    <input type="text" hidden="hidden" id="debitOtherTypeId" value="<?php echo DEBIT_OTHER_TYPE_ID; ?>">
                    <input type="text" hidden="hidden" id="canEdit" value="1">
                    <input type="text" hidden="hidden" id="transportId" value="0">
                    <input type="text" hidden="hidden" id="customerAddressId" value="0">
                    <input type="text" hidden="hidden" id="remindOwnCost" value="0">
                    <input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                </ul>
                <?php $this->load->view('includes/modal/add_customer', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php $this->load->view('includes/modal/customer_address', array('listCountries' => $listCountries, 'listProvinces' => $listProvinces, 'listDistricts' => $listDistricts)); ?>
                <?php $this->load->view('includes/modal/remind'); ?>
                <?php $this->load->view('order/modal'); ?>
                <?php $this->load->view('transaction/modal_bank'); ?>
            </section>
            <input type="hidden" class="append_html" value="<?= base_url('order/appendHtml') ?>">
            <input type="hidden" value="" id="idcheck" name="" class="form-control">
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>