<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <style>
            #tbodyProduct .tdQuantity{padding-left: 0;padding-right: 0;}
            #tbodyProduct .spanQuantity, #tbodyProduct button{margin-right: 5px;}
            #tbodyProduct input.quantity{width: 50px;margin-right: 8px;}
            #tbodyProduct input.comment{width: 200px;margin-right: 8px;}
            #tbodyProduct select{height: 26px;}
        </style>
        <div class="container-fluid">
            <section class="content-header no-pd-lr">
                <h1 class="ttl-list-order ft-seogeo"><?php echo $title; ?></h1>
                <ul class="list-inline new-stl mgt-10">
                    <li>
                        <button type="button" class="btn btn-primary btnStatus" data-id="2">Duyệt</button>
                        <button type="button" class="btn btn-danger btnStatus" data-id="3">Không duyệt</button>
                    </li>
                    <li>
                        <?php echo form_open('inventory/active', array('id' => 'inventoryForm')); ?>
                        <?php $this->Mconstants->selectObject($listStores, 'StoreId', 'StoreName', 'StoreId', $storeId, true, "--Chọn cơ sở--"); ?>
                        <?php echo form_close(); ?>
                    </li>
                </ul>
            </section>
            <section class="content upn ft-seogeo">
                <div class="">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table new-style table-hover table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 50px;"><input type="checkbox" class="iCheckTable" id="checkAll"></th>
                                <th>Sản phẩm</th>
                                <th>Người thêm</th>
                                <th>Ngày thêm</th>
                                <th class="text-center">Tồn kho</th>
                                <!--<th class="text-center">Đang về</th>-->
                                <th class="text-center">Loại</th>
                                <th>Barcode</th>
                                <th class="text-center">Trạng thái</th>
                                <?php if($storeId == 0){ ?>
                                    <th>Cơ sở</th>
                                <?php } ?>
                                <th class="text-center" style="width: 425px;">Cập nhật tồn kho mới</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct">
                            <?php $productKinds = $this->Mconstants->productKinds;
                            $productStatus = $this->Mconstants->productStatus;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listInventoríes as $p){
                                $quantity = priceFormat($p['InventoryQuantity']); ?>
                            <tr id="trItem_<?php echo $p['InventoryId']; ?>">
                                <td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="<?php echo $p['InventoryId']; ?>"></td>
                                <td>
                                    <a href="<?php echo $p['ProductKindId'] == 3 ? base_url('product/editCombo/'.$p['ProductId']) : base_url('product/editCombo/'.$p['ProductId']); ?>">
                                        <?php echo $p['ProductName']; if(!empty($p['ProductChildName'])) echo ' ('.$p['ProductChildName'].')'; ?>
                                    </a>
                                </td>
                                <td><?php echo $p['FullName']; ?></td>
                                <td><?php echo ddMMyyyy($p['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                <td class="text-center tdQuantityCurrent"><?php echo $quantity; ?></td>
                                <!--<td class="text-center"></td>-->
                                <td class="text-center">
                                    <?php if($p['ProductChildId'] > 0) echo '<span class="label label-default">Sản phẩm con</span>';
                                    else echo '<span class="'.$labelCss[$p['ProductKindId']].'">'.$productKinds[$p['ProductKindId']].'</span>'; ?>
                                </td>
                                <td><?php echo empty($p['ProductChildBarCode']) ? $p['BarCode'] : $p['ProductChildBarCode']; ?></td>
                                <td class="text-center"><span class="<?php echo $labelCss[$p['ProductStatusId']]; ?>"><?php echo $productStatus[$p['ProductStatusId']]; ?></span></td>
                                <?php if($storeId == 0){ ?>
                                    <td><?php echo $this->Mconstants->getObjectValue($listStores, 'StoreId', $p['StoreId'], 'StoreName'); ?></td>
                                <?php } ?>
                                <td class="text-center tdQuantity">
                                    <span class="spanQuantity"><span class="old"><?php echo $quantity; ?></span> -> <span class="current"><?php echo $p['InventoryTypeId'] == 1 ? $p['InventoryQuantity'] + $p['Quantity'] : $p['InventoryQuantity'] - $p['Quantity']; ?></span></span>
                                    <select disabled>
                                        <option value="1"<?php if($p['InventoryTypeId'] == 1) echo ' selected="selected"'; ?>>Tăng</option>
                                        <option value="2"<?php if($p['InventoryTypeId'] == 2) echo ' selected="selected"'; ?>>Giảm</option>
                                    </select>
                                    <input type="text" class="quantity" value="<?php echo $p['Quantity']; ?>" disabled>
                                    <input type="text" class="comment" value="<?php echo $p['Comment']; ?>" placeholder="Ghi chú thay đổi" disabled>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="updateStatusInventoryUrl" value="<?php echo base_url('inventory/updateStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>