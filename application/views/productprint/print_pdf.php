<!DOCTYPE html>
<html>
<head>
    <title>In barcode - Ricky</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        body {
            /*border:1px solid #ddd;*/
            height: 297mm;
            width: 210mm;
            margin-left: auto;
            margin-right: auto;
            padding: 12mm 10mm;
            box-sizing: border-box;
        }

        .wrapA4 {
            /*border: 1px solid #ddd;*/
            overflow: hidden;

        }

        .barcode {
            width: 38mm;
            height: 21mm;
            /*border:1px solid #ddd;*/
            float: left;
            box-sizing: border-box;
            text-align: center;
            position: relative;
            font-weight: bold;
            font-size: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
            /*padding-right:11px;*/
        }

        .impress {
            background: #000;
            color: #fff;
            font-size: 7px;
            position: absolute;
            right: 0px;
            top: 38px;
            padding: 1px 2px 0 1px;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
        }

        img {
            width: 116px;
            height: 34px;
        }

        .h-24 {
            height: 24px;
        }

        .pd-12 {
            padding-top: 12px;
        }

        .pdj-10 {
            padding: 0 10px;
        }
    </style>
</head>
<body>
<div class="wrapA4">
    <?php foreach ($listBarcode as $l){ ?>
        <div class="barcode clearfix">
            <p class="fz-12 h-24 <?php if (strlen($l['name']) <= 26) echo 'pd-12'; else echo 'pdj-10'; ?>"><?php echo $l['name']; ?></p>
            <img src="<?php echo $l['link']; ?>">
            <p class="barcode-z fz-12"><?php echo $l['barcode']; ?></p>
            <div class="clearfix"></div>
        </div>
    <?php } ?>
</div>
<script type="text/javascript">
    window.onload = function() { window.print(); }
</script>
</body>

