<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
        	<?php if($productPrintId > 0){ ?>
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" type="button">Hoàn thành</button></li>
                    <li><a href="<?php echo base_url('productprint'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php echo form_open('productprint/update', array('id' => 'productPrintForm')); ?>

				<div class="box box-default padding20">
					<div class="clearfix top-aded">
						<div class="left">
							<div class="code light-dark">
								<a href="javascript:void(0)"><i class="fa fa-check-circle" aria-hidden="true"></i> <?php echo $productPrint['ProductPrintCode']; ?></a>
							</div>
						</div>
					</div>
					<div class="">
						<div class="table-responsive no-padding divTable">
							<table class="table table-hover table-bordered-bottom">
								<thead class="theadNormal">
								<tr>
									<th>Sản phẩm</th>
									<th class="text-center" style="width: 60px;">Đơn vị</th>
									<th class="text-center" style="width: 130px;">SKU</th>
									<th class="text-center" style="width: 120px;">Bảo hành</th>
									<th class="text-center" style="width: 150px;">Giá</th>
									<th style="width: 85px;">Số lượng</th>
									<th style="width: 30px;"></th>
								</tr>
								</thead>
								<tbody id="tbodyProduct">
									<?php $products = array();
									$productChilds = array();
									$totalWeight = 0;
									foreach($listProductPrintProducts as $op){
										if(!isset($products[$op['ProductId']])) $products[$op['ProductId']] = $this->Mproducts->get($op['ProductId'], true, '', 'ProductName, ProductImage, Price, BarCode, ProductUnitId, GuaranteeMonth');
										$productName = $products[$op['ProductId']]['ProductName'];
										$productImage = $products[$op['ProductId']]['ProductImage'];
										$price = $products[$op['ProductId']]['Price'];
										$barCode = $products[$op['ProductId']]['BarCode'];
										$productUnitId = $products[$op['ProductId']]['ProductUnitId'];
										$guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
										$productChildName = '';
										if($op['ProductChildId'] > 0){
											if(!isset($productChilds[$op['ProductChildId']])) $productChilds[$op['ProductChildId']] = $this->Mproductchilds->get($op['ProductChildId'], true, '', 'ProductName, ProductImage, Price, BarCode, GuaranteeMonth');
											//$productName .= '<br/>(' . $productChilds[$op['ProductChildId']]['ProductName'] .')';
											$productChildName = $productChilds[$op['ProductChildId']]['ProductName'];
											$productImage = $productChilds[$op['ProductChildId']]['ProductImage'];
											$price = $productChilds[$op['ProductChildId']]['Price'];
											$barCode = $productChilds[$op['ProductChildId']]['BarCode'];
											$guaranteeMonth = $products[$op['ProductId']]['GuaranteeMonth'];
										}
										if(empty($productImage)) $productImage = NO_IMAGE; ?>
										<tr data-id="<?php echo $op['ProductId']; ?>" data-child="<?php echo $op['ProductChildId']; ?>">
											<td>
												<img src="<?php echo PRODUCT_PATH.$productImage; ?>" class="productImg">
												<a href="<?php echo base_url('product/edit/'.$op['ProductId']); ?>" class="light-dark" target="_blank">
													<?php echo $productName;
													if(!empty($productChildName)) echo ' ('.$productChildName.')'; ?>
												</a>
											</td>
											<td class="text-center"><?php echo $this->Mconstants->getObjectValue($listProductUnits, 'ProductUnitId', $productUnitId, 'ProductUnitName'); ?></td>
											<td class="text-center"><?php echo $barCode; ?></td>
											<td class="text-center"><?php echo $guaranteeMonth; ?> tháng</td>
											<td class="tdPrice"><span class="spanPrice"><?php echo priceFormat($price); ?></span></td>
											<td><input class="form-control quantity sll" value="<?php echo priceFormat($op['Quantity']); ?>"></td>
											<td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<div class="border-top-title-main">
							<div class="clearfix">
								<div class="box-search-advance product">
									<div>
										<input type="text" class="form-control textbox-advancesearch" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm">
									</div>
									<div class="panel panel-default" id="panelProduct">
										<div class="panel-body" style="width:100%;">
											<div class="list-search-data">
												<div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
												<div>
													<div class="form-group pull-right" style="width: 300px;">
														<?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="table-responsive no-padding divTable">
													<table class="table table-hover table-bordered">
														<thead class="theadNormal">
														<tr>
															<th style="width: 100px;">Ảnh</th>
															<th>Sản phẩm</th>
															<th style="width: 100px;">SKU</th>
															<th style="width: 100px;">Giá</th>
															<th style="width: 100px;">Bảo hành</th>
														</tr>
														</thead>
														<tbody id="tbodyProductSearch"></tbody>
													</table>
												</div>
											</div>
										</div>
										<div class="panel-footer">
											<div class="btn-group pull-right">
												<button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
												<button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
												<input type="text" hidden="hidden" id="pageIdProduct" value="1">
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row mgt-10">
							<div class="col-sm-6">
								<label class="light-blue">Ghi chú</label>
								<div class="box-transprt clearfix mb10">
									<button type="button" class="btn-updaten save" id="btnInsertComment">
										Lưu
									</button>
									<input type="text" class="add-text" id="comment" value="">
								</div>
								<div class="listComment" id="listComment">
									<?php $i = 0;
									$now = new DateTime(date('Y-m-d'));
									foreach($listProductPrintComments as $oc){
										$avatar = (empty($oc['Avatar']) ? NO_IMAGE : $oc['Avatar']);
										$i++;
										if($i < 3){ ?>
											<div class="box-customer mb10">
												<table>
													<tbody>
													<tr>
														<th rowspan="2" valign="top" style="width: 50px;"><img src="<?php echo USER_PATH.$avatar; ?>" alt=""></th>
														<th><a href="javascript:void(0)" class="name"><?php echo $oc['FullName']; ?></a></th>
														<th class="time">
															<?php $dayDiff = getDayDiff($oc['CrDateTime'], $now);
															echo getDayDiffText($dayDiff).ddMMyyyy($oc['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i'); ?>
														</th>
													</tr>
													<tr>
														<td colspan="2">
															<p class="pComment"><?php echo $oc['Comment']; ?></p>
														</td>
													</tr>
													</tbody>
												</table>
											</div>
										<?php }
									} ?>
								</div>
								<?php if(count($listProductPrintComments) > 2){ ?>
								<div class="text-right light-dark">
									<a href="javascript:void(0)" id="aShowComment">Xem tất cả &gt;&gt;</a>
								</div>
								<?php } ?>
							</div>
							<div class="col-sm-6">
								<div class="row tb-sead">
									<div class="col-md-10 text-right">Tổng số lượng sản phẩm:</div>
									<div class="col-md-2 text-right">
										<span id="totalQuantity">0</span> (tem)
									</div>
								</div>
								<p class="pull-right"><a href="<?php echo base_url('productprint/printPdf/'.$productPrintId); ?>" target="_blank" style="width: 100%; padding: 5px 18px;" class="btn btn-default check-order list-btnn"><span>In tem barcode</span></a></p>
							</div>
						</div>
					</div>
				</div>
				<?php $this->load->view('includes/action_logs_new'); ?>
				<input type="text" hidden="hidden" id="productPrintId" value="<?php echo $productPrintId; ?>">
				<input type="text" hidden="hidden" id="productPrintEditUrl" value="<?php echo base_url('productprint/edit'); ?>">
				<input type="text" hidden="hidden" id="insertProductPrintCommentUrl" value="<?php echo base_url('productprint/insertComment'); ?>">
				<input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
				<input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
				<input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                <?php echo form_close(); ?>
                <?php $this->load->view('includes/modal/comment', array('itemName' => 'phiếu in barcode', 'listItemComments' => $listProductPrintComments)); ?>
            </section>
            <?php } else{ ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>