<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit" type="button">Hoàn thành</button></li>
                    <li><a href="<?php echo base_url('productprint'); ?>" class="btn btn-default">Đóng</a></li>
                </ul>
            </section>
            <section class="content new-box-stl ft-seogeo">
                <?php echo form_open('productprint/update', array('id' => 'productPrintForm')); ?>
					<div class="box box-default padding20">
						<div class="">
							<div class="table-responsive no-padding divTable">
								<table class="table table-hover table-bordered-bottom">
									<thead class="theadNormal">
									<tr>
										<th>Sản phẩm</th>
										<th class="text-center" style="width: 60px;">Đơn vị</th>
										<th class="text-center" style="width: 130px;">SKU</th>
										<th class="text-center" style="width: 120px;">Bảo hành</th>
										<th class="text-center" style="width: 150px;">Giá</th>
										<th style="width: 85px;">Số lượng</th>
										<th style="width: 30px;"></th>
									</tr>
									</thead>
									<tbody id="tbodyProduct"></tbody>
								</table>
							</div>
							<div class="border-top-title-main">
								<div class="clearfix">
									<div class="box-search-advance product">
										<div>
											<input type="text" class="form-control textbox-advancesearch updated" id="txtSearchProduct" placeholder="Tìm kiếm sản phẩm">
										</div>
										<div class="panel panel-default" id="panelProduct">
											<div class="panel-body" style="width:100%;">
												<div class="list-search-data">
													<div class="search-loading" style="display: none;">Đang tìm kiếm...</div>
													<div>
														<div class="form-group pull-right" style="width: 300px;">
															<?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', 0, true, 'Nhóm sản phẩm', ' select2'); ?>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="table-responsive no-padding divTable">
														<table class="table table-hover table-bordered">
															<thead class="theadNormal">
															<tr>
																<th style="width: 100px;">Ảnh</th>
																<th>Sản phẩm</th>
																<th style="width: 100px;">SKU</th>
																<th style="width: 100px;">Giá</th>
																<th style="width: 100px;">Bảo hành</th>
															</tr>
															</thead>
															<tbody id="tbodyProductSearch"></tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="panel-footer">
												<div class="btn-group pull-right">
													<button type="button" class="btn btn-default" id="btnPrevProduct"><i class="fa fa-chevron-left"></i></button>
													<button type="button" class="btn btn-default" id="btnNextProduct"><i class="fa fa-chevron-right"></i></button>
													<input type="text" hidden="hidden" id="pageIdProduct" value="1">
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row mgt-10">
								<div class="col-sm-6">
									<label class="light-blue">Ghi chú</label>
									<div class="box-transprt clearfix mb10">
										<button type="button" class="btn-updaten save" id="btnInsertComment">Lưu</button>
										<input type="text" class="add-text" id="comment" value="">
									</div>
									<div class="listComment" id="listComment"></div>
								</div>
								<div class="col-sm-6">
									<div class="row tb-sead">
										<div class="col-md-10 text-right">Tổng số lượng sản phẩm:</div>
										<div class="col-md-2 text-right">
											<span id="totalQuantity">0</span> (tem)
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<input type="text" hidden="hidden" id="productPrintId" value="0">
					<input type="text" hidden="hidden" id="productPrintEditUrl" value="<?php echo base_url('productprint/edit'); ?>">
					<input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('api/product/get'); ?>">
					<input type="text" hidden="hidden" id="getListProductUrl" value="<?php echo base_url('api/product/getList'); ?>">
					<input type="text" hidden="hidden" id="productPath" value="<?php echo PRODUCT_PATH; ?>">
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>