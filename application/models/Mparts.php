<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mparts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "parts";
        $this->_primary_key = "PartId";
    }

    public function checkExist($partId, $partCode){
        $query = "SELECT PartId FROM parts WHERE PartId!=? AND StatusId=? AND PartCode = ?";
        $parts = $this->getByQuery($query, array($partId, STATUS_ACTIVED, $partCode));
        if(!empty($parts)) return true;
        return false;
    }

    public function getList($isIncludeChild = false){
        $retVal = array();
        $parts = $this->getBy(array('StatusId' => STATUS_ACTIVED));
        if($isIncludeChild){
            foreach ($parts as $p) {
                if ($p['ParentPartId'] == 0) {
                    $childs = array();
                    foreach ($parts as $p1) {
                        if ($p1['ParentPartId'] == $p['PartId']) $childs[] = $p1;
                    }
                    $p['Childs'] = $childs;
                    $retVal[] = $p;
                }
            }
        }
        else {
            foreach ($parts as $p) {
                if ($p['ParentPartId'] == 0) {
                    $retVal[] = $p;
                    foreach ($parts as $p1) {
                        if ($p1['ParentPartId'] == $p['PartId']) $retVal[] = $p1;
                    }
                }
            }
        }
        return $retVal;
    }

    public function selectHtml($partId = 0, $selectName = 'PartId', $listParts = array()){
        if(empty($listParts)) $listParts = $this->getList(true);
        $retVal = '<select class="form-control select2" name="'.$selectName.'" id="'.lcfirst($selectName).'"><option value="0" data-id="0">--Chọn--</option>';
        foreach($listParts as $p){
            $retVal .= '<optgroup label="'.$p['PartName'].'">';
            $retVal .= '<option value="'.$p['PartId'].'"'.($p['PartId'] == $partId ? ' selected="selected"' : '').'>'.$p['PartName'].'</option>';
            foreach($p['Childs'] as $p1) $retVal .= '<option value="'.$p1['PartId'].'"'.($p['PartId'] == $partId ? ' selected="selected"' : '').'>'.$p1['PartName'].'</option>';
            $retVal .= '</optgroup>';
        }
        $retVal .= '</select>';
        return $retVal;
    }
}