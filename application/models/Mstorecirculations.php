<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstorecirculations extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "storecirculations";
        $this->_primary_key = "StoreCirculationId";
    }

    public $labelCss = array(
        'StoreCirculationStatus' => array(
            1 => 'label lable-grey',
            2 => 'label lable-yellow',
            3 => 'label lable-green',
            4 => 'label lable-red'
        )
    );

    public function update($postData, $storeCirculationId = 0, $products = array(), $tagNames = array(), $comments = array(), $actionLogs = array()){
        $this->load->model('Mtags');
        $itemTypeId = 7;
        $isUpdate = $storeCirculationId > 0 ? true : false;
        $this->db->trans_begin();
        $storeCirculationId = $this->save($postData, $storeCirculationId);
        if($storeCirculationId > 0){
            if(!empty($actionLogs)){
                $actionLogs['ItemId'] = $storeCirculationId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if($isUpdate){
                $this->db->delete('storecirculationproducts', array('StoreCirculationId' => $storeCirculationId));
                $this->db->delete('itemtags', array('ItemId' => $storeCirculationId, 'ItemTypeId' => $itemTypeId));
            }
            else{
                $this->db->update('storecirculations', array('StoreCirculationCode' => $this->genStoreCirculationCode($storeCirculationId)), array('StoreCirculationId' => $storeCirculationId));
                if(!empty($comments)){
                    $storeCirculationComments = array();
                    foreach($comments as $comment){
                        $storeCirculationComments[] = array(
                            'StoreCirculationId' => $storeCirculationId,
                            'UserId' => $postData['CrUserId'],
                            'Comment' => $comment,
                            'CrDateTime' => $postData['CrDateTime']
                        );
                    }
                    if (!empty($storeCirculationComments)) $this->db->insert_batch('storecirculationcomments', $storeCirculationComments);
                }
            }
            if(!empty($products)){
                $storeCirculationProducts = array();
                foreach($products as $p){
                    $p['StoreCirculationId'] = $storeCirculationId;
                    $storeCirculationProducts[] = $p;
                }
                $this->db->insert_batch('storecirculationproducts', $storeCirculationProducts);
            }
            if(!empty($tagNames)){
                $itemTags = array();
                foreach($tagNames as $tagName){
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if($tagId > 0){
                        $itemTags[] = array(
                            'ItemId' => $storeCirculationId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if(!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $storeCirculationId;
        }
    }

    public function updateField($postData, $storeCirculationId, $actionLog = array(), $inventoryData = array()){
        $this->db->trans_begin();
        $this->save($postData, $storeCirculationId);
        foreach($inventoryData as $id) $this->Minventories->update($id);
        if(!empty($actionLog)){
            $this->load->model('Mactionlogs');
            $this->Mactionlogs->save($actionLog);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function genStoreCirculationCode($storeCirculationId){
        return 'LCK-' . ($storeCirculationId + 10000);
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds = array()){
        $queryCount = "select storecirculations.StoreCirculationId AS totalRow from storecirculations {joins} where {wheres} ORDER BY storecirculations.StoreCirculationId";
        $query = "select {selects} from storecirculations {joins} where {wheres} GROUP BY storecirculations.StoreCirculationId ORDER BY storecirculations.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'storecirculations.*'
        ];
        $joins = [
        ];
        $wheres = array('storecirculations.StoreCirculationStatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        if(!empty($storeIds)){
            $wheres[] = '(storecirculations.StoreSourceId IN ? OR storecirculations.StoreDestinationId IN ?)';
            $dataBind[] = $storeIds;
            $dataBind[] = $storeIds;
        }
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'storecirculations.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'storecirculations.StoreCirculationCode like ? or storecirculations.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'storecirculations.StoreCirculationCode like ?';
                $dataBind[] = "$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        // //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                 $conds = $item['conds'];
                 // var_dump($conds);
                switch ($filed_name) {
                    case 'store_circulation_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'storecirculations.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "storecirculations.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "storecirculations.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(storecirculations.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'store_circulation_status':
                        $wheres[] = "storecirculations.StoreCirculationStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'store_source':
                        $wheres[] = "storecirculations.StoreSourceId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'store_destination':
                        $wheres[] = "storecirculations.StoreDestinationId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_tag':
                        $wheres[] = "storecirculations.StoreCirculationId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 7 AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }

        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $dataStorecirculations = $this->getByQuery($query, $dataBind);
        $now = new DateTime(date('Y-m-d'));
        $listStores = $this->Mstores->getBy(array('ItemStatusId >' => 0));
        for ($i = 0; $i < count($dataStorecirculations); $i++) {
            $dayDiff = getDayDiff($dataStorecirculations[$i]['CrDateTime'], $now);
            $dataStorecirculations[$i]['CrDateTime'] = ddMMyyyy($dataStorecirculations[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataStorecirculations[$i]['DayDiff'] = $dayDiff;
            $dataStorecirculations[$i]['StoreSource'] = $this->Mconstants->getObjectValue($listStores, 'StoreId', $dataStorecirculations[$i]['StoreSourceId'], 'StoreName');
            $dataStorecirculations[$i]['StoreDestination'] = $this->Mconstants->getObjectValue($listStores, 'StoreId', $dataStorecirculations[$i]['StoreDestinationId'], 'StoreName');
            $dataStorecirculations[$i]['StoreCirculationStatus'] = $dataStorecirculations[$i]['StoreCirculationStatusId'] > 0 ? $this->Mconstants->storeCirculationStatus[$dataStorecirculations[$i]['StoreCirculationStatusId']] : '';
            $dataStorecirculations[$i]['labelCss'] = $this->labelCss['StoreCirculationStatus'];
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataStorecirculations;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderStoreCirculations';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}