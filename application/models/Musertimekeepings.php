<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musertimekeepings extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "usertimekeepings";
        $this->_primary_key = "UserTimeKeepingId";
    }

    public function getCurrent($userId){
        $utks = $this->getBy(array('UserId' => $userId, 'StatusId' => 1));
        if(!empty($utks)) return $utks[0];
        return false;
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT utk.*, u.FullName FROM usertimekeepings utk LEFT JOIN users u ON u.UserId = utk.UserId WHERE utk.StatusId > 0" . $this->buildQuery($postData).' ORDER BY utk.CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND utk.UserId=".$postData['UserId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND utk.StatusId=".$postData['StatusId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND utk.KeepingDate >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND utk.KeepingDate <= '{$postData['EndDate']}'";
        return $query;
    }
}