<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpromotiontypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "promotiontypes";
        $this->_primary_key = "PromotionTypeId";
    }
}
