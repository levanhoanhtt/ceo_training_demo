<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musagestatus extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "usagestatus";
        $this->_primary_key = "UsageStatusId";
    }
}
