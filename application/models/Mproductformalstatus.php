<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductformalstatus extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productformalstatus";
        $this->_primary_key = "ProductFormalStatusId";
    }
}