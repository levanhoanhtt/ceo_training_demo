<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mactionlogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "actionlogs";
        $this->_primary_key = "ActionLogId";
    }

    public function getList($itemId, $itemTypeId){
        return $this->getBy(array('ItemId' => $itemId, 'ItemTypeId' => $itemTypeId), false, 'CrDateTime');
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM actionlogs WHERE 1=1" . $this->buildQuery($postData).' ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['ItemTypeId']) && !empty($postData['ItemTypeId'])) $query .= " AND ItemTypeId = '{$postData['ItemTypeId']}'";
        if(isset($postData['UserId']) && !empty($postData['UserId'])) $query .= " AND CrUserId = '{$postData['UserId']}'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }
}
