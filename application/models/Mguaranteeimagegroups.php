<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mguaranteeimagegroups extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "guaranteeimagegroups";
        $this->_primary_key = "GuaranteeImageGroupId";
    }
}