<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfbpages extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "fb_pages";
        $this->_primary_key = "FbPageId";
    }

    public function get_first_page($prefix){
        $pages = $this->getBy(array('Prefix' => $prefix),true,'ASC');
        if (count($pages) > 0) {
            return $pages;
        }
        return null;
    }

    public function searchList($fbPageId){
        $query = "SELECT FbPageId, Prefix FROM fb_pages WHERE FbPageId =".$fbPageId;
        $data = $this->getByQuery($query);
        return $data[0];
    }

    public function getList($fbPageCode){
        $query = "SELECT FbPageId, Prefix FROM fb_pages WHERE FbPageCode =".$fbPageCode;
        $data = $this->getByQuery($query);
        return $data[0];
    }

    public function checkPrefix($fbPageId,$prefix){

        $sql = "SELECT * FROM fb_pages WHERE FbPageId != {$fbPageId} AND Prefix LIKE '%{$prefix}%'";
        $coutnData = $this->getByQuery($sql);
        return count($coutnData);
    }

    public function checkPage($id){
        return count($this->getByQuery("SELECT * FROM fb_pages WHERE FbPageCode = {$id} AND Prefix = {$id}"));
    }


    public function createTableDB($prefix){
        $this->load->dbforge();
        // create table fb_chats_....
        if($this->db->table_exists('fb_chats_'.$prefix)){
            return false;
        }else{
            $fields = array(
                'FbChatId' => array('type' => 'INT','constraint' => 11,'unsigned' => TRUE,'auto_increment' => TRUE),
                'FbPageId' => array('type' => 'smallint','constraint' => '6',),
                'FbChatCode' => array('type' => 'VARCHAR','constraint' => '45',),
                'Message' => array('type' => 'text',),
                'StaffId' => array('type' => 'int','constraint' => '11',),
                'FbUserId' => array('type' => 'int','constraint' => '11',),
                'IsCustomerSend' => array('type' => 'tinyint','constraint' => '4',),
                'ViewStatusId' => array('type' => 'tinyint','constraint' => '4',),
                'AnswerStatusId' => array('type' => 'tinyint','constraint' => '4',),
                'CreatedDate' => array('type' => 'datetime',),
                'CrDateTime' => array('type' => 'datetime',),
            );

            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('FbChatId', TRUE);
            // gives PRIMARY KEY (blog_id)
            // $this->dbforge->add_key('blog_title');
            // gives KEY (blog_title)
            $this->dbforge->create_table('fb_chats_'.$prefix);
        }

        // create table fb_comments_....
        if($this->db->table_exists('fb_comments_'.$prefix)){
            return false;
        }else{
            $fieldsFbComment = array(
                'FbCommentId' => array('type' => 'INT','constraint' => 11,'unsigned' => TRUE,'auto_increment' => TRUE),
                'FbPageId' => array('type' => 'smallint','constraint' => '6',),
                'FbUserId' => array('type' => 'int','constraint' => '11',),
                'CommentId' => array('type' => 'VARCHAR','constraint' => '45', 'COLLATE' => 'utf8_unicode_ci'),
                'SenderId' => array('type' => 'VARCHAR','constraint' => '45', 'COLLATE' => 'utf8_unicode_ci'),
                'SenderName' => array('type' => 'VARCHAR','constraint' => '250','COLLATE' => 'utf8_unicode_ci',),
                'FbPostCode' => array('type' => 'VARCHAR','constraint' => '45','COLLATE' => 'utf8_unicode_ci',),
                'FbPostId' => array('type' => 'int','constraint' => '11',),
                'ParentId' => array('type' => 'VARCHAR','constraint' => '45', 'COLLATE' => 'utf8_unicode_ci',),
                'ParentCommentId' =>array('type' => 'int','constraint' => '11',),
                'Message' => array('type' => 'text', 'COLLATE' => 'utf8_unicode_ci',),
                'ViewStatusId' => array('type' => 'tinyint','constraint' => '4',),
                'AnswerStatusId' => array('type' => 'tinyint','constraint' => '4',),
                'CreatedDateTime' => array('type' => 'VARCHAR','constraint' => '45','COLLATE' => 'utf8_unicode_ci',),
                'CrDateTime' => array('type' => 'datetime',),
                'CommentStatusId' => array('type' => 'tinyint','constraint' => '4',),
                'Image' => array('type' => 'VARCHAR','constraint' => '250',),
            );
            $this->dbforge->add_field($fieldsFbComment);
            $this->dbforge->add_key('FbCommentId', TRUE);
            $this->dbforge->create_table('fb_comments_'.$prefix);
        }

        // create table fb_posts_....

        if($this->db->table_exists('fb_posts_'.$prefix)){
            return false;
        }else{
            $fieldsFbPosts = array(
                'FbPostId' => array('type' => 'INT','constraint' => 11,'unsigned' => TRUE,'auto_increment' => TRUE),
                'PostId' => array('type' => 'varchar','constraint' => '45', 'COLLATE' => 'utf8_unicode_ci',),
                'FbPageId' => array('type' => 'smallint','constraint' => '6',),
                'PostContent' => array('type' => 'text', 'COLLATE' => 'utf8_unicode_ci'),
                'FbUserId' => array('type' => 'int','constraint' => '11',),
                'PostLink' => array('type' => 'VARCHAR','constraint' => '650','COLLATE' => 'utf8_unicode_ci',),
                'SenderId' => array('type' => 'VARCHAR','constraint' => '45','COLLATE' => 'utf8_unicode_ci',),
                'SenderName' => array('type' => 'VARCHAR','constraint' => '250','COLLATE' => 'utf8_unicode_ci',),
                'CreatedDateTime' => array('type' => 'VARCHAR','constraint' => '45','COLLATE' => 'utf8_unicode_ci',),
                'CrDateTime' => array('type' => 'datetime',),
            );
            $this->dbforge->add_field($fieldsFbPosts);
            $this->dbforge->add_key('FbPostId', TRUE);
            $this->dbforge->create_table('fb_posts_'.$prefix);
        }

        // create table fb_threads....

        if($this->db->table_exists('fb_threads_'.$prefix)){
            return false;
        }else{
            $fieldsFbThreads = array(
                'FbThreadId' => array('type' => 'INT','constraint' => 11,'unsigned' => TRUE,'auto_increment' => TRUE),
                'FbPageId' => array('type' => 'smallint','constraint' => '6',),
                'FbPageCode' => array('type' => 'VARCHAR','constraint' => '100','COLLATE' => 'utf8_unicode_ci',),
                'TimeStr' => array('type' => 'VARCHAR','constraint' => '100','COLLATE' => 'utf8_unicode_ci',),
                'ThreadId' => array('type' => 'VARCHAR','constraint' => '45','COLLATE' => 'utf8_unicode_ci',),
            );
            $this->dbforge->add_field($fieldsFbThreads);
            $this->dbforge->add_key('FbThreadId', TRUE);
            $this->dbforge->create_table('fb_threads_'.$prefix);
        }

    }

    // search post, kiem tra khi từ node js tra về nếu post có đã có cmt thì cho hiện lên con chua co thi ko cho hien len
    public function searchPost($fbPages = array(),$fbPostId){
        $sql = "SELECT c.*, pa.FbPageCode AS FbPageCode, pa.FbPageName AS FbPageName, p.FbPostId AS FbPostId, p.PostContent AS PostContent, p.PostLink  AS PostLink, pa.Prefix AS Prefix FROM fb_posts_{$fbPages['Prefix']} p JOIN fb_comments_{$fbPages['Prefix']} c ON c.FbPostId = p.FbPostId JOIN fb_pages pa ON pa.FbPageId = p.FbPageId WHERE p.FbPageId = {$fbPages['FbPageId']} AND c.FbCommentId IN (SELECT Max(FbCommentId) AS FbCommentId FROM fb_comments_{$fbPages['Prefix']} WHERE FbPostId = {$fbPostId} GROUP BY FbPostId ORDER BY CreatedDateTime DESC ) GROUP BY c.FbPostId ORDER BY  p.CreatedDateTime DESC";
        return $this->getByQuery($sql);
    }

    // fb_posts_....
    public function getListPost($fbPages = array(), $viewStatusId, $answerStatusId){
        $sql = "SELECT c.*, pa.FbPageCode AS FbPageCode, pa.FbPageName AS FbPageName, p.FbPostId AS FbPostId, p.PostContent AS PostContent, p.PostLink  AS PostLink, pa.Prefix AS Prefix FROM fb_posts_{$fbPages['Prefix']} p right JOIN fb_comments_{$fbPages['Prefix']} c ON c.FbPostId = p.FbPostId right JOIN fb_pages pa ON pa.FbPageId = p.FbPageId WHERE p.FbPageId = {$fbPages['FbPageId']} ".$this->buildQueryPost($fbPages['FbPageId'], $viewStatusId, $answerStatusId)." AND c.FbCommentId IN (SELECT Max(FbCommentId) AS FbCommentId FROM fb_comments_{$fbPages['Prefix']} GROUP BY FbPostId ORDER BY CreatedDateTime DESC ) GROUP BY c.FbPostId ORDER BY  p.CreatedDateTime DESC";
        return $this->getByQuery($sql);
    }

    private function buildQueryPost($pageId, $viewStatusId, $answerStatusId){
        $query = '';
        if($pageId > 0 && !empty($viewStatusId) == "" && !empty($answerStatusId) == "") $query.=""; // tất cả
        if($pageId > 0 && $viewStatusId == 2) $query.=" AND c.ViewStatusId = ".$viewStatusId; // chua đọc
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 1) $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId}"; // đa đọc đã trả lời
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 2) $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId}"; // doc chua tra lời
        return $query;
    }
    //...

    public function getByPost($fbPostId, $prefix){
        $query = "SELECT * FROM fb_posts_{$prefix} WHERE FbPostId = {$fbPostId}";
        return $this->getByQuery($query);
    }

    public function getByComment($fbPostId, $prefix){
        $query = "SELECT * FROM fb_comments_{$prefix} WHERE FbPostId = {$fbPostId}";
        return $this->getByQuery($query);
    }

    public function getByCommentOne($commentId, $tableName){
        $query = "SELECT * FROM {$tableName} WHERE FbCommentId = {$commentId}";
        return $this->getByQuery($query);
    }


    //fb_chats_...
    public function getListChats($fbPages = array(),$viewStatusId,$answerStatusId,$keySearch){
        return $this->getByQuery("SELECT p.FbPageCode AS FbPageCode, c.FbChatId AS FbChatId, c.Message AS Message, u.FbId AS FbId, u.FbUserName AS FbUserName, u.Email AS Email, u.Address AS Address, c.CrDateTime as CrDateTime, c.IsCustomerSend AS IsCustomerSend, p.Prefix AS Prefix FROM fb_chats_{$fbPages['Prefix']} c LEFT JOIN fb_users u ON c.FbUserId = u.FbUserId LEFT JOIN fb_pages p ON p.FbPageId = c.FbPageId
            WHERE  c.FbChatId IN (SELECT Max(FbChatId) AS FbChatId FROM fb_chats_{$fbPages['Prefix']} GROUP BY FbUserId ORDER BY CrDateTime DESC ) ".$this->buildQueryChats($fbPages['FbPageId'], $viewStatusId, $answerStatusId,$keySearch)."
            GROUP BY u.FbUserId ORDER BY  c.CrDateTime DESC");
    }

    private function buildQueryChats($pageId, $viewStatusId, $answerStatusId,$keySearch){
        $query = '';
        if($pageId > 0 && !empty($viewStatusId) == "" && !empty($answerStatusId) == "") $query.=""; // tất cả
        if($pageId > 0 && $viewStatusId == 2) $query.=" AND c.ViewStatusId = ".$viewStatusId; // chua đọc
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 1) $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId}"; // đa đọc đã trả lời
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 2) $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId}"; // doc chua tra lời
        if($pageId > 0 && !empty($viewStatusId) == "" && !empty($answerStatusId) == "" && !empty($keySearch) != "") $query .= "  AND u.FbUserName LIKE '%{$keySearch}%'";
        if($pageId > 0 && $viewStatusId == 2 && !empty($keySearch) != "") $query.=" AND c.ViewStatusId = {$viewStatusId} AND u.FbUserName LIKE '%{$keySearch}%'"; // chua đọc + search
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 1 && !empty($keySearch) != "") $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId} AND u.FbUserName LIKE '%{$keySearch}%'"; // đa đọc đã trả lời + search
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 2 && !empty($keySearch) != "") $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId} AND u.FbUserName LIKE '%{$keySearch}%'"; // doc chua tra lời
        return $query;
    }

    public function getMessageChats($fbPageCode, $fbId, $prefix){
        return $this->getByQuery("SELECT c.IsCustomerSend AS IsCustomerSend , u.FbId AS FbId, c.FbUserId AS FbUserId, u.FbUserName AS FbUserName,c.FbPageId AS FbPageId, p.FbPageCode AS FbPageCode, p.FbPageName AS FbPageName , c.CrDateTime AS CrDateTime, c.Message AS Message FROM fb_chats_{$prefix} c LEFT JOIN fb_users u ON u.FbUserId = c.FbUserId
        LEFT JOIN fb_pages p ON p.FbPageId = c.FbPageId WHERE  p.FbPageCode = ? AND u.FbId = ? ORDER BY c.CrDateTime ASC", array($fbPageCode, $fbId));
    }
    // end


    protected $_primary_key = '';
// hàm save chung
    public function saveGeneral($data, $id = 0, $tableName, $fieldNull = array()) {
        if ($id == 0) {
            foreach($fieldNull as $field){
                if(!isset($data[$field]) || empty($data[$field])){
                    $this->db->set($field, null);
                    unset($data[$field]);
                }
            }
            $this->db->insert($tableName, $data);
            return $this->db->insert_id();
        }
        else {
            foreach($fieldNull as $field){
                if(!isset($data[$field]) || empty($data[$field])){
                    $this->db->set($field, null);
                    unset($data[$field]);
                }
            }
            $this->db->where($this->_primary_key, $id);
            $this->db->update($tableName, $data);
            return $id;
        }
    }

}
