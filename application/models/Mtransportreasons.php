<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransportreasons extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transportreasons";
        $this->_primary_key = "TransportReasonId";
    }
}
