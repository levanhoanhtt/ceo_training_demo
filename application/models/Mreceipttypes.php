<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mreceipttypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "receipttypes";
        $this->_primary_key = "ReceiptTypeId";
    }
}
