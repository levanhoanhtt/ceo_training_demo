<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muserstaffs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "userstaffs";
        $this->_primary_key = "UserStaffId";
    }
}