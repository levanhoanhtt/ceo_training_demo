<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlibrarypages extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "librarypages";
        $this->_primary_key = "LibraryPageId";
    }

    public function update($fbPageId, $librarySentenceIds){
    	$this->db->trans_begin();
    	$this->db->delete('librarypages', array('FbPageId' => $fbPageId));
        $libraryPages = array();
    	foreach($librarySentenceIds as $librarySentenceId){
            $libraryPages[] = array(
                'LibrarySentenceId' => $librarySentenceId,
                'FbPageId' => $fbPageId
            );
        }
    	if(!empty($libraryPages)) $this->db->insert_batch('librarypages', $libraryPages);
    	if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }
}
