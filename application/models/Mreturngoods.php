<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mreturngoods extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "returngoods";
        $this->_primary_key = "ReturnGoodId";
    }

    public function update($postData, $returnGoodId, $products = array(), $tagNames = array(), $actionLogs = array()){
        $this->load->model('Mtags');
        $itemTypeId = 14;
        $isUpdate = $returnGoodId > 0 ? true : false;
        $this->db->trans_begin();
        $returnGoodId = $this->save($postData, $returnGoodId, array('UpdateUserId', 'UpdateDateTime'));
        if($returnGoodId > 0){
            if(!empty($actionLogs)){
                $actionLogs['ItemId'] = $returnGoodId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if($isUpdate){
                $this->db->delete('returngoodproducts', array('ReturnGoodId' => $returnGoodId));
                $this->db->delete('itemtags', array('ItemId' => $returnGoodId, 'ItemTypeId' => $itemTypeId));
            }
            else{
                $returnGoodCode = 'HDH-' . ($returnGoodId + 10000);
                $this->db->update('returngoods', array('ReturnGoodCode' => $returnGoodCode), array('ReturnGoodId' => $returnGoodId));
            }
            if (!empty($products)) {
                $returnGoodProducts = array();
                foreach ($products as $p) {
                    $p['ReturnGoodId'] = $returnGoodId;
                    $returnGoodProducts[] = $p;
                }
                if (!empty($returnGoodProducts)) $this->db->insert_batch('returngoodproducts', $returnGoodProducts);
            }
            if(!empty($tagNames)){
                $itemTags = array();
                foreach($tagNames as $tagName){
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if($tagId > 0){
                        $itemTags[] = array(
                            'ItemId' => $returnGoodId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if(!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $returnGoodId;
        }
    }

    public function changeStatusBatch($returnGoodIds, $statusId, $user){
        $crDateTime = getCurentDateTime();
        $statusName = $this->Mconstants->returnGoodStatus[$statusId];
        $this->db->trans_begin();
        $this->db->query('UPDATE returngoods SET TransportStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE ReturnGoodId IN('.implode(',', $returnGoodIds).')', array($statusId, $user['UserId'], $crDateTime));
        $actionLogs = array();
        foreach($returnGoodIds as $returnGoodId){
            $actionLogs[] = array(
                'ItemId' => $returnGoodId,
                'ItemTypeId' => 9,
                'ActionTypeId' => $statusId > 0 ? 2 : 3,
                'Comment' => $statusId > 0 ? ($user['FullName'].' thay đổi trạng thái đơn hoàn hàng về về '.$statusName) : ($user['FullName'].' xóa đơn hoàn hàng về'),
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page){
        $queryCount = "select returngoods.ReturnGoodId AS totalRow from  returngoods {joins} where {wheres}";
        $query = "select {selects} from returngoods {joins} where {wheres} ORDER BY returngoods.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'returngoods.*',
            'customers.CustomerId',
            'customers.FullName',
            'customers.Email',
            'customers.PhoneNumber',
            'customers.PhoneNumber2',
            'stores.StoreName'
        ];
        $joins = [
            'customers' => "left join customers on customers.CustomerId = returngoods.CustomerId",
            'stores' => "left join stores on stores.StoreId = returngoods.StoreId"
        ];
        $wheres = array('returngoods.TransportStatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'returngoods.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            else{
                $whereSearch = 'returngoods.ReturnGoodCode like ? or customers.Email like ? or customers.PhoneNumber like ? or customers.PhoneNumber2 like ?';
                for( $i = 0; $i < 4; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                switch ($filed_name) {
                    case 'order_status':
                        $wheres[] = "returngoods.ReturnGoodTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_status_transport':
                        $wheres[] = "returngoods.TransportStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'returngoods.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "returngoods.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "returngoods.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(returngoods.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'order_store':
                        $wheres[] = "returngoods.StoreId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_tag':
                        $wheres[] = "returngoods.ReturnGoodId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 14 AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataReturnGoods = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataReturnGoods); $i++) {
            $dataReturnGoods[$i]['TransportStatus'] = $dataReturnGoods[$i]['TransportStatusId'] > 0 ? $this->Mconstants->transportStatus[$dataReturnGoods[$i]['TransportStatusId']] : '';
            $dataReturnGoods[$i]['ReturnGoodType'] = $dataReturnGoods[$i]['ReturnGoodTypeId'] > 0 ? $this->Mconstants->returnGoodTypes[$dataReturnGoods[$i]['ReturnGoodTypeId']] : '';
            $dayDiff = getDayDiff($dataReturnGoods[$i]['CrDateTime'], $now);
            $dataReturnGoods[$i]['CrDateTime'] = ddMMyyyy($dataReturnGoods[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataReturnGoods[$i]['DayDiff'] = $dayDiff;
            $dataOrders[$i]['transportStatusCss'] = $this->Mtransports->labelCss['TransportStatusCss'];
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataReturnGoods;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentReturnGoods';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}