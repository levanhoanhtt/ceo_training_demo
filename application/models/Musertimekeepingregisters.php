<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musertimekeepingregisters extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "usertimekeepingregisters";
        $this->_primary_key = "UserTimeKeepingRegisterId";
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $isJoin = false){
        if($isJoin) $query = "SELECT utk.*, u.FullName FROM usertimekeepingregisters utk LEFT JOIN users u ON u.UserId = utk.UserId WHERE utk.StatusId > 0" . $this->buildQuery($postData).' ORDER BY utk.DateTimeIn DESC';
        else $query = "SELECT * FROM usertimekeepingregisters utk WHERE StatusId > 0" . $this->buildQuery($postData).' ORDER BY DateTimeIn DESC';
        if($perPage > 0){
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND utk.UserId=".$postData['UserId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND utk.KeepingDate >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND utk.KeepingDate <= '{$postData['EndDate']}'";
        return $query;
    }

    public function checkDateTimeIn($userId, $keepingDate, $dateTimeIn){
        $retVal = array('UserTimeKeepingRegisterId' => -1, 'Diff' => 0);
        $utkrs = $this->getByQuery('SELECT UserTimeKeepingRegisterId, DateTimeIn FROM usertimekeepingregisters WHERE UserId = ? AND KeepingDate = ?', array($userId, $keepingDate));
        foreach($utkrs as $utkr){
            $diff = strtotime($dateTimeIn) - strtotime($utkr['DateTimeIn']);
            if(abs($diff) <= 60 * 60){
                $retVal['UserTimeKeepingRegisterId'] = $utkr['UserTimeKeepingRegisterId'];
                $retVal['Diff'] = $diff;
                break;
            }
            else $retVal['UserTimeKeepingRegisterId'] = 0;
        }
        return $retVal;
    }
}