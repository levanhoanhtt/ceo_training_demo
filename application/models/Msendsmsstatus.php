<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msendsmsstatus extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "sendsmsstatus";
        $this->_primary_key = "SendSMSStatusId";
    }

}