<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfamilytransactions extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "familytransactions";
        $this->_primary_key = "FamilyTransactionId";
    }

    public function update($postData, $transactionInternalId = 0, $tagNames = array(), $actionLogs = array()){
        $this->load->model('Mtags');
        $transactionTypeId = $postData['TransactionTypeId'];
        $itemTypeId = $transactionTypeId == 1 ? 28 : 29;
        $isUpdate = $transactionInternalId > 0;
        $this->db->trans_begin();
        $transactionInternalId = $this->save($postData, $transactionInternalId);
        if ($transactionInternalId > 0) {
            if (!empty($actionLogs)) {
                $actionLogs['ItemId'] = $transactionInternalId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if($isUpdate) $this->db->delete('itemtags', array('ItemId' => $transactionInternalId, 'ItemTypeId' => $itemTypeId));
            else{
                $transactionCode =  ($postData['TransactionTypeId'] == 1 ? 'PTGD-' : 'PCGD-') . ($transactionInternalId + 10000);
                $this->db->update('familytransactions', array('TransactionCode' => $transactionCode), array('FamilyTransactionId' => $transactionInternalId));
            }
            if(!empty($tagNames)) {
                $itemTags = array();
                foreach ($tagNames as $tagName) {
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if ($tagId > 0) {
                        $itemTags[] = array(
                            'ItemId' => $transactionInternalId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if (!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $transactionInternalId;
        }
    }

    public function updateField($postData, $transactionInternalId, $actionLogs){
        $this->db->trans_begin();
        $transactionId = $this->save($postData, $transactionInternalId);
        if($transactionId > 0 && !empty($actionLogs)){
            $this->load->model('Mactionlogs');
            $this->Mactionlogs->save($actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateVerifyLevel($transactionInternalId, $verifyLevelId, $transactionInternal){
        $crDateTime = getCurentDateTime();
        $transactionTypeId = $transactionInternal['TransactionTypeId'];
        $flag = $verifyLevelId == 6 && $transactionInternal['TransactionStatusId'] == 2;
        $actionLogs = array();
        $actionLogs[] = array(
            'ItemId' => $transactionInternalId,
            'ItemTypeId' => $transactionTypeId == 1 ? 28 : 29,
            'ActionTypeId' => 2,
            'Comment' => $transactionInternal['Comment'],
            'CrUserId' => $transactionInternal['CrUserId'],
            'CrDateTime' => $crDateTime
        );
        $this->db->trans_begin();
        if($flag) $this->db->query('UPDATE familytransactions SET TransactionStatusId = 3, VerifyLevelId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE FamilyTransactionId = ?', array($verifyLevelId, $transactionInternal['CrUserId'], $crDateTime, $transactionInternalId));
        else $this->db->query('UPDATE familytransactions SET VerifyLevelId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE FamilyTransactionId = ?', array($verifyLevelId, $transactionInternal['CrUserId'], $crDateTime, $transactionInternalId));
        $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $transactionTypesId = 0){
        $queryCount = "SELECT familytransactions.FamilyTransactionId AS totalRow FROM familytransactions {joins} WHERE {wheres}";
        $query = "SELECT {selects} FROM familytransactions {joins} WHERE {wheres} ORDER BY familytransactions.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'familytransactions.*',
            //'funds.FundName',
            'transactionkinds.TransactionKindName'
        ];
        $joins = [
            //'funds' => "left join funds on funds.FundId = familytransactions.FundId",
            'transactionkinds' => "left join transactionkinds on transactionkinds.TransactionKindId = familytransactions.TransactionKindId",
        ];

        $wheres = array('TransactionStatusId > 0');
        if($transactionTypesId > 0) $wheres[] = "familytransactions.TransactionTypeId = {$transactionTypesId}";
        $whereSearch= '';
        $dataBind = [];
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'familytransactions.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*else if(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'familytransactions.TransactionCode like ? or familytransactions.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'familytransactions.TransactionCode like ? or transactionkinds.TransactionKindName like ?';
                for( $i = 0; $i < 2; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'group_money':
                        $wheres[] = "familytransactions.PaidCost $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'group_date':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'familytransactions.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "familytransactions.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "familytransactions.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(familytransactions.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'group_status_trans':
                        $wheres[] = "familytransactions.TransactionStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_status_verify':
                        $wheres[] = "familytransactions.VerifyLevelId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_moneysource':
                        $wheres[] = "familytransactions.MoneySourceId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'group_kind':
                        $wheres[] = "familytransactions.TransactionKindId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    /*case 'group_fund':
                        $wheres[] = "familytransactions.FundId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;*/
                    case 'group_bank':
                        $wheres[] = "transactions.BankId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    /*case 'group_treasurer':
                        $wheres[] = "familytransactions.TreasurerId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;*/
                    case 'transaction_tag':
                        $wheres[] = "familytransactions.FamilyTransactionId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId IN(28,29) AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataTransactions = $this->getByQuery($query, $dataBind);
        $transactionStatus =  $this->Mconstants->transactionStatus;
        $verifyLevels =  $this->Mconstants->verifyLevels;
        $moneySources =  $this->Mconstants->moneySources;
        for ($i = 0; $i < count($dataTransactions); $i++) {
            $dataTransactions[$i]['TransactionStatusName'] = $transactionStatus[$dataTransactions[$i]['TransactionStatusId']];
            $dataTransactions[$i]['VerifyLevelName'] = $verifyLevels[$dataTransactions[$i]['VerifyLevelId']];
            $dataTransactions[$i]['MoneySourceName'] = $moneySources[$dataTransactions[$i]['MoneySourceId']];
            $dayDiff = getDayDiff($dataTransactions[$i]['CrDateTime'], $now);
            $dataTransactions[$i]['CrDateTime'] = ddMMyyyy($dataTransactions[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataTransactions[$i]['DayDiff'] = $dayDiff;
            $dataTransactions[$i]['labelCss'] = $this->Mtransactions->labelCss;
        }

        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data = array();
        $data['dataTables'] = $dataTransactions;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentFamilyTransactions';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}