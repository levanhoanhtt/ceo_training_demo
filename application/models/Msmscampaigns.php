<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msmscampaigns extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "smscampaigns";
        $this->_primary_key = "SMSCampaignId";
    }
}