<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mimports extends MY_Model
{

    function __construct() {
        parent::__construct();
        $this->_table_name = 'imports';
        $this->_primary_key = 'ImportId';
    }

    public function update($postData, $importId = 0, $products = array(), $productsError = array(), $tagNames = array(), $services = array(), $inventoryData = array(), $actionLogs = array()){
        $this->load->model('Mpricechanges');
        $this->load->model('Mpricechangelogs');
        $this->load->model('Minventories');
        $this->load->model('Mtags');
        $itemTypeId = 8;
        $isUpdate = $importId > 0;
        $this->db->trans_begin();
        $importId = $this->save($postData, $importId, array('UpdateUserId', 'UpdateDateTime'));
        if($importId > 0){
            if(!empty($actionLogs)){
                $actionLogs['ItemId'] = $importId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if($isUpdate){
                $this->db->delete('importproducts', array('ImportId' => $importId));
                $this->db->delete('importproducterrors', array('ImportId' => $importId));
                $this->db->delete('itemtags', array('ItemId' => $importId, 'ItemTypeId' => $itemTypeId));
                $this->db->delete('importservices', array('ImportId' => $importId));
            }
            else $this->db->update('imports', array('ImportCode' => $this->genImportCode($importId)), array('ImportId' => $importId));
            if(!empty($products)){
                $importProducts = array();
                foreach($products as $p){
                    $p['ImportId'] = $importId;
                    $importProducts[] = $p;
                }
                if(!empty($importProducts)) $this->db->insert_batch('importproducts', $importProducts);
                if($postData['ImportStatusId'] == STATUS_ACTIVED){
                    $crUserId = isset($postData['UpdateUserId']) ? $postData['UpdateUserId'] : $postData['CrUserId'];
                    $crDateTime = isset($postData['UpdateDateTime']) ? $postData['UpdateDateTime'] : $postData['CrDateTime'];
                    foreach($products as $p){
                        //gia ca
                        $pc = $this->Mpricechanges->getBy(array('ProductId' => $p['ProductId'], 'ProductChildId' => $p['ProductChildId']), true);
                        //-Tong so luong cu tat ca co so + gia cu
                        $oldPrice = $pc ? $pc['Price'] : $p['AveragePrice'];
                        $oldQuantity = $this->Mproductquantity->getQuantity($p['ProductId'], $p['ProductChildId']);
                        if($oldQuantity + $p['Quantity'] > 0){
                            $price = ($oldPrice * $oldQuantity + $p['AveragePrice'] * $p['Quantity']) / ($oldQuantity + $p['Quantity']);
                            $price = ceil($price);
                        }
                        else $price = $p['AveragePrice'];
                        //-Cap nhat gia cuoi vao productprices
                        if($pc) $this->Mpricechanges->save(array('Price' => $price, 'UpdateUserId' => $crUserId, 'UpdateDateTime' => $crDateTime), $pc['PriceChangeId']);
                        else{
                            $this->Mpricechanges->save(array(
                                'ProductId' => $p['ProductId'],
                                'ProductChildId' => $p['ProductChildId'],
                                'Price' => $price,
                                'CrUserId' => $crUserId,
                                'CrDateTime' => $crDateTime
                            ));
                        }
                        //-Cap nhat gia vao log de tra cuu, theo doi
                        $this->Mpricechangelogs->save(array(
                            'ProductId' => $p['ProductId'],
                            'ProductChildId' => $p['ProductChildId'],
                            'ImportId' => $importId,
                            'OldPrice' => $oldPrice,
                            'Price' => $price,
                            'CrUserId' => $crUserId,
                            'CrDateTime' => $crDateTime
                        ));
                    }
                    //so luong tren 1 co so
                    $importCode = $this->genImportCode($importId);
                    foreach($inventoryData as $id){
                        $id['Comment'] = 'Cộng số lượng từ nhập kho '.$importCode;
                        $this->Minventories->update($id);
                    }
                }
            }
            if(!empty($productsError)){
                $importProductErrors = array();
                foreach ($productsError as $pe) {
                    $pe['ImportId'] = $importId;
                    $importProductErrors[] = $pe;
                }
                if(!empty($importProductErrors)) $this->db->insert_batch('importproducterrors', $importProductErrors);
            }
            if(!empty($tagNames)){
                $itemTags = array();
                foreach($tagNames as $tagName){
                    $tagId = $this->Mtags->getTagId($tagName, $itemTypeId);
                    if($tagId > 0){
                        $itemTags[] = array(
                            'ItemId' => $importId,
                            'ItemTypeId' => $itemTypeId,
                            'TagId' => $tagId
                        );
                    }
                }
                if(!empty($itemTags)) $this->db->insert_batch('itemtags', $itemTags);
            }
            if(!empty($services)){
                $importServices = array();
                foreach ($services as $s) {
                    $s['ImportId'] = $importId;
                    $importServices[] = $s;
                }
                if(!empty($importServices)) $this->db->insert_batch('importservices', $importServices);
            }
        }
        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $importId;
        }
    }

    public function updateField($postData, $importId, $actionLogs = array()){
        $this->db->trans_begin();
        $importId = $this->save($postData, $importId);
        if($importId > 0){
            if(!empty($actionLogs)){
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function genImportCode($importId){
        return 'NK-' . ($importId + 10000);
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds = array()){
        $queryCount = "select imports.ImportId AS totalRow from imports {joins} where {wheres} GROUP BY imports.ImportId";
        $query = "select {selects} from imports {joins} where {wheres} GROUP BY imports.ImportId ORDER BY imports.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'imports.*',
            'suppliers.SupplierName',
            'stores.StoreName'
        ];
        $joins = [
            'suppliers' => "LEFT JOIN suppliers ON suppliers.SupplierId = imports.SupplierId",
            'stores' => "LEFT JOIN stores ON stores.StoreId = imports.StoreId"
        ];
        $wheres = array('imports.ImportStatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        if(!empty($storeIds)){
            $wheres[] = 'imports.StoreId IN ?';
            $dataBind[] = $storeIds;
        }
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'imports.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'imports.ImportCode like ? or imports.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'imports.ImportCode like ? or imports.DeliverName like ? or imports.DeliverPhone like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                 $conds = $item['conds'];
                switch ($filed_name) {
                    case 'import_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'imports.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "imports.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "imports.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(imports.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'import_status':
                        $wheres[] = "imports.ImportStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'import_store':
                        $wheres[] = "imports.StoreId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'import_suppliers':
                        $wheres[] = "imports.SupplierId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'import_tag':
                        $wheres[] = "imports.ImportId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 8 AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }

        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataImports = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataImports); $i++) {
            $dayDiff = getDayDiff($dataImports[$i]['CrDateTime'], $now);
            $dataImports[$i]['CrDateTime'] = ddMMyyyy($dataImports[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataImports[$i]['DayDiff'] = $dayDiff;
            $dataImports[$i]['ImportStatus'] = $dataImports[$i]['ImportStatusId'] > 0 ? $this->Mconstants->importStatus[$dataImports[$i]['ImportStatusId']] : '';
            $dataImports[$i]['labelCss'] = $this->Mconstants->labelCss;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataImports;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentImports';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}