<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msmslogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "smslogs";
        $this->_primary_key = "SMSLogId";
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "SELECT smslogs.SMSLogId AS totalRow FROM smslogs {joins} WHERE {wheres}";
        $query = "SELECT {selects} FROM smslogs {joins} WHERE {wheres} ORDER BY smslogs.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'smslogs.*',
            'smscampaigns.SMSCampaignName'
        ];
        $joins = [
            'smscampaigns' => "left join smscampaigns on smscampaigns.SMSCampaignId = smslogs.SMSCampaignId"
        ];
        $wheres = array('smslogs.SMSLogId > 0');
        $whereSearch= '';
        $dataBind = [];
        
        //search theo text
        if(!empty($searchText)){
            if(filter_var($searchText, FILTER_VALIDATE_EMAIL)){
                $whereSearch = 'smscampaigns.SMSCampaignName like ?';
                $dataBind[] = "%$searchText%";
            }
            else if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'smslogs.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            else{
                $whereSearch = 'smslogs.CrDateTime like ? or smscampaigns.SMSCampaignName like ?';
                for( $i = 0; $i < 3; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                switch ($filed_name) {
                    case 'sms_campaign':
                        $wheres[] = "smslogs.SMSCampaignId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'sms_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'smslogs.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "smslogs.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "smslogs.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(smslogs.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    
                    // case 'transaction_tag':
                    //     $wheres[] = "transactions.TransactionId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId IN(17,18) AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                    //     $dataBind[] = $conds[1];
                    //     break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataSmslogs = $this->getByQuery($query, $dataBind);
        
        $verifyLevels =  $this->Mconstants->verifyLevels;
        $moneySources =  $this->Mconstants->moneySources;
        for ($i = 0; $i < count($dataSmslogs); $i++) {
            // $dataTransactions[$i]['MoneySourceName'] = $dataTransactions[$i]['MoneySourceId'] > 0 ? $moneySources[$dataTransactions[$i]['MoneySourceId']] : '';
            $dayDiff = getDayDiff($dataSmslogs[$i]['CrDateTime'], $now);
            $dataSmslogs[$i]['CrDateTime'] 	= ddMMyyyy($dataSmslogs[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataSmslogs[$i]['DayDiff'] 	= $dayDiff;
            $dataSmslogs[$i]['labelCss'] 	= $this->Mconstants->labelCss;
        }

        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data = array();
        $data['dataTables'] = $dataSmslogs;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentSpeedSms';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}