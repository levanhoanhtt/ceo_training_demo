<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfbusers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "fb_users";
        $this->_primary_key = "FbUserId";
    }

    public function update($fbId, $fullName = ''){
        $fbUserId = $this->getFieldValue(array('FbId' => $fbId), 'FbUserId', 0);
        if($fbUserId == 0) $fbUserId = $this->save(array(
            'FbId' => $fbId,
            'FirstName' => '',
            'LastName' => '',
            'FullName' => $fullName,
            'Email' => '',
            'CustomerId' => 0,
            'FbUserName' => '',
            'Address' => '',
            'GenderId' => 1,
            'CrDateTime' => getCurentDateTime()
        ), 0, array('BirthDay'));
        return $fbUserId;
    }
}
