<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcontributors extends MY_Model
{
    function __construct() {
        parent::__construct();
        $this->_table_name = "contributors";
        $this->_primary_key = "ContributorId";
    }

    public function update($postData, $contributorId){
        $this->db->trans_begin();
        $contributorId = $this->save($postData, $contributorId);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $contributorId;
        }
    }
}