<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomerconsults extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customerconsults";
        $this->_primary_key = "CustomerConsultId";
    }

    public function update($postData, $customerConsultId = 0, $products = array(), $comments = array(), $actionLog= array()){
        $isUpdate = $customerConsultId > 0;
        $this->db->trans_begin();
        $actionLogs = array();
        $customerConsultId = $this->save($postData, $customerConsultId);
        if($customerConsultId > 0){
            if(!empty($actionLog)) {
                $actionLog['ItemId'] = $customerConsultId;
                $actionLogs[] = $actionLog;
            }
            if($isUpdate) $this->db->delete('consultproducts', array('CustomerConsultId' => $customerConsultId));
            if(!empty($products)) {
                $consultProducts = array();
                $totalPrice = 0;
                foreach ($products as $p) {
                    $totalPrice += $p['Quantity'] * $p['Price'];
                    $p['CustomerConsultId'] = $customerConsultId;
                    $consultProducts[] = $p;
                }
                if (!empty($consultProducts)){
                    $this->db->update('customerconsults', array('TotalPrice' => $totalPrice), array('CustomerConsultId' => $customerConsultId));
                    $this->db->insert_batch('consultproducts', $consultProducts);
                } 
            }
            if(!empty($comments)){
                $consultComments = array();
                foreach ($comments as $comment) {
                    $comment['CustomerConsultId'] = $customerConsultId;
                    $comment['UserId'] = $postData['CrUserId'];
                    $comment['CrDateTime'] = $postData['CrDateTime'];
                    $consultComments[] = $comment;
                }
                if (!empty($consultComments)){
                    $this->db->insert_batch('consultcomments', $consultComments);
                    if($postData['CrUserId'] > 0){
                        $this->Mcustomerconsultusers->save(array(
                            'ConsultSellId' => 0,
                            'CustomerConsultId' => $customerConsultId,
                            'UserId' => $postData['CrUserId'],
                            'IsCrOrder' => 1,
                            'Percent' => 0,
                            'Comment' => '',
                            'ConsultCommentId' => 0,
                            'CrDateTime' => $postData['CrDateTime']
                        ), 0, array('UpdateDateTime'));
                    }
                }
            }
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $customerConsultId;
        }
    }

    public function updateCustomerFacebook($postData, $customerId, $customerConsultId){
        $this->db->trans_begin();
        $this->Mcustomers->save($postData, $customerId);
        if($customerConsultId > 0) {
            $this->save($postData, $customerId);
        }
        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $postData = array()){
        $queryCount = "select customerconsults.CustomerConsultId AS totalRow from customerconsults {joins} where {wheres} GROUP BY customerconsults.CustomerConsultId";
        $query = "select {selects} from customerconsults {joins} where {wheres} GROUP BY customerconsults.CustomerConsultId ORDER BY customerconsults.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'customerconsults.*',
            ///'parts.PartName',
            'users.FullName AS CrFullName'
        ];
        $joins = [
            //'parts' => "left join parts on customerconsults.PartId = parts.PartId",
            'users' => "left join users on users.UserId = customerconsults.UserId"
        ];
        $wheres = array('customerconsults.RemindStatusId > 0');
        $whereSearch= '';
        $dataBind = [];
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) {
            $wheres[] = 'customerconsults.CustomerId = ?';
            $dataBind[] = $postData['CustomerId'];
        }
        if(isset($postData['UserId']) && $postData['UserId'] > 0) {
            $wheres[] = 'customerconsults.UserId = ?';
            $dataBind[] = $postData['UserId'];
        }
        if(isset($postData['OrderChannelId']) && $postData['OrderChannelId'] > 0) {
            $wheres[] = 'customerconsults.OrderChannelId = ?';
            $dataBind[] = $postData['OrderChannelId'];
        }
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'customerconsults.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'products.BarCode like ? or productchilds.BarCode like ? or inventories.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            else{
                //$whereSearch = 'products.ProductName like ? or categories.CategoryName like ? or producttypes.ProductTypeName like ? or suppliers.SupplierName like ?';
                //for( $i = 0; $i < 4; $i++) $dataBind[] = "%$searchText%";
                $whereSearch = 'customerconsults.ConsultTitle like ? or customerconsults.FullName like ? or customerconsults.PhoneNumber like ? or CrFullName LIKE ?';
                for( $i = 0; $i < 4; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'remind_status':
                        $wheres[] = "customerconsults.RemindStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'user_id':
                        $wheres[] = "customerconsults.UserId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break; 
                    case 'part_id':
                        $wheres[] = "customerconsults.PartId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'order_channel':
                        $wheres[] = "customerconsults.OrderChannelId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'consult_date':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'customerconsults.ConsultDate between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "customerconsults.ConsultDate < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "customerconsults.ConsultDate > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(customerconsults.ConsultDate) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'consult_date_period':
                        if($conds[0] == 'today') $wheres[] = "DATE(customerconsults.ConsultDate) = DATE(NOW())";
                        elseif($conds[0] == 'yesterday') $wheres[] = "DATE(customerconsults.ConsultDate) = DATE(NOW() - INTERVAL 1 DAY)";
                        elseif($conds[0] == 'current_week'){
                            $thisWeek = (date('w', strtotime(date("Y-m-d"))) == 0) ? strtotime(date("Y-m-d")) : strtotime('last Monday', strtotime(date("Y-m-d")));
                            $wheres[] = 'customerconsults.ConsultDate between ? and ?';
                            $dataBind[] = date('Y-m-d', $thisWeek);
                            $dataBind[] = date('Y-m-d 23:59:59', strtotime('next Sunday', $thisWeek));
                        }
                        elseif($conds[0] == 'current_month'){
                            $wheres[] = 'customerconsults.ConsultDate between ? and ?';
                            $dataBind[] = date('Y-m-1');
                            $dataBind[] = date('Y-m-31 23:59:59');
                        }
                        break;
                    case 'consult_time':
                        $wheres[] = "customerconsults.TimeProcessed $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'customer_consult_tag':
                        $wheres[] = "customerconsults.CustomerConsultId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId = 30 AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataCustomerConsults = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataCustomerConsults); $i++) {
            $dataCustomerConsults[$i]['RemindStatus'] = $dataCustomerConsults[$i]['RemindStatusId'] > 0 ? $this->Mconstants->remindStatus[$dataCustomerConsults[$i]['RemindStatusId']] : '';
            $dayDiff = getDayDiff($dataCustomerConsults[$i]['ConsultDate'], $now, true);
            $dataCustomerConsults[$i]['ConsultDate'] = empty($dataCustomerConsults[$i]['ConsultDate']) ? '' : ddMMyyyy($dataCustomerConsults[$i]['ConsultDate'], $dayDiff > 2 || $dayDiff < -2 ? 'd/m/Y H:i' : 'H:i');
            $dataCustomerConsults[$i]['RemindDayDiff'] = $dayDiff;
            $dayDiff = getDayDiff($dataCustomerConsults[$i]['CrDateTime'], $now);
            $dataCustomerConsults[$i]['CrDateTime'] = empty($dataCustomerConsults[$i]['CrDateTime']) ? '' : ddMMyyyy($dataCustomerConsults[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataCustomerConsults[$i]['CrDayDiff'] = $dayDiff;
            $dataCustomerConsults[$i]['OrderChannel'] = $dataCustomerConsults[$i]['OrderChannelId'] > 0 ? $this->Mconstants->orderChannels[$dataCustomerConsults[$i]['OrderChannelId']] : '';
            $dataCustomerConsults[$i]['labelCss'] = $this->Mconstants->labelCss;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataCustomerConsults;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentCustomerConsults';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}