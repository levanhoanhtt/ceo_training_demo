 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductquantity extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productquantity";
        $this->_primary_key = "ProductQuantityId";
    }

    public function getQuantity($productId, $productChildId, $storeId = 0){
        $retVal = 0;
        $query = 'SELECT SUM(Quantity) AS SumQuantity FROM productquantity WHERE ProductId = ? AND ProductChildId = ?';
        $param = array($productId, $productChildId);
        if($storeId > 0){
            $query .= ' AND StoreId = ?';
            $param[] = $storeId;
        }
        $pqs = $this->getByQuery($query, $param);
        if(!empty($pqs)){
            if(!empty($pqs[0]['SumQuantity'])) $retVal = $pqs[0]['SumQuantity'];
        }
        return $retVal;
    }

    public function getListQuantity($productIds, $storeId = 0){
        $param = array($productIds);
        $query = 'SELECT ProductId, ProductChildId, Quantity, StoreId FROM productquantity WHERE ProductId IN ?';
        if($storeId > 0){
            $query .= ' AND StoreId = ?';
            $param[] = $storeId;
        }
        return $this->getByQuery($query, $param);
    }

    public function getAllStore($productId, $productChildId){
        return $this->getBy(array('ProductId' => $productId, 'ProductChildId' => $productChildId), false, '', 'StoreId, Quantity');
    }
}