<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomerconsultusers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customerconsultusers";
        $this->_primary_key = "CustomerConsultUserId";
    }


    public function getCount($postData){
        $query = "SELECT customerconsultusers.CustomerConsultUserId FROM customerconsultusers INNER JOIN orders ON customerconsultusers.OrderId = orders.OrderId WHERE customerconsultusers.StatusId > 0 " . $this->buildQuery($postData) . ' GROUP BY orders.OrderId';
        return count($this->getByQuery($query));
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT customerconsultusers.*, orders.OrderCode, orders.CustomerId, orders.CrDateTime, customers.FullName, SUM(orderproducts.Quantity * orderproducts.Price) As Total FROM customerconsultusers INNER JOIN orders ON customerconsultusers.OrderId = orders.OrderId INNER JOIN orderproducts ON orderproducts.OrderId = orders.OrderId INNER JOIN customers ON customers.CustomerId = orders.CustomerId WHERE customerconsultusers.StatusId > 0 " . $this->buildQuery($postData) . ' GROUP BY orders.OrderId ORDER BY orders.CrDateTime DESC';
        if ($perPage > 0) {
            $from = ($page - 1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND customerconsultusers.UserId=".$postData['UserId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND orders.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND orders.CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }

    public function getList($customerConsultId){
        return $this->getByQuery('SELECT customerconsultusers.*, users.FullName FROM customerconsultusers INNER JOIN users ON customerconsultusers.UserId = users.UserId WHERE CustomerConsultId = ?', array($customerConsultId));
    }
}