<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mguarantees extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "guarantees";
        $this->_primary_key = "GuaranteeId";
    }

    public function update($postData, $guaranteeId, $products, $comments, $actionLog){
        $isUpdate = $guaranteeId > 0;
        $crUserId = $isUpdate ? $postData['UpdateUserId'] : $postData['CrUserId'];
        $crDateTime = $isUpdate ? $postData['UpdateDateTime'] : $postData['CrDateTime'];
        $this->db->trans_begin();
        $guaranteeId = $this->save($postData, $guaranteeId, array('UpdateUserId', 'UpdateDateTime'));
        if($guaranteeId > 0){
            if(!empty($actionLog)){
                $actionLog['ItemId'] = $guaranteeId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLog);
            }
            if($isUpdate) $this->db->update('guaranteeproducts', array('StatusId' => 0, 'UpdateUserId' => $crUserId, 'UpdateDateTime' => $crDateTime), array('GuaranteeId' => $guaranteeId));
            else{
                $this->db->update('guarantees', array('GuaranteeCode' => 'BH-' . ($guaranteeId + 10000)), array('GuaranteeId' => $guaranteeId));
                if(!empty($comments)){
                    $guaranteeComments = array();
                    foreach($comments as $comment){
                        $guaranteeComments[] = array(
                            'GuaranteeId' => $guaranteeId,
                            'UserId' => $crUserId,
                            'Comment' => $comment,
                            'CommentTypeId' => 1,
                            'CrDateTime' => $crDateTime
                        );
                    }
                    if (!empty($guaranteeComments)) $this->db->insert_batch('guaranteecomments', $guaranteeComments);
                }
            }
            if(!empty($products)){
                $productInserts = array();
                $productUpdates = array();
                foreach($products as $p){
                    $p['GuaranteeId'] = $guaranteeId;
                    $p['StatusId'] = STATUS_ACTIVED;
                    if($p['GuaranteeProductId'] > 0){
                        $p['UpdateUserId'] = $crUserId;
                        $p['UpdateDateTime'] = $crDateTime;
                        $productUpdates[] = $p;
                    }
                    else{
                        $p['CrUserId'] = $crUserId;
                        $p['CrDateTime'] = $crDateTime;
                        $productInserts[] = $p;
                    }
                }
                if(!empty($productInserts)) $this->db->insert_batch('guaranteeproducts', $productInserts);
                if(!empty($productUpdates)) $this->db->update_batch('guaranteeproducts', $productUpdates, 'GuaranteeProductId');
            }
        }
        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $guaranteeId;
        }
    }

    public function updateField($postData, $guaranteeId, $actionLog){
        $this->db->trans_begin();
        $guaranteeId = $this->save($postData, $guaranteeId);
        if($guaranteeId > 0) {
            if(!empty($actionLog)){
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLog);
            }
        }
        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateProduct($productUpdates, $actionLog){
        $this->db->trans_begin();
        $count = $this->db->update_batch('guaranteeproducts', $productUpdates, 'GuaranteeProductId');
        if($count > 0) {
            if(!empty($actionLog)){
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLog);
            }
        }
        if($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $count;
        }
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $storeIds = array()){
        $queryCount = "select guarantees.GuaranteeId AS totalRow from guarantees {joins} where {wheres}";
        $query = "select {selects} from guarantees {joins} where {wheres} ORDER BY guarantees.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'guarantees.*',
            'customers.FullName',
            'customers.CustomerId',
            'receipttypes.ReceiptTypeName',
            'paymenttypes.PaymentTypeName',
            'stores.StoreName'
        ];
        $joins = [
        	'customers' => "left join customers on customers.CustomerId = guarantees.CustomerId",
            'paymenttypes' => "left join paymenttypes on paymenttypes.PaymentTypeId = guarantees.PaymentTypeId",
            'receipttypes' => "left join receipttypes on receipttypes.ReceiptTypeId = guarantees.ReceiptTypeId",
            'stores' => "left join stores on stores.StoreId = guarantees.StoreId",
        ];
        $wheres = array('GuaranteeStatusId > 0');
        $dataBind = [];
        if(!empty($storeIds)){
            if(count($storeIds) == 1){
                $wheres[] = 'guarantees.StoreId = ?';
                $dataBind[] = $storeIds[0];
            }
            else {
                $wheres[] = 'guarantees.StoreId IN ?';
                $dataBind[] = $storeIds;
            }
        }
        $whereSearch= '';
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'guarantees.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            else{
                $whereSearch = 'guarantees.GuaranteeCode like ? or customers.FullName like ? or customers.PhoneNumber like ? or customers.PhoneNumber2 like ?';
                for( $i = 0; $i < 4; $i++) $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'guarantee_store':
                        $wheres[] = "guarantees.StoreId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'guarantee_status':
                        $wheres[] = "guarantees.GuaranteeStatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'guarantee_customer':
                        $wheres[] = "guarantees.CustomerId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'guarantee_receipttypes':
                        $wheres[] = "guarantees.ReceiptTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'guarantee_paymenttypes':
                        $wheres[] = "guarantees.PaymentTypeId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'guarantee_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'guarantees.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "guarantees.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "guarantees.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(guarantees.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataGuarantee = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataGuarantee); $i++) {
            $dataGuarantee[$i]['GuaranteeStatus'] = $dataGuarantee[$i]['GuaranteeStatusId'] > 0 ? $this->Mconstants->guaranteeStatus[$dataGuarantee[$i]['GuaranteeStatusId']] : '';
            $dayDiff = getDayDiff($dataGuarantee[$i]['CrDateTime'], $now);
            $dataGuarantee[$i]['CrDateTime'] = ddMMyyyy($dataGuarantee[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataGuarantee[$i]['DayDiff'] = $dayDiff;
            $dataGuarantee[$i]['labelCss'] = $this->Mconstants->labelCss; 
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataGuarantee;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentGuarantees';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }

    public function monthDiff($beginDate, $endDate){
        $dStart = new DateTime($beginDate);
        $dEnd = new DateTime($endDate);
        $diff = $dStart->diff($dEnd);
        $months = $diff->y * 12 + $diff->m + $diff->d / 30;
        return round($months);
    }
}