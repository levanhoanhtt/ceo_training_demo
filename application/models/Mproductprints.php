<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductprints extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productprints";
        $this->_primary_key = "ProductPrintId";
    }

    public function update($postData, $productPrintId, $products = array(), $comments = array(), $actionLogs = array()){
        $isUpdate = $productPrintId > 0 ? true : false;
        $this->db->trans_begin();
        $productPrintId = $this->save($postData, $productPrintId);
        if ($productPrintId > 0) {
            if (!empty($actionLogs)) {
                $actionLogs['ItemId'] = $productPrintId;
                $this->load->model('Mactionlogs');
                $this->Mactionlogs->save($actionLogs);
            }
            if ($isUpdate) $this->db->delete('productprintproducts', array('ProductPrintId' => $productPrintId));
            else {
                $productPrintCode = 'INBC-' . ($productPrintId + 10000);
                $this->db->update('productprints', array('ProductPrintCode' => $productPrintCode), array('ProductPrintId' => $productPrintId));
                if(!empty($comments)){
                    $productPrintComments = array();
                    foreach($comments as $comment){
                        $productPrintComments[] = array(
                            'ProductPrintId' => $productPrintId,
                            'UserId' => $postData['CrUserId'],
                            'Comment' => $comment,
                            'CrDateTime' => $postData['CrDateTime']
                        );
                    }
                    if (!empty($productPrintComments)) $this->db->insert_batch('productprintcomments', $productPrintComments);
                }
            }
            if (!empty($products)) {
                $productPrintProducts = array();
                foreach ($products as $p) {
                    $p['ProductPrintId'] = $productPrintId;
                    $productPrintProducts[] = $p;
                }
                if (!empty($productPrintProducts)) $this->db->insert_batch('productprintproducts', $productPrintProducts);
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $productPrintId;
        }
    }

    public function changeStatusBatch($productIds, $statusId, $user){
        $crDateTime = getCurentDateTime();
        $comment = $statusId > 0 ? ($user['FullName'].' thay đổi trạng thái phiếu in barcode về '.$this->Mconstants->status[$statusId]) : ($user['FullName'].' xóa phiếu in barcode');
        $this->db->trans_begin();
        $this->db->query('UPDATE productprints SET StatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE ProductPrintId IN ?', array($statusId, $user['UserId'], $crDateTime, $productIds));
        $actionLogs = array();
        foreach($productIds as $productId){
            $actionLogs[] = array(
                'ItemId' => $productId,
                'ItemTypeId' => 22,
                'ActionTypeId' => $statusId > 0 ? 2 : 3,
                'Comment' => $comment,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function searchByFilter($searchText, $itemFilters, $limit, $page){
        $queryCount = "select productprints.ProductPrintId AS totalRow from productprints {joins} where {wheres} GROUP BY productprints.ProductPrintId";
        $query = "select {selects} from productprints {joins} where {wheres} GROUP BY productprints.ProductPrintId ORDER BY productprints.CrDateTime DESC LIMIT {limits}";
        $selects = [
            'productprints.*',
            'SUM(productprintproducts.Quantity) AS Quantity'
        ];
        $joins = [
            'productprintproducts' => "left join productprintproducts on productprints.ProductPrintId = productprintproducts.ProductPrintId",
        ];
        $wheres = array('productprints.ProductPrintId IS NOT NULL');
        $whereSearch= '';
        $dataBind = [];
        $searchText = strtolower($searchText);
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'productprints.CrDateTime like ?';
                $dataBind[] = "$searchText%";
            }
            /*elseif(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'productprints.ProductPrintCode like ? or productprints.CrDateTime like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }*/
            else{
                $whereSearch = 'productprints.ProductPrintCode like ?';
                $dataBind[] = "%$searchText%";
            }
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'product_print_status':
                        $wheres[] = "productprints.StatusId $conds[0] ?";
                        $dataBind[] = $conds[1];
                        break;
                    case 'product_print_create':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'productprints.CrDateTime between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "productprints.CrDateTime < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "productprints.CrDateTime > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(productprints.CrDateTime) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $now = new DateTime(date('Y-m-d'));
        $dataProductPrints = $this->getByQuery($query, $dataBind);
        for ($i = 0; $i < count($dataProductPrints); $i++) {
            $dayDiff = getDayDiff($dataProductPrints[$i]['CrDateTime'], $now);
            $dataProductPrints[$i]['CrDateTime'] = ddMMyyyy($dataProductPrints[$i]['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
            $dataProductPrints[$i]['DayDiff'] = $dayDiff;
        }
        $data = array();
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data['dataTables'] = $dataProductPrints;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentProductPrints';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}