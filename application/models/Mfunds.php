<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfunds extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "funds";
        $this->_primary_key = "FundId";
    }

    public function update($postData, $fundId = 0, $storeIds = array()){
    	$isUpdate = $fundId > 0;
        $this->db->trans_begin();
        $fundId = $this->save($postData, $fundId);
        if($fundId > 0){
            if($isUpdate) $this->db->delete('storefunds', array('FundId' => $fundId));
            $storeFunds = array();
            foreach($storeIds as $storeId) $storeFunds[] = array('FundId' => $fundId, 'StoreId' => $storeId);
            if(!empty($storeFunds)) $this->db->insert_batch('storefunds', $storeFunds);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $fundId;
        }
    }
}