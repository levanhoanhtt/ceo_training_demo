<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpricechangelogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "pricechangelogs";
        $this->_primary_key = "PriceChangeLogId";
    }

    public function getList($productId, $productChildId){
        $query = "SELECT pl.ImportId, pl.OldPrice, pl.Price, DATE_FORMAT(im.CrDateTime, '%d/%m/%Y') AS CrDateTime FROM pricechangelogs pl INNER JOIN imports im ON pl.ImportId = im.ImportId WHERE ProductId = ? AND ProductChildId = ? GROUP BY pl.ImportId";
        $param = array($productId, $productChildId);
        return $this->getByQuery($query, $param);
    }
}