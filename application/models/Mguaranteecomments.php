<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mguaranteecomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "guaranteecomments";
        $this->_primary_key = "GuaranteeCommentId";
    }

    public function getListByGuaranteeId($guaranteeId){
        return $this->getByQuery('SELECT guaranteecomments.*, users.FullName, users.Avatar FROM guaranteecomments INNER JOIN users ON guaranteecomments.UserId = users.UserId WHERE guaranteecomments.GuaranteeId = ? ORDER BY guaranteecomments.CrDateTime DESC', array($guaranteeId));
    }
}