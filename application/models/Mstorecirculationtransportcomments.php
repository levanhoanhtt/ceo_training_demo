<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstorecirculationtransportcomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "storecirculationtransportcomments";
        $this->_primary_key = "StoreCirculationTransportCommentId";
    }

    public function getList($storeCirculationTransportId){
        return $this->getByQuery('SELECT storecirculationtransportcomments.*, users.FullName, users.Avatar FROM storecirculationtransportcomments INNER JOIN users ON storecirculationtransportcomments.UserId = users.UserId WHERE storecirculationtransportcomments.StoreCirculationTransportId = ? ORDER BY storecirculationtransportcomments.CrDateTime DESC', array($storeCirculationTransportId));
    }
}