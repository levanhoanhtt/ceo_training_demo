<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconsultproducts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "consultproducts";
        $this->_primary_key = "ConsultProductId";
    }
}
