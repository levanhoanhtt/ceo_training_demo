<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransportcaretypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transportcaretypes";
        $this->_primary_key = "TransportCareTypeId";
    }
}