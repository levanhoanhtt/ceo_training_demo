<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mtransportcomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transportcomments";
        $this->_primary_key = "TransportCommentId";
    }

    public function getList($transportId){
        return $this->getByQuery('SELECT transportcomments.*, users.FullName, users.Avatar FROM transportcomments INNER JOIN users ON transportcomments.UserId = users.UserId WHERE transportcomments.TransportId = ? ORDER BY transportcomments.CrDateTime DESC', array($transportId));
    }
}