<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconsultsells extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "consultsells";
        $this->_primary_key = "ConsultSellId";
    }
    
    public function getCount($postData){
        $query = "SELECT ConsultSellId FROM consultsells INNER JOIN orders ON consultsells.OrderId = orders.OrderId WHERE consultsells.StatusId > 0  AND orders.OrderStatusId IN(2, 5, 6)" . $this->buildQuery($postData);
        return count($this->getByQuery($query));
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT consultsells.*, orders.OrderCode, orders.CustomerId, orders.CrDateTime, customers.FullName, SUM(orderproducts.Quantity * orderproducts.Price) As SumCost
                  FROM consultsells INNER JOIN orders ON consultsells.OrderId = orders.OrderId
                  INNER JOIN orderproducts ON orderproducts.OrderId = orders.OrderId
                  INNER JOIN customers ON customers.CustomerId = orders.CustomerId WHERE consultsells.StatusId > 0 AND orders.OrderStatusId IN(2, 5, 6)" . $this->buildQuery($postData) . ' GROUP BY orders.OrderId ORDER BY orders.CrDateTime DESC';
        if ($perPage > 0) {
            $from = ($page - 1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND ConsultSellId IN(SELECT ConsultSellId FROM customerconsultusers WHERE UserId = {$postData['UserId']})";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND orders.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND orders.CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }
}