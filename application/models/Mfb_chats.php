<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfb_chats extends MY_Model{

    private $prefix = '';

	function __construct(){
        parent::__construct();
        $this->_primary_key = "FbChatId";
    }

    public function setPrefix($prefix){
        $this->prefix = $prefix;
        $this->_table_name = "fb_chats_".$prefix;
    }

    public function getPrefix(){
        return $this->prefix;
    }

    public function getList($pageId,$viewStatusId,$answerStatusId){
    	return $this->getByQuery("SELECT p.FbPageCode AS FbPageCode, c.FbChatId AS FbChatId, c.Message AS Message, u.FbId AS FbId, u.FbUserName AS FbUserName, u.Email AS Email, u.Address AS Address, c.CrDateTime as CrDateTime, c.IsCustomerSend AS IsCustomerSend FROM fb_chats c LEFT JOIN fb_users u ON c.FbUserId = u.FbUserId LEFT JOIN fb_pages p ON p.FbPageId = c.FbPageId
			WHERE  c.FbChatId IN (SELECT Max(FbChatId) AS FbChatId FROM fb_chats GROUP BY FbUserId ORDER BY CrDateTime DESC ) ".$this->buildQuery($pageId, $viewStatusId, $answerStatusId)."
			GROUP BY u.FbUserId ORDER BY  c.CrDateTime DESC");
    }

    private function buildQuery($pageId, $viewStatusId, $answerStatusId){
        $query = '';
        if($pageId > 0 && !empty($viewStatusId) == "" && !empty($answerStatusId) == "") $query.=""; // tất cả
        if($pageId > 0 && $viewStatusId == 2) $query.=" AND c.ViewStatusId = ".$viewStatusId; // chua đọc
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 1) $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId}"; // đa đọc đã trả lời
        if($pageId > 0 && $viewStatusId == 1 && $answerStatusId == 2) $query.=" AND c.ViewStatusId = {$viewStatusId} AND c.AnswerStatusId = {$answerStatusId}"; // doc chua tra lời
        return $query;
    }

    public function getMessage($fbPageCode, $fbId){
    	return $this->getByQuery("SELECT c.IsCustomerSend AS IsCustomerSend , u.FbId AS FbId, c.FbUserId AS FbUserId, u.FbUserName AS FbUserName,c.FbPageId AS FbPageId, p.FbPageCode AS FbPageCode, p.FbPageName AS FbPageName , c.CrDateTime AS CrDateTime, c.Message AS Message FROM fb_chats c LEFT JOIN fb_users u ON u.FbUserId = c.FbUserId
		LEFT JOIN fb_pages p ON p.FbPageId = c.FbPageId WHERE  p.FbPageCode = ? AND u.FbId = ? ORDER BY c.CrDateTime ASC", array($fbPageCode, $fbId));
    }
}