<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msuppliers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "suppliers";
        $this->_primary_key = "SupplierId";
    }

    public function update($postData, $supplierId,  $contacts){
        $this->db->trans_begin();
        $isUpdate = $supplierId > 0;
        $supplierId = $this->save($postData, $supplierId, array('UpdateUserId', 'UpdateDateTime'));
        if($supplierId > 0){
            if($isUpdate) $this->db->delete('suppliercontacts', array('SupplierId' => $supplierId));
            if(!empty($contacts)){
                $valueData = array();
                foreach($contacts as $c){
                    $c['SupplierId'] = $supplierId;
                    $valueData[] = $c;
                }
                $this->db->insert_batch('suppliercontacts', $valueData);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $supplierId;
        }
    }
}