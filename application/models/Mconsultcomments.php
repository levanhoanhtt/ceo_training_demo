<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconsultcomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "consultcomments";
        $this->_primary_key = "ConsultCommentId";
    }

    public function insert($postData, $customerConsultUser, $customerConsultUserId){
        $this->db->trans_begin();
        $consultCommentId = $this->save($postData);
        if($consultCommentId > 0 && !empty($customerConsultUser)){
            $customerConsultUser['ConsultCommentId'] = $consultCommentId;
            $customerConsultUserId = $this->Mcustomerconsultusers->save($customerConsultUser);
        }
        $retVal = array(
            'ConsultCommentId' => $consultCommentId,
            'CustomerConsultUserId' => $customerConsultUserId
        );
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return $retVal;
        }
    }

    public function getList($customerConsultId){
    	$query = "SELECT consultcomments.*, users.FullName, users.Avatar FROM consultcomments INNER JOIN users ON users.UserId = consultcomments.UserId WHERE consultcomments.CustomerConsultId = ? ORDER BY consultcomments.CrDateTime DESC";
        return $this->getByQuery($query, array($customerConsultId));
    }

    public function getUserIds($customerConsultId){
        $retVal = array();
        $ccs = $this->getByQuery('SELECT DISTINCT UserId FROM consultcomments WHERE CustomerConsultId = ?', array($customerConsultId));
        foreach($ccs as $cc) $retVal[] = $cc['UserId'];
        return $retVal;
    }
}
