<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactionbusiness extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transactionbusiness";
        $this->_primary_key = "TransactionBusinessId";
    }

    /*public function getCount($postData){
        $query = "TransactionStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM transactionbusiness WHERE TransactionStatusId > 0" . $this->buildQuery($postData).' ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['TransactionTypeId']) && $postData['TransactionTypeId'] > 0) $query.=" AND TransactionTypeId=".$postData['TransactionTypeId'];
        if(isset($postData['TransactionStatusId']) && $postData['TransactionStatusId'] > 0) $query.=" AND TransactionStatusId=".$postData['TransactionStatusId'];
        if(isset($postData['TransactionKindId']) && $postData['TransactionKindId'] > 0) $query.=" AND TransactionKindId=".$postData['TransactionKindId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }*/

    public function searchByFilter($searchText, $itemFilters, $limit, $page, $transactionTypeId = 0){
        $queryCount = "SELECT transactionbusiness.TransactionBusinessId AS totalRow FROM transactionbusiness {joins} WHERE {wheres} GROUP BY transactionbusiness.TransactionBusinessId";
        $query = "SELECT {selects} FROM transactionbusiness {joins} WHERE {wheres} GROUP BY transactionbusiness.TransactionBusinessId ORDER BY transactionbusiness.TransactionDate DESC LIMIT {limits}";
        $selects = [
            'transactionbusiness.*',
            'SUM(transactionbusinessitems.PaidCost) AS PaidCost'
        ];
        $joins = [
            'transactionbusinessitems' => 'left join transactionbusinessitems on (transactionbusinessitems.TransactionBusinessId = transactionbusiness.TransactionBusinessId AND transactionbusinessitems.TransactionStatusId = 2)'
        ];

        $wheres = array();
        if($transactionTypeId > 0) $wheres[] = "transactionbusiness.TransactionTypeId = {$transactionTypeId}";
        $whereSearch= '';
        $dataBind = [];
        //search theo text
        if(!empty($searchText)){
            if(preg_match('/\d{4}-\d{2}-\d{2}/im',$searchText)){
                $whereSearch = 'transactionbusiness.TransactionDate like ?';
                $dataBind[] = "$searchText%";
            }
            /*else if(preg_match('/\d+|\w+-/im',$searchText)){
                $whereSearch = 'transactionbusiness.TransactionCode like ? or transactionbusiness.TransactionDate like ?';
                $dataBind[] = "%$searchText%";
                $dataBind[] = "%$searchText%";
            }
            else{
                $whereSearch = 'transactionkinds.TransactionKindName like ?';
                $dataBind[] = "%$searchText%";
            }*/
        }
        if(!empty($whereSearch)) {
            $whereSearch = "( $whereSearch )";
            $wheres[] = $whereSearch;
        }
        //search theo bộ lọc ,
        if (!empty($itemFilters) && count($itemFilters)) {
            foreach ($itemFilters as $item) {
                $filed_name = $item['field_name'];
                $conds = $item['conds'];
                //$cond[0] là điều kiện ví dụ : < > = like .....   $cons[1] và $cond[2]  là gía trị điều kiện như 2017-01-02 và 2017-01-01
                switch ($filed_name) {
                    case 'group_money':
                        $wheres[] = "PaidCost $conds[0] ?";
                        $dataBind[] = replacePrice($conds[1]);
                        break;
                    case 'group_date':
                        if ($conds[0] == 'between') {
                            $wheres[] = 'transactionbusiness.TransactionDate between ? and ?';
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                            $dataBind[] = @ddMMyyyyToDate($conds[2], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '<'){
                            $wheres[] = "transactionbusiness.TransactionDate < ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1], 'd/m/Y', 'Y-m-d 23:59:59');
                        }
                        elseif($conds[0] == '>'){
                            $wheres[] = "transactionbusiness.TransactionDate > ?";
                            $dataBind[] = @ddMMyyyyToDate($conds[1]);
                        }
                        else{
                            $wheres[] = "DATE(transactionbusiness.TransactionDate) $conds[0] ?";
                            $dataBind[] = $conds[1];
                        }
                        break;
                    case 'transaction_tag':
                        $wheres[] = "transactionbusiness.TransactionBusinessId $conds[0](SELECT ItemId FROM itemtags WHERE ItemTypeId IN(31,32) AND TagId IN(SELECT TagId FROM tags WHERE TagName = ?))";
                        $dataBind[] = $conds[1];
                        break;
                    default :
                        break;
                }
            }
        }
        $selects_string = implode(',', $selects);
        $wheres_string = implode(' and ', $wheres);
        $joins_string = implode(' ', $joins);
        $query = str_replace('{selects}', $selects_string, $query);
        $query = str_replace('{joins}', $joins_string, $query);
        $query = str_replace('{wheres}', $wheres_string, $query);
        $query = str_replace('{limits}', $limit * ($page - 1) . "," . $limit, $query);
        $queryCount = str_replace('{joins}', $joins_string, $queryCount);
        $queryCount = str_replace('{wheres}', $wheres_string, $queryCount);
        if (count($wheres) == 0){
            $query = str_replace('where', '', $query);
            $queryCount = str_replace('where', '', $queryCount);
        }
        $dataTransactions = $this->getByQuery($query, $dataBind);
        for($i = 0; $i < count($dataTransactions); $i++) $dataTransactions[$i]['TransactionMonth'] = ddMMyyyy($dataTransactions[$i]['TransactionDate'], 'm/Y');
        $totalRow = $this->db->query($queryCount, $dataBind)->num_rows();
        $pageSize = ceil($totalRow / $limit);
        $data = array();
        $data['dataTables'] = $dataTransactions;
        $data['page'] = $page;
        $data['pageSize'] = $pageSize;
        $data['callBackTable'] = 'renderContentTransactionBusiness';
        $data['callBackTagFilter'] = 'renderTagFilter';
        $data['totalRow'] = $totalRow;
        return $data;
    }
}