<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mstorecirculationcomments extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "storecirculationcomments";
        $this->_primary_key = "StoreCirculationCommentId";
    }

    public function getListByStoreCirculationId($storeCirculationId){
        return $this->getByQuery('SELECT storecirculationcomments.*, users.FullName, users.Avatar FROM storecirculationcomments INNER JOIN users ON storecirculationcomments.UserId = users.UserId WHERE storecirculationcomments.StoreCirculationId = ? ORDER BY storecirculationcomments.CrDateTime DESC', array($storeCirculationId));
    }
}