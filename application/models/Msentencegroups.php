<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msentencegroups extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "sentencegroups";
        $this->_primary_key = "SentenceGroupId";
    }

    public function getList(){
        return $this->getByQuery("SELECT * FROM sentencegroups");
    }

    public function checkName($name){
        return $this->getByQuery("SELECT * FROM sentencegroups WHERE SentenceGroupName LIKE '%{$name}%'");
    }

    public function update($sentenceGroupName){
        $retVal = 0;
        if(!empty($sentenceGroupName)){
            $arr = array('SentenceGroupName' => $sentenceGroupName);
            $retVal = $this->getFieldValue($arr, 'SentenceGroupId', 0);
            if($retVal == 0) $retVal = $this->save($arr);
        }
        return $retVal;
    }
}