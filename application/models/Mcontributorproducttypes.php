<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcontributorproducttypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "contributorproducttypes";
        $this->_primary_key = "ContributorProductTypeId";
    }

    public function getByContributorId($contributorId){
        return $this->getBy(array('ContributorId' => $contributorId, 'StatusId' => STATUS_ACTIVED), false, 'PaidDateTime');
    }
}