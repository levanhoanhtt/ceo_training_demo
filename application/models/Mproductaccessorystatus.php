<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproductaccessorystatus extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "productaccessorystatus";
        $this->_primary_key = "ProductAccessoryStatusId";
    }
}