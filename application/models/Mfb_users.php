<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfb_users extends MY_Model{
	function __construct(){
        parent::__construct();
        $this->_table_name = "fb_users";
        $this->_primary_key = "FbUserId";
    }
}