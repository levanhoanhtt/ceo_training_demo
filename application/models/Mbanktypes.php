<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbanktypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "banktypes";
        $this->_primary_key = "BankTypeId";
    }
}
