
TRUNCATE TABLE actions;
INSERT INTO `actions` (`ActionId`, `ActionName`, `ActionUrl`, `ParentActionId`, `StatusId`, `DisplayOrder`, `FontAwesome`, `ActionLevel`) VALUES
(1, 'Quản lý bán hàng', '', 0, 2, 1, 'fa-shopping-cart', 1),
(2, 'Đơn hàng Khách lẻ', 'order', 1, 2, 1, '', 2),
(3, 'Cấu hình bán hàng', '', 1, 2, 11, '', 2),
(4, 'Bảo hành - Đổi trả', '', 1, 2, 7, '', 2),
(5, 'Cấu hình Bảo hành - Đổi trả', '', 1, 2, 12, '', 2),
(6, 'Mã khuyến mại', 'promotion', 1, 2, 10, '', 2),
(7, 'Bảo hành - Đổi trả', 'guarantee', 4, 2, 1, '', 3),
(8, 'Hình thức/ tình trạng sử dụng', 'usagestatus', 5, 2, 3, '', 3),
(9, 'Phương thức nhận hàng', 'receipttype', 5, 2, 1, '', 3),
(10, 'Phương thức thanh toán', 'paymenttype', 5, 2, 2, '', 3),
(28, 'Quản lý khách hàng', '', 0, 2, 4, 'fa-male', 1),
(29, 'Danh sách khách lẻ', 'customer/1', 30, 2, 0, '', 3),
(30, 'Danh sách khách hàng', 'customer', 28, 2, 1, '', 2),
(31, 'Nhóm khách hàng', 'customergroup', 28, 2, 2, '', 2),
(40, 'Khách có nhu cầu', 'customerconsult', 28, 2, 5, '', 2),
(58, 'Tài chính', '', 0, 2, 7, 'fa-money', 1),
(59, 'Tổng quan', '', 58, 2, 4, '', 2),
(62, 'Quản lý công nợ', '', 58, 2, 6, '', 2),
(114, 'Loại đơn hàng', 'ordertype', 3, 2, 4, '', 3),
(115, 'Lý do mua hàng', 'orderreason', 3, 2, 3, '', 3),
(116, 'Lý do hủy đơn hàng', 'cancelreason', 3, 2, 2, '', 3),
(118, 'Phiếu thu khách hàng', 'transaction/1', 58, 2, 7, '', 2),
(119, 'Cấu hình tham sô', '', 1, 2, 0, '', 2),
(120, 'Cấu hình', '', 58, 2, 19, '', 2),
(121, 'Lý do thu chi', 'transactionreason', 120, 2, 3, '', 3),
(122, 'Máy nạp tiền', 'moneyphone', 120, 2, 6, '', 3),
(125, 'Dịch vụ đơn hàng', 'otherservice', 3, 2, 1, '', 3),
(128, 'Loại ngân hàng', 'banktype', 120, 2, 7, '', 3),
(130, 'Phiếu chi khách hàng', 'transaction/2', 58, 2, 10, '', 2),
(131, 'Trạng thái chờ xử lý', 'pendingstatus', 3, 2, 5, '', 3),
(133, 'Duyệt tài chính bán hàng cấp 1', 'transaction/editLevel_1', 58, 2, 0, '', 2),
(134, 'Duyệt tài chính bán hàng cấp 2', 'transaction/editLevel_2', 58, 2, 0, '', 2),
(137, 'Danh sách Ngân hàng', 'bank', 120, 2, 1, '', 3),
(140, 'Dịch vụ đơn hàng', 'ortherservice', 3, 2, 6, '', 3),
(149, 'Quỹ tài chính', 'fund', 120, 2, 4, '', 3),
(150, 'Loại thu chi kinh doanh', 'transactionkind/1', 120, 2, 8, '', 3),
(151, 'Phiếu thu kinh doanh', 'transactioninternal/1', 58, 2, 12, '', 2),
(152, 'Phiếu chi kinh doanh', 'transactioninternal/2', 58, 2, 15, '', 2),
(153, 'Duyệt tài chính kinh doanh cấp 1', 'transactioninternal/editLevel_1', 58, 2, 0, '', 2),
(154, 'Duyệt tài chính kinh doanh cấp 2', 'transactioninternal/editLevel_2', 58, 2, 0, '', 2),
(160, 'Xem tất cả đơn hàng', 'order/viewAll', 1, 2, 0, '', 2),
(161, 'Đơn hàng Khách CTV', 'order/3', 1, 2, 5, '', 2),
(166, 'Đơn hàng Khách buôn', 'order/2', 1, 2, 4, '', 2),
(174, 'Danh sách Khách buôn', 'customer/2', 30, 2, 0, '', 3),
(175, 'Danh sách khách CTV', 'customer/3', 30, 2, 0, '', 3),
(176, 'Xem tất cả Khách hàng', 'customer/viewAll', 30, 2, 0, '', 3),
(177, 'Hướng xử lý Bảo hành đổi trả', 'guaranteesolution', 5, 2, 4, '', 3),
(178, 'Loại thu chi gia đình', 'transactionkind/2', 120, 2, 10, '', 3),
(179, 'Phiều thu gia đình', 'familytransaction/1', 58, 2, 17, '', 2),
(180, 'Phiếu chi gia đình', 'familytransaction/2', 58, 2, 18, '', 2),
(181, 'Duyệt tài chính gia đình cấp 1', 'familytransaction/editLevel_1', 58, 2, 0, '', 2),
(182, 'Duyệt tài chình gia đình cấp 2', 'familytransaction/editLevel_2', 58, 2, 0, '', 2);

