var app = app || {};

app.init = function() {
    app.product();
    app.productOld();
};

app.product = function(){
    chooseProduct(function (tr) {
        var flag = false;
        var trs = $('#tbodyProduct tr');
        if(trs.length == 1){
            showNotification('Chỉ được chọn 1 sản phẩm', 0);
            flag = true;
            return false;
        }
        var id = tr.attr('data-id');
        var childId = tr.attr('data-child');
        trs.each(function () {
            if ($(this).attr('data-id') == id && $(this).attr('data-child') == childId) {
                flag = true;
                return false;
            }
        });
        if (!flag) {
            $.ajax({
                type: "POST",
                url: $('input#getProductDetailUrl').val(),
                data: {
                    ProductId: id,
                    ProductChildId: childId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        var data = json.data;
                        var products = data.Products;
                        var productPath = $('input#productPath').val();
                        var html = '';
                        var productId = 0, productChildId = 0;
                        for(var i = 0; i < products.length; i++) {
                            if(parseInt(products[i].ProductChildId) > 0) $('input#productOrChild').val('2');
                            else $('input#productOrChild').val('1');
                            productId = products[i].ProductId;
                            productChildId = products[i].ProductChildId;
                            $('input#parentProductId').val(productId);
                            $('input#parentProductChildId').val(productChildId);
                            html += '<tr data-id="' + products[i].ProductId + '" data-child="' + products[i].ProductChildId + '">';
                            html += '<td><img src="' + productPath + products[i].ProductImage + '" class="productImg"><a href="javascript:void(0)" class="light-dark aProductLink" target="_blank">' + products[i].ProductName + '</a></td>';
                            html += '<td class="text-center">' + products[i].ProductUnitName + '</td>';
                            html += '<td class="text-center">' + products[i].BarCode + '</td>';
                            html += '<td class="text-center">' + products[i].GuaranteeMonth + ' tháng</td>';
                            html += '<td class="tdPrice text-right"><span class="spanPrice">' + formatDecimal(products[i].Price.toString()) + '</span> ₫</td>';
                            html += '<td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td></tr>';
                            break;
                        }
                        $('#tbodyProduct').append(html);
                        resetProductOldForm();
                        $('div.product').hide();
                        $("div#viewOldProductNew").show();
                        $('div#viewOldProduct').show();
                        getListOldProduct(productId, productChildId);
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
    jwerty.key('f3', function (){
        var panelProduct = $('#panelProduct');
        if(panelProduct.hasClass('active')){
            panelProduct.removeClass('active');
            panelProduct.find('.panel-body').css("width", "99%");
        }
        else{
            panelProduct.addClass('active');
            setTimeout(function (){
                panelProduct.find('.panel-body').css("width", "100%");
                $('.wrapper').addClass('open-search');
            }, 100);
            $('input#pageIdProduct').val('1');
            getListProducts();
        }
    });
    $('#tbodyProduct').on('click', '.link_delete', function(){
        $(this).parent().parent().remove();
        $('input#parentProductId, input#parentProductChildId').val('0');
        $('#tbodyOldProduct .trOldProduct').remove();
        $('div.product').show();
        $("div#viewOldProductNew").hide();
        $('div#viewOldProduct').hide();
        return false;
    });
};

app.productOld = function(){
    $('body').on('click', 'a#link_update', function(){
        if(validateEmpty('#tbodyOldProduct')) {
            var id = $(this).attr('data-id');
            if($('input#productOrChild').val() == '1'){
                $.ajax({
                    type: "POST",
                    url: $('input#updateOldProductUrl').val(),
                    data: {
                        ProductId: id,
                        ParentProductId: $('input#parentProductId').val(),
                        IMEI: $('input#IMEI').val().trim(),
                        ProductFormalStatusId: $('select#productFormalStatusId').val().trim(),
                        ProductUsageStatusId: $('select#productUsageStatusId').val().trim(),
                        ProductAccessoryStatusId: $('select#productAccessoryStatusId').val().trim(),
                        ProductShortDesc: $('input#comment').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1){
                            resetProductOldForm();
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr class="trOldProduct" data-id="' + data.ProductId + '" data-child="0">';
                                html += '<td class="tdIMEI">' + data.IMEI + '</td>';
                                html += '<td class="tdFormalStatus">' + $('select#productFormalStatusId option[value="' + data.ProductFormalStatusId + '"]').text() + '</td>';
                                html += '<td class="tdUsageStatus">' + $('select#productUsageStatusId option[value="' + data.ProductUsageStatusId + '"]').text() + '</td>';
                                html += '<td class="tdAccessoryStatus">' + $('select#productAccessoryStatusId option[value="' + data.ProductAccessoryStatusId + '"]').text() + '</td>';
                                html += '<td class="tdComment">' + data.ProductShortDesc + '</td>';
                                // html += '<td><a href="javascript:void(0)" class="link_edit"><i class="fa fa-pencil" title="Sửa"></i></a>';
                                // html += '<a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a>';
                                // html += '<input type="text" hidden="hidden" class="productFormalStatusId" value="' + data.ProductFormalStatusId + '">';
                                // html += '<input type="text" hidden="hidden" class="productUsageStatusId" value="' + data.ProductUsageStatusId + '">';
                                // html += '<input type="text" hidden="hidden" class="productAccessoryStatusId" value="' + data.ProductAccessoryStatusId + '"></td>';
                                html += '</tr>';
                                $('#tbodyOldProduct').prepend(html);
                            }
                            else{
                                var tr = $('.trOldProduct[data-id="' + data.ProductId + '"]');
                                tr.find('.tdIMEI').text(data.IMEI);
                                tr.find('.tdFormalStatus').text($('select#productFormalStatusId option[value="' + data.ProductFormalStatusId + '"]').text());
                                tr.find('.tdUsageStatus').text($('select#productUsageStatusId option[value="' + data.ProductUsageStatusId + '"]').text());
                                tr.find('.tdAccessoryStatus').text($('select#productAccessoryStatusId option[value="' + data.ProductAccessoryStatusId + '"]').text());
                                tr.find('.tdComment').text(data.ProductShortDesc);
                                tr.find('input.productFormalStatusId').val(data.ProductFormalStatusId);
                                tr.find('input.productUsageStatusId').val(data.ProductUsageStatusId);
                                tr.find('input.productAccessoryStatusId').val(data.ProductAccessoryStatusId);
                            }
                        }
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
            else{
                $.ajax({
                    type: "POST",
                    url: $('input#updateOldProductChildUrl').val(),
                    data: {
                        ProductChildId: id,
                        ProductId: $('input#parentProductId').val(),
                        ParentProductChildId: $('input#parentProductChildId').val(),
                        IMEI: $('input#IMEI').val().trim(),
                        ProductFormalStatusId: $('select#productFormalStatusId').val().trim(),
                        ProductUsageStatusId: $('select#productUsageStatusId').val().trim(),
                        ProductAccessoryStatusId: $('select#productAccessoryStatusId').val().trim(),
                        Comment: $('input#comment').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1){
                            resetProductOldForm();
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr class="trOldProduct" data-id="' + data.ProductId + '" data-child="' + data.ProductChildId + '">';
                                html += '<td class="tdIMEI">' + data.IMEI + '</td>';
                                html += '<td class="tdFormalStatus">' + $('select#productFormalStatusId option[value="' + data.ProductFormalStatusId + '"]').text() + '</td>';
                                html += '<td class="tdUsageStatus">' + $('select#productUsageStatusId option[value="' + data.ProductUsageStatusId + '"]').text() + '</td>';
                                html += '<td class="tdAccessoryStatus">' + $('select#productAccessoryStatusId option[value="' + data.ProductAccessoryStatusId + '"]').text() + '</td>';
                                html += '<td class="tdComment">' + data.Comment + '</td>';
                                // html += '<td><a href="javascript:void(0)" class="link_edit"><i class="fa fa-pencil" title="Sửa"></i></a>';
                                // html += '<a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a>';
                                // html += '<input type="text" hidden="hidden" class="productFormalStatusId" value="' + data.ProductFormalStatusId + '">';
                                // html += '<input type="text" hidden="hidden" class="productUsageStatusId" value="' + data.ProductUsageStatusId + '">';
                                // html += '<input type="text" hidden="hidden" class="productAccessoryStatusId" value="' + data.ProductAccessoryStatusId + '"></td>';
                                html += '</tr>';
                                $('#tbodyOldProduct').prepend(html);
                            }
                            else{
                                var tr = $('.trOldProduct[data-child="' + data.ProductChildId + '"]');
                                tr.find('.tdIMEI').text(data.IMEI);
                                tr.find('.tdFormalStatus').text($('select#productFormalStatusId option[value="' + data.ProductFormalStatusId + '"]').text());
                                tr.find('.tdUsageStatus').text($('select#productUsageStatusId option[value="' + data.ProductUsageStatusId + '"]').text());
                                tr.find('.tdAccessoryStatus').text($('select#productAccessoryStatusId option[value="' + data.ProductAccessoryStatusId + '"]').text());
                                tr.find('.tdComment').text(data.Comment);
                                tr.find('input.productFormalStatusId').val(data.ProductFormalStatusId);
                                tr.find('input.productUsageStatusId').val(data.ProductUsageStatusId);
                                tr.find('input.productAccessoryStatusId').val(data.ProductAccessoryStatusId);
                            }
                        }
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        return false;
    }).on('click', 'a#link_cancel', function(){
        resetProductOldForm();
        return false;
    }).on('click', 'a.link_edit', function(){
        var tr = $(this).parent().parent();
        if($('input#productOrChild').val() == '1') $('a#link_update').attr('data-id', tr.attr('data-id'));
        else $('a#link_update').attr('data-id', tr.attr('data-child'));
        $('input#IMEI').val(tr.find('.tdIMEI').text().trim());
        $('input#comment').val(tr.find('.tdComment').text().trim());
        $('select#productFormalStatusId').val(tr.find('input.productFormalStatusId').val());
        $('select#productUsageStatusId').val(tr.find('input.productUsageStatusId').val());
        $('select#productAccessoryStatusId').val(tr.find('input.productAccessoryStatusId').val());
        return false;
    }).on('click', 'a.link_delete', function(){
        var tr = $(this).parent().parent();
        if($('input#productOrChild').val() == '1') {
            $.ajax({
                type: "POST",
                url: $('input#changeProductStatusUrl').val(),
                data: {
                    ItemIds: JSON.stringify([tr.attr('data-id')]),
                    StatusId: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) tr.remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else{
            $.ajax({
                type: "POST",
                url: $('input#deleteProductChildUrl').val(),
                data: {
                    ProductChildId: tr.attr('data-child')
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) tr.remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
};

$(document).ready(function () {
    app.init();
});

function getListOldProduct(productId, productChildId){
    $.ajax({
        type: "POST",
        url: $('input#getListOldProductUrl').val(),
        data: {
            ProductId: productId,
            ProductChildId: productChildId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            console.log(json)
            if(json.code == 1){
                var data = json.data;
                var html = '';
                for(var i = 0; i < data.length; i++){
                    html += '<tr class="trOldProduct" data-id="' + data[i].ProductId + '" data-child="' + data[i].ProductChildId + '">';
                    html += '<td class="tdIMEI">' + data[i].IMEI + '</td>';
                    html += '<td class="tdFormalStatus">' + data[i].FormalStatus + '</td>';
                    html += '<td class="tdUsageStatus">' + data[i].UsageStatus + '</td>';
                    html += '<td class="tdAccessoryStatus">' + data[i].AccessoryStatus + '</td>';
                    html += '<td>'+data[i].Comment+'</td>';
                    // html += '<td><a href="javascript:void(0)" class="link_edit"><i class="fa fa-pencil" title="Sửa"></i></a>';
                    // html += '<a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Xóa"></i></a></td>';
                    html += '</tr>';
                }
                $('#tbodyOldProduct').prepend(html);
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function resetProductOldForm(){
    $('input#IMEI, input#comment').val('');
    $('select#productFormalStatusId, select#productUsageStatusId, select#productAccessoryStatusId').val(0);
    $('a#link_update').attr('data-id', 0);
}