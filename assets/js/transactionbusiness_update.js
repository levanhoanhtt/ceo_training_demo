$(document).ready(function() {
    $('select#transactionKindIdSearch1').change(function(){
        $('select#transactionKindIdSearch2 option').hide();
        $('select#transactionKindIdSearch2 option[value="0"]').show();
        $('select#transactionKindIdSearch2 option[data-id="' + $(this).val() + '"]').show();
        $('select#transactionKindIdSearch2').val('0');
    });
    $('#btnSearch').click(function(){
        var transactionKindId1 = $('select#transactionKindIdSearch1').val();
        var transactionKindId2 = $('select#transactionKindIdSearch2').val();
        if(transactionKindId1 == '0' && transactionKindId2 == '0') $('#tbodyTransactionBusiness tr').show();
        else{
            $('#tbodyTransactionBusiness tr').hide();
            if(transactionKindId2 != '0') $('#trItem_' + transactionKindId1 + '_' + transactionKindId2).show();
            else if(transactionKindId1 != '0') $('.trItem_' + transactionKindId1).show();
        }
    });
    $('input.paidCost').keydown(function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).keyup(function (e) {
        var value = formatDecimal($(this).val());
        $(this).val(value);
    });
    var cost = $('input#sumCost').val();
    $('#spanSumCost').text(formatDecimal(cost));
    cost = Math.ceil(parseInt(cost) / 30);
    $('#spanDayCost').text(formatDecimal(cost.toString()));

    $('.submit').click(function(){
        $('.submit').prop('disabled', true);
        var items = [];
        var paidCost = 0;
        var cost = 0;
        $('#tbodyTransactionBusiness tr').each(function () {
            paidCost = replaceCost($(this).find('input.paidCost').val(), true);
            cost += paidCost;
            items.push({
                TransactionBusinessItemId: $(this).attr('data-id'),
                TransactionBusinessId: $('input#transactionBusinessId').val(),
                TransactionKindId: $(this).find('input.transactionKindId').val(),
                TransactionStatusId: 2,
                PaidCost: paidCost,
                Comment: $(this).find('input.comment').val()
            });
        });
        $.ajax({
            type: "POST",
            url: $('input#updateTransactionBusinessUrl').val(),
            data: {
                Items: items
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                $('.submit').prop('disabled', false);
                if(json.code == 1){
                    $('#spanSumCost').text(formatDecimal(cost.toString()));
                    cost = Math.ceil(parseInt(cost) / 30);
                    $('#spanDayCost').text(formatDecimal(cost.toString()));
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('.submit').prop('disabled', false);
            }
        })
    });
});