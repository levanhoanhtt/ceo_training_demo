function chooseProductError(fnChooseProduct){
    var panelProduct = $('#panelProductError');
    var pageIdProduct = $('input#pageIdProductError');
    $(document).on('click','.open-search',function(){
        panelProduct.removeClass('active');
        $('.wrapper').removeClass('open-search');

    }).on('click', '.panel-default.active', function(e) {
        e.stopPropagation();
    });
    var statusSearch = null;
    $('#txtSearchProductError').click(function(){
        if(panelProduct.hasClass('active')){
            panelProduct.removeClass('active');
            panelProduct.find('.panel-body').css("width", "99%");
        }
        else{
            panelProduct.addClass('active');
            setTimeout(function (){
                panelProduct.find('.panel-body').css("width", "100%");
                $('.wrapper').addClass('open-search');
            }, 100);
            pageIdProduct.val('1');
            getListProductsError();
        }
    }).keydown(function () {
        if (statusSearch != null) {
            clearTimeout(statusSearch);
            statusSearch = null;
        }
    }).keyup(function () {
        if (statusSearch == null) {
            statusSearch = setTimeout(function () {
                if(!panelProduct.hasClass('active')){
                    panelProduct.addClass('active');
                    setTimeout(function (){
                        panelProduct.find('.panel-body').css("width", "100%");
                        $('.wrapper').addClass('open-search');
                    }, 100);
                }
                pageIdProduct.val('1');
                getListProductsError();
            }, 500);
        }
    });
    $('select.categoryId').change(function(){
        pageIdProduct.val('1');
        getListProductsError();
    });
    $('#btnPrevProductError').click(function(){
        var pageId = parseInt(pageIdProduct.val());
        if(pageIdProduct > 1){
            pageIdProduct.val(pageId - 1);
            getListProductsError();
        }
    });
    $('#btnNextProductError').click(function(){
        var pageId = parseInt(pageIdProduct.val());
        pageIdProduct.val(pageId + 1);
        getListProductsError();
    });
    $('#tbodyProductSearchError').on('click', 'tr', function () {
        panelProduct.removeClass('active');
        panelProduct.find('.panel-body').css("width", "99%");
        $('#txtSearchProductError').val('');
        $('select#categoryId').val('0');
        pageIdProduct.val('1');
        fnChooseProduct($(this));
    });
}

function getListProductsError(){
    var loading = $('#panelProductError .search-loading');
    loading.show();
    $('#tbodyProductSearchError').html('');
    $.ajax({
        type: "POST",
        url: $('input#getListProductUrl').val(),
        data: {
            SearchText: $('input#txtSearchProductError').val().trim(),
            CategoryId: $('select.categoryId').val(),
            PageId: parseInt($('input#pageIdProductError').val()),
            Limit: 10
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1){
                loading.hide();
                var data = json.data;
                var html = '';
                var i, j;
                var productPath = $('input#productPath').val();
                var noImage = 'logo.png';
                for(i = 0; i < data.length; i++){
                    html += '<tr class="pProduct" data-id="' + data[i].ProductId + '" data-child="0">';
                    html += '<td><img src="' + productPath + (data[i].ProductImage == '' ? noImage : data[i].ProductImage) + '" class="productImg"></td>';
                    html += '<td class="productName" style="color:#1782db">' + data[i].ProductName + '</td>';
                    html += '<td>' + data[i].BarCode + '</td>';
                    html += '<td class="text-right">' + formatDecimal(data[i].Price.toString()) + '</td>';
                    html += '<td>' + data[i].GuaranteeMonth + ' tháng</td></tr>';
                    if(data[i].Childs.length > 0){
                        for(j = 0; j < data[i].Childs.length; j++){
                            html += '<tr class="cProduct" data-id="' + data[i].ProductId + '" data-child="' + data[i].Childs[j].ProductChildId + '">';
                            html += '<td><img src="' + productPath + (data[i].Childs[j].ProductImage == '' ? noImage : data[i].Childs[j].ProductImage) + '" class="productImg"></td>';
                            html += '<td class="productName">' + data[i].ProductName + ' (' + data[i].Childs[j].ProductName + ')</td>';
                            html += '<td>' + data[i].Childs[j].BarCode + '</td>';
                            html += '<td class="text-right">' + formatDecimal(data[i].Childs[j].Price.toString()) + '</td>';
                            html += '<td>' + data[i].GuaranteeMonth + ' tháng</td></tr>';
                        }
                    }
                }
                $('#tbodyProductSearchError').html(html);
                $('#panelProductError .panel-body').slimScroll({
                    height: '300px',
                    alwaysVisible: true,
                    wheelStep: 20,
                    touchScrollStep: 500
                });
            }
            else loading.text('Có lỗi xảy ra').show();
        },
        error: function (response) {
            //showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            loading.text('Có lỗi xảy ra').show();
        }
    });
}