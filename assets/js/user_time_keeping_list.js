$(document).ready(function(){
    dateRangePicker();
    var keepingDateOld = '';
    var keepingDate = '';
    var rowspan = 1;
    $('#tbodyTimeLog tr').each(function(){
        keepingDate = $(this).attr('data-id');
        if(keepingDate != keepingDateOld){
            keepingDateOld = keepingDate;
            rowspan = $('#tbodyTimeLog tr[data-id="' + keepingDate + '"]').length;
            if(rowspan > 1){
                var trFirst = $('#tbodyTimeLog tr[data-id="' + keepingDate + '"]').first();
                trFirst.find('.tdMerge').attr('rowspan', rowspan).css('vertical-align', 'middle');
                $('#tbodyTimeLog tr[data-id="' + keepingDate + '"] .tdMerge[rowspan="1"]').remove();
                trFirst.find('.tdTotalTime').text($('input#duration_' + keepingDate.replace(/\//g, '_')).val());
            }
        }
    });
    var cellThis, cellPrev, spanning;
    $('#tbodyTimeLog .col-x').each(function () {
        cellThis = $(this);
        spanning = 0;
        if(cellPrev) {
            if($(cellPrev).find('span.spanMergeName').html() == $(cellThis).find('span.spanMergeName').html()){
                $(cellThis).remove();
                $(cellPrev).prop("rowspan", parseInt($(cellPrev).prop("rowspan")) + 1);
                spanning = 1;
            }
        }
        if(spanning == 0) cellPrev = $(this);
    });

    $('#showModalImport').click(function(){
        $('input#fileExcelUrl').val('');
        $('#modalImportExcel').modal('show');
    });
    $('#btnUploadExcel').click(function(){
        chooseFile('Files', function(fileUrl) {
            $('input#fileExcelUrl').val(fileUrl);
        });
    });

    $('#btnImportExcel').click(function(){
        var fileExcelUrl = $('input#fileExcelUrl').val();
        if(fileExcelUrl != ''){
            var btn = $(this);
            btn.prop('disabled', true);
            $('.imgLoading').show();
            $.ajax({
                type: "POST",
                url: $('input#importTimeKeepingUrl').val(),
                data: {
                    FileUrl: fileExcelUrl
                },
                success: function (response) {
                    $('.imgLoading').hide();
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) $('#modalImportExcel').modal('hide');
                    btn.prop('disabled', false);

                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong qua trình xử lý', 0);
                    $('.imgLoading').hide();
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Please choose Excel file', 0);
    });
});

