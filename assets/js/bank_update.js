$(document).ready(function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    bankType($('input[name="BankTypeId"]:checked').val());
    $('input.iCheckBankType').on('ifToggled', function(e){
        if(e.currentTarget.checked) bankType(e.currentTarget.value);
    });
    $('.submit').click(function() {
        if (validateEmpty('#bankForm')) {
            $('.submit').prop('disabled', true);
            var form = $('#bankForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if($('input#bankId').val() == '0') redirect(false, $('#aBankList').attr('href'));
                        else $('.submit').prop('disabled', false);
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });
});

function bankType(bankTypeId){
    if(bankTypeId == '1') $('div#bankType2').fadeOut();
    else $('div#bankType2').fadeIn();
}