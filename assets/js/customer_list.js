$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Khách hàng',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
            if(actionCode == 'create_group'){
                $('#cbCustomerGroup1').prop('checked', true);
                $("input#customerIds").val(JSON.stringify(itemIds));
                $("input#customerGroupName").hide();
                $("#divSelectGroup").show();
                $('#customerGroupId').val('0').trigger("change");
                $("#customerGroupName, input#comment").val('');
                $('#ulCondition li.condition').remove();
                var html = '';
                $('ul#container-filters li.item .btn-field').each(function(){
                    html += '<li class="condition">' + $(this).text() + '</li>';
                });
                $("span#count_customer").text(' ( '+itemIds.length+' được chọn )');
                $('#ulCondition').append(html);
                $("#modalCustomerGroup").modal('show');
            }
        }
    });
    $('input.history').click(function(){
        $('#btn-filter').attr('data-href', $('input#searchProductUrl').val() + $(this).val() + '/' + $('input#customerGroupId').val());
        actionItemAndSearch({
            ItemName: 'Khách hàng',
            IsRenderFirst: true,
            extendFunction: function(itemIds, actionCode){}
        });
        $("input#customerKindId").val($(this).val());
        if($(this).val() == '1') $("#spanCustomerKindName").text('Khách lẻ');
        else if($(this).val() == '2') $("#spanCustomerKindName").text("Khách buôn");
        else $("#spanCustomerKindName").text("CTV");
        $('#aAddCustomer').attr('href', $('input#urlAddCustomer').val() + $(this).val());
    });
    $('input.cbCustomerGroup').click(function(){
        if($(this).val() == 'old'){
            $("input#customerGroupName").hide();
            $("#divSelectGroup").show();
            $('#customerGroupId').val('0').trigger("change");
        }
        else{
            $("input#customerGroupName").show();
            $("#divSelectGroup").hide();
            $("#customerGroupName").val('');
        }
    });
    $('#btnUpdateCustomerGroup').click(function(){
        var customerGroupId = $("#customerGroupId").val();
        var customerGroupName = $("#customerGroupName").val().trim();
        if($('input[name="cbCustomerGroup"]:checked').val() == 'old'){
            if(customerGroupId == '0'){
                showNotification("Vui lòng chọn nhóm khách hàng", 0);
                return false;
            }
            else customerGroupName = $('#customerGroupId option[value="' + customerGroupId + '"]').text();
        }
        else{
            if(customerGroupName == ''){
                showNotification("Vui lòng điền nhóm khách hàng", 0);
                return false;
            }
        }
        var conditions = [];
        $('#ulCondition li').each(function(){
            conditions.push($(this).text().trim());
        });
        var filterId = $('#ulFilter li.active').find('a').attr('data-id');
        $.ajax({
            type: "POST",
            url: $('input#updateCustomerGroupUrl').val(),
            data: {
                FilterId : filterId,
                FilterData: JSON.stringify(data_filter.itemFilters),
                TagFilter : JSON.stringify(data_filter.tagFilters),
                CustomerGroupId : customerGroupId,
                CustomerGroupName : customerGroupName,
                CustomerKindId : $("input#customerKindId").val().trim(),
                //Comment : $("input#comment").val().trim(),
                Conditions: JSON.stringify(conditions),
                CustomerIds: $("input#customerIds").val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) $("#modalCustomerGroup").modal('hide');
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
    $("a#create_group").click(function(){
        $('#cbCustomerGroup1').prop('checked', true);
        $("input#customerIds").val(JSON.stringify([]));
        $("input#customerGroupName").hide();
        $("#divSelectGroup").show();
        $('#customerGroupId').val('0').trigger("change");
        $("#customerGroupName, input#comment").val('');
        $('#ulCondition li.condition').remove();
        var html = '';
        $('ul#container-filters li.item .btn-field').each(function(){
            html += '<li class="condition">' + $(this).text() + '</li>';
        });
        $("span#count_customer").text('');
        $('#ulCondition').append(html);
        $("#modalCustomerGroup").modal('show');
        return false;
    })
});

function renderContentCustomers(data){
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var totalOrder = 0;
        var totalPrice = 0;
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].CustomerId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].CustomerId + '"></td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td>' + data[item].PhoneNumber + '</td>';
            html += '<td>' + data[item].Address + '</td>';
            html += '<td class="text-right">';
            if(data[item].TotalOrder != null){
                html += formatDecimal(data[item].TotalOrder);
                totalOrder += parseInt(data[item].TotalOrder);
            }
            html += '</td><td class="text-right">';
            if(data[item].TotalPrice != null){
                html += formatDecimal(data[item].TotalPrice);
                totalPrice += parseInt(data[item].TotalPrice);
            }
            html += '</td><td class="text-right">' + formatDecimal(data[item].Balance) + '</td></tr>';
        }
        if(html != '') html += '<tr><td colspan="5"></td><td class="text-right">' + formatDecimal(totalOrder.toString()) + '</td><td class="text-right">' + formatDecimal(totalPrice.toString()) + '</td><td></td></tr>';
        html += '<tr><td colspan="8" class="paginate_table"></td></tr>';
        $('#tbodyCustomer').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}