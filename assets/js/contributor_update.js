$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    province();
    $('.submit').click(function (){
        if(validateEmpty('#contributorForm')) {
            var form = $('#contributorForm');
            $('.submit').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if($('input#contributorId').val() == '0') redirect(false, $('a#contributorListUrl').attr('href'));
                        else $('.submit').prop('disabled', false);
                    }
                    else $('.submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.submit').prop('disabled', false);
                }
            });
        }
        return false;
    });
    $('#tbodyContributor').on('keyup', 'input#paidVN', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('click', 'a.link_edit', function(){
        var id = $(this).attr('data-id');
        $('input#contributorProductTypeId').val(id);        
        $('select#productTypeId').val($('input#productTypeId_' + id).val());
        $('input#paidVN').val($('td#paidVN_' + id).text());
        $('input#paidDateTime').val($('td#paidDateTime_' + id).text());
        scrollTo('input#paidVN');
        return false;
    }).on('click', 'a.link_delete', function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteContributeUrl').val(),
                data: {
                    ContributorProductTypeId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) $('tr#contributor_' + id).remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('input#paidVN, input#contributorProductTypeId').val('0');
        $('input#paidDateTime').val('');
        return false;
    });
    $('a#link_update').click(function(){
        if(validateEmpty('#tbodyContributor')){
            if(validateNumber('#tbodyContributor', true)){
                $.ajax({
                    type: "POST",
                    url: $('input#updateContributeUrl').val(),
                    data: {
                        ContributorProductTypeId: $('input#contributorProductTypeId').val(),
                        ContributorId: $('input#contributorId').val(),
                        ProductTypeId: $('select#productTypeId').val(),
                        PaidVN: replaceCost($('input#paidVN').val(), true), 
                        PaidDateTime: $('input#paidDateTime').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1){
                            $('a#link_cancel').trigger("click");
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr id="contributor_' + data.ContributorProductTypeId + '">';
                                html += '<td id="productTypeName_' + data.ContributorProductTypeId + '">' + data.ProductTypeName + '</td>';
                                html += '<td id="paidVN_' + data.ContributorProductTypeId + '">' + formatDecimal(data.PaidVN.toString()) + '</td>';
                                html += '<td id="paidDateTime_' + data.ContributorProductTypeId + '">' + data.PaidDateTime + '</td>';
                                html += '<td class="actions">' +
                                    '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ContributorProductTypeId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                    '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ContributorProductTypeId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                    '<input type="text" hidden="hidden" id="productTypeId_' + data.ContributorProductTypeId + '" value="' + data.ProductTypeId + '">' +
                                    '</td>';
                                html += '</tr>';
                                $('#tbodyContributor').prepend(html);
                            }
                            else{
                                $('td#productTypeName_' + data.ContributorProductTypeId).text(data.ProductTypeName);
                                $('td#paidVN_' + data.ContributorProductTypeId).text(formatDecimal(data.PaidVN.toString()));
                                $('td#paidDateTime_' + data.ContributorProductTypeId).text(data.PaidDateTime);
                            }
                        }
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        return false;
    });
});
