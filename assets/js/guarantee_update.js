var app = app || {};

app.init = function(guaranteeId, canEditAll) {
    app.initLibrary();
    if(guaranteeId > 0) app.tabStep();
    app.customer(canEditAll);
    app.product(canEditAll);
    app.guaranteeComment(guaranteeId);
    app.remind();
    app.submit(guaranteeId);
};

app.initLibrary = function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('input.datetimepicker').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        startDate: new Date()
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
    $('body').on('keydown', 'input.cost', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.cost', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
};

app.tabStep = function(){
    $('#ulGuaranteeStep').on('click', 'a', function(){
        var id = $(this).attr('data-id');
        $('input#currentGuaranteeStep').val(id);
        $('.divTableGuarantee').hide();
        if(id != $('input#guaranteeStep').val()) $('#divSubmit').hide();
        else $('#divSubmit').show();
        if(id == '2') id = '1';
        $('#divTableGuarantee_' + id).show();
        if(id == '1'){
            $('#divStore').show();
            $('#divCustomerComment').hide();
        }
        else{
            $('#divStore').hide();
            $('#divCustomerComment').show();
        }
        if(id == '4') $('#divGuaranteeInfo').show();
        else $('#divGuaranteeInfo').hide();
    });
    $('#ulGuaranteeStep4').on('click', 'a', function(){
        $('input#currentGuaranteeStep4').val($(this).attr('data-id'));
    });
    if(getCurrentGuaranteeStep() == '4') setProductGuaranteeInfo();
};

app.customer = function(canEditAll){
    if(canEditAll == 1){
        chooseCustomer(function (li) {
            var customerId = parseInt(li.attr('data-id'));
            $('input#customerId').val('0');
            if (customerId > 0) showInfoCustomer(customerId);
        }, 0);
        jwerty.key('f4', function () {
            var panelCustomer = $('#panelCustomer');
            if (panelCustomer.hasClass('active')) {
                panelCustomer.removeClass('active');
                panelCustomer.find('panel-body').css("width", "99%");
            }
            else {
                panelCustomer.addClass('active');
                setTimeout(function () {
                    panelCustomer.find('panel-body').css("width", "100%");
                    $('.wrapper').addClass('open-search-customer');
                }, 100);
                $('input#pageIdCustomer').val('1');
                getListCustomers();
            }
        });
        $('#btnCloseBoxCustomer').click(function () {
            $('#divCustomer').hide();
            $('#boxChooseCustomer').show();
            $('input#customerId').val('0');
            return false;
        });
    }
    else $('#btnCloseBoxCustomer').remove();
    if($('input#customerId').val() != '0') showInfoCustomer($('input#customerId').val());
};

app.product = function(canEditAll){
    if(canEditAll == 1) {
        $('#btnAddProduct').click(function () {
            var customerId = parseInt($('input#customerId').val());
            if (customerId > 0) {
                $.ajax({
                    type: "POST",
                    url: $('input#getListProductUrl').val(),
                    data: {
                        CustomerId: customerId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            var data = json.data;
                            var html = '';
                            var editOrderUrl = $('input#editOrderUrl').val() + '/';
                            var editProductUrl = $('input#editProductUrl').val() + '/';
                            for (var i = 0; i < data.length; i++) {
                                html += '<tr data-product="' + data[i].ProductId + '" data-child="' + data[i].ProductChildId + '" data-order="' + data[i].OrderId + '">';
                                html += '<td><a href="' + editOrderUrl + data[i].OrderId + '" target="_blank" class="aSearchText">' + data[i].OrderCode + '</a></td>';
                                html += '<td><a href="' + editProductUrl + data[i].ProductId + '" target="_blank" class="aSearchText">' + data[i].ProductName;
                                if (data[i].ProductChildName != null) html += data[i].ProductChildName;
                                html += '</a></td><td class="text-center" ">' + (data[i].ProductUnitName != null ? data[i].ProductUnitName : '') + '</td><td class="quantity">' + data[i].Quantity + '</td><td>' + data[i].CrDateTime + '</td>';
                                html += '<td class="text-right actions"><a href="javascript:void(0)" class="link_add" title="Thêm"><i class="fa fa-plus"></i></a></td></tr>';
                            }
                            $('#tbodyProductSearch').html(html);
                            $('#modalProduct').modal('show');
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
            else showNotification('Vui lòng chọn Khách hàng');
        });
        var statusSearch = null;
        $('#itemSearchName').keydown(function () {
            if (statusSearch != null) {
                clearTimeout(statusSearch);
                statusSearch = null;
            }
        }).keyup(function () {
            if (statusSearch == null) {
                statusSearch = setTimeout(function () {
                    var searchText = $('#itemSearchName').val().trim();
                    if(searchText == '') $('#tbodyProductSearch tr').show();
                    else{
                        $('#tbodyProductSearch tr').hide();
                        $('#tbodyProductSearch .aSearchText').each(function(){
                            if($(this).text().indexOf(searchText) > -1) $(this).parent().parent().show();
                        });
                    }
                }, 500);
            }
        });
        $('#aGuaranteeImage').click(function () {
            chooseFile('Guarantees', function (fileUrl) {
                $('input#guaranteeImage').val(fileUrl);
                $('#imgGuaranteeImage').attr('src', fileUrl).show();
                $('#aGuaranteeImage1').attr('href', fileUrl);
            });
            return false;
        });
        $('#tbodyProductSearch').on('click', '.link_add', function () {
            var tr = $(this).parent().parent();
            var productId = tr.attr('data-product');
            var productChildId = tr.attr('data-child');
            var orderId = tr.attr('data-order');
            var quantityAll = 0;
            var currentGuaranteeStep = getCurrentGuaranteeStep();
            $('#tbodyProduct_' + currentGuaranteeStep + ' tr').each(function () {
                if ($(this).attr('data-product') == productId && $(this).attr('data-child') == productChildId && $(this).attr('data-order') == orderId) {
                    quantityAll++;
                    return false;
                }
            });
            var flag = false;
            if (quantityAll > 0) {
                var quantity = parseInt(tr.find('td.quantity').text());
                if (quantity > quantityAll) flag = true;
            }
            else flag = true;
            if (flag) {
                $.ajax({
                    type: "POST",
                    url: $('input#getListAccessoryUrl').val(),
                    data: {
                        ProductId: productId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) addProductGuarantee(productId, productChildId, orderId, tr, json.data);
                        else addProductGuarantee(productId, productChildId, orderId, tr, []);
                    },
                    error: function (response) {
                        addProductGuarantee(productId, productChildId, orderId, tr, []);
                    }
                });

            }
            else showNotification('Sản phẩm đã có', 0);
        });
    }
    $('.tbodyProduct').on('click', '.link_delete', function(){
        $('.trProduct_' + getCurrentGuaranteeStep() + '_' + $(this).parent().parent().attr('data-index')).remove();
    }).on('click', '.aProductImage', function(){
        var ele = $(this);
        chooseFile('Guarantees', function (fileUrl) {
            ele.html('<img src="' + fileUrl + '" style="height: 50px;">');
        });
        return false;
    }).on('click', '.aProductVideo', function(){
        var videoUrl = $(this).attr('data-video');
        $('input#vìdeoUrl').val(videoUrl);
        if(videoUrl != ''){
            videoUrl = getYoutubeVideoId(videoUrl);
            if(videoUrl != '') $('#iframeVideo').attr('src', 'https://www.youtube.com/embed/' + videoUrl + '?rel=0&amp;showinfo=0&amp;modestbranding=1').show();
            else $('#iframeVideo').hide();
        }
        else $('#iframeVideo').hide();
        $('input#curentProductIndex').val($(this).parent().parent().attr('data-index'));
        $('#modalVideo').modal('show');
        return false;
    }).on('change', 'select.usageStatusId2', function(){
        var select = $(this).parent().parent().find('select.usageStatusId3');
        select.find('option').hide();
        select.find('option[value="0"]').show();
        select.find('option[data-id="' + $(this).val() + '"]').show();
        select.val('0');
    }).on('click', '.spanComment', function(){
        var comment = $(this).parent().find('input').val().trim();
        if(comment != ''){
            var id = $(this).attr('data-id');
            var no = $(this).attr('data-no');
            $.ajax({
                type: 'POST',
                url: $('input#updateProductCommentUrl').val(),
                data: {
                    GuaranteeProductId: id,
                    Comment: comment,
                    CommentNo: no
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) $('.tbodyProduct .comment' + no + '_' + id).val(comment);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else showNotification('Ghi chú không được bỏ trống', 0);
    });/*.on('change', 'select.guaranteeSolutionId4', function(){
        var select = $(this).parent().parent().find('select.guaranteeSolutionId5');
        select.find('option').hide();
        select.find('option[value="0"]').show();
        select.find('option[data-id="' + $(this).val() + '"]').show();
        select.val('0');
    });*/
    $('input#vìdeoUrl').blur(function(){
        var videoUrl = $(this).val().trim();
        if(videoUrl != ''){
            videoUrl = getYoutubeVideoId(videoUrl);
            if(videoUrl != '') $('#iframeVideo').attr('src', 'https://www.youtube.com/embed/' + videoUrl + '?rel=0&amp;showinfo=0&amp;modestbranding=1').show();
            else $('#iframeVideo').hide();
        }
        else $('#iframeVideo').hide();
    });
    $('#btnUpdateVideo').click(function(){
        var curentProductIndex = $('input#curentProductIndex').val();
        $('.trProduct_' + getCurrentGuaranteeStep() + '_' + curentProductIndex).first().find('.aProductVideo').attr('data-video', $('input#vìdeoUrl').val().trim());
        $('#modalVideo').modal('hide');
    });
    $('#tbodyProductMoney')/*.on('click', '.spanControl', function(){
        $(this).parent().find('.spanControl').removeClass('active');
        $(this).addClass('active');
    }).on('change', 'select.guaranteeSolutionId4', function(){
        var select = $(this).parent().parent().find('select.guaranteeSolutionId5');
        select.find('option').hide();
        select.find('option[value="0"]').show();
        select.find('option[data-id="' + $(this).val() + '"]').show();
        select.val('0');
    })*/.on('keydown', 'input.percent, input.number', function (e) {
        if(checkKeyCodeNumber(e)) e.preventDefault();
    }).on('keyup', 'input.percent', function () {
        var tr = $(this).parent().parent().parent();
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        value = parseInt(value);
        if(value < 0 || value > 100){
            $(this).val('0');
            tr.find('input.number').val('0');
            calcPriceCustomer(tr, 0, true);
        }
        else{
            var price = replaceCost(tr.find('.spanPrice').text(), true);
            price = Math.ceil(price * value / 100);
            tr.find('input.number').val(formatDecimal(price.toString()));
            calcPriceCustomer(tr, price, true);
        }
    }).on('keyup', 'input.number', function (){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        var tr = $(this).parent().parent().parent();
        tr.find('input.percent').val('0');
        calcPriceCustomer(tr, replaceCost(value, true), true);
    });
};

app.guaranteeComment = function(guaranteeId){
    $('.btnInsertComment').click(function(){
        var id = $(this).attr('data-id');
        var comment = $('input#comment_' + id).val().trim();
        if(comment != ''){
            if(guaranteeId > 0){
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertGuaranteeCommentUrl').val(),
                    data: {
                        GuaranteeId: guaranteeId,
                        Comment: comment,
                        CommentTypeId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1) {
                            $('div#listComment_' + id).prepend(genItemComment(comment));
                            $('input#comment_' + id).val('');
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else{
                $('div#listComment_' + id).prepend(genItemComment(comment));
                $('input#comment_' + id).val('');
            }
        }
        else{
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment_' + id).focus();
        }
    });
    if(guaranteeId > 0){
        $('.aShowComment').click(function(){
            var id = $(this).attr('data-id');
            if(id == '1') $('#modalItemComment h4').html('<i class="fa fa-comments-o"></i> Ghi chú nội bộ');
            else $('#modalItemComment h4').html('<i class="fa fa-comments-o"></i> Lịch sử trao đổi với KH');
            $('#modalItemComment .box-customer').hide();
            $('#modalItemComment .divBoxComment_' + id).show();
            $('#modalItemComment').modal('show');
            return false;
        });
    }    
};

app.remind = function(){
    $('.aRemind').click(function(){
        $('#modalRemind').modal('show');
        return false;
    });
    $('select#userId').change(function(){
        if($(this).val() != '0') $('select#partId').val('0').trigger('change');
    });
    $('select#partId').change(function(){
        if($(this).val() != '0') $('select#userId').val('0').trigger('change');
    });
    $('#btnAddRemind').click(function(){
        if(validateEmpty('#modalRemind')){
            var btn = $(this);
            btn.prop('disabled', true);
            var comments = [];
            var remindComment = $('input#remindComment').val().trim();
            if(remindComment != '') comments.push(remindComment);
            $.ajax({
                type: "POST",
                url: $('input#insertRemindUrl').val(),
                data: {
                    RemindId: 0,
                    RemindTitle: $('input#remindTitle').val().trim(),
                    RemindDate: $('input#remindDate1').val().trim(),
                    IsRepeat: 1,
                    RepeatDay: 0,
                    RepeatHour: 0,
                    RemindStatusId: 1,
                    UserId: $('select#userId').val(),
                    PartId: $('select#partId').val(),

                    Comments: JSON.stringify(comments)
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) {
                        $('#modalRemind input').val('');
                        $('select#userId').val($('input#userLoginId').val());
                        $('select#partId').val('0');
                        $('#modalRemind').modal('hide');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });
};

app.submit = function(guaranteeId){
    $('#btnSaveGuarantee').click(function () {
        var customerId = parseInt($('input#customerId').val());
        if(customerId > 0) {
            if(validateNumber('#guaranteeForm', true, ' không được bỏ trống')){
                var guaranteeStep = parseInt($('input#guaranteeStep').val());
                var products = [];
                var productImage = '';
                var usageStatusId1 = 0;
                var btn = $(this);
                btn.prop('disabled', true);
                if(guaranteeStep < 3) {
                    var productAccessoryIds = [];
                    $('#tbodyProduct_1 .trProductMain').each(function () {
                        usageStatusId1 = $(this).find('select.usageStatusId1').val();
                        if(usageStatusId1 == '0'){
                            products = [];
                            showNotification('Hình thức không được bỏ trống', 0);
                            return false;
                        }
                        productAccessoryIds = [];
                        $(this).find('input.iCheckAccessory').each(function () {
                            if ($(this).parent('div').hasClass('checked')) productAccessoryIds.push(parseInt($(this).val()));
                        });
                        productImage = '';
                        if ($(this).find('.aProductImage img').length > 0) productImage = $(this).find('.aProductImage img').attr('src');
                        products.push({
                            GuaranteeProductId: $(this).attr('data-id'),
                            OrderId: $(this).attr('data-order'),
                            ProductId: $(this).attr('data-product'),
                            ProductChildId: $(this).attr('data-child'),
                            ProductImage1: productImage,
                            ProductImage2: '',
                            VideoUrl: '',
                            ProductAccessoryIds: JSON.stringify(productAccessoryIds),
                            UsageStatusId1: usageStatusId1,
                            UsageStatusId2: 0,
                            UsageStatusId3: 0,
                            GuaranteeSolutionId1: 0,
                            GuaranteeSolutionId2: 0,
                            GuaranteeSolutionId3: 0,
                            GuaranteeSolutionId4: 0,
                            GuaranteeSolutionId5: 0,
                            CustomerConditionId: 0,
                            ReducePercent: 0,
                            ReduceNumber: 0,
                            Comment1: $('input#productComment_1_' + $(this).attr('data-index')).val().trim(),
                            Comment2: ''
                        });
                    });
                    if(products.length > 0){
                        var comments = [];
                        if (guaranteeId == 0) {
                            $('#listComment_1 .pComment').each(function () {
                                comments.push($(this).text());
                            });
                            var comment = $('input#comment_1').val().trim();
                            if(comment != '') comments.push(comment);
                        }
                        $.ajax({
                            type: 'POST',
                            url: $('#guaranteeForm').attr('action'),
                            data: {
                                GuaranteeId: guaranteeId,
                                GuaranteeStep: guaranteeStep,
                                GuaranteeImage: $('#imgGuaranteeImage').attr('src'),
                                CustomerId: customerId,
                                ReceiptTypeId: $('select#receiptTypeId').val(),
                                PaymentTypeId: $('select#paymentTypeId').val(),
                                ShipCost: replaceCost($('input#shipCost').val(), true),
                                StoreId: $('select#storeId').val(),
                                GuaranteeStatusId: $('input#guaranteeStatusId').val(),

                                Products: JSON.stringify(products),
                                Comments: JSON.stringify(comments)
                            },
                            success: function (response) {
                                var json = $.parseJSON(response);
                                showNotification(json.message, json.code);
                                if (json.code == 1) {
                                    if (guaranteeId == 0) redirect(false, $('#aGuaranteeList').attr('href'));
                                }
                                btn.prop('disabled', false);
                            },
                            error: function (response) {
                                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                                btn.prop('disabled', false);
                            }
                        });
                    }
                    else btn.prop('disabled', false);
                }
                else if(guaranteeStep == 3){ //Kiểm tra kĩ thuật
                    var usageStatusId2 = 0, usageStatusId3 = 0, guaranteeSolutionId1 = 0;
                    $('#tbodyProduct_3 .trProductMain').each(function () {
                        usageStatusId1 = $(this).find('select.usageStatusId1').val();
                        usageStatusId2 = $(this).find('select.usageStatusId2').val();
                        usageStatusId3 = $(this).find('select.usageStatusId3').val();
                        guaranteeSolutionId1 = $(this).find('select.guaranteeSolutionId1').val();
                        if(usageStatusId1 == '0' || usageStatusId2 == '0' || usageStatusId3 == '0' || guaranteeSolutionId1 == '0'){
                            products = [];
                            showNotification('Hình thức, TT sử dụng, gợi ý không được bỏ trống', 0);
                            return false;
                        }
                        productImage = '';
                        if ($(this).find('.aProductImage img').length > 0) productImage = $(this).find('.aProductImage img').attr('src');
                        products.push({
                            GuaranteeProductId: $(this).attr('data-id'),
                            ProductImage2: productImage,
                            VideoUrl: $(this).find('.aProductVideo').attr('data-video'),
                            UsageStatusId1: usageStatusId1,
                            UsageStatusId2: usageStatusId2,
                            UsageStatusId3: usageStatusId3,
                            GuaranteeSolutionId1: guaranteeSolutionId1,
                            Comment2: $('input#productComment_3_' + $(this).attr('data-index')).val().trim()
                        });
                    });
                    if(products.length > 0) updateGuaranteeProduct(products, guaranteeId, guaranteeStep, btn, function(){});
                    else btn.prop('disabled', false);
                }
                else if(guaranteeStep == 4){ //dua p/a xu ly
                    var currentGuaranteeStep4 = parseInt($('input#currentGuaranteeStep4').val());
                    if(currentGuaranteeStep4 == 1) {
                        var guaranteeSolutionId2 = 0, guaranteeSolutionId3 = 0, guaranteeSolutionId4 = 0, customerConditionId = 0;
                        $('#tbodyProduct_4 .trProductMain').each(function () {
                            guaranteeSolutionId2 = $(this).find('select.guaranteeSolutionId2').val();
                            guaranteeSolutionId3 = $(this).find('select.guaranteeSolutionId3').val();
                            guaranteeSolutionId4 = $(this).find('select.guaranteeSolutionId4').val();
                            customerConditionId = $(this).find('select.customerConditionId').val();
                            if (guaranteeSolutionId2 == '0' || guaranteeSolutionId3 == '0' || guaranteeSolutionId4 == '0' || customerConditionId == '0') {
                                products = [];
                                showNotification('Yêu cầu KH, kết luận chốt không được bỏ trống', 0);
                                return false;
                            }
                            products.push({
                                GuaranteeProductId: $(this).attr('data-id'),
                                GuaranteeSolutionId2: guaranteeSolutionId2,
                                GuaranteeSolutionId3: guaranteeSolutionId3,
                                GuaranteeSolutionId4: guaranteeSolutionId4,
                                GuaranteeSolutionId5: $(this).find('select.guaranteeSolutionId5').val(),
                                CustomerConditionId: customerConditionId
                            });
                        });
                        if(products.length > 0) updateGuaranteeProduct(products, guaranteeId, guaranteeStep, btn, function(){
                            var tr;
                            for(var i = 0; i< products.length; i++){
                                tr = $('#tbodyProductMoney tr[data-id="' + products[i].GuaranteeProductId + '"]');
                                tr.find('select.guaranteeSolutionId4').val(products[i].GuaranteeSolutionId4);
                                tr.find('select.guaranteeSolutionId5').val(products[i].GuaranteeSolutionId5);
                            }
                            setProductGuaranteeInfo();
                        });
                        else btn.prop('disabled', false);
                    }
                    else if(currentGuaranteeStep4 == 2){
                        $('#tbodyProductMoney .trProductMain').each(function (){
                            products.push({
                                GuaranteeProductId: $(this).attr('data-id'),
                                ReducePercent: $(this).find('input.percent').val(),
                                ReduceNumber: replaceCost($(this).find('input.number').val(), true)
                            });
                        });
                        if(products.length > 0) updateGuaranteeProduct(products, guaranteeId, guaranteeStep, btn, function(){});
                        else btn.prop('disabled', false);
                    }
                    else btn.prop('disabled', false);
                }
                else btn.prop('disabled', false);
            }
        }
        else showNotification('Vui lòng chọn Khách hàng');
    });
    if(guaranteeId > 0){
        $('#btnNextStep').click(function () {
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: 'POST',
                url: $('input#updateFieldUrl').val(),
                data: {
                    GuaranteeId: guaranteeId,
                    GuaranteeStep: parseInt($('input#guaranteeStep').val()) + 1
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        });
    }
};

$(document).ready(function(){
    var guaranteeId = parseInt($('input#guaranteeId').val());
    var canEditAll = parseInt($('input#canEditAll').val());
    app.init(guaranteeId, canEditAll);
});

function showInfoCustomer(customerId) {
    $.ajax({
        type: "POST",
        url: $('input#getCustomerDetailUrl').val(),
        data: {
            CustomerId: customerId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1) {
                var data = json.data;
                $('input#customerId').val(customerId);
                if (data.PhoneNumber != '' && data.PhoneNumber2 != '') data.PhoneNumber = data.PhoneNumber + ' - ' + data.PhoneNumber2;
                var editCustomerUrl = $('input#editCustomerUrl').val() + '/' + customerId + '/';
                $('h4.i-name').html('<a href="' + editCustomerUrl + '3" target="_blank">' + data.FullName + '</a>');
                $('div.i-phone').text(data.PhoneNumber == '' ? '-' : data.PhoneNumber);
                if(data.CustomerGroupName != '') $('.i-cusType').html('<span class="label label-success">' + data.CustomerGroupName + '</span>').show();
                else $('.i-cusType').hide();
                $('.i-total-orders').html('<a href="' + editCustomerUrl + '1" target="_blank">' + data.TotalOrders + '</a>');
                $('span#customerBalance').html('<a href="' + editCustomerUrl + '2" target="_blank">' + formatDecimal(data.Balance) + '</a>');
                $('#spanLastGuarantee').text('BH-10001 | 21/04/2018');
                $('#boxChooseCustomer').hide();
                $('#divCustomer').show();
            }
            else{
                showNotification(json.message, json.code);
                $('input#customerId').val('0');
            }
            if(getCurrentGuaranteeStep() == '4') calcPriceCustomer(false, 0, false);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            $('input#customerId').val('0');
            if(getCurrentGuaranteeStep() == '4') calcPriceCustomer(false, 0, false);
        }
    });
}

function addProductGuarantee(productId, productChildId, orderId, tr, listAccessories){
    var productIndex = parseInt($('input#productIndex').val()) + 1;
    $('input#productIndex').val(productIndex);
    var currentGuaranteeStep = getCurrentGuaranteeStep();
    var html = '<tr data-index="' + productIndex + '" class="trProductMain trProduct_' + currentGuaranteeStep + '_' + productIndex + '" data-id="0" data-product="' + productId + '" data-child="' + productChildId + '" data-order="' + orderId + '">' + tr.html().replace('<td class="text-right actions"><a href="javascript:void(0)" class="link_add" title="Thêm"><i class="fa fa-plus"></i></a></td>', '');
    html += '<td class="trPadding"><select class="form-control usageStatusId1"><option value="0">--Chọn--</option>' + $('select#usageStatusIdOriginal').html() + '</select></td><td class="trPadding">';
    var n = listAccessories.length;
    for(var i = 0; i < n; i++){
        html += '<p class="item"><input type="checkbox" class="iCheck iCheckAccessory accessoryId_' + productId + '" value="' + listAccessories[i].ProductAccessoryId + '"><span class="spanCheck">' + listAccessories[i].AccessoryName + '</span></p>';
    }
    html += '</td><td class="text-center"><a href="javascript:void(0)" class="aProductImage"><i class="fa fa-file-image-o"></i></a></td><td class="text-right"><a href="javascript:void(0)" class="link_delete"><img src="assets/vendor/dist/img/icon-close.png"></a></td></tr>';
    html += '<tr class="trProduct_' + currentGuaranteeStep + '_' + productIndex + '"><td colspan="9"><input type="text" class="form-control" id="productComment_' + currentGuaranteeStep + '_' + productIndex + '" value="" placeholder="Ghi chú mô tả"></td></tr>';
    $('#tbodyProduct_' + currentGuaranteeStep).append(html);
    $('#modalProduct').modal('hide');
    if(n > 0){
        $('input.iCheckAccessory').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '0%'
        });
    }
}

function updateGuaranteeProduct(products, guaranteeId, guaranteeStep, btn, fn){
    $.ajax({
        type: 'POST',
        url: $('input#updateProductUrl').val(),
        data: {
            GuaranteeId: guaranteeId,
            GuaranteeStep: guaranteeStep,
            Products: JSON.stringify(products)
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(6) + '</span></li>');
                fn();
            }
            btn.prop('disabled', false);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            btn.prop('disabled', false);
        }
    });
}

function getYoutubeVideoId(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match && match[7].length==11)? match[7] : '';
}

function getCurrentGuaranteeStep(){
    var currentGuaranteeStep = $('input#currentGuaranteeStep').val();
    if(currentGuaranteeStep == '2') currentGuaranteeStep = '1';
    return currentGuaranteeStep;
}

function setProductGuaranteeInfo(){
    var guaranteeSolutionId4 = 0;
    var guaranteeSolutionIds = [];
    var i = 0;
    for(i = 0; i < 6; i++) guaranteeSolutionIds[i] = 0;
    $('#tbodyProduct_4 tr').each(function(){
        guaranteeSolutionId4 = parseInt($(this).find('select.guaranteeSolutionId4').val());
        guaranteeSolutionIds[guaranteeSolutionId4]++;
    });
    var html = '';
    for(i = 1; i < 6; i++){
        if(guaranteeSolutionIds[i] > 0) html += '<li>' + guaranteeSolutionIds[i] + ' sản phẩm: ' + $('select#guaranteeSolutionIdOriginal option[value="' + i + '"]').text() + '</li>';
    }
    $('#divGuaranteeInfo ul').html(html);
}

function calcPriceCustomer(trCurent, number, flag){
    if(flag) {
        var price = replaceCost(trCurent.find('.spanPrice').text(), true) - number;
        if (trCurent.find('.spanDiscount').length > 0) price -= replaceCost(trCurent.find('.spanDiscount').text(), true);
        trCurent.find('input.diffCost').val(formatDecimal(price.toString()));
    }
    var totalNumber = 0;
    var totalDiffCost = 0;
    $('#tbodyProductMoney tr').each(function(){
        totalNumber += replaceCost($(this).find('input.number').val(), true);
        totalDiffCost += replaceCost($(this).find('input.diffCost').val(), true);
    });
    $('#spanCost1').text(formatDecimal(totalNumber.toString()));
    $('#spanCost2').text(formatDecimal(totalDiffCost.toString()));
    totalNumber = totalNumber - totalDiffCost - replaceCost($('span#customerBalance').text(), true);
    if(totalNumber > 0) $('#spanCost3').text(formatDecimal(totalNumber.toString()));
    else $('#spanCost3').text('0');
}