$(document).ready(function(){
    $('select#storeId').change(function(){
        $('#inventoryForm').trigger('submit');
    });
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    var inventoryIds = [];
    var isCheckAll = false;
    $('body').on('ifToggled', 'input#checkAll', function (e) {
        inventoryIds = [];
        if (e.currentTarget.checked) {
            isCheckAll = true;
            $('input.iCheckItem').iCheck('check');
            $('#tbodyProduct input.iCheckItem').each(function(){
                inventoryIds.push($(this).val());
            });
        }
        else{
            $('input.iCheckItem').iCheck('uncheck');
            isCheckAll = false;
        }
    }).on('ifToggled', 'input.iCheckItem', function (e) {
        if(e.currentTarget.checked){
            if(!isCheckAll) inventoryIds.push(e.currentTarget.value);
        }
        else{
            var index = inventoryIds.indexOf(e.currentTarget.value);
            if(index >= 0) inventoryIds.splice(index, 1);
        }
    });
    $('.btnStatus').click(function(){
        if(inventoryIds.length > 0){
            var statusId = $(this).attr('data-id');
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateStatusInventoryUrl').val(),
                data: {
                    InventoryIds: JSON.stringify(inventoryIds),
                    StatusId: statusId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        for(var i = 0; i < inventoryIds.length; i++) $('#trItem_' + inventoryIds[i]).remove();
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn sản phần', 0);

    });
});