$(document).ready(function(){
    if($('input#autoLoad').val() == '1') {
        province();
        $('#btnUpLogo').click(function () {
            chooseFile('Images', function (fileUrl) {
                $('#imgLogo').attr('src', fileUrl);
                $('input#logoImage').val(fileUrl);
            });
        });
        $('#btnUpAddress').click(function () {
            chooseFile('Images', function (fileUrl) {
                $('#imgAddress').attr('src', fileUrl);
                $('input#addressImage').val(fileUrl);
            });
        });
        $('.submit').click(function(){
            if(validateEmpty('#configForm')) {
                $('.submit').prop('disabled', true);
                var form = $('#configForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        $('.submit').prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        $('.submit').prop('disabled', false);
                    }
                });
            }
            return false;
        });
    }
    else {
        $('.js-switch').bootstrapSwitch({size: 'mini'}).on('switchChange.bootstrapSwitch', function(event, state) {
            $.ajax({
                type: "POST",
                url: $('input#updateConfigUrl').val(),
                data: {
                    ConfigCode: $(this).val(),
                    ConfigValue: state ? 'ON' : 'OFF'
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code != 1) redirect(true, '');
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    redirect(true, '');
                }
            });
        });
    }
});
