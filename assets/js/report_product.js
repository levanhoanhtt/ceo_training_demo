$(document).ready(function() {
    dateRangePicker(function(dates){
        dates = dates.trim().split('-');
        if(dates.length == 2) getReport(dates[0].trim(), dates[1].trim());
        else if(dates.length == 1) getReport(dates[0].trim(), '');
        else getReport('', '');
    });
    var dates = $("#dateRangePicker").val().trim().split('-');
    if(dates.length == 2) getReport(dates[0].trim(), dates[1].trim());
    else if(dates.length == 1) getReport(dates[0].trim(), '');
    else getReport('', '');
    var statusSearch = null;
    $('#itemSearchName').keydown(function () {
        if (statusSearch != null) {
            clearTimeout(statusSearch);
            statusSearch = null;
        }
    }).keyup(function () {
        if (statusSearch == null) {
            statusSearch = setTimeout(function () {
                var searchText = $('#itemSearchName').val().trim();
                if(searchText == '') $('#tbodyProduct tr').show();
                else{
                    $('#tbodyProduct tr').hide();
                    $('#tbodyProduct td.searchText').each(function(){
                        if($(this).text().indexOf(searchText) > -1) $(this).parent().show();
                    });
                }
            }, 500);
        }
    });
});

function getReport(beginDate, endDate){
    $('#tbodyProduct').html('');
    $.ajax({
        type: "POST",
        url: $('input#getReportUrl').val(),
        data: {
            BeginDate: beginDate,
            EndDate: endDate
        },
        success: function (response) {
            var json = $.parseJSON(response);
            var data = json.data;
            var html = '';
            var levelName = '';
            var urlEditProduct = $('#urlEditProduct').val() + '/';
            var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
            for(var i = 0; i < data.length; i++){
                html += '<tr data-id="' + data[i].ProductId + '" data-child="' + data[i].ProductChildId + '">';
                html += '<td class="searchText"><a href="' + ((data[i].ProductKindId == 3) ? urlEditProductCombo + data[i].ProductId : urlEditProduct + data[i].ProductId)  + '">' + data[i].ProductName + '</a></td>';
                html += '<td class="searchText">' + data[i].BarCode + '</td>';
                if(i == 0) levelName = 'Chạy nhất';
                else if(i == 1) levelName = 'Chạy nhì';
                else if(i == 2) levelName = 'Chạy ba';
                else levelName = i + 1;
                html += '<td class="text-center">' + levelName + '</td>';
                html += '<td class="text-center">' + formatDecimal(data[i].SumQuantity.toString()) + '</td>';
                html += '<td class="text-center">' + formatDecimal(data[i].SumQuantityDeliver.toString()) + '</td>';
                html += '<td class="text-center tdQuantity">' + formatDecimal(data[i].TotalQuantity.toString()) + '</td>';
                html += '<td class="text-center">' + formatDecimal(data[i].Quantity.toString()) + '</td>';
                html += '<td class="text-right">' + formatDecimal(data[i].SumPrice.toString()) + '</td>';
            }
            $('#tbodyProduct').html(html);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}