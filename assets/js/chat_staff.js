$(document).ready(function(){
    var noticeAudioUrl = 'assets/vendor/dist/chat';
    var socket = io($('input#chatServerUrl').val());
    var isChatPage = $('#ulChatHeader').length == 0;
    var isChatAdminPage = $('#chatAdminPage').length > 0;
    if (!isChatPage) getListChatHeader();
    else{
        $('#listChat').css('height', ($(window).height() - 250) + 'px');
        socket.emit('user state', {
            userId: $('input#userLoginId').val(),
            roleId: 1,
            state: 1
        });
        $('#cbOnline').change(function(){
            socket.emit('user state', {
                userId: $('input#userLoginId').val(),
                roleId: 1,
                state: this.checked ? 1 : 0
            });
        });
        $('#ulStaffRole').on('click', 'a', function(){
            $('input#staffRoleId').val($(this).attr('data-id'));
        });
        $('.box-comments').on('click', 'div.box-comment', function(){
            $('div.box-comment').removeClass('active');
            $(this).addClass('active');
            $('input#customerId').val($(this).attr('data-id'));
            $('input#customerName').val($(this).attr('data-name'));
            $('input#customerPhone').val($(this).attr('data-phone'));
            $('#h3StaffChat').text($(this).attr('data-name') + ' (' + $(this).attr('data-phone') + ')');
            $('input#startChatPagging').val(0);
            $('input#totalChatMsg').val(0);
            if(isChatAdminPage) getListChatAdmin();
            else getListChat('');
        });
        $('#chatForm').on('keydown', '#chatMsg', function (e) {
            if(e.keyCode == 13){
                var staffId = $('input#userLoginId').val();
                var customerPhone = $('input#customerPhone').val();
                var chatMsg = $(this).val().trim();
                if(staffId == '0') showNotification('Vui lòng chọn nhân viên', 0);
                else if(customerPhone == '') $('#modalCustomerName').modal('show');
                else if(chatMsg == '') showNotification('Nội dung chat không được bỏ trống', 0);
                else{
                    var chatData = {
                        customerId: $('input#customerId').val(),
                        customerName: $('input#customerName').val(),
                        customerPhone: $('input#customerPhone').val(),
                        customerAvatar: 'customer.png',
                        staffId: staffId,
                        staffRoleId: $('input#staffRoleId').val(),
                        staffName: $('input#fullNameLoginId').val(),
                        staffAvatar: 'logo.png',
                        msg: chatMsg,
                        fileUrl: '',
                        isCustomerSend: 0
                    };
                    socket.emit('new message', chatData);
                    $(this).val('');
                }
            }
        }).on('focus', '#chatMsg', function(){
            if($('#listChat .unread').length > 0){
                 var customerPhone = $('input#customerPhone').val();
                $.ajax({
                    type: "POST",
                    url: $('input#updateCountChatUnReadUrl').val(),
                    data: {
                        StaffId: $('input#userLoginId').val(),
                        PhoneNumber: customerPhone,
                        IsStaff: 1
                    },
                    success: function (response) {
                        if (response == 1) {
                            $('#listChat .direct-chat-msg').removeClass('unread');
                            $('.box-comments div.box-comment[data-phone="' + customerPhone + '"] span.text-muted').text('').attr('data-count', '0');
                            //$('div.box-comment').removeClass('unread');
                            getListChatHeader();
                        }
                    },
                    error: function (response){}
                });
            }
        });
        $("#listChat").scroll(function () {
            if ($(this).scrollTop() == 0) {
                var totalChatMsg = parseInt($('input#totalChatMsg').val());
                if (totalChatMsg % 20 == 0) {
                    $('input#startChatPagging').val(20 + parseInt($('input#startChatPagging').val()));
                    if(isChatAdminPage) getListChatAdmin();
                    else getListChat('');
                }
            }
        });
        if(isChatAdminPage){
            $('.selectStaff').change(function(){
                var staffRoleId = $(this).attr('data-id');
                $('input#staffRoleId').val(staffRoleId);
                $('#listCustomerChat' + staffRoleId).html('');
                var staffId = $(this).val();
                $('input#staffId').val(staffId);
                getListCustomerChat(staffId, staffRoleId);
            });
        }
        socket.on('error message', function (data) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        });
    }
    socket.on('new message', function (data) {
        if(data.staffId == $('input#userLoginId').val()){
            if(isChatPage) {
                var html = '';
                var tabEle = data.staffRoleId == 1 ? $('#tabCare .box-comments') : $('#tabTech .box-comments');
                var boxComment = tabEle.find('.box-comment[data-phone="' + data.customerPhone + '"]');
                if ($('input#customerPhone').val() == data.customerPhone) {
                    var userImagePath = $('input#userImagePath').val();
                    var imagePath = $('input#imagePath').val();
                    var divClass = ' unread';
                    if (data.isCustomerSend == 1) divClass = ' right';
                    html = '<div class="direct-chat-msg' + divClass + '">';
                    html += '<div class="direct-chat-info clearfix">';
                    html += '<span class="direct-chat-name pull-left">' + ((data.isCustomerSend == 1) ? data.customerName : data.staffName) + '</span>';
                    html += '<span class="direct-chat-timestamp pull-right">' + data.crDateTime + '</span>';
                    html += '</div><img class="direct-chat-img" src="' + userImagePath + ((data.isCustomerSend == 1) ? data.customerAvatar : data.staffAvatar) + '" alt="">';
                    if (data.msg != '') html += '<div class="direct-chat-text">' + data.msg + '</div></div>';
                    else if (data.fileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data.fileUrl + '" target="_blank"><img src="' + imagePath + data.fileUrl + '" alt=""></a></div></div>';
                    $('#listChat').append(html).slimScroll({
                        height: ($(window).height() - 250) + 'px',
                        alwaysVisible: true,
                        wheelStep: 20,
                        touchScrollStep: 500,
                        start: 'bottom',
                        scrollTo: $('#listChat').prop('scrollHeight') + 'px'
                    });
                }
                //else $.playSound(noticeAudioUrl);
                if (boxComment.length > 0){
                    boxComment.find('span.lastMsgChat').text(data.msg);
                    if(data.staffRoleId != $('input#staffRoleId').val() || $('input#customerPhone').val() == '') $.playSound(noticeAudioUrl);
                }
                else {
                    html = '<div class="box-comment" data-id="' + data.customerId + '" data-name="' + data.customerName + '" data-phone="' + data.customerPhone + '">';
                    html += '<img class="img-circle img-sm" src="' + userImagePath + 'customer.png" alt="' + data.customerName + '">';
                    html += '<div class="comment-text"><span class="username">' + data.customerName + ' (' + data.customerPhone + ')<span class="online"></span>';
                    html += '<span class="text-muted pull-right" data-count="0"></span></span><span class="lastMsgChat">' + data.msg + '</span></div></div>';
                    tabEle.prepend(html);
                    $.playSound(noticeAudioUrl);
                }
                $('#chatMsg').val('');
            }
            else{
                getListChatHeader();
                $.playSound(noticeAudioUrl);
            }
        }
    });
});

function getListChatHeader(){
    $.ajax({
        type: "POST",
        url: $('input#getListChatUnReadUrl').val(),
        data: {},
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1) {
                var html = '';
                var data = json.data;
                var n = data.length;
                $('span.countMsgChat').text(n);
                var userImagePath = $('input#userImagePath').val();
                var chatPageUrl = $('a#aViewAllChat').attr('href');
                for (var i = 0; i < n; i++) {
                    html += '<li><a href="' + chatPageUrl + '"><div class="pull-left">';
                    html += '<img src="' + userImagePath + 'customer.png" class="img-circle" alt="' + data[i].CustomerName + '">';
                    html += '</div><h4>' + data[i].CustomerName + '<br>' + data[i].PhoneNumber + '<small><i class="fa fa-clock-o"></i> ' + data[i].CrTime + '<br>' + data[i].CrDate + '</small></h4>';
                    html += '<p>' + data[i].Message + '</p></a></li>';
                }
                $('#ulChatHeader').html(html);
            }
        },
        error: function (response) {}
    });
}

function getListChat(htmlGreet){
    var customerPhone = $('input#customerPhone').val();
    var staffId = $('input#userLoginId').val();
    if(staffId != '0' && customerPhone != ''){
        var startChatPagging = $('input#startChatPagging').val();
        $.ajax({
            type: "POST",
            url: $('input#getChatUrl').val(),
            data: {
                PhoneNumber: customerPhone,
                StaffId: staffId,
                StaffRoleId: $('input#staffRoleId').val(),
                Limit: 20,
                Start: startChatPagging
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var n = data.length;
                    $('input#totalChatMsg').val(n + parseInt($('input#totalChatMsg').val()));
                    var divClass = '';
                    var userImagePath = $('input#userImagePath').val();
                    var imagePath = $('input#imagePath').val();
                    var staffName = $('input#fullNameLoginId').val();
                    var customerName = $('input#customerName').val();
                    for (var i = 0; i < n; i++) {
                        if(data[i].IsCustomerSend == 1) divClass = ' right';
                        else if(data[i].IsRead == 0) divClass = ' unread';
                        else divClass = '';
                        html += '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + (data[i].IsCustomerSend == 1 ? customerName : staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data[i].CrDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + (data[i].IsCustomerSend == 1 ? 'customer.png' : 'logo.png') + '" alt="">';
                        if(data[i].Message != '') html += '<div class="direct-chat-text">' + data[i].Message + '</div></div>';
                        else if(data[i].FileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data[i].FileUrl + '" target="_blank"><img src="' + imagePath + data[i].FileUrl + '" alt=""></a></div></div>';
                    }
                    if(startChatPagging == '0'){
                        if(htmlGreet != '' && html == '') html += htmlGreet;
                        $('#listChat').html(html).slimScroll({
                            height: ($(window).height() - 250) + 'px',
                            alwaysVisible: true,
                            wheelStep: 20,
                            touchScrollStep: 500,
                            start: 'bottom',
                            scrollTo : $('#listChat').prop('scrollHeight') + 'px'
                        });
                    }
                    else $('#listChat').prepend(html);
                }
            },
            error: function (response) {}
        });
    }
}

function getListChatAdmin(){
    var customerPhone = $('input#customerPhone').val();
    var staffId = $('input#staffId').val();
    if(staffId != '0' && customerPhone != ''){
        var staffRoleId = $('input#staffRoleId').val();
        var startChatPagging = $('input#startChatPagging').val();
        $.ajax({
            type: "POST",
            url: $('input#getChatUrl').val(),
            data: {
                PhoneNumber: customerPhone,
                StaffId: staffId,
                StaffRoleId: staffRoleId,
                Limit: 20,
                Start: startChatPagging
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var n = data.length;
                    $('input#totalChatMsg').val(n + parseInt($('input#totalChatMsg').val()));
                    var divClass = '';
                    var userImagePath = $('input#userImagePath').val();
                    var imagePath = $('input#imagePath').val();
                    var staffName = $('.selectStaff[data-id="' + staffRoleId + '"] option[value="' + staffId + '"]').text();
                    var customerName = $('input#customerName').val();
                    for (var i = 0; i < n; i++) {
                        if(data[i].IsCustomerSend == 1) divClass = ' right';
                        else if(data[i].IsRead == 0) divClass = ' unread';
                        else divClass = '';
                        html += '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + (data[i].IsCustomerSend == 1 ? customerName : staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data[i].CrDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + (data[i].IsCustomerSend == 1 ? 'customer.png' : 'logo.png') + '" alt="">';
                        if(data[i].Message != '') html += '<div class="direct-chat-text">' + data[i].Message + '</div></div>';
                        else if(data[i].FileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data[i].FileUrl + '" target="_blank"><img src="' + imagePath + data[i].FileUrl + '" alt=""></a></div></div>';
                    }
                    if(startChatPagging == '0'){
                        $('#listChat').html(html).slimScroll({
                            height: ($(window).height() - 250) + 'px',
                            alwaysVisible: true,
                            wheelStep: 20,
                            touchScrollStep: 500,
                            start: 'bottom',
                            scrollTo : $('#listChat').prop('scrollHeight') + 'px'
                        });
                    }
                    else $('#listChat').prepend(html);
                }
            },
            error: function (response) {}
        });
    }
}

function getListCustomerChat(staffId, staffRoleId){
    if(staffId != '0'){
        $.ajax({
            type: "POST",
            url: $('input#getListCustomerUrl').val(),
            data: {
                StaffId: staffId,
                StaffRoleId: staffRoleId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var userImagePath = $('input#userImagePath').val();
                    var i;
                    var data = json.data;
                    for (i = 0; i < data.length; i++) {
                        html += '<div class="box-comment" data-id="' + data[i].CustomerId + '" data-name="' + data[i].CustomerName + '" data-phone="' + data[i].PhoneNumber + '">';
                        html += '<img class="img-circle img-sm" src="' + userImagePath + 'logo.png" alt="' + data[i].CustomerName + '">';
                        html += '<div class="comment-text"><span class="username">' + data[i].CustomerName + ' (' + data[i].PhoneNumber + ')<span class="offline"></span>';
                        html += '<span class="text-muted pull-right" data-count="0"></span></span><span class="lastMsgChat">' + data[i].Message + '</span></div></div>';
                    }
                    $('#listCustomerChat' + staffRoleId).html(html);
                }
            },
            error: function (response) {}
        });
    }
}