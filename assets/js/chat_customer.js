$(document).ready(function(){
    /*$('#star1').starrr({

        change: function(e, value){
        if (value) {
            if(value == 1) $("label#rate").text("Kém");
            if(value == 2) $("label#rate").text("Trung bình");
            if(value == 3) $("label#rate").text("Khá");
            if(value == 4) $("label#rate").text("Tốt");
            if(value == 5) $("label#rate").text("Rất tốt");
        } else {
            $("label#rate").text('');
        }
       
      }

    });*/
    $('#listChat').css('height', ($(window).height() - 250) + 'px');
    var socket = io($('input#chatServerUrl').val());
    if($('input#customerPhone').val() == '') $('#modalCustomerName').modal('show');
    else getListStaff($('input#customerPhone').val());
    $('#btnSetCustomerName').click(function(){
        if(validateEmpty('#modalCustomerName')){
            var customerNameInput = $('#customerNameInput').val().trim();
            var customerPhoneInput = $('#customerPhoneInput').val().trim();
            $.ajax({
                type: "POST",
                url: $('input#setCustomerChatUrl').val(),
                data: {
                    FullName: customerNameInput,
                    PhoneNumber: customerPhoneInput
                },
                success: function (response) {
                    $('input#customerId').val(response);
                    $('input#customerName').val(customerNameInput);
                    $('input#customerPhone').val(customerPhoneInput);
                    $('#spanCustomerName').text(customerNameInput + ' (' + customerPhoneInput + ')');
                    $('#modalCustomerName').modal('hide');
                    getListStaff(customerPhoneInput);
                },
                error: function (response) {
                    $('input#customerId').val('0');
                    $('input#customerName').val(customerNameInput);
                    $('input#customerPhone').val(customerPhoneInput);
                    $('#spanCustomerName').text(customerNameInput + ' (' + customerPhoneInput + ')');
                    $('#modalCustomerName').modal('hide');
                    getListStaff(customerPhoneInput);
                }
            });
        }
    });
    setTimeout(function(){
        socket.emit('user online', {
            roleId: 1
        });
    }, 2000);
    socket.on('staff online', function (staffOnlines) {
        $('.box-comments .box-comment span.online').removeClass('online').addClass('offline');
        for(var i = 0; i < staffOnlines.length; i++){
            $('.box-comments .box-comment[data-id="' + staffOnlines[i] + '"] span.offline').removeClass('offline').addClass('online');
        }
    });
    $('#ulStaffRole').on('click', 'a', function(){
        $('input#staffRoleId').val($(this).attr('data-id'));
    });
    $('.box-comments').on('click', 'div.box-comment', function(){
        $('div.box-comment').removeClass('active');
        $(this).addClass('active');
        $('input#staffId').val($(this).attr('data-id'));
        $('input#staffName').val($(this).attr('data-name'));
        var staffRoleId = $('input#staffRoleId').val();
        $('#h3StaffChat').text($(this).attr('data-name') + ' - ' + (staffRoleId == '1' ? 'Bộ phận CSKH' : 'Bộ phận kĩ thuật'));
        var chatData = {
            customerId: $('input#customerId').val(),
            customerName: $('input#customerName').val(),
            customerPhone: $('input#customerPhone').val(),
            customerAvatar: 'customer.png',
            staffId: $(this).attr('data-id'),
            staffRoleId: staffRoleId,
            staffName: $(this).attr('data-name'),
            staffAvatar: $(this).attr('data-avatar')
        };
        socket.emit('chat begin', chatData);
    });
    $('#chatForm').on('keydown', '#chatMsg', function (e) {
        //if(checkKeyCodeNumber(e)) e.preventDefault();
        if(e.keyCode == 13){
            var staffId = $('input#staffId').val();
            var customerPhone = $('input#customerPhone').val();
            var chatMsg = $(this).val().trim();
            if(staffId == '0') showNotification('Vui lòng chọn nhân viên', 0);
            else if(customerPhone == '') $('#modalCustomerName').modal('show');
            else if(chatMsg == '') showNotification('Nội dung chat không được bỏ trống', 0);
            else{
                var chatData = {
                    customerId: $('input#customerId').val(),
                    customerName: $('input#customerName').val(),
                    customerPhone: $('input#customerPhone').val(),
                    customerAvatar: 'customer.png',
                    staffId: staffId,
                    staffRoleId: $('input#staffRoleId').val(),
                    staffName: $('input#staffName').val(),
                    staffAvatar: 'logo.png',
                    msg: chatMsg,
                    fileUrl: '',
                    isCustomerSend: 1
                };
                socket.emit('new message', chatData);
                $(this).val('');
            }
        }
    });
    $('#listChat').scroll(function () {
        if ($(this).scrollTop() == 0) {
            var totalChatMsg = parseInt($('input#totalChatMsg').val());
            if (totalChatMsg % 20 == 0) {
                $('input#startChatPagging').val(20 + parseInt($('input#startChatPagging').val()));
                getListChat('');
            }
        }
    });
    socket.on('chat begin', function (data) {
        if($('input#customerPhone').val() == data.customerPhone) {
            var userImagePath = $('input#userImagePath').val();
            var html = '<div class="direct-chat-msg">';
            html += '<div class="direct-chat-info clearfix">';
            html += '<span class="direct-chat-name pull-left">' + data.staffName + '</span>';
            html += '<span class="direct-chat-timestamp pull-right">' + getCurrentDateTime(6) + '</span>';
            html += '</div><img class="direct-chat-img" src="' + userImagePath + data.staffAvatar + '" alt="">';
            html += '<div class="direct-chat-text">' + data.msg + '</div></div>';
            $('input#startChatPagging').val(0);
            $('input#totalChatMsg').val(0);
            getListChat(html);
        }
    });
    socket.on('new message', function (data) {
        if($('input#customerPhone').val() == data.customerPhone) {
            var userImagePath = $('input#userImagePath').val();
            var imagePath = $('input#imagePath').val();
            var divClass = ' unread';
            if (data.isCustomerSend == 1) divClass = ' right';
            var html = '<div class="direct-chat-msg' + divClass + '">';
            html += '<div class="direct-chat-info clearfix">';
            html += '<span class="direct-chat-name pull-left">' + ((data.isCustomerSend == 1) ? data.customerName : data.staffName) + '</span>';
            html += '<span class="direct-chat-timestamp pull-right">' + data.crDateTime + '</span>';
            html += '</div><img class="direct-chat-img" src="' + userImagePath + ((data.isCustomerSend == 1) ? data.customerAvatar : data.staffAvatar) + '" alt="">';
            if (data.msg != '') html += '<div class="direct-chat-text">' + data.msg + '</div></div>';
            else if (data.fileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data.fileUrl + '" target="_blank"><img src="' + imagePath + data.fileUrl + '" alt=""></a></div></div>';
            $('#listChat').append(html).slimScroll({
                height: ($(window).height() - 250) + 'px',
                alwaysVisible: true,
                wheelStep: 20,
                touchScrollStep: 500,
                start: 'bottom',
                scrollTo : $('#listChat').prop('scrollHeight') + 'px'
            });
            $('#chatMsg').val('');
        }
    });
    socket.on('error message', function (data) {
        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
    });


});

function getListStaff(customerPhone){
    $('.tab-pane .box-comments').html('<img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter">');
    $.ajax({
        type: "POST",
        url: $('input#getListStaffUrl').val(),
        data: {
            CustomerPhone: customerPhone
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1) {
                var html = '';
                var userImagePath = $('input#userImagePath').val();
                var i;
                var data = json.data.ChatCares;
                for (i = 0; i < data.length; i++) {
                    html += '<div class="box-comment" data-id="' + data[i].UserId + '" data-name="' + data[i].FullName + '" data-avatar="logo.png">';
                    html += '<img class="img-circle img-sm" src="' + userImagePath + 'logo.png" alt="' + data[i].FullName + '">';
                    html += '<div class="comment-text"><span class="username">' + data[i].FullName + '<span class="offline"></span>';
                    html += '<span class="text-muted pull-right" data-count="0"></span></span><span class="lastMsgChat">' + data[i].Message + '</span></div></div>';
                }
                $('#tabCare .box-comments').html(html);
                html = '';
                data = json.data.ChatTechs;
                for (i = 0; i < data.length; i++) {
                    html += '<div class="box-comment" data-id="' + data[i].UserId + '" data-name="' + data[i].FullName + '" data-avatar="logo.png">';
                    html += '<img class="img-circle img-sm" src="' + userImagePath + 'logo.png" alt="' + data[i].FullName + '">';
                    html += '<div class="comment-text"><span class="username">' + data[i].FullName + '<span class="offline"></span>';
                    html += '<span class="text-muted pull-right" data-count="0"></span></span><span class="lastMsgChat">' + data[i].Message + '</span></div></div>';
                }
                $('#tabTech .box-comments').html(html);
            }
            else $('.tab-pane .box-comments').html('');
        },
        error: function (response) {
            $('.tab-pane .box-comments').html('');
        }
    });
}

function getListChat(htmlGreet){
    var customerPhone = $('input#customerPhone').val();
    var staffId = $('input#staffId').val();
    if(staffId != '0' && customerPhone != ''){
        var startChatPagging = $('input#startChatPagging').val();
        $.ajax({
            type: "POST",
            url: $('input#getChatUrl').val(),
            data: {
                PhoneNumber: customerPhone,
                StaffId: staffId,
                StaffRoleId: $('input#staffRoleId').val(),
                Limit: 20,
                Start: startChatPagging
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var n = data.length;
                    $('input#totalChatMsg').val(n + parseInt($('input#totalChatMsg').val()));
                    var divClass = '';
                    var userImagePath = $('input#userImagePath').val();
                    var imagePath = $('input#imagePath').val();
                    var staffName = $('input#staffName').val();
                    var customerName = $('input#customerName').val();
                    for (var i = 0; i < n; i++) {
                        if(data[i].IsCustomerSend == 1) divClass = ' right';
                        else if(data[i].IsRead == 0) divClass = ' unread';
                        else divClass = '';
                        html += '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + (data[i].IsCustomerSend == 1 ? customerName : staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data[i].CrDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + (data[i].IsCustomerSend == 1 ? 'customer.png' : 'logo.png') + '" alt="">';
                        if(data[i].Message != '') html += '<div class="direct-chat-text">' + data[i].Message + '</div></div>';
                        else if(data[i].FileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data[i].FileUrl + '" target="_blank"><img src="' + imagePath + data[i].FileUrl + '" alt=""></a></div></div>';
                    }
                    if(startChatPagging == '0'){
                        if(htmlGreet != '' && html == '') html += htmlGreet;
                        $('#listChat').html(html).slimScroll({
                            height: ($(window).height() - 250) + 'px',
                            alwaysVisible: true,
                            wheelStep: 20,
                            touchScrollStep: 500,
                            start: 'bottom',
                            scrollTo : $('#listChat').prop('scrollHeight') + 'px'
                        });
                    }
                    else $('#listChat').prepend(html);
                }
            },
            error: function (response) {}
        });
    }
}