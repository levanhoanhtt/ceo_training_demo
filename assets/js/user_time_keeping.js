var app = app || {};

app.init = function(userTimeKeepingId) {
    app.setTimeCurrent();
    if(userTimeKeepingId == 0) app.checkIn();
    else{
        app.checkOut(userTimeKeepingId);
        app.setRealDuration();
    }
    app.sortTableKeeping();
};

app.setTimeCurrent = function(){
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    if(hour < 10) hour = '0' + hour;
    if(minute < 10) minute = '0' + minute;
    $('#spanTimeCurrent').text(hour + ':' + minute);
    setInterval(function(){
        now = new Date();
        hour = now.getHours();
        minute = now.getMinutes();
        $('#spanTimeCurrent').text(convertTime(hour) + ':' + convertTime(minute));
    }, 1000 * 60);
};

app.checkIn = function(){
    $('#divKeepingTypeIn').on('click', 'span', function(){
        $('#divKeepingTypeIn span').removeClass('active');
        $('#divKeepingTypeIn span i').remove();
        $(this).addClass('active');
        var keepingTypeId = $(this).attr('data-id');
        $('input#keepingTypeId').val(keepingTypeId);
        $(this).prepend('<i class="fa fa-check"></i> ');
        if(keepingTypeId == '1') $('#spanKeepingOut').html('<i class="fa fa-check"></i> OFFLINE');
        else $('#spanKeepingOut').html('<i class="fa fa-check"></i> ONLINE');
    });
    $('#divKeepingIn').click(function(){
        $('input#timeInOutComment').val('');
        $('#modalKeepingInOut').modal('show');
    });
    $('#btnKeepingInOut').click(function(){
        $('#btnKeepingInOut').prop('disabled', true);
        updateTimeKeeping(0);
    });
};

app.checkOut = function(userTimeKeepingId){
    $('#divKeepingOut').click(function(){
        $('input#timeInOutComment').val('');
        $('#modalKeepingInOut').modal('show');
    });
    $('#btnKeepingInOut').click(function(){
        $('#btnKeepingInOut').prop('disabled', true);
        updateTimeKeeping(userTimeKeepingId);
    });
};

app.setRealDuration = function(){
    var dateTimeIn = $('input#dateTimeIn').val();
    var timeStart = new Date(dateTimeIn);
    var now, timeDiff, hour, minute, second;
    setInterval(function () {
        now = new Date();
        timeDiff = (now - timeStart);
        hour = Math.floor(timeDiff / 1000 / 60 / 60);
        timeDiff = timeDiff - hour * 1000 * 60 * 60;
        minute = Math.floor(timeDiff / 1000 / 60);
        timeDiff = timeDiff - minute * 1000 * 60;
        second = Math.floor(timeDiff / 1000);
        var text = '';
        if(hour > 0) text = convertTime(hour) + ':';
        text += convertTime(minute) + ':' + convertTime(second);
        $('#spanRealDuration').text(text);
    }, 1000);
};

app.sortTableKeeping = function(){
    var keepingDateOld = '';
    var keepingDate = '';
    var rowspan = 1;
    $('#tbodyTimeLog tr').each(function(){
        keepingDate = $(this).attr('data-id');
        if(keepingDate != keepingDateOld){
            keepingDateOld = keepingDate;
            rowspan = $('#tbodyTimeLog tr[data-id="' + keepingDate + '"]').length;
            if(rowspan > 1){
                var trFirst = $('#tbodyTimeLog tr[data-id="' + keepingDate + '"]').first();
                trFirst.find('.tdMerge').attr('rowspan', rowspan).css('vertical-align', 'middle');
                $('#tbodyTimeLog tr[data-id="' + keepingDate + '"] .tdMerge[rowspan="1"]').remove();
                trFirst.find('.tdTotalTime').text($('input#duration_' + keepingDate.replace(/\//g, '_')).val());
            }
        }
    });
};

$(document).ready(function(){
    var userTimeKeepingId = parseInt($('input#userTimeKeepingId').val());
    app.init(userTimeKeepingId);
});

function updateTimeKeeping(userTimeKeepingId){
    $.ajax({
        type: "POST",
        url: $('input#updateTimeKeepingUrl').val(),
        data: {
            UserTimeKeepingId: userTimeKeepingId,
            KeepingTypeId: $('input#keepingTypeId').val(),
            DateTimeIn: $('input#dateTimeIn').val(),
            Comment: $('input#timeInOutComment').val().trim()
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1) redirect(true, '');
            else $('#btnKeepingInOut').prop('disabled', false);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            $('#btnKeepingInOut').prop('disabled', false);
        }
    });
}

function convertTime(val) {
    return val < 10 ? "0" + val : val;
}