$(document).ready(function () {
    // Save create menu name
    $('.submit').click(function(){
        if(validateEmpty('#createMenuForm')){
            $.ajax({
                type: "POST",
                url: $('#createMenuForm').attr('action'),
                data: {
                    MenuName: $('input#menuName').val(),
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1 && $('input#menuId').val() == '0') redirect(false, $('a#menuListUrl').attr('href'));
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    // Save menu item
    $('.submit-menu').click(function(){
        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) { console.log(list.nestable('serialize'));
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };
        updateOutput($('#nestable').data('output', $('#nestable-output')));
        actionUpdateSortId('save');
        
    });
    // Save and edit menu item
    $('.submit-menu-edit').click(function(){
        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) { console.log(list.nestable('serialize'));
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };
        updateOutput($('#nestable').data('output', $('#nestable-output')));
        actionUpdateSortId('save-edit');
        
    });
});

function actionUpdateSortId(typeSubmit) {
    var value = $('#nestable-output').val();
    $.ajax({
        url: '/dragmenu/createmenuitem',
        data: { value: value, menuId: $('#menuId').val(), menuName:$('#menuName').val() },
        method: 'post',
        dataType: 'json',
        success: function(response) {
            showNotification(response.message, response.code);
            if(response.code == 1 && typeSubmit == "save") redirect(false, $('a#menuListUrl').attr('href'));
            if(response.code == 1 && typeSubmit == "save-edit") {
                setTimeout(function(){
                    location.reload();
                },5000);
            }
        },
        error: function(xhr, err) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}