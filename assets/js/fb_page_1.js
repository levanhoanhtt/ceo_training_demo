var dataJson = {
  "data": [
    {
      "access_token": "EAACEdEose0cBAOhkclZCIiuhE7xxHmW3G4pbKWRLK3HDoTspwhhWg0zg5ZAOttRV4kHO9WjEHdk840Y4qzqqANpXgsva3MM5ZCCYGA2WMCl3ZA2tZAwTLzGGtZCfAdTG0o87PqiFqSxiGim3xPTga4GIuuPZAUWUfvNaT0Ih484xk1g7hZBjsUJHqxMMC3eM7ZCcq44JQ3AF2CgZDZD",
      "category": "College & University",
      "category_list": [
        {
          "id": "2602",
          "name": "College & University"
        }
      ],
      "name": "TEST PAGE CHƠI",
      "id": "151709995578508",
      "perms": [
        "ADMINISTER",
        "EDIT_PROFILE",
        "CREATE_CONTENT",
        "MODERATE_CONTENT",
        "CREATE_ADS",
        "BASIC_ADMIN"
      ]
    },
    {
      "access_token": "EAACEdEose0cBANaTm9QMxFZBXvEixazpUoIL1DK1SMYhGEP6MYV2fXj26cyP8ZBJUUA2ClI3CoFdYEg8WKH8Fs5yvCvVYvpCq9gatNI36QKXIyj2y9TzzwRUPJKSJ62ZBSLHM2m8pkjrG2dME2wMFqHTPq8fkswIzqfXTcikC3H6AkM1hbdzaTJXYC8mOdMJKmMIEHQdwZDZD",
      "category": "Community",
      "name": "Con mòe",
      "id": "1759192184381061",
      "perms": [
        "ADMINISTER",
        "EDIT_PROFILE",
        "CREATE_CONTENT",
        "MODERATE_CONTENT",
        "CREATE_ADS",
        "BASIC_ADMIN"
      ]
    },
    {
      "access_token": "EAACEdEose0cBACu6pDMsCUGluZBqdYc2F2AnBaZCJqiC3d55uQKIQljsxFJx9TxAG2ZBfkb5pdLgU6h3EvjTtlylXzIkWxXKbd5mB336onyPJne3lFUnPXHipbsSNC6a3RBJmZAi81aeERPZCRKE8WG4ZApT2OeE0EZAgvhZAdRvXvZAQoVhGCbDnZCH7lBXZBpwFlZAIs91FImzRwZDZD",
      "category": "Community",
      "name": "Ngày Em Đến",
      "id": "1380180122007846",
      "perms": [
        "ADMINISTER",
        "EDIT_PROFILE",
        "CREATE_CONTENT",
        "MODERATE_CONTENT",
        "CREATE_ADS",
        "BASIC_ADMIN"
      ]
    },
    {
      "access_token": "EAACEdEose0cBAPLqGzPat9JT8isF3myNHpcCxGmZAZCVMHelSVGK5ZAVmUslP7UCHZCIIZB5U5lB7HiCwiywgXGJoZCGhPVTj7wPoxnfbjmTeP5Ks80aQ8ZCZASUaugY3xpXtHiwWJ4PBPgjPPAKmSxVrPZBDJZAYNiRzcJSN4ijQ3ARR3i3fJznJJlhLW80ZBIxWylc90RZBRSI4AZDZD",
      "category": "Jewelry/Watches",
      "name": "Lotus Shop",
      "id": "766650570111730",
      "perms": [
        "ADMINISTER",
        "EDIT_PROFILE",
        "CREATE_CONTENT",
        "MODERATE_CONTENT",
        "CREATE_ADS",
        "BASIC_ADMIN"
      ]
    },
    {
      "access_token": "EAACEdEose0cBAIBjWGhPgmREuLnSdtMP0QRbxigZAqz6SgxqzSBcAfs9SZCZA94Rbc33RjtfKlyz5ZCqd1XwHOgdJyNIQYSC6ZBYDKZBptaoaC55LOt1reU7cvIRtZBLcGsOiRsVFZCIh5RXDbjs9qv3ZCYBogK1APgHJ4V78WJxpLBbUyeZAsY2ZAiBZBsZCQJQEZBwPnk2KHtRK7SwZDZD",
      "category": "Camera/Photo",
      "name": "Camera Thanh Hóa",
      "id": "525129794323052",
      "perms": [
        "ADMINISTER",
        "EDIT_PROFILE",
        "CREATE_CONTENT",
        "MODERATE_CONTENT",
        "CREATE_ADS",
        "BASIC_ADMIN"
      ]
    },
    {
      "access_token": "EAACEdEose0cBAEtoRTZBs96jvkhcfENZBhLhPfeUwEJnvBMZCGV2fyj7qFJyKRV3jlwzj5fhzdVm3cdwu6lFRAIVjHixjSk5Kcr0BqqT2IR7DQZCa1YOZBqRPig7EcL6Xo9wTKYYOgN5favNRVLqrZBsHEBwyPgyideNyJHlX53lRok9qrFqYK5jir9pJ0CJDYnxyPZBqNkZBwZDZD",
      "category": "Internet Company",
      "category_list": [
        {
          "id": "2256",
          "name": "Internet Company"
        }
      ],
      "name": "Hoàn Mưa Đá",
      "id": "356217654569421",
      "perms": [
        "ADMINISTER",
        "EDIT_PROFILE",
        "CREATE_CONTENT",
        "MODERATE_CONTENT",
        "CREATE_ADS",
        "BASIC_ADMIN"
      ]
    }
  ],
  "paging": {
    "cursors": {
      "before": "MTUxNzA5OTk1NTc4NTA4",
      "after": "MzU2MjE3NjU0NTY5NDIx"
    }
  }
};

$(window).ready(function(){
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
});

window.fbAsyncInit = function() {
    FB.init({
        appId      : '157454151622226',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.12'
    });
    FB.AppEvents.logPageView();
};

/*FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
});*/

function checkLoginState() {
    /*FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });*/
    FB.login(function(response) {
        statusChangeCallback(response);
    }, {scope: 'manage_pages'});
}
function statusChangeCallback(response){
    //console.log(response);
    if (response.status === 'connected'){
        //console.log(response.authResponse.accessToken);
        //console.log(response.authResponse.userID);
        FB.api('/me/accounts', function(response1) {
           response1 = dataJson;
            if(response1.data.length > 0){
                 // console.log(response1);
                $.ajax({
                    type: "POST",
                    url: $("input#urlSaveDataPages").val().trim(),
                    data:  {data: JSON.stringify(response1)},
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                           loadFbPape()
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
                return false;
            }else showNotification('Không có dữ liệu', 0);
        });
    }
}

function loadFbPape(){
    $.ajax({
        type: "POST",
        url: $("input#urlLoadDataPages").val().trim(),
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1){
                loadFbPageNotActive(json.fbPageNotActive);
                loadFbPageActive(json.fbPageActive);
            }else showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function loadFbPageNotActive(datas){
    var html = "";
    $.each( datas, function( key, value ) {
        html += `
            <div class="box-header with-border">
                <div class="user-block">
                    <img class="img-circle" src="https://graph.facebook.com/${value.FbPageCode}/picture?type=square" alt="User Image">
                    <span class="username"><a href="#">${value.FbPageName}</a></span>
                    <span class="description">${(value.CrDateTime)}</span>
                </div>
                <div class="box-tools">
                    <button type="button" class="btn btn-primary btn-sm btn-active-page" data-prefix ="${value.Prefix}" data-id="${value.FbPageId}">Kích hoạt</button>
                </div>
            </div>
        `;
    });

    $(".bodyPageNotActive").html(html);
}
function loadFbPageActive(datas){
    var html = "";
    $.each( datas, function( key, value ) {
        html += `
            <div class="box-header with-border">
                <div class="user-block">
                    <img class="img-circle" src="https://graph.facebook.com/${value.FbPageCode}/picture?type=square" alt="User Image">
                    <span class="username"><a href="#">${value.FbPageName}</a></span>
                    <span class="description">${(value.CrDateTime)}</span>
                </div>
                <div class="box-tools">
                    <button type="button" class="btn btn-danger btn-sm btn-not-active-page" data-prefix ="${value.Prefix}" data-id="${value.FbPageId}">Hủy kích hoạt</button>
                </div>
            </div>
        `;
    });
    $(".bodyPageActive").html(html);
}
$(document).ready(function(){
    $('#btnGetPage').click(function(){
        checkLoginState();
    });
    //load page fb
    loadFbPape();

    var body = $("body");
    body.on('click','.btn-active-page', function(){
        changeStatus($(this), '2');
    })

    body.on('click','.btn-not-active-page', function(){
        changeStatus($(this), '1');
    })
});

function changeStatus($this, statusId){
    var fbPageId = $this.attr('data-id').trim();
    var prefix = $this.attr('data-prefix').trim();
    if(fbPageId != "" && prefix != ""){
        $.ajax({
            type: "POST",
            url: $("input#urlChangeStatusPage").val().trim(),
            data:  {
                FbPageId: fbPageId,
                StatusId:statusId,
                Prefix:prefix,
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    loadFbPape();
                }
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }else showNotification("Có lỗi xảy ra, vui lòng thử lại", 0);

    return false;
    
}

