$(document).ready(function(){
    $("#tbodyProductFormalStatus").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#productFormalStatusId').val(id);
        $('input#productFormalStatusName').val($('td#productFormalStatusName_' + id).text());
        scrollTo('input#productFormalStatusName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteProductFormalStatusUrl').val(),
                data: {
                    ProductFormalStatusId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#productFormalStatus_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#productFormalStatusForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#productFormalStatusForm')) {
            var form = $('#productFormalStatusForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="productFormalStatus_' + data.ProductFormalStatusId + '">';
                            html += '<td id="productFormalStatusName_' + data.ProductFormalStatusId + '">' + data.ProductFormalStatusName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ProductFormalStatusId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ProductFormalStatusId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyProductFormalStatus').prepend(html);
                        }
                        else{
                            $('td#productFormalStatusName_' + data.ProductFormalStatusId).text(data.ProductFormalStatusName);
                            $('td#itemTypeName_' + data.ProductFormalStatusId).html(data.ItemTypeName);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});