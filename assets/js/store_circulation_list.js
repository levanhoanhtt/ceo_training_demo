$(document).ready(function () {
    actionItemAndSearch({
        ItemName: 'Lưu chuyển kho',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderStoreCirculations(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var editStorecirculationURL = $('#editStorecirculationURL').val() + '/';
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].StoreCirculationId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].StoreCirculationId + '"></td>';
            html += '<td><a href="' +  editStorecirculationURL + data[item].StoreCirculationId  + '">' + data[item].StoreCirculationCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td>'+ data[item].StoreSource +'</td>';
            html += '<td>'+ data[item].StoreDestination +'</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].StoreCirculationStatusId] + '">' + data[item].StoreCirculationStatus + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="6" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}