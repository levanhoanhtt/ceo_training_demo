$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Sản phẩm',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
    $("#tbodyProduct").on('click', '.showDetail', function(){
        var parent = $(this).parent().parent().find('td');
        $('td#productName').text(parent.eq(1).text());
        $('input#IMEI').val(parent.eq(3).text());
        $('select#productFormalStatusId').val(parent.eq(8).find('input.productFormalStatusId').val()).trigger('change');
        $('select#productUsageStatusId').val(parent.eq(8).find('input.productUsageStatusId').val()).trigger('change');
        $('select#productAccessoryStatusId').val(parent.eq(8).find('input.productAccessoryStatusId').val()).trigger('change');
        $('input#comment').val(parent.eq(7).text());
        $('input#productId').val(parent.eq(0).attr('product-id'));
        $('input#parentProductId').val(parent.eq(0).attr('parent-product-id'));
        $('input#productChildId').val(parent.eq(0).attr('product-child-id'));
        $('input#parentProductChildId').val(parent.eq(0).attr('parent-product-child-id'));
        $("#modalProductChild").modal('show')
    });

    $('a#link_update').click(function(){
        if(validateEmpty('#tbodyModalProductOld')) {
            if($('input#productChildId').val() == '0'){
                $.ajax({
                    type: "POST",
                    url: $('input#updateOldProductUrl').val(),
                    data: {
                        ProductId: $('input#productId').val(),
                        ParentProductId: $('input#parentProductId').val(),
                        IMEI: $('input#IMEI').val().trim(),
                        ProductFormalStatusId: $('select#productFormalStatusId').val().trim(),
                        ProductUsageStatusId: $('select#productUsageStatusId').val().trim(),
                        ProductAccessoryStatusId: $('select#productAccessoryStatusId').val().trim(),
                        ProductShortDesc: $('input#comment').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1){
                            $("#modalProductChild").modal('hide');
                            redirect(true, '');
                        }
                        
                    }
                });
            }
            else{
                $.ajax({
                    type: "POST",
                    url: $('input#updateOldProductChildUrl').val(),
                    data: {
                        ProductChildId: $('input#productChildId').val(),
                        ProductId: $('input#parentProductId').val(),
                        ParentProductChildId: $('input#parentProductChildId').val(),
                        IMEI: $('input#IMEI').val().trim(),
                        ProductFormalStatusId: $('select#productFormalStatusId').val().trim(),
                        ProductUsageStatusId: $('select#productUsageStatusId').val().trim(),
                        ProductAccessoryStatusId: $('select#productAccessoryStatusId').val().trim(),
                        Comment: $('input#comment').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1){
                            $("#modalProductChild").modal('hide');
                            redirect(true, '');
                        }
                    }
                })
            }
        }
    });

    $('a#link_delete').click(function(){
        if($('input#productChildId').val() == '0') {
            $.ajax({
                type: "POST",
                url: $('input#changeProductStatusUrl').val(),
                data: {
                    ItemIds: JSON.stringify([$('input#productId').val()]),
                    StatusId: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $("#modalProductChild").modal('hide');
                        redirect(true, '');
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else{
            $.ajax({
                type: "POST",
                url: $('input#deleteProductChildUrl').val(),
                data: {
                    ProductChildId: $('input#productChildId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $("#modalProductChild").modal('hide');
                        redirect(true, '');
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});


function renderContentProducts(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditProduct = $('#urlEditProduct').val() + '/';
        var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
        //' + urlEditProduct + data[item].ParentProductId  + '/' + data[item].ParentProductChildId + '
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            trClass = '';
            if(data[item].ProductStatusId != 2) trClass = ' trItemCancel';
            html += '<tr id="trItem_'+data[item].ProductId+'" class="trItem' + trClass + '" >';
            html += '<td product-id="'+data[item].ProductId+'" product-child-id="'+data[item].ProductChildId+'" parent-product-id="'+data[item].ParentProductId+'" parent-product-child-id="'+data[item].ParentProductChildId+'"><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].ProductId + '_' + data[item].ProductChildId + '"></td>';
            html += '<td><a href="javascript:void(0)" class="showDetail">' + data[item].ProductName + (data[item].ProductStatusId == 1 ? ' <i class="fa fa-pause-circle"></i>' : '') + '</a></td>';
            html += '<td>'+ data[item].ProductTypeName +'</td>';
            html += '<td>'+ data[item].IMEI +'</td>';
            html += '<td>'+ ((data[item].ProductFormalStatusName != null) ? data[item].ProductFormalStatusName : '') + '</td>';
            html += '<td>'+ ((data[item].ProductUsageStatusName != null) ? data[item].ProductUsageStatusName : '') + '</td>';
            html += '<td>'+ ((data[item].ProductAccessoryStatusName != null) ? data[item].ProductAccessoryStatusName : '') + '</td>';
            html += '<td>' + data[item].Comment + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductStatusId] + '">' + data[item].ProductStatus + '</span>';
            html += '<input type="text" hidden="hidden" class="productFormalStatusId" value="' + data[item].ProductFormalStatusId + '">';
            html += '<input type="text" hidden="hidden" class="productUsageStatusId" value="' + data[item].ProductUsageStatusId + '">';
            html += '<input type="text" hidden="hidden" class="productAccessoryStatusId" value="' + data[item].ProductAccessoryStatusId + '">';
            html += '</td></tr>';
        }
        html += '<tr><td colspan="9" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}