var app = app || {};
app.init = function () {
    app.updateTracking();
    app.comment();
    app.hideAndShow();
    app.updateShipCost();
    app.updateTransportStatus();
};
$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Vận chuyển',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){
            if(actionCode == 'print_order'){
                $('input#transportIds').val(JSON.stringify(itemIds));
                $('#printForm').attr('action', $('input#printOrderMultiple').val()).submit();
            }
            else if(actionCode == 'print_transport'){
                $('input#transportIds').val(JSON.stringify(itemIds));
                $('#printForm').attr('action', $('input#printTransportMultiple').val()).submit();
            }
            else if(actionCode == 'export_transport'){
                $('input#transportIds').val(JSON.stringify(itemIds)); 
                $('#printForm').attr('action', $('input#exportTransport').val()).submit();

            }
            else if(actionCode == 'change_status'){
                $('input#transportIds').val(JSON.stringify(itemIds)); 
                $('#modalTransportStatus').modal('show');
            }
        }
    });
    $('#btnUpdateTransportStatus').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        var transportIds = $('input#transportIds').val();
        $.ajax({
            type: "POST",
            url: $('input#changeStatusBatchUrl').val(),
            data: {
                TransportIds: transportIds,
                TransportStatusId: $('select#transportStatusId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1){
                    transportIds = $.parseJSON(transportIds);
                    for(var i = 0; i < transportIds.length; i++) $('#tdStatus_' + transportIds[i]).html(json.data.StatusName);
                    $('#modalTransportStatus').modal('hide');
                }
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
    });

    app.init();
});

function renderContentTransports(data){
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var sumCODCost = 0;
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var urlEditOrder = $('#urlEditOrder').val() + '/';
        var urlEditTransport = $('#urlEditTransport').val() + '/';
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            sumCODCost += parseInt(data[item].CODCost);
            trClass = '';
            if(data[item].TransportStatusId == '5') trClass = ' trItemCancel';
            html += '<tr id="trItem_' + data[item].TransportId + '" class="trItem' + trClass + '">';
            html += '<td><a href="javascript:void(0);" transport-id="' + data[item].TransportId + '" class="treetable_open fa fa-angle-right  parent_all" style="margin-right: 9px;"></a><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].TransportId + '"></td>';
            html += '<td><a href="' + urlEditTransport + data[item].TransportId + '">' + data[item].TransportCode + '</a></td>';
            html += '<td><a href="' + urlEditOrder + data[item].OrderId + '">' + data[item].OrderCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
            html += '<td>' + (data[item].TransporterName != null ? data[item].TransporterName : '') + '</td>';
            html += '<td class="text-center" id="tdStatus_'+data[item].TransportId+'"><span class="' + labelCss.TransportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
            html += '<td class="text-center"><span class="' + labelCss.CODStatusCss[data[item].CODStatusId] + '">' + data[item].CODStatus + '</span></td>';
            html += '<td class="text-right">' + formatDecimal(data[item].CODCost) + '</td>';
            html += '<td>' + data[item].StoreName + '</td>';
            html += '</tr>';
            html += '<tr class="chose-all" style="display:none;" id="child_' + data[item].TransportId + '"><td colspan="10" id="content_data_detail_' + data[item].TransportId + '"></td></tr>';
        }

        if(html != '') html += '<tr><td colspan="8"></td><td class="text-right">' + formatDecimal(sumCODCost.toString()) + '</td><td></td></tr>';
        html += '<tr><td colspan="10" class="paginate_table"></td></tr>';

        $('#tbodyTransport').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}

app.comment = function () {
    $('#tbodyTransport').on('click', '.aShowComment', function () {
        var userImagePath = $('input#userImagePath').val();
        var json = $(this).parent().find('span.jsonComment').text();
        var html = "";
        $.each($.parseJSON(json), function (k, v) {
            html += '<div class="box-customer mb10"><table><tbody><tr>'+
                '<th rowspan="2" valign="top" style="width: 50px;"><img src="'+userImagePath+v.Avatar+'" alt=""></th>'+
                '<th><a href="javascript:void(0)" class="name">'+v.FullName+'</a></th>'+
                '<th class="time">'+v.CrDateTime+'</th></tr><tr>'+
                '<td colspan="2"><p class="pComment">'+v.Comment+'</p></td></tr></tbody></table></div>';
        });
        $(".listCommentAll").html(html);
        $("#modalItemComment").modal('show');
    }).on('click', '.btnInsertComment', function () {
        var transportId = $(this).attr('transport-id');
        var comment = $('input#comment_' + transportId).val().trim();
        if (comment != '') {
            if (transportId > 0) {
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertTransportCommentUrl').val(),
                    data: {
                        TransportId: transportId,
                        Comment: comment
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            $('div#listComment_' + transportId).prepend(genItemComment(comment));
                            $('input#comment_' + transportId).val('');
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else {
                $('div#listComment_' + transportId).prepend(genItemComment(comment));
                $('input#comment_' + transportId).val('');
            }
        }
        else {
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment_' + transportId).focus();
        }
    });

};


app.hideAndShow = function () {
    $('#tbodyTransport').on('click', 'a.treetable_open', function () {
        var id = $(this).attr('transport-id');
        $('.chose-all').hide();
        $(this).removeClass('treetable_open fa fa-angle-right').addClass('treetable_close fa fa-angle-down');
        $('.fa-angle-down').css("margin-right", 5);
        getListTransposts(id);
        return false;
    }).on('click', 'a.treetable_close', function () {
        var id = $(this).attr('transport-id');
        $('#child_' + id).hide();
        $(this).removeClass('treetable_close fa fa-angle-down').addClass('treetable_open fa fa-angle-right');
        $('.fa-angle-right').css("margin-right", 9);
        return false;
    })
};

app.updateTracking = function(){
    $("body").on('click','.aTracking', function(){
        $("input#transportId").val($(this).attr('transport-id'));
        var transportStatusId = parseInt($(this).attr('transport-status-id'));
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) {
            var tracking = $(this).text();
            if (tracking == 'Cập nhật') tracking = '';
            $('input#tracking').val(tracking);
            $('#modalTracking').modal('show');
        }
        return false;
    });
    $('#btnUpdateTracking').click(function(){
        var transportId = parseInt($("input#transportId").val());
        var tracking = $('input#tracking').val().trim();
        if(tracking != '' && transportId > 0){
            var tracking1 = $('.aTracking').text();
            if(tracking1 == 'Cập nhật') tracking1 = '';
            if(tracking != tracking1) {
                var btn = $(this);
                btn.prop('disabled', true);
                updateTransportField(transportId, 'Tracking', tracking, function(data){
                    $('.aTracking').text(data.Tracking);
                    $('#modalTracking').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn mã vận đơn khác', 0);
        }
        else{
            showNotification('Mã vận đơn không được bỏ trống', 0);
            $('input#tracking').focus();
        }
    });
};
app.updateShipCost = function(){
    $("body").on('click','.aShipCost', function(){
        $("input#transportId").val($(this).attr('transport-id'));
        var transportStatusId = parseInt($(this).attr('transport-status-id'));
        if(transportStatusId == 1 || transportStatusId == 2 || transportStatusId == 3 || transportStatusId == 4) {
            var shipCost = $(this).find('span').text();
            if (shipCost == 'Cập nhật') shipCost = '0';
            $('input#shipCost').val(shipCost);
            $('#modalShipCost').modal('show');
        }
        return false;
    });
    $('#btnUpdateShipCost').click(function(){
        var shipCost = replaceCost($('input#shipCost').val(), true);
        var transportId = parseInt($("input#transportId").val());
        if(shipCost > 0 && transportId > 0){
            var shipCost1 = $('#aShipCost').find('span').text();
            if(shipCost1 == 'Cập nhật') shipCost1 = '0';
            if(shipCost != replaceCost(shipCost1, true)){
                var btn = $(this);
                btn.prop('disabled', true);
                updateTransportField(transportId, 'ShipCost', shipCost, function (data) {
                    $('.aShipCost span').text(formatDecimal(shipCost.toString()));
                    $('#modalShipCost').modal('hide');
                    btn.prop('disabled', false);
                }, function(){
                    btn.prop('disabled', false);
                });
            }
            else showNotification('Vui lòng chọn phí ship khác', 0);
        }
        else{
            showNotification('Phí ship thực tế phải lớn hơn 0', 0);
            $('input#shipCost').focus();
        }
    });
};
app.updateTransportStatus = function(){
    $('select#transportStatusId').change(function(){
        if($(this).val() == '5' || $(this).val() == '6' || $(this).val() == '7') $('#divCancelReason').fadeIn();
        else{
            $('#divCancelReason').fadeOut();
            $('select#cancelReasonId').val('0');
            $('input#cancelComment').val('');
        }
    });
    $("body").on('click','.aTransportStatus', function(){
        $("input#transportStatusId").val($(this).attr('transport-status-id'));
        $("input#transportId").val($(this).attr('transport-id'));
        $("span#jsonTransport").text($(this).parent().find('span.jsonTransport').text());
        var transportStatusId = parseInt($(this).attr('transport-status-id'));
        if(transportStatusId != 4 && transportStatusId != 5 && transportStatusId != 7) $('#modalTransportStatus').modal('show');
        return false;
    });
    $('#btnUpdateTransportStatus').click(function(){
        var transportStatusId = $('select#transportStatusId').val();
        var jsonTransport = $.parseJSON($("span#jsonTransport").text());
        if(transportStatusId != $("input#transportStatusId").val()) {
            var cancelComment = $('input#cancelComment').val().trim();
            if(transportStatusId == '5' || transportStatusId == '6' || transportStatusId == '7'){
                if(cancelComment.length < 30){
                    showNotification('Ghi chú hủy giao hàng quá ngắn - Hãy nhập có trách nhiệm !', 0);
                    return false;
                }
            }
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateFieldUrl').val(),
                data: {
                    TransportId: $("input#transportId").val(),
                    FieldName: 'TransportStatusId',
                    FieldValue: transportStatusId,
                    OrderId: jsonTransport.OrderId,
                    CancelReasonId: jsonTransport.CancelReasonId,
                    CancelComment: cancelComment,
                    StoreId: jsonTransport.StoreId,
                    CustomerId: jsonTransport.CustomerId,
                    CODCost: jsonTransport.CODCost
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(true, '');
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn trạng thái khác', 0);
    });
};
function getListTransposts(transportId) {
    if($('#spanTranspost_' + transportId).length > 0){
        var data =  $.parseJSON($('#spanTranspost_' + transportId).text());
        genPanelTransport(transportId, data);
    }
    else {
        $.ajax({
            type: "POST",
            url: $('input#getDetailUrl').val(),
            data: {
                TransportId: transportId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code = 1) {
                    var data = json.data;
                    $('#divTranspostJson').append('<span id="spanTranspost_' + transportId + '">' + JSON.stringify(data) + '</span>');
                    genPanelTransport(transportId, data);
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
}

function genPanelTransport(transportId, data){
    var productImagePath = $('input#productImagePath').val();
    var product = "";
    $.each(data.Products, function (key, value) {
        product += '<div class="medium-box-n boxst-1 primary-gradient" style="margin-bottom: 5px; font-size:13px">'+
                    '<img class="pull-left" width="40px" src="' + productImagePath + value.ProductImage + '" style="margin-right: 5px;">'+
                    '<div class="right-s">'+
                    '<p style="margin: 0 0 0px"><a href="javascript:void(0)">'+value.ProductName+'</a></p>'+
                    '<p style="margin: 0 0 0px">'+value.Quantity+' x '+formatDecimal(value.Price.toString())+' đ</p>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '</div>';
    });

    var status="", shipCost = "Cập nhật", tracking = "Cập nhật", customerName = "", phoneNumber = "", email = "", address = "", countryName = "Việt Nam", comment = "", detailComment = "";
    var customerAddress = data.customerAddress;
    if (customerAddress.length > 0) {
        customerName = customerAddress.CustomerName;
        phoneNumber = customerAddress.PhoneNumber;
        email = customerAddress.Email;
    }
    if (customerAddress.ZipCode == "" || customerAddress.ZipCode == '0') {
        address = '<i class="fa fa-map-marker" aria-hidden="true"></i>'+
                '<span class="i-ward">'+customerAddress.Address+' '+customerAddress.WardName+'</span>'+
                '<span class="br-line i-district">'+customerAddress.DistrictName+'</span>'+
                '<span class="br-line i-province">'+customerAddress.ProvinceName+'</span>';
    }
    else {
        address = '<i class="fa fa-list-alt" aria-hidden="true"></i><span class="i-province">ZipCode: '+customerAddress.ZipCode+'</span>';
    }
    if (customerAddress.CountryName != "") countryName = customerAddress.CountryName;

    if(data.transport.TransportStatusId != 4 && data.transport.TransportStatusId != 5 && data.transport.TransportStatusId != 7){
        status = '<p><a href="javascript:void(0)" transport-id="'+transportId+'" transport-status-id="'+data.transport.TransportStatusId+'" class="btn-updaten aTransportStatus" style="color:#fff"><i class="fa fa-truck fa-flip-horizontal"></i> Cập nhật vận chuyển</a><span hidden="hidden" class="jsonTransport">'+JSON.stringify(data.transport)+'</span></p>';
    }else{ 
        status = '<p><a href="javascript:void(0)" transport-id="'+transportId+'" transport-status-id="'+data.transport.TransportStatusId+'" class="btn-updaten aTransportStatus" style="color:#bbb"><i class="fa fa-truck fa-flip-horizontal"></i> Cập nhật vận chuyển</a><span hidden="hidden" class="jsonTransport">'+JSON.stringify(data.transport)+'</span></p>';
    }

    var i = 0;
    var userImagePath = $('input#userImagePath').val();
    $.each(data.listTransportComments, function (k, v) {
        i++;
        if (i < 3) {
            comment = '<div class="box-customer mb10">'+
                '<table><tbody><tr>'+
                '<th rowspan="2" valign="top" style="width: 50px;"><img src="'+userImagePath+''+ v.Avatar+'" alt=""></th>'+
                '<th><a href="javascript:void(0)" class="name">'+v.FullName+'</a></th><th class="time">'+v.CrDateTime+'</th></tr>'+
                '<tr><td colspan="2"><p class="pComment">'+v.Comment+'</p></td></tr></tbody></table></div>';
        }
    });
    if (data.listTransportComments.length > 2) detailComment = `<div class="text-right light-dark"><span hidden="hidden" class="jsonComment">${JSON.stringify(data.listTransportComments)}</span><a href="javascript:void(0)" class="aShowComment" data-id="1">Xem tất cả &gt;&gt;</a></div>`;

    if(data.transport.Tracking != "") tracking = data.transport.Tracking;
    if(parseInt(data.transport.ShipCost) > 0) shipCost = '<span>'+formatDecimal(data.transport.ShipCost.toString())+ '</span> đ';  
    var html = ''+
        '<div class="row">'+
        '<div class="col-sm-3" style="padding-left: 33px;">'+product+'</div>'+
        '<div class="col-sm-3"><label class="light-blue">Ghi chú</label><div class="box-transprt clearfix mb10">'+
        '<button type="button" class="btn-updaten save btnInsertComment" transport-id="'+transportId+'">Lưu</button>'+
        '<input type="text" class="add-text" id="comment_'+transportId+'" value="">'+
        '</div>'+
        '<div class="listComment" id="listComment_'+transportId+'">'+comment+'</div>'+detailComment+
        '</div>'+
        '<div class="col-sm-3">'+
        '<div class=""><div><h4 class="mgbt-20 light-blue">Thông tin giao hàng</h4></div>'+
        '<div class="row" style="margin-top: -10px;">'+
        '<div class="col-sm-12 mh-wrap-customer"><div class="item"><i class="fa fa-user" aria-hidden="true"></i>'+
        '<span class="i-name">'+customerAddress.CustomerName+'</span></div><div class="item">'+
        '<i class="fa fa-phone" aria-hidden="true"></i><span class="i-phone">'+customerAddress.PhoneNumber+'</span></div>'+
        '<div class="item"><i class="fa fa-envelope" aria-hidden="true"></i><span class="i-email">'+customerAddress.Email+'</span>'+
        '</div><div class="item i-address">'+address+'</div><div class="item">'+
        '<i class="fa fa-id-card" aria-hidden="true"></i>'+
        '<span class="i-country" >'+countryName+'</span></div></div></div></div></div><div class="col-sm-3 " style="text-align:  right;">'+
        '<div class="row"><div class="col-sm-12 form-group">'+status+'</div><div class="col-sm-12 form-group">'+
        '<div class="row"><div class="col-sm-7"><span>Mã vận đơn</span></div>'+
        '<div class="col-sm-5"><a href="javascript:void(0)" transport-id="'+transportId+'" transport-status-id="'+data.transport.TransportStatusId+'" class="light-dark aTracking">'+tracking+'</a></div>'+
        '</div></div><div class="col-sm-12 form-group"><div class="row"><div class="col-sm-7"><span>Phí ship thực tế</span></div>'+
        '<div class="col-sm-5"><a href="javascript:void(0)" transport-id="'+transportId+'" transport-status-id="'+data.transport.TransportStatusId+'" class="light-dark aShipCost">'+shipCost+'</a></div>'+
        '</div></div><div class="col-sm-12 form-group"><div class="row"><div class="col-sm-7"><span>Tổng cần thu (COD)</span></div>'+
        '<div class="col-sm-5"><span>'+formatDecimal(data.transport.CODCost)+' đ</span></div></div>'+
        '</div> </div> </div></div>';
    
    $('#content_data_detail_' + transportId).html(html);
    $('span#jsonProduct_' + transportId).text(JSON.stringify(data.Products));
    $('#child_' + transportId).show();
    $(this).removeClass('treetable_open fa fa-angle-right').addClass('treetable_close fa fa-angle-down');
}

function updateTransportField(transportId, fieldName, fieldValue, fnSuccess, fnError){
    $.ajax({
        type: "POST",
        url: $('input#updateFieldUrl').val(),
        data: {
            TransportId: transportId,
            FieldName: fieldName,
            FieldValue: fieldValue,
            OrderId: $('input#orderId').val()
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                $('#ulActionLogs').prepend('<li>' + json.data.Comment + '<span class="time">' + getCurrentDateTime(2) + '</span></li>');
                fnSuccess(json.data);
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            fnError();
        }
    });
}
