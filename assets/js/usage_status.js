$(document).ready(function(){
    $("#tbodyUsageStatus").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#usageStatusId').val(id);
        $('input#usageStatusName').val($('td#usageStatusName_' + id).text());
        $('select#parentUsageStatusId').val($('input#parentUsageStatusId_' + id).val());
        $('select#usageStatusTypeId').val($('input#usageStatusTypeId_' + id).val());
        scrollTo('input#usageStatusName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteUsageStatusUrl').val(),
                data: {
                    UsageStatusId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#usageStatus_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#usageStatusForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#usageStatusForm')) {
            var form = $('#usageStatusForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        var parentUsageStatusName = '';
                        if(data.ParentUsageStatusId != '0') parentUsageStatusName = $('td#usageStatusName_' + data.ParentUsageStatusId).text();
                        if(data.IsAdd == 1){
                            var html = '<tr id="usageStatus_' + data.UsageStatusId + '">';
                            html += '<td id="usageStatusName_' + data.UsageStatusId + '">' + data.UsageStatusName + '</td>';
                            html += '<td id="parentUsageStatusName_' + data.UsageStatusId + '">' + parentUsageStatusName + '</td>';
                            html += '<td id="usageStatusTypeName_' + data.UsageStatusId + '"><span class="label label-' + (data.UsageStatusTypeId == '1' ? 'default' : 'success') + '">' + data.UsageStatusTypeName + '</span></td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.UsageStatusId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.UsageStatusId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="parentUsageStatusId_' + data.UsageStatusId + '" value="' + data.ParentUsageStatusId + '">' +
                                '<input type="text" hidden="hidden" id="usageStatusTypeId_' + data.UsageStatusId + '" value="' + data.UsageStatusTypeId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyUsageStatus').prepend(html);
                            if(data.ParentUsageStatusId == '0') $('select#parentUsageStatusId').append('<option value="' + data.UsageStatusId + '">' + data.UsageStatusName + '</option>');
                        }
                        else{
                            $('td#usageStatusName_' + data.UsageStatusId).text(data.UsageStatusName);
                            $('td#parentUsageStatusName_' + data.UsageStatusId).text(parentUsageStatusName);
                            $('td#usageStatusTypeName_' + data.UsageStatusId).html('<span class="label label-' + (data.UsageStatusTypeId == '1' ? 'default' : 'success') + '">' + data.UsageStatusTypeName + '</span>');
                            $('input#parentUsageStatusId_' + data.UsageStatusId).val(data.ParentUsageStatusId);
                            $('input#usageStatusTypeId_' + data.UsageStatusId).val(data.UsageStatusTypeId);
                            var option = $('select#parentUsageStatusId option[value="' + data.UsageStatusId + '"]');
                            if(data.ParentUsageStatusId == '0'){
                                if(option.length > 0) option.text(data.UsageStatusName);
                                else $('select#parentUsageStatusId').append('<option value="' + data.UsageStatusId + '">' + data.UsageStatusName + '</option>');
                            }
                            else if(option.length > 0) option.remove();
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});