$(document).ready(function(){
    $("#tbodyTransportCareType").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#transportCareTypeId').val(id);
        $('input#transportCareTypeName').val($('td#transportCareTypeName_' + id).text());
        $('select#transportTypeId').val($('input#transportTypeId_' + id).val());
        scrollTo('input#transportCareTypeName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteTransportCareTypeUrl').val(),
                data: {
                    TransportCareTypeId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) $('tr#transportCareType_' + id).remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#transportCareTypeForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#transportCareTypeForm')) {
            var form = $('#transportCareTypeForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="transportCareType_' + data.TransportCareTypeId + '">';
                            html += '<td id="transportCareTypeName_' + data.TransportCareTypeId + '">' + data.TransportCareTypeName + '</td>';
                            html += '<td id="transportTypeName_' + data.TransportCareTypeId + '">' + data.TransportTypeName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.TransportCareTypeId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.TransportCareTypeId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="transportTypeId_' + data.TransportCareTypeId + '" value="' + data.TransportTypeId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyTransportCareType').prepend(html);
                        }
                        else {
                        	$('td#transportCareTypeName_' + data.TransportCareTypeId).text(data.TransportCareTypeName);
                        	$('td#transportTypeName_' + data.TransportCareTypeId).text(data.TransportTypeName);
                        	$('input#transportTypeId_' + data.TransportCareTypeId).val(data.TransportTypeId);
                        }
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});