$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Log SMS',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });

    $("#tbodySpeedSms").on('click', 'a.check_status_sms', function(){
        var sMSLogId    = parseInt($(this).attr('data-id'));
        var tranId      = parseInt($(this).attr('data-tranId'));  
        var sMSCampaignName   = $(this).parent().parent().find('td').eq(0).text();
        if(sMSLogId > 0 && tranId > 0){
            $.ajax({
                type: "POST",
                url: $("input#urlStatusSMS").val().trim(),
                data: {
                    SMSLogId: sMSLogId,
                    TranId: tranId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        $("#sMSCampaignName").text(sMSCampaignName);
                        renferContentStatusSms(json.data, json.status);
                    } 
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }else showNotification('Có lỗi xảy ra, vui lòng thực hiện lại.', 0);
        return false;
    }).on('click', 'a.check_delivery_status_report_sms', function(){
        var sMSLogId    = parseInt($(this).attr('data-id'));
        var tranId      = parseInt($(this).attr('data-tranId'));  
        var sMSCampaignName   = $(this).parent().parent().find('td').eq(0).text();
        if(sMSLogId > 0 && tranId > 0){
            $.ajax({
                type: "POST",
                url: $("input#urlDeliveryStatusReport").val().trim(),
                data: {
                    SMSLogId: sMSLogId,
                    TranId: tranId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        $("#sMSCampaignName").text(sMSCampaignName);
                        renferContentDelivery(json.data);
                    } 
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }else showNotification('Có lỗi xảy ra, vui lòng thực hiện lại.', 0);
        return false;
    });
   
});

function renderContentSpeedSms(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].SMSLogId+'">';
            // html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].SMSLogId + '"></td>';
            html += '<td>' + data[item].SMSCampaignName + '</td>';
            html += '<td>' + data[item].SendContent + '</td>'; 
            html += '<td>' + data[item].SendPhones + '</td>';
            html += '<td>' + data[item].StatusName + '</td>';
            html += '<td>' + data[item].TotalSMS + '</td>';
            html += '<td class="text-right">' + formatDecimal(data[item].TotalPrice) + '</td>';
            html += '<td><a href="javascript:void(0)" title="Kiểm tra trạng thái tin nhắn" data-id="'+data[item].SMSLogId+'" data-tranId ="'+data[item].TranId+'" class="check_status_sms"><i class="fa fa-check"></i></a>';
            html += ' <a href="javascript:void(0)" data-id="'+data[item].SMSLogId+'" data-tranId ="'+data[item].TranId+'" class="check_incoming_sms" title="Nhận tin nhắn phản hồi"><span class="glyphicon glyphicon-download-alt"></span></a>';
            html += ' <a href="javascript:void(0)" data-id="'+data[item].SMSLogId+'" data-tranId ="'+data[item].TranId+'" class="check_delivery_status_report_sms"  title="Thông báo tin nhắn đã tới máy người nhận hay chưa"><span class="glyphicon glyphicon-info-sign"></span></a></td>';
            html += '</tr>';
        }
        $('#table-data').find('tbody').html(html);
    }
    // $('input.iCheckTable').iCheck({
    //     checkboxClass: 'icheckbox_square-blue',
    //     radioClass: 'iradio_square-blue',
    //     increaseArea: '20%'
    // });
}

function renferContentStatusSms(data, status){
    var html = "";
    if(data['StatusName'] == 'success'){
        $("#table-status-sms").show();
        $("#sms-error").hide();
        var responseJson = $.parseJSON(data['ResponseJson']);
        $.each(responseJson, function(k, v){
            html += '<tr><td>'+v.phone+'</td>';
            html += '<td> <span class="' + status.lableCss[v.status] + '">'+status.statusSms[v.status]+' </span></td></tr>';
        });
        $("#tbodyStatusSms").html(html);
    }else{
        $("#table-status-sms").hide();
        $("#sms-error").show();
        $("#sms-error").html('Lỗi '+data.ErrorCode+'. Do: '+data.ErrorMessage);
    }
    $("#modalSmsStatus").modal('show');
    return false;
}

function renferContentDelivery(data){
    var html = "";
    $.each(data, function(k, v){
        var status = "";
        if(parseInt(v.StatusId) >= 0 && parseInt(v.StatusId) < 64) status = '<span class="label label-success">Thành công</span>';
        else if(parseInt(v.StatusId) >= 64) status = '<span class="label label-danger">Thất bại</span>';
        html += '<tr><td>'+v.PhoneNumber+'</td>';
        html += '<td>'+status+'</td></tr>';
    });
    $("#tbodyStatusSms").html(html);
    $("#modalSmsStatus").modal('show');
    return false;
}
