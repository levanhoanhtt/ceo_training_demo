$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Bảo hành',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
    
});

function renderContentGuarantees(data){
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditGuarantee = $('#urlEditGuarantee').val() + '/';
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        for (var item = 0; item < data.length; item++) {
            html += '<tr id="trItem_'+data[item].GuaranteeId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].GuaranteeId + '"></td>';
            html += '<td><a href="' + urlEditGuarantee + data[item].GuaranteeId + '">' + data[item].GuaranteeCode + '</a></td>';
            html += '<td>'+ getDayText(data[item].DayDiff) + data[item].CrDateTime +'</td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
            html += '<td>' + data[item].StoreName + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].GuaranteeStatusId] + '">' + data[item].GuaranteeStatus + '</span></td>';
            html += '</tr>';
        }
        $('#tbodyGuarantee').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}