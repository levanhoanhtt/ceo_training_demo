$(document).ready(function(){
    $("#tbodyProductAccessoryStatus").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#productAccessoryStatusId').val(id);
        $('input#productAccessoryStatusName').val($('td#productAccessoryStatusName_' + id).text());
        scrollTo('input#productAccessoryStatusName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteProductAccessoryStatusUrl').val(),
                data: {
                    ProductAccessoryStatusId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#productAccessoryStatus_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#productAccessoryStatusForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#productAccessoryStatusForm')) {
            var form = $('#productAccessoryStatusForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="productAccessoryStatus_' + data.ProductAccessoryStatusId + '">';
                            html += '<td id="productAccessoryStatusName_' + data.ProductAccessoryStatusId + '">' + data.ProductAccessoryStatusName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ProductAccessoryStatusId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ProductAccessoryStatusId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyProductAccessoryStatus').prepend(html);
                        }
                        else{
                            $('td#productAccessoryStatusName_' + data.ProductAccessoryStatusId).text(data.ProductAccessoryStatusName);
                            $('td#itemTypeName_' + data.ProductAccessoryStatusId).html(data.ItemTypeName);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});