$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Sản phẩm',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
    /*$('#btnImportOrder').click(function(){
        $('#modalImportExcel').modal('show');
        $('input#fileExcelUrl').val('');
        $(this).parents('form').submit(function(e) {
           e.preventDefault();
        });
    });
    $('#btnUploadExcel').click(function(){
        chooseFile('Files', function(fileUrl) {
            $('input#fileExcelUrl').val(fileUrl);
        });
    });
    $('#btnImportExcel').click(function(){
        var fileExcelUrl = $('input#fileExcelUrl').val();
        if(fileExcelUrl != ''){
            $('#btnImportExcel').prop('disabled', true);
            $('.imgLoading').show();
            $.ajax({
                type: "POST",
                url: $('input#importProductUrl').val(),
                data: {
                    FileUrl: fileExcelUrl
                },
                success: function (response) {
                    $('.imgLoading').hide();
                    // $('#modalImportExcel').modal('hide');
                    $('#btnImportExcel').prop('disabled', false);
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    // if(json.code == 1) redirect(true, '');
                },
                error: function (response) {
                    $('.imgLoading').hide();
                    showNotification('An error occurred during the execution', 0);
                    $('#btnImportExcel').prop('disabled', false);
                }
            });
        }
        else showNotification('Please choose Excel file', 0);
    });*/
});


function renderContentProducts(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditProduct = $('#urlEditProduct').val() + '/';
        var urlEditProductCombo = $('#urlEditProductCombo').val() + '/';
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            trClass = '';
            if(data[item].ProductStatusId != 2) trClass = ' trItemCancel';
            html += '<tr id="trItem_'+data[item].ProductId+'" class="trItem' + trClass + '">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].ProductId + '"></td>';
            html += '<td><a href="' + ((data[item].ProductKindId == 3) ? urlEditProductCombo + data[item].ProductId : urlEditProduct + data[item].ProductId)  + '">' + data[item].ProductName + (data[item].ProductStatusId == 1 ? ' <i class="fa fa-pause-circle"></i>' : '') + '</a></td>';
            html += '<td>'+ data[item].ProductTypeName +'</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductKindId] + '">' + data[item].ProductKind + '</span></td>';
            html += '<td>'+ ((data[item].SupplierName != null && data[item].ProductKindId != 3) ? data[item].SupplierName : '') + '</td>';
            html += '<td>'+ data[item].BarCode + '</td>';
            html += '<td class="text-right">'+ formatDecimal(data[item].Price.toString()) + '</td>';
            html += '<td class="text-center"><span class="' + labelCss[data[item].ProductStatusId] + '">' + data[item].ProductStatus + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="8" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}