$(document).ready(function(){
    $("#tbodyFbPage").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#fbPageId').val(id);
        $('input#fbPageName').val($('td#fbPageName_' + id).text());
        $('input#fbPageCode').val($('td#fbPageCode_' + id).text());
        $('input#prefix').val($('td#prefix_' + id).text()).prop('readonly', true);
        scrollTo('input#fbPageName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteFbPageUrl').val(),
                data: {
                    FbPageId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#fbpage_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#fbPageForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#fbPageForm')) {
            var form = $('#fbPageForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="fbpage_' + data.FbPageId + '">';
                            html += '<td id="fbPageName_' + data.FbPageId + '">' + data.FbPageName + '</td>';
                            html += '<td id="fbPageCode_' + data.FbPageId + '">' + data.FbPageCode + '</td>';
                            html += '<td id="prefix_' + data.FbPageId + '">' + data.Prefix + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.FbPageId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.FbPageId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyFbPage').prepend(html);
                        }
                        else{
                        	$('td#fbPageName_' + data.FbPageId).text(data.FbPageName);
                        	$('td#fbPageCode_' + data.FbPageId).text(data.FbPageCode);
                        	$('td#prefix_' + data.FbPageId).text(data.Prefix);
                        } 

                        $("input#prefix").prop('readonly', false);
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});