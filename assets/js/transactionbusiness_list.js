$(document).ready(function(){
    actionItemAndSearch({
        ItemName: 'Phiếu',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderContentTransactionBusiness(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var urlEditTransactionBusiness = $('#urlEditTransactionBusiness').val() + '/';
        var sumPaidCost = 0;
        var paidCost = 0;
        for (var item = 0; item < data.length; item++) {
            if(data[item].PaidCost != null){
                paidCost = parseInt(data[item].PaidCost);
                sumPaidCost += paidCost;
                paidCost = Math.ceil(paidCost / 30);
            }
            html += '<tr id="trItem_'+data[item].TransactionBusinessId+'" class="trItem">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].TransactionBusinessId + '"></td>';
            html += '<td><a href="' + urlEditTransactionBusiness + data[item].TransactionBusinessId + '">'+ data[item].TransactionMonth +'</a></td>';
            if(data[item].PaidCost != null){
                html += '<td class="text-right">' + formatDecimal(data[item].PaidCost) + '</td>';
                html += '<td class="text-right">' + formatDecimal(paidCost.toString()) + '</td>';
            }
            else{
                html += '<td class="text-right">0</td>';
                html += '<td class="text-right">0</td>';
            }
            html += '</tr>';
        }
        if(html != '') html += '<tr><td colspan="2"></td><td class="text-right">' + formatDecimal(sumPaidCost.toString()) + '</td><td></td></tr>';
        html += '<tr><td colspan="4" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}