$(document).ready(function () {
    actionItemAndSearch({
        ItemName: 'Khuyến mại',
        IsRenderFirst: true,
        extendFunction: function(itemIds, actionCode){}
    });
});

function renderContentPromotions(data) {
    var html = '';
    if(data!=null) {
        var labelCss = [];
        if(data.length > 0) labelCss = data[0].labelCss;
        var reduceType = "";
        for (var item = 0; item < data.length; item++) {
        	if(data[item].ReduceTypeId == 3){
                reduceType = 'Miễn phí vận chuyển đối với mức phí vận chuyển nhỏ hơn hoặc bằng ' + formatDecimal(data[item].ReduceNumber) + ' VNĐ áp dụng cho ';
        		if(data[item].ProvinceId > 0) reduceType += data[item].ProvinceName;
        		else reduceType += 'tất cả tỉnh thành';
        	}
            else{
        		reduceType = 'Giảm ' + formatDecimal(data[item].ReduceNumber);
        		if(data[item].ReduceTypeId == 1) reduceType += ' VNĐ ';
        		else reduceType += ' % ';
        		if(data[item].PromotionItemTypeId == 0) reduceType += 'cho tất cả đơn hàng';
        		else if(data[item].PromotionItemTypeId == 6) reduceType += 'cho đơn hàng có giá trị từ ' + formatDecimal(data[item].MinimumCost) + ' VNĐ';
        		else if(data[item].PromotionItemId > 0){
        			if(data[item].PromotionItemTypeId == 1) reduceType += 'cho Nhóm sản phẩm ' + data[item].CategoryName + ' (' + data[item].DiscountType + ')';
        			else if(data[item].PromotionItemTypeId == 11) reduceType += 'cho Nhóm khách hàng ' + data[item].CustomerGroupName;
        			else if(data[item].PromotionItemTypeId == 3) reduceType += 'cho Sản phẩm ' + data[item].ProductName + ' (' + data[item].DiscountType + ')';
                    else if(data[item].PromotionItemTypeId == 13) reduceType += 'cho Sản phẩm ' + data[item].ProductChildName + ' (' + data[item].DiscountType + ')';
                    else if(data[item].PromotionItemTypeId == 5) reduceType += 'cho Khách hàng ' + data[item].CustomerName;
                }
        	}
        	if(data[item].PromotionTypeId == 2 && data[item].ProductNumber > 0) reduceType += ' (số lượng tối thiểu là ' + data[item].ProductNumber + ')';
            var isUnLimit = "";
            if(data[item].IsUnLimit == 2) isUnLimit = data[item].UseTime + '/∞';
            else isUnLimit = data[item].UseTime + '/'+data[item].NumberUse;
            html += '<tr id="trItem_'+data[item].PromotionId+'">';
            html += '<td><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].PromotionId + '"></td>';
            html += '<td>' + data[item].PromotionName + '</td>';
            html += '<td><span class="' + labelCss[data[item].PromotionTypeId] + '">' + data[item].PromotionType+ '</span></td>';
            html += '<td id="statusName_'+data[item].PromotionId+'"><span class="' + labelCss[data[item].PromotionStatusId] + '">' + data[item].PromotionStatus+ '</span></td>';
            html += '<td>'+ reduceType +'</td>';
            html += '<td>'+  isUnLimit + '</td>';
            html += '<td><p>Bắt đâu: <span>' + data[item].BeginDate + '</span></p><p>Kết thúc: <span>' + data[item].EndDate + '</span></td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="7" class="paginate_table"></td></tr>';
        $('#table-data').find('tbody').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
}