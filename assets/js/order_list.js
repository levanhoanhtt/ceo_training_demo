var app = app || {};
app.init = function () {
    app.checkOrder();
    app.comment();
    app.hideAndShow();
};
$(document).ready(function () {
    var config = {
        ItemName: 'Đơn hàng',
        extendFunction: function (itemIds, actionCode) {
            if (actionCode == 'print_order') {
                $('input#orderIds').val(JSON.stringify(itemIds));
                $('#printForm').submit();
            }
            else if (actionCode.indexOf('verify_order-') >= 0) {
                actionCode = actionCode.split('-');
                if (actionCode.length == 2) {
                    var verifyStatusId = parseInt(actionCode[1]);
                    $.ajax({
                        type: "POST",
                        url: $('input#changeVerifyStatusBatchUrl').val(),
                        data: {
                            OrderIds: JSON.stringify(itemIds),
                            VerifyStatusId: verifyStatusId
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            showNotification(json.message, json.code);
                            if (json.code == 1) {
                                var i;
                                if (verifyStatusId == 2) {
                                    for (i = 0; i < itemIds.length; i++) {
                                        if ($('td#orderCode_' + itemIds[i] + ' i.fa-check').length == 0) $('td#orderCode_' + itemIds[i]).append('<i class="fa fa-check tooltip1 active" title="Đã xác thực"></i>');
                                    }
                                    $('#tbodyOrder i.fa-check').tooltip();
                                }
                                else {
                                    for (i = 0; i < itemIds.length; i++) $('td#orderCode_' + itemIds[i] + ' i.fa-check').remove();
                                }
                            }
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        }
                    });
                }
            }
        }
    };
    if ($('input#isSearchCustomerPhone').val() == '0'){
        config.IsRenderFirst = true;
        app.init();
    }
    actionItemAndSearch(config);
    $('#tbodyOrder i.tooltip1').tooltip();
});

function renderContentOrders(data) {
    var html = '';
    if (data != null) {
        var labelCss = [];
        var orderLabelCss = [];
        var transportStatusCss = [];
        if (data.length > 0) {
            labelCss = data[0].labelCss;
            orderLabelCss = data[0].orderLabelCss;
            transportStatusCss = data[0].transportStatusCss;
        }
        var customerKindId = $('input#customerKindId').val();
        var urlEditOrder = $('#urlEditOrder').val() + '/';
        var urlEditCustomer = $('#urlEditCustomer').val() + '/';
        var isSearchCustomerPhone = $('input#isSearchCustomerPhone').val() == '0';
        var sumTotalCost = 0;
        var trClass = '';
        for (var item = 0; item < data.length; item++) {
            sumTotalCost += parseInt(data[item].TotalCost);
            trClass = '';
            if (data[item].TransportStatusId == '5' || data[item].OrderStatusId == '3' || data[item].OrderStatusId == '5') trClass = ' trItemCancel';
            html += '<tr id="trItem_' + data[item].OrderId + '" class="trItem' + trClass + '" data-node="treetable-650__' + item + '" data-pnode="">';
            html += '<td class="competency sm-text" data-code="A" data-competencyid="650-' + item + '"><a href="javascript:void(0);" order-id="' + data[item].OrderId + '" class="treetable_open fa fa-angle-right  parent_all" style="margin-right: 9px;"></a><input class="checkTran iCheckTable iCheckItem" type="checkbox" value="' + data[item].OrderId + '"></td>';
            if (data[item].VerifyStatusId == 2) html += '<td id="orderCode_' + data[item].OrderId + '"><a href="' + urlEditOrder + data[item].OrderId + '">' + data[item].OrderCode + '</a><i class="fa fa-check tooltip1 active" title="Đã xác thực"></i></td>';
            else html += '<td id="orderCode_' + data[item].OrderId + '"><a href="' + urlEditOrder + data[item].OrderId + '">' + data[item].OrderCode + '</a></td>';
            html += '<td>' + getDayText(data[item].DayDiff) + data[item].CrDateTime + '</td>';
            html += '<td><a href="' + urlEditCustomer + data[item].CustomerId + '">' + data[item].FullName + '</a></td>';
            if (customerKindId == 3) {
                if (data[item].CTVName != null) html += '<td><a href="' + urlEditCustomer + data[item].CTVId + '">' + data[item].CTVName + '</a></td>';
                else html += '<td></td>';
            }
            html += '<td class="text-center"><span class="' + orderLabelCss.OrderStatusCss[data[item].OrderStatusId] + '">' + data[item].OrderStatus + '</span></td>';
            if (data[item].DeliveryTypeId == 1) {
                if (data[item].OrderStatusId == 6) {
                    html += '<td class="text-center"><span class="' + transportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
                    if(isSearchCustomerPhone) html += '<td class="text-center"><span class="' + orderLabelCss.PaymentStatusCss[data[item].PaymentStatusId] + '">' + data[item].PaymentStatus + '</span></td>';
                }
                else html += '<td></td><td></td>';
            }
            else {
                if (data[item].OrderStatusId == 1) html += '<td></td><td></td>';
                else {
                    html += '<td class="text-center"><span class="' + transportStatusCss[data[item].TransportStatusId] + '">' + data[item].TransportStatus + '</span></td>';
                    if(isSearchCustomerPhone) html += '<td class="text-center"><span class="' + orderLabelCss.PaymentStatusCss[data[item].PaymentStatusId] + '">' + data[item].PaymentStatus + '</span></td>';
                }
            }
            if(isSearchCustomerPhone){
                html += '<td class="text-right">' + formatDecimal(data[item].TotalCost) + '</td>';
                if (customerKindId != 3) html += '<td class="text-right"><span class="' + labelCss[data[item].OrderChanelId] + '">' + data[item].OrderChanel + '</span></td>';
            }
            html += '</tr><tr class="chose-all" style="display:none;" id="child_' + data[item].OrderId + '"><td colspan="9" id="content_data_detail_' + data[item].OrderId + '"></td></tr>';
        }
        if(html != '' && isSearchCustomerPhone) html += '<tr><td colspan="7"></td><td class="text-right">' + formatDecimal(sumTotalCost.toString()) + '</td><td></td></tr>';
        html += '<tr><td colspan="' + (isSearchCustomerPhone ? 9 : 6) + '" class="paginate_table"></td></tr>';
        $('#tbodyOrder').html(html);
    }
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('#tbodyOrder i.tooltip1').tooltip();
}

app.checkOrder = function () {
    $("body").on('click', '.aCheckOrder', function () {
        var orderId = $(this).attr('order-id');
        $('#modalCheckQuantity .modal-body').html('');
        var products = $.parseJSON($(this).parent().find('span#jsonProduct_' + orderId).text());
        $.ajax({
            type: "POST",
            url: $('input#checkOrderUrl').val(),
            data: {
                Products: products,
                StoreId: 0//storeId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1) {
                    var data = json.data;
                    var html = '';
                    var j;
                    for (var i = 0; i < data.length; i++) {
                        html += '<div class="no-padding">';
                        if (data[i].IsInStock) html += '<label class="text-success"><i class="fa fa-check-circle">';
                        else html += '<label class="text-danger"><i class="fa fa-times-circle">';
                        html += '</i> ' + data[i].StoreName + '</label>';
                        html += '<div class="table-responsive divTable"><table class="table table-hover"><thead class="theadNormal">';
                        html += '<tr><th>Sản phẩm</th><th class="text-center">Số lượng cần</th><th class="text-center">Số lượng tồn</th><th class="text-center"></th></tr></thead><tbody>';
                        for (j = 0; j < data[i].Products.length; j++) {
                            html += '<tr>';
                            html += '<td>' + data[i].Products[j].ProductName + '</td>';
                            html += '<td class="text-center">' + data[i].Products[j].Quantity + '</td>';
                            html += '<td class="text-center">' + data[i].Products[j].StockQuantity + '</td>';
                            if (data[i].Products[j].StockQuantity >= data[i].Products[j].Quantity) html += '<td class="text-center text-success"><i class="fa fa-check-circle"></i></td>';
                            else html += '<td class="text-center text-danger"><i class="fa fa-times-circle"></i></td>';
                            html += '</tr>';
                        }
                        html += '</tbody></table></div></div>';
                    }
                    $('#modalCheckQuantity .modal-body').html(html);
                    $('#modalCheckQuantity').modal('show');
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
};

app.comment = function () {
    $('#tbodyOrder').on('click', '.aShowComment', function () {
        var userImagePath = $('input#userImagePath').val();
        var json = $(this).parent().find('span.jsonComment').text();
        var html = "";
        $.each($.parseJSON(json), function (k, v) {
            html += `
                <div class="box-customer mb10">
                    <table>
                        <tbody>
                        <tr>
                            <th rowspan="2" valign="top" style="width: 50px;"><img src="${userImagePath + v.Avatar}" alt=""></th>
                            <th><a href="javascript:void(0)" class="name">${v.FullName}</a></th>
                            <th class="time">${v.CrDateTime}</th>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p class="pComment">${v.Comment}</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            `;
        });
        $(".listCommentAll").html(html);
        $("#modalItemComment").modal('show');
    }).on('click', '.btnInsertComment', function () {
        var orderId = $(this).attr('order-id');
        var comment = $('input#comment_' + orderId).val().trim();
        if (comment != '') {
            if (orderId > 0) {
                var btn = $(this);
                btn.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertOrderCommentUrl').val(),
                    data: {
                        OrderId: orderId,
                        Comment: comment,
                        CommentTypeId: 1
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            $('div#listComment_' + orderId).prepend(genItemComment(comment));
                            $('input#comment_' + orderId).val('');
                        }
                        else showNotification(json.message, json.code);
                        btn.prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop('disabled', false);
                    }
                });
            }
            else {
                $('div#listComment_' + orderId).prepend(genItemComment(comment));
                $('input#comment_' + orderId).val('');
            }
        }
        else {
            showNotification('Vui lòng nhập ghi chú', 0);
            $('input#comment_' + orderId).focus();
        }
    });

};

app.hideAndShow = function () {
    $('#tbodyOrder').on('click', 'a.treetable_open', function () {
        var id = $(this).attr('order-id');
        $('.chose-all').hide();
        $(this).removeClass('treetable_open fa fa-angle-right').addClass('treetable_close fa fa-angle-down');
        $('.fa-angle-down').css("margin-right", 5);
        getListOrderProducts(id);
        return false;
    }).on('click', 'a.treetable_close', function () {
        var id = $(this).attr('order-id');
        $('#child_' + id).hide();
        $(this).removeClass('treetable_close fa fa-angle-down').addClass('treetable_open fa fa-angle-right');
        $('.fa-angle-right').css("margin-right", 9);
        return false;
    })
};

function getListOrderProducts(orderId) {
    if($('#spanOrder_' + orderId).length > 0){
        var data =  $.parseJSON($('#spanOrder_' + orderId).text());
        genPanelOrder(orderId, data);
    }
    else {
        $.ajax({
            type: "POST",
            url: $('input#getDetailUrl').val(),
            data: {
                OrderId: orderId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                // console.log(json)
                if (json.code = 1) {
                    var data = json.data;
                    $('#divOrderJson').append('<span id="spanOrder_' + orderId + '">' + JSON.stringify(data) + '</span>');
                    genPanelOrder(orderId, data);
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
}

function genPanelOrder(orderId, data){
    var productImagePath = $('input#productImagePath').val();
    var product = "";
    $.each(data.Products, function (key, value) {
        product += `
            <div class="medium-box-n boxst-1 primary-gradient" style="margin-bottom: 5px; font-size:13px">
                <img class="pull-left" width="40px" src="${productImagePath + value.ProductImage}" style="margin-right: 5px;">
                <div class="right-s">
                    <p style="margin: 0 0 0px"><a href="javascript:void(0)">${value.ProductName}</a></p>
                    <p style="margin: 0 0 0px">${value.Quantity + ' x ' + formatDecimal(value.Price.toString())} đ</p>
                </div>
                <div class="clearfix"></div>
            </div>
            `;
    });

    var customerName = "", phoneNumber = "", email = "", address = "", countryName = "Việt Nam", comment = "", detailComment = "";
    var customerAddress = data.customerAddress;
    if (customerAddress.length > 0) {
        customerName = customerAddress.CustomerName;
        phoneNumber = customerAddress.PhoneNumber;
        email = customerAddress.Email;
    }
    if (customerAddress.ZipCode == "" || customerAddress.ZipCode == '0') {
        address = `
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <span class="i-ward">${customerAddress.Address + ' ' + customerAddress.WardName}</span>
            <span class="br-line i-district">${customerAddress.DistrictName}</span>
            <span class="br-line i-province">${customerAddress.ProvinceName}</span>
            `;
    }
    else {
        address = `
            <i class="fa fa-list-alt" aria-hidden="true"></i>
            <span class="i-province">ZipCode: ${customerAddress.ZipCode}</span>
            `;
    }
    if (customerAddress.CountryName != "") countryName = customerAddress.CountryName;

    var printPdfUrl = $('input#printPdfUrl').val() + '/';

    var i = 0;
    var userImagePath = $('input#userImagePath').val();
    $.each(data.listOrderComments, function (k, v) {
        i++;
        if (i < 3) {
            comment += `
                <div class="box-customer mb10">
                    <table>
                        <tbody>
                        <tr>
                            <th rowspan="2" valign="top" style="width: 50px;"><img src="${userImagePath + v.Avatar}" alt=""></th>
                            <th><a href="javascript:void(0)" class="name">${v.FullName}</a></th>
                            <th class="time">${v.CrDateTime}</th>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p class="pComment">${v.Comment}</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            `;
        }
    });
    if (data.listOrderComments.length > 2) detailComment = `<div class="text-right light-dark"><span hidden="hidden" class="jsonComment">${JSON.stringify(data.listOrderComments)}</span><a href="javascript:void(0)" class="aShowComment" data-id="1">Xem tất cả &gt;&gt;</a></div>`;

    var htmlTransport = "";
    if(data.transport != null){
        var transport = data.transport;
        var transportStatusIds = data.transportStatusIds;
        var urlTransportEdit = $("input#urlTransportEdit").val();
        var htmlTransportStatus = "";
        $.each(transportStatusIds, function( index, value ) {
            var active = "", color ="", number = '1';

            if(transport.TransportStatusId == value){
                active = 'active';
                color = 'style="color:#fff!important;"';
                number = '2';
            }
            htmlTransportStatus += `<li  class="block-display liTransportStatus ${active}" id="liTransportStatus_${i}">
                        <a ${color} href="javascript:void(0);">${data.transportStatus[value]} <div class="icon"><img src="assets/vendor/dist/img//transport/${value+'.'+number}.png" alt="${data.transportStatus[value]}"></div></a>
                    </li>`;
        });
            
        var border = ""; 
        var displayNone = "";
        if(!jQuery.inArray(transport.TransportStatusId, [1, 2, 9] == -1) || transport.TransportId > 0) displayNone = 'style="display: none;"';
        if(transport.TransportStatusId == '5') border = 'style="border: 2px solid red;"';
        var CODStatus = "";
        if(transport.CODStatusId > 0) CODStatus= `<span class="btn-status ${data.labelCss['CODStatusCss'][transport.CODStatusId]} "> ${data.CODStatus[transport.CODStatusId]}</span>`;
        htmlTransport = `
        <div class="listTransport">
            <div class="">
                <table class="tb-cancel-cart">
                    <thead>
                    <tr>
                        <td><span class="light-blue">Thông tin giao hàng</span></td>
                        <td class="text-center"></td>
                        <td class="text-right"><a href="javascript:void(0)" class="btn-cancle-cart" ${displayNone}>Báo hủy giao hàng</a></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ${border}>
                        <td style="background: #dfeaf4!important" class=""><a href="${urlTransportEdit+'/'+transport.TransportId}" target="_blank" class="light-dark bold"><img src="assets/vendor/dist/img/icon09.png">${transport.TransportCode}</a></td>
                        <td style="background: #dfeaf4!important" class="text-center light-blue">Loại vận chuyển: ${data.transportTypeName}</td>
                        <td style="background: #dfeaf4!important" class="text-right light-blue">Mã vận đơn: <a href="javascript:void(0)" class="bold light-dark">${transport.Tracking}</a></td>
                    </tr>
                    </tbody>
                </table>
                <div class="row no-margin sent-cart">
                    <div class="col-sm-4">
                        <table class="tb-senw">
                            <tr>
                                <th>Trạng thái giao hàng</th>
                                <td><span class="btn-status ${data.labelCss['TransportStatusCss'][transport.TransportStatusId]}">${data.transportStatus[transport.TransportStatusId]}</span></td>
                            </tr>
                            <tr>
                                <th>Trạng thái thu hộ (COD)</th>
                                <td>${CODStatus}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-8" style="top:33px">
                        <div class="box-step has-slider">
                            <ul class="clearfix bxslider short">
                                ${htmlTransportStatus}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;
    }
    var html = `
        <div class=" col-sm-12">
            <div class="row">
                <div class="col-sm-3" style="padding-left: 33px;">${product}</div>
                <div class="col-sm-3">
                    <label class="light-blue">Ghi chú</label>
                    <div class="box-transprt clearfix mb10">
                        <button type="button" class="btn-updaten save btnInsertComment" order-id="${orderId}">
                            Lưu
                        </button>
                        <input type="text" class="add-text" id="comment_${orderId}" value="">
                    </div>
                    <div class="listComment" id="listComment_${orderId}">${comment}</div>
                    ${detailComment}
                </div>
                <div class="col-sm-4">
                    <div class="">
                        <div>
                            <h4 class="mgbt-20 light-blue">Thông tin giao hàng</h4>
                        </div>
                        <div class="row" style="margin-top: -10px;">
                            <div class="col-sm-12 mh-wrap-customer">
                                <div class="item">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <span class="i-name">${customerAddress.CustomerName}</span>
                                </div>
                                <div class="item">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <span class="i-phone">${customerAddress.PhoneNumber}</span>
                                </div>
                                <div class="item">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <span class="i-email">${customerAddress.Email}</span>
                                </div>
                                <div class="item i-address">
                                    ${address}
                                </div>
                                <div class="item">
                                    <i class="fa fa-id-card" aria-hidden="true"></i>
                                    <span class="i-country" data-id="232" data-province="0" data-district="0" data-ward="0" data-zip="">${countryName}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2" style="text-align:  right;">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <a href="javascript:void(0)" class="btn btn-primary aCheckOrder"  order-id="${orderId}" style="color:#fff"><i class="fa fa-check-square"></i> Check đơn</a>
                            <span hidden="hidden" id="jsonProduct_${orderId}"></span>
                        </div>
                        <div class="col-sm-12 form-group">
                            <a href="${printPdfUrl + orderId}" target="_blank" class="btn btn-default" style="font-size: 13px;"><i class="fa fa-print"></i> In</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                ${htmlTransport}
            </div>
        </div>`;
    $('#content_data_detail_' + orderId).html(html);
    $('span#jsonProduct_' + orderId).text(JSON.stringify(data.Products));
    $('#child_' + orderId).show();
    $(this).removeClass('treetable_open fa fa-angle-right').addClass('treetable_close fa fa-angle-down');
}