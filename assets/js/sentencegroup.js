$(document).ready(function(){
    $("#tbodySentencegroups").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#sentenceGroupId').val(id);
        $('input#sentenceGroupName').val($('td#sentenceGroupName_' + id).text());
        scrollTo('input#sentenceGroupName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteSentencegroupUrl').val(),
                data: {
                    SentenceGroupId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#sentenceGroupName_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });

    $('a#link_cancel').click(function(){
        $('#sentencegroupsForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#sentencegroupsForm')) {
            var form = $('#sentencegroupsForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="sentencegroup_' + data.SentenceGroupId + '">';
                            html += '<td id="sentenceGroupName_' + data.SentenceGroupId + '">' + data.SentenceGroupName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.SentenceGroupId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.SentenceGroupId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodySentencegroups').prepend(html);
                        }
                        else $('td#sentenceGroupName_' + data.SentenceGroupId).text(data.SentenceGroupName);
                        showNotification(json.message, json.code);
                    }else if(json.code == -2)showNotification(json.message, json.code);
                    else showNotification(json.message, json.code);
                    return false;
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});